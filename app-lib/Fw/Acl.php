<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Acl
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

/**
 * @see Zend_Acl
 */
require_once 'Zend/Acl.php';

/**
 * @abstract
 * @package Fw_Acl
 */
abstract class Fw_Acl extends Zend_Acl
{
	
}