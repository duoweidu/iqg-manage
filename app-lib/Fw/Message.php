<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Message
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

/**
 * @see Fw_Message_Exception
 */
require_once 'Fw/Message/Exception.php';

/**
 * @see Fw_Message_Exception
 */
require_once 'Fw/Message/Handler.php';

/**
 * @see Fw_Message_Exception
 */
require_once 'Fw/Message/Queue.php';

/**
 * @abstract
 * @package Fw_Message
 * @example Message.php Example for using Fw_Message class
 */
class Fw_Message
{
	/**
	 * @var int
	 */
	public $type = 0;
	
	/**
	 * @var mixed
	 */
	public $data = null;
	
	/**
	 * Set message type
	 * 
	 * @param int $type
	 * @return void
	 */
	public function setType ($type)
	{
		$this->type = intval($type);
	}
	
	/**
	 * Get message type
	 * 
	 * @return int
	 */
	public function getType ()
	{
		return $this->type;
	}
	
	/**
	 * Set message data
	 * 
	 * @param mixed $data
	 * @return void
	 */
	public function setData ($data)
	{
		$this->data = $data;
	}
	
	/**
	 * Get message data
	 * 
	 * @return mixed
	 */
	public function getData ()
	{
		return $this->data;
	}
	
}