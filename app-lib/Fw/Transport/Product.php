<?php

require_once 'Fw/Transport/Exception.php';

/**
 * @package Fw_Transport
 */
class Fw_Transport_Product
{
	protected $_type = null;
	
	protected $_mount = null;
	
	protected $_price = null;
	
	protected $_weight = null;
	
	protected $_volume = null;
	
	protected $_noperm_adaptors = array();
	
	protected $_except_locations = array();
	
	public function addNopermAdaptor ($adaptor_name)
	{
		$this->_noperm_adaptors[] = $adaptor_name;
		return $this;
	}
	
	public function addExceptLocation ($except_location)
	{
		$this->_except_locations[] = $except_location;
		return $this;
	}
	
	public function getType ()
	{
		return $this->_type;
	}
	
	public function setType ($type)
	{
		if (!is_numeric($type)) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Product\'s type must be an integer');
		}
		$this->_type = $type;
	}
	
	public function getMount ()
	{
		return $this->_mount;
	}
	
	public function setMount ($mount)
	{
		$this->_mount = $mount;
	}
	
	public function getPrice ()
	{
		return $this->_price;
	}
	
	public function setPrice ($price)
	{
		$this->_price = $price;
	}
	
	public function getWeight ()
	{
		return $this->_weight;
	}
	
	public function setWeight ($weight)
	{
		$this->_weight = $weight;
	}
	
	public function getVolume ()
	{
		return $this->_volume;
	}
	
	public function setVolume ($volume)
	{
		$this->_volume = $volume;
	}
	
	/**
	 * Judge if product is transportable by different adaptors
	 */
	public function transportable ($adaptor_name, Fw_Transport_Location $location)
	{
		if (!$this->_type) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Product\'s type can not be empty');
		}
		
		// if not in bad_type_ids array, then passed
		if (!in_array($adaptor_name, $this->_noperm_adaptors)) {
			return true;
		}
		
		// if excepted location is matched, then passed
		foreach ($this->_except_locations as $except_location) {
			if (is_object($except_location) && $location->isEqualProvice($except_location)) {
				return true;
			}
		}
		
		return false;
	}
}