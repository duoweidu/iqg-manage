<?php

/**
 * @package Fw_Transport
 */
class Fw_Transport_Location
{
	protected $_encoding = null;
	
	protected $_province = null;
	
	protected $_city = null;
	
	protected $_area = null;
	
	protected $_postcode = null;
	
	protected $_areacode = null;
	
	protected $_index = null;
	
	public static $index_file = null;
	
	public function getEncoding ()
	{
		return $this->_encoding;
	}
	
	public function setEncoding ($encoding)
	{
		$this->_encoding = strtoupper($encoding);
	}
	
	public function getProvince ()
	{
		return $this->_province;
	}
	
	public function setProvince ($province)
	{
		$this->_province = $province;
	}
	
	public function getCity ()
	{
		return $this->_city;
	}
	
	public function setCity ($city)
	{
		$this->_city = $city;
	}
	
	public function getArea ()
	{
		return $this->_area;
	}
	
	public function setArea ($area)
	{
		$this->_area = $area;
	}
	
	public function getPostcode ()
	{
		return $this->_postcode;
	}
	
	public function setPostcode ($postcode)
	{
		$this->_postcode = $postcode;
	}
	
	public function getAreacode ()
	{
		return $this->_areacode;
	}
	
	public function setAreacode ($areacode)
	{
		$this->_areacode = $areacode;
	}
	
	public function getIndexFile ()
	{
		return self::$index_file;
	}
	
	public function setIndexFile ($index_file)
	{
		self::$index_file = $index_file;
	}
	
	/**
	 * Judge if location is near enough, for fare is different
	 */
	public function isNearLocation ()
	{
		if (!$this->_province) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s province can not be empty');
		}
		
		$pattern = $this->doEncode('/江苏|浙江|上海|安徽/');
		if (preg_match($pattern, $this->_province)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Judge if location's province equal with another
	 */
	public function isEqualProvice (Fw_Transport_Location $location)
	{
		if ($this->getProvince() == $location->getProvince()) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Judge if location's city equal with another
	 */
	public function isEqualCity (Fw_Transport_Location $location)
	{
		if ($this->getCity() == $location->getCity()) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Judge if location's area equal with another
	 */
	public function isEqualArea (Fw_Transport_Location $location)
	{
		if ($this->getArea() == $location->getArea()) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Remove noise factor for blur searching
	 */
	protected function clearNoise ($location)
	{
		$noise_table = array(
			array('省', '直辖市', '市', '自治区', '辖区', '特区', '区'),
			array('藏族', '壮族', '维吾尔族', '维吾尔')
		);
		
		/*if ($this->_encoding) {
			$pattern = iconv('UTF-8', $this->_encoding, $location);
		}*/
		
		foreach ($noise_table as $noise_row) {
			$noise_row = $this->doEncode($noise_row);
			$location = str_replace($noise_row, '*', $location);
		}
		
		return preg_replace('/\*+/', '*', $location);
	}
	
	/**
	 * Internal encoding convert
	 */
	protected function doEncode ($vars)
	{
		if (!$this->_encoding) {
			return $vars;
		}
		
		if (is_string($vars)) {
			return iconv('UTF-8', $this->_encoding, $vars);
		}
		
		if (is_array($vars)) {
			foreach ($vars as $k => $v) {
				$vars[$k] = iconv('UTF-8', $this->_encoding, $v);
			}
		}
		
		return $vars;
	}
	
	/**
	 * Get location hash string for save & search
	 */
	public function getHash ($encode = false)
	{
//		if (!$this->_province || !$this->_city || !$this->_area) {
//			require_once 'Fw/Transport/Exception.php';
//			throw new Fw_Transport_Exception('Location\'s province, city, area can not be empty');
//		}
		
		$indexHash[] = $this->clearNoise($this->_province);
		
		// Get parent city if not county
		$county = '县';
		$county = $this->doEncode($county);
		if (strpos($this->_area, $county)) {
			$indexHash[] = $this->clearNoise($this->_area);
		} else {
			$indexHash[] = $this->clearNoise($this->_city);
		}
		
		$indexName = implode(',', $indexHash);
		
		return $encode ? md5($indexName) : $indexName;
	}
	
	/**
	 * Search location from index file
	 */
	public static function search (Fw_Transport_Location $location)
	{
		if (!$location->getProvince() || !$location->getCity() || !$location->getArea()) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s province, city, area can not be empty');
		}
		
		$index_file = $location->getIndexFile();
		if (!$index_file || !file_exists($index_file)) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s index file does not exists');
		}
		
		$index_encode = file_get_contents($index_file);
		$index_decode = self::decode($index_encode);
		
		return array_key_exists($location->getHash(), $index_decode);
	}
	
	/**
	 * Encode data for index file
	 */
	public static function encode ($data)
	{
		return gzcompress(serialize($data));
	}
	
	/**
	 * Encode data for index file
	 */
	public static function decode ($data)
	{
		return unserialize(gzuncompress($data));
	}

	public static function checkTopname(Fw_Transport_Location $location)
	{
		if (!$location->getProvince() || !$location->getCity() || !$location->getArea()) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s province, city, area can not be empty');
		}

		$province = iconv("GB2312","UTF-8",$location->getProvince());
		$city = iconv("GB2312","UTF-8",$location->getCity());
		
		if ( $province == '上海市' && $city == '上海辖区' )
		{
			return true;
		}
		return false;
	}
	
	public static function checkNengda(Fw_Transport_Location $location)
	{
		if (!$location->getProvince() || !$location->getCity() || !$location->getArea()) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s province, city, area can not be empty');
		}

		$province = iconv("GB2312","UTF-8",$location->getProvince());
		//$city = iconv("GB2312","UTF-8",$location->getCity());
		
		if ( $province == '广东省' || $province == '湖南省' )
		{
			return true;
		}
		return false;
	}
	
	public static function checkRufengda(Fw_Transport_Location $location)
	{
		if (!$location->getProvince() || !$location->getCity() || !$location->getArea()) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s province, city, area can not be empty');
		}

		$province = iconv("GB2312","UTF-8",$location->getProvince());
		//$city = iconv("GB2312","UTF-8",$location->getCity());
		
		if ( $province == '江苏省' || $province == '浙江省' )
		{
			return true;
		}
		return false;
	}

	public static function checkQuanfeng(Fw_Transport_Location $location)
	{
		if (!$location->getProvince() || !$location->getCity() || !$location->getArea()) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Location\'s province, city, area can not be empty');
		}

		$province = iconv("GB2312","UTF-8",$location->getProvince());
		//$city = iconv("GB2312","UTF-8",$location->getCity());
		if ( $province == '北京市' || $province == '重庆市' )
		{
			return true;
		}
		return false;
	}
}