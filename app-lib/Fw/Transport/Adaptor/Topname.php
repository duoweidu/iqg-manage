<?php

/**
 * @see Fw_Transport_Adaptor
 */
require_once 'Fw/Transport/Adaptor.php';

/**
 * @package Fw_Transport
 */
class Fw_Transport_Adaptor_Topname extends Fw_Transport_Adaptor
{
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function getAdaptorName ()
	{
		return $this->_adaptor_name;
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function locationReached ()
	{
		return Fw_Transport_Location::checkTopname($this->getLocation());
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function productDelivered (Fw_Transport_Location $location)
	{
		if (count($this->_products)) {
			foreach ($this->_products as $product) {
				// $product must be Fw_Transport_Product instance
				if (!$product->transportable($this->_adaptor_name, $location)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function getTotalFare ()
	{
		// nofare when price is up to a threshold
		if ($this->getTotalPrice() >= $this->fare_saving) {
			return 0;
		}
		// If in near location
		if ($this->getLocation()->isNearLocation()) {
			return $this->fare_near;
		}
		return $this->fare_comm;
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	protected function getClassName ()
	{
		return get_class();
	}
}