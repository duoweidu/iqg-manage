<?php

/**
 * @see Fw_Transport_Adaptor
 */
require_once 'Fw/Transport/Adaptor.php';

/**
 * @package Fw_Transport
 */
class Fw_Transport_Adaptor_Proxy extends Fw_Transport_Adaptor
{
	/**
	 * Proxy class name
	 */
	private $_proxy_name = null;
	
	/**
	 * Proxy class variables
	 */
	private $_variables = array();
	
	public function setProxyName ($proxy_name)
	{
		$this->_proxy_name = $proxy_name;
		return $this;
	}
	
	public function getProxyName ()
	{
		return $this->_proxy_name;
	}
	
	public function setVariables ($variables)
	{
		$this->_variables = $variables;
		$varNameArr = array_keys(get_class_vars(get_class($this)));
		foreach ($variables as $varName => $varValue) {
			if (in_array($varName, $varNameArr)) {
				$this->$varName = $varValue;
			}
		}
		return $this;
	}
	
	public function getVariables ()
	{
		return $this->_variables;
	}
	
	/**
	 * Counting fare's formula
	 */
	public function formulaFare ()
	{
		if ($this->total_costs >= $this->fare_saving) {
			return 0;
		}
		return $this->fare_init;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function getAdaptorName ()
	{
		return $this->_adaptor_name;
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function locationReached ()
	{
		return Fw_Transport_Location::search($this->getLocation());
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function productDelivered (Fw_Transport_Location $location)
	{
		if (count($this->_products)) {
			foreach ($this->_products as $product) {
				// $product must be Fw_Transport_Product instance
				if (!$product->transportable($this->_adaptor_name, $location)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	public function getTotalFare ()
	{
		// nofare when price is up to a threshold
		if ($this->getTotalPrice() >= $this->fare_saving) {
			return 0;
		}
		// If in near location
		if ($this->getLocation()->isNearLocation()) {
			return $this->fare_near;
		}
		return $this->fare_comm;
	}
	
	/**
	 * @see Fw_Transport_Adaptor
	 */
	protected function getClassName ()
	{
		return get_class();
	}
}