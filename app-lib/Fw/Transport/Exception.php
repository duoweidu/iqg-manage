<?php

/**
 * @see Fw_Exception
 */
require_once 'Fw/Exception.php';
 
/**
 * @package Fw_Transport
 */
class Fw_Transport_Exception extends Fw_Exception
{
	
}