<?php

/**
 * @package Fw_Transport
 */
abstract class Fw_Transport_Adaptor
{
	/**
	 * unique adaptor name
	 */
	protected $_adaptor_name = null;
	
	/**
	 * Fw_Transport_Location
	 */
	protected $_location = null;
	
	/**
	 * Fw_Transport_Product
	 */
	protected $_products = array();
	
	/**
	 * Order total costs
	 */
	public $total_costs = 0;
	
	/**
	 * Order total costs
	 */
	public $fare_init = 0;
	
	/**
	 * Average fare price
	 */
	public $fare_comm = 20;
	
	/**
	 * Near location's fare
	 */
	public $fare_near = 15;
	
	/**
	 * Fw_Transport_Product
	 */
	public $fare_saving = 288;
	
	/**
	 * If support cash on delivery 
	 */
	public $cash_on_delivery = false;
	
	/**
	 * 
	 */
	public function getLocation ()
	{
		return $this->_location;
	}
	
	/**
	 * 
	 */
	public function setLocation (Fw_Transport_Location $location)
	{
		$this->_location = $location;
	}
	
	/**
	 * 
	 */
	public function addProduct (Fw_Transport_Product $product)
	{
		$this->_products[] = $product;
		return $this;
	}
	
	/**
	 * 
	 */
	public function __construct ()
	{
		$_adaptor_name = $this->getClassName();
		$_adaptor_name = str_replace(array(get_class(), '_'), '', $_adaptor_name);
		$this->_adaptor_name = strtolower($_adaptor_name);
	}
	
	/**
	 * 
	 */
	public function getTotalPrice ()
	{
		$total_price = 0;
		if (count($this->_products)) {
			foreach ($this->_products as $product) {
				// $product must be Fw_Transport_Product instance
				$mount = $product->getMount();
				$price = $product->getPrice();
				$total_price += $mount * $price;
			}
		}
//		return number_format($total_price, 1);
		return sprintf("%.1f", $total_price);
	}
	
	/**
	 * 
	 */
	public function getTotalWeight ()
	{
		$total_weight = 0;
		if (count($this->_products)) {
			foreach ($this->_products as $product) {
				// $product must be Fw_Transport_Product instance
				$mount = $product->getMount();
				$weight = $product->getWeight();
				$total_weight += $mount * $weight;
			}
		}
//		return number_format($total_weight, 1);
		return sprintf("%.1f", $total_weight);
	}
	
	/**
	 * If support cash on delivery
	 */
	public function cashOnDelivery ()
	{
		return $this->cash_on_delivery;
	}
	
	/**
	 * Is location can be reached
	 */
	abstract public function getAdaptorName ();
	
	/**
	 * Is location can be reached
	 */
	abstract public function locationReached ();
	
	/**
	 * Is product can be delivered
	 */
	abstract public function productDelivered (Fw_Transport_Location $location);
	
	/**
	 * Total transport fare
	 */
	abstract public function getTotalFare ();
	
	/**
	 * Get adaptor name
	 */
	abstract protected function getClassName ();
}