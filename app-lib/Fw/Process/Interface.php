<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Process
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @package Fw_Process
 */
interface Fw_Process_Interface
{
	public function run ();
}