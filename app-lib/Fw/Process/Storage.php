<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Process
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @package Fw_Process
 */
abstract class Fw_Process_Storage
{
	/**
	 * Create storage object
	 * @param string $engine Engine name (sysv, file)
	 * @return object
	 */
	public static function factory ($engine, $config = array())
	{
		$class_file = 'Fw/Process/Storage/' . ucfirst($engine) . '.php';
		$class_name = 'Fw_Process_Storage_' . ucfirst($engine);
		
		require_once $class_file; // include storage class
		
		return new $class_name($config);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// abstract methods 
	
	abstract public function get ($k);
	
	abstract public function set ($k, $v);
	
	abstract public function delete ($k);
	
	abstract public function remove ();
}