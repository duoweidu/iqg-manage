<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_View
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @see Zend_View
 */
require_once 'Zend/View.php';

/**
 * @see Zend_View_Interface
 */
require_once 'Zend/View/Interface.php';

/**
 * @package Fw_View
 */
class Fw_View extends Zend_View
{
	/**
	 * @staticvar Fw_View
	 */
	public static $engine = null;
	
	/**
	 * Method for singleton mode
	 * Return Fw_View instance such as Fw_View_Smarty
	 * @static
	 * @param string $engine
	 * @param array $configs Configurations used by Fw_View subclass
	 * @return Fw_View
	 */
	public static function getInstance($engine, $configs)
	{
		if (!$engine) {
			require_once 'Fw/Exception.php';
			throw new Fw_Exception('View & Template Engine can not be empty');
		}
		if (!self::$engine) {
			$file_name = 'Fw/View/' . ucfirst($engine) . '.php';
			$class_name = 'Fw_View_' . ucfirst($engine);
			
			require_once $file_name;
			self::$engine = new $class_name($configs);
		}
		return self::$engine;
	}
}