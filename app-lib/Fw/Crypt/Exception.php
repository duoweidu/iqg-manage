<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Crypt
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @see Fw_Exception
 */
require_once 'Fw/Exception.php';
 
/**
 * @package Fw_Crypt
 */
class Fw_Crypt_Exception extends Fw_Exception
{
	
}