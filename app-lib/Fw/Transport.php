<?php
/**
 * @package Fw_Transport
 */
abstract class Fw_Transport
{
	/**
	 * Fw_Transport_Adaptor
	 */
	protected static $_adaptor = null;
	
	/**
	 * Adaptor factory method
	 */
	public static function factory ($adaptor)
	{
		$classPath = 'Fw/Transport/Adaptor/' . ucfirst($adaptor) . '.php';
		$className = 'Fw_Transport_Adaptor_' . ucfirst($adaptor);
		require_once $classPath;
		return new $className;
	}
	
	/**
	 * Adaptor proxy method
	 * 
	 * Use proxy to count fare with formula
	 * 
	 * Here is an example:
	 * <code>
	 * $proxy = Fw_Transport::getProxy('test', array(
	 * 	'fare_init'    => 10,    // 初始运费
	 * 	'fare_saving'  => 288,   // 免运费�??
	 * 	'total_costs'  => 1000,  // 订单总额
	 * ));
	 * 
	 * echo $proxy->formulaFare();
	 * </code>
	 */
	public static function getProxy ($proxyClassName, $varsArr = array())
	{
		require_once 'Fw/Transport/Adaptor/Proxy.php';
		$proxyClass = new Fw_Transport_Adaptor_Proxy();
		$proxyClass->setProxyName($proxyClassName);
		$proxyClass->setVariables($varsArr);
		return $proxyClass;
	}
	
	/**
	 * Singleton adaptor method
	 */
	public static function getInstance ($adaptor)
	{
		if (!self::$_adaptor) {
			self::$_adaptor = self::factory($adaptor);
		}
		return self::$_adaptor;
	}
	
	/**
	 * Main API method
	 * 
	 * Auto-selecting transport adaptor
	 * 
	 * Here is an example:
	 * <code>
	 * $index_file = '/path/to/adaptor-location.idx';
	 * $transport_location = new Fw_Transport_Location();
	 * //$transport_location->setIndexFile($index_file);
	 * $transport_location->setProvince('xxx');
	 * $transport_location->setCity('xxx');
	 * $transport_location->setArea('xxx');
	 * 
	 * $transport_product1 = new Fw_Transport_Product();
	 * $transport_product1->setType(1);
	 * $transport_product1->setMount(2);
	 * $transport_product1->setPrice(50);
	 * $transport_product1->setWeight(1.0);
	 * 
	 * $transport_product2 = new Fw_Transport_Product();
	 * $transport_product2->setType(2);
	 * $transport_product2->setMount(1);
	 * $transport_product2->setPrice(80);
	 * $transport_product2->setWeight(1.5);
	 * 
	 * // set all configs before all
	 * Fw_Transport::setLocationIndexFile($index_file);
	 * Fw_Transport::setProductAccessTable(array(
	 * 		'sfexpress' => array(46, 47),
	 * 		'ems' => array()
	 * ));
	 * 
	 * $transport = Fw_Transport::autoSelector($transport_location, array($transport_product1, $transport_product2));
	 * echo 'Adaptor : ' . $transport->getAdaptorName() . '<br/>';
	 * echo 'Total fare : ' . $transport->getTotalFare() . '<br/>';
	 * echo 'Total price : ' . $transport->getTotalPrice() . '<br/>';
	 * echo 'Total weight : ' . $transport->getTotalWeight() . '<br/>';
	 * echo 'Cash on delivery : ' . $transport->cashOnDelivery() . '<br/>';
	 * </code>
	 */
	public static function autoSelector ($location, $products = array())
	{
		if (!is_object($location) || !is_array($products)) {
			require_once 'Fw/Transport/Exception.php';
			throw new Fw_Transport_Exception('Please pass correct location and products array');
		}
		
		// try topname adaptor firstly
		$transport = self::factory('topname');
		$transport->setLocation($location);
		foreach ($products as $product) {
			$transport->addProduct($product);
		}
		if ($transport->locationReached() && $transport->productDelivered($location)) {
			return $transport;
		}

		// try nengda adaptor firstly
		$transport = self::factory('nengda');
		$transport->setLocation($location);
		foreach ($products as $product) {
			$transport->addProduct($product);
		}
		if ($transport->locationReached() && $transport->productDelivered($location)) {
			return $transport;
		}
		
		// try rufengda adaptor firstly
		$transport = self::factory('rufengda');
		$transport->setLocation($location);
		foreach ($products as $product) {
			$transport->addProduct($product);
		}
		if ($transport->locationReached() && $transport->productDelivered($location)) {
			return $transport;
		}

		// try quanfeng adaptor firstly
		$transport = self::factory('quanfeng');
		$transport->setLocation($location);
		foreach ($products as $product) {
			$transport->addProduct($product);
		}
		if ($transport->locationReached() && $transport->productDelivered($location)) {
			return $transport;
		}
		
		// try sfexpress adaptor firstly
		$transport = self::factory('sfexpress');
		$transport->setLocation($location);
		foreach ($products as $product) {
			$transport->addProduct($product);
		}
		if ($transport->locationReached() && $transport->productDelivered($location)) {
			return $transport;
		}
		
		//  try ems adaptor secondly
		$transport = self::factory('ems');
		$transport->setLocation($location);
		foreach ($products as $product) {
			$transport->addProduct($product);
		}
		if ($transport->productDelivered($location)) {
			return $transport;
		}
	}
	
	/**
	 * Set location index file if needed
	 */
	public static function setLocationIndexFile ($index_file)
	{
		Fw_Transport_Location::$index_file = trim($index_file);
	}
	
	/**
	 * Set product access table
	 * Check product's transportable from different adaptors
	 */
	public static function setProductAccessTable ($access_table)
	{
		Fw_Transport_Product::$access_table = (array) $access_table;
	}
}