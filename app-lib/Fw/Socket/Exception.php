<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Socket
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @see Fw_Exception
 */
require_once 'Fw/Exception.php';
 
/**
 * @package Fw_Socket
 */
class Fw_Socket_Exception extends Fw_Exception
{
	
}