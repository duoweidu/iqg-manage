<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Http
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id: james $
 */
 
/**
 * @see Zend_Http_Client
 */
require_once 'Zend/Http/Client.php';

/**
 * @package Fw_Http
 */
class Fw_Http_Client extends Zend_Http_Client
{
//	public function setNonBlocking ()
//	{
//		if ($this->adapter) {
//			stream_set_blocking($http_client->adapter->socket, 0);
//		}
//	}
}