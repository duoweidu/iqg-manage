<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Auth
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

/**
 * @see Zend_Auth
 */
require_once 'Zend/Auth.php';

/**
 * @package Fw_Auth
 */
class Fw_Auth extends Zend_Auth
{

}