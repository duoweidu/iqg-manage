<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Chart
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id: james $
 */

/**
 * @see Fw_Exception
 */ 
require_once 'Fw/Exception.php';

/**
 * @package Fw_Chart
 */
class Fw_Chart_Exception extends Fw_Exception
{
	
}