<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Debug
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id: james $
 */

/**
 * @see Fw_Debug_Writer_Html
 */
require_once 'Fw/Debug/Writer/Html.php';

/**
 * @abstract
 * @package Fw_Debug
 */
abstract class Fw_Debug_Writer
{
	/**
	 * Set debug level
	 *
	 * @param int $level
	 * @return void
	 */
	abstract public function level($level = 0);
	
	/**
	 * Set display styles, how to show the debug infomation
	 *
	 * @param string $style
	 * @return void
	 */
	abstract public function style($style = "");
	
	/**
	 * Add debug infomation to the pool to be displayed
	 *
	 * @param mixed $msg
	 * @param mixed $label
	 * @return void
	 */
	abstract public function debug($msg = null, $label = null);
	
	/**
	 * Write debug to the backend
	 *
	 * @return void
	 */
	abstract public function write();
}