<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Debug
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id: james $
 */

require_once 'Fw/Debug/Writer.php';

/**
 * @package Fw_Debug
 */
class Fw_Debug_Writer_Html extends Fw_Debug_Writer
{	
    /**
     * Debug level
     *
     * @var int
     */
	private $_level = 0;
	
    /**
     * Debug display styles
     *
     * @var string
     */
	private $_style = 'default';
	
    /**
     * Valid debug display style types
     *
     * @var array
     */
	private $_styleTypes = array('default', 'bottom', 'top');
	
    /**
     * Debug message
     *
     * @var array
     */
	public static $_debug_msg = array();
	
	/**
	 * Construct
	 * 
	 */
	public function __construct ($levels = array()) 
	{
		$this->_levels = $levels;
	}
	
	/**
	 * @see Fw_Debug_Writer
	 */
	public function level ($level = 0) 
	{
		$this->_level = $level;
	}
	
	/**
	 * @see Fw_Debug_Writer
	 */
	public function style ($style = 'default') 
	{
		$style = strtolower($style);
		if (!in_array($style, $this->_styleTypes)) {
			require_once 'Fw/Debug/Exception.php';
			throw new Fw_Debug_Exception(sprintf('Invalid debug display style "%s"; cannot retrieve', $style));
		}
		$this->_style = $style;
		return $this;
	}
	
	/**
	 * @see Fw_Debug_Writer
	 */
	public function debug ($msg = null, $label = null, $level = 0) 
	{
		self::$_debug_msg[] = array(
			'value' => Zend_Debug::dump($msg, $label, false),
			'level' => $level
		);
		return $this;
	}
	
	/**
	 * @see Fw_Debug_Writer
	 */
	public function write ($echo = true) 
	{
		$out = '';
		$tpl = $this->_loadtpl();

		if (sizeof(self::$_debug_msg) > 0) {
			// get all msg
			foreach (self::$_debug_msg as $msg) {
				$level = isset($msg['level']) ? $msg['level'] : 0;
				$value = isset($msg['value']) ? $msg['value'] : 0;
				// msg level must above debug level
				if ($level < $this->_level) {
					continue;
				}
				// decode once for xdebug extension
				if (function_exists('xdebug_disable')) {
					$out .= '<div>'.html_entity_decode($value).'</div>';
				} else {
					$out .= '<div>'.$value.'</div>';
				}
			}
			// fill tpl
			if ($out && $tpl) {
				$out = str_replace('{DEBUGMSG}', $out, $tpl);
			}
		}
		
		if ($echo !== true) {
			return $out;
		} else {
			echo $out;
		}
	}
	
	/**
	 * Html template for debug
	 * @return string
	 */
	private function _loadtpl ()
	{
		switch ($this->_style) {
			case 'default' :
			case 'bottom' :
				$style_pos = 'left:0px;bottom:0px';
				break;
			case 'top' :
				$style_pos = 'left:0px;top:0px';
				break;
			default :
				$style_pos = null;
				break;
		}
		if ($style_pos) {
			$tpl = '<style>body{margin:0px;height:100%;overflow:auto;}</style>'
				 . '<div id="debug_box" style="position:fixed;z-index:9999;background:#ffffe0;border-top:#bbb solid 2px;height:320px;width:100%;'.$style_pos.'">'
				 . '<div id="debug_box_nav" style="height:20px;padding-top:2px;width:auto;background:#eee;padding-left:10px;cursor:pointer;color:#888;font-weight:bold">Toggle Debug Info (NO IE6) ></div>'
				 . '<div id="debug_box_body" style="height:300px;width:auto;overflow:auto;padding-left:10px;">{DEBUGMSG}</div></div>'
				 . '<script>var ed=document.getElementById("debug_box");var edn=document.getElementById("debug_box_nav");var edb=document.getElementById("debug_box_body");'
				 . 'edb.scrollTop = edb.scrollHeight;edn.onclick=function(){if(edb.style.display!="none"){ed.style.height="20px";edb.style.display="none";}else{ed.style.height="320px";edb.style.display=""}}</script>';
			return $tpl;
		}
		return null;
	}
}
?>