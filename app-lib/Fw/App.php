<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_App
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

/**
 * @see Fw_App_Dispatcher
 */
require_once 'Fw/App/Dispatcher.php';

/**
 * @see Fw_App_Mapper
 */
require_once 'Fw/App/Mapper.php';

/**
 * @see Fw_Util
 */
require_once 'Fw/Util.php';

/**
 * @package Fw_App
 */
class Fw_App
{
	/**
	 * App class dirs
	 * @var array
	 */
	private $_dirs = array();
	
	/**
	 * App mappings
	 * @var array
	 */
	private $_maps = array();
	
	/**
	 * Template dir
	 * @var string
	 */
	private $_tpls = '';
	
	/**
	 * Cache dir
	 * @var string
	 */
	private $_caches = '';
	
	/**
	 * Error page
	 * @var string
	 */
	private $_epage = '';
	
	/**
	 * Debug mode
	 * @var bool
	 */
	private $_debug = false;
	
	/**
	 * Set debug mode for display app's dispatch infomation
	 * @param bool $debug
	 * @return Fw_App
	 */
	public function setDebug ($debug = true)
	{
		$this->_debug = $debug;
		return $this;
	}
	
	/**
	 * Set App's error page (404 page)
	 * @param string $err_page (error page url)
	 * @return Fw_App
	 */
	public function setErrorPage ($err_page)
	{
		$this->_epage = $err_page;
		return $this;
	}
	
	/**
	 * Add App's classes dirs for dispatch
	 * @param string $dir
	 * @throws Fw_App_Exception
	 * @return Fw_App
	 */
	public function addAppDir ($dir) 
	{
		if (!is_dir($dir)) {
			require_once 'Fw/App/Exception.php';
			throw new Fw_App_Exception('Could not found app directory \'' . $dir . '\'');
		}
		$this->_dirs[] = $dir;
		return $this;
	}
	
	/**
	 * Get App's classes dirs
	 * @return array
	 */
	public function getAppDirs () 
	{
		return $this->_dirs;
	}
	
	/**
	 * Add App's router mapping files for dispatch
	 * @param string $map
	 * @throws Fw_App_Exception
	 * @return Fw_App
	 */
	public function addMapFile ($map) 
	{
		if (!is_file($map)) {
			require_once 'Fw/App/Exception.php';
			throw new Fw_App_Exception('Could not found map file \'' . $map . '\'');
		}
		$this->_maps[] = $map;
		return $this;
	}
	
	/**
	 * Get App's mapping files
	 * @return array
	 */
	public function getMapFiles () 
	{
		return $this->_maps;
	}
	
	/**
	 * Set App's template dir
	 * @param string $dir
	 * @throws Fw_App_Exception
	 * @return Fw_App
	 */
	public function setTplDir ($dir) 
	{
		if (!is_dir($dir)) {
			require_once 'Fw/App/Exception.php';
			throw new Fw_App_Exception('Could not found template directory \'' . $dir . '\'');
		}
		$this->_tpls = $dir;
		return $this;
	}
	
	/**
	 * Get App's template dirs
	 * @return array
	 */
	public function getTplDir ()
	{
		return $this->_tpls;
	}
	
	/**
	 * Set App's  cache dir
	 * @param string $dir
	 * @throws Fw_App_Exception
	 * @return Fw_App
	 */
	public function setCacheDir ($dir)
	{
		if (!is_dir($dir)) {
			require_once 'Fw/App/Exception.php';
			throw new Fw_App_Exception('Could not found template directory \'' . $dir . '\'');
		}
		$this->_caches = $dir;
		return $this;
	}
	/**
	 * Get App's cache dirs
	 * @return array
	 */
	public function getCacheDir () 
	{
		return $this->_caches;
	}
	
	
	
	/**
	 * stripMagicQuotes if magic_quotes_gpc is On
	 * @return Fw_App
	 */
	public function closeMagicQuotes ()
	{
		global $_GET, $_POST, $_COOKIE, $_REQUEST;
		
		$_GET = $this->_stripMagicQuotes($_GET);
		$_POST = $this->_stripMagicQuotes($_POST);
		$_COOKIE = $this->_stripMagicQuotes($_COOKIE);
		$_REQUEST = $this->_stripMagicQuotes($_REQUEST);
		
		return $this;
	}
	
	/**
	 * callback function for closeMagicQuotes
	 */
	private function _stripMagicQuotes (&$value)
	{
		$value = (is_array($value)) 
			? array_map(array($this, '_stripMagicQuotes'), $value) 
			: stripslashes($value);
		
		return $value;
	}
	
	/**
	 * Start main router and dispatch process for App
	 * @see Fw_App_Dispatcher
	 * @throws Fw_App_Exception
	 * @return unknown
	 */
	public function run ()
	{
		if (!$this->getAppDirs()) {
			require_once 'Fw/App/Exception.php';
			throw new Fw_App_Exception('Please specify app directory first');
		}
		
		$dispatcher = new Fw_App_Dispatcher();
		
		// if open debug
		if ($this->_debug) {
			$dispatcher->setDebug(true);
		}
		
		// if set error page
		if ($this->_epage) {
			$dispatcher->setErrorPage($this->_epage);
		}
		
		// add page mappings if needed
		if ($this->getMapFiles()) {
			$mapper = new Fw_App_Mapper($this->getMapFiles());
			$dispatcher->setMapper($mapper);
		}
		
		
		// dispatch request to pages' actions
		$dispatcher->dispatch($this->getAppDirs(), $this->getTplDir(),$this->getCacheDir());
	}
	
	/**
	 * Set if using page view (default is smarty)
	 * @param boolean $pview
	 * @return Fw_App
	 */
	public static function setPageView ($pageViewClass = true)
	{
		Fw_App_Dispatcher::$pageViewClass = $pageViewClass;
		return $this;
	}
	
	/**
	 * Start session manually
	 * @return Fw_App
	 */
	public static function startSession ()
	{
		static $started = false;
		
		if (!$started) {
			session_start();
			$started = true;
		}
		
		return $this;
	}
}