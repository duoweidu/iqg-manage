<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Message
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @see Fw_Message_Exception
 */
require_once 'Fw/Message/Exception.php';

/**
 * @see Fw_Message
 */
require_once 'Fw/Message.php';

/**
 * @package Fw_Message
 */
abstract class Fw_Message_Handler
{
	/**
	 * @var Fw_Message
	 */
	public $message = null;
	
	/**
	 * Set message object
	 * 
	 * @param Fw_Message $message
	 * @return void
	 */
	public function setMessage ($message)
	{
		$this->message = $message;
	}
	
	/**
	 * Get message object
	 * 
	 * @return Fw_Message
	 */
	public function getMessage ()
	{
		return $this->message;
	}
	
	/**
	 * @abstract
	 */
	abstract protected function doSend ();
	
	/**
	 * @abstract
	 */
	abstract protected function doRecv ();
}