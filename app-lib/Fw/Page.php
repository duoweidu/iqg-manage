<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Page
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

/**
 * @see Fw_Exception
 */
require_once 'Fw/Exception.php';

/**
 * @see Fw_Session
 */
require_once 'Fw/Session.php';

/**
 * @see Fw_Debug
 */
require_once 'Fw/Debug.php';

/**
 * @see Fw_View
 */
require_once 'Fw/View.php';

/**
 * @see Fw_Util
 */
require_once 'Fw/Util.php';

/**
 * @package Fw_Page
 */
class Fw_Page
{
	
	/** invoke $ctr->act() ?
	 * @access protected
	 * @var bool
	 */
	protected $invoke=true;
	
	/**
	 * @staticvar bool
	 */
	public static $autoLoad = true;
	
	/**
	 * @access protected
	 * @var string
	 */
	protected $tplDir;
	
	/**
	 * @access protected
	 * @var string
	 */
	protected $cacheDir;
	
	/**
	 * @access protected
	 * @var string
	 */
	protected $pageName;
	
	/**
	 * @access protected
	 * @var string
	 */
	protected $actionName;
	
	/**
	 * @access protected
	 * @var string
	 */
	protected $tplName;
	
	/**
	 * @access protected
	 * @var Fw_View
	 */
	protected $view;	// Fw_View
	
	/**
	 * @access private
	 * @var Fw_Debug
	 */
	private $_debug;	// Fw_Debug
	
	/**
	 * @return the $actionName
	 */
	public function getActionName() {
		return $this->actionName;
	}

	/**
	 * @param string $actionName
	 */
	public function setActionName($actionName) {
		$this->view->_action = $this->actionName = $actionName;
		return $this;
	}

	/**
	 * @return the $invoke
	 */
	public function getInvoke() {
		return $this->invoke;
	}

	/**
	 * @param boolean $invoke
	 */
	public function setInvoke($invoke) {
		$this->invoke = $invoke;
	}

	/**
	 * Construct
	 * Main page process
	 */
	public function __construct() 
	{
		if (self::$autoLoad === true) {
			$this->__prepare();
			$this->__process();
			$this->__display();
		}
	}
	
	/**
	 * Call action method
	 * @throws Fw_Page_Exception
	 */
	public function __call ($name, $arguments) 
	{
		if (!method_exists($this, $name)) {
			require_once 'Fw/Page/Exception.php';
			throw new Fw_Page_Exception('Could not find action method \'' . $name . '\' in page class \'' . get_class($this) . '\'');
		}
	}
	
	/**
	 * Close autoload process
	 * You should call each page process manually
	 * @static
	 * @return unknown
	 */
	public static function closeAutoLoad () 
	{
		self::$autoLoad = false;
	}
	
	/**
	 * Set template dir
	 * @param string $dir
	 * @throws Fw_Page_Exception
	 * @return Fw_Page
	 */
	public function setTemplateDir ($dir) 
	{
		if (!is_dir($dir)) {
			require_once 'Fw/Page/Exception.php';
			throw new Fw_Page_Exception('Could not find tpl directory \'' . $dir . '\'');
		}
		$this->tplDir = $dir; // for tpl base dir
		return $this;
	}
	
	/**
	 * Return template dir
	 * @return string
	 */
	public function getTemplateDir () 
	{
		return $this->tplDir;
	}
	
	/**
	 * Set template dir
	 * @param string $dir
	 * @throws Fw_Page_Exception
	 * @return Fw_Page
	 */
	public function setCacheDir ($dir)
	{
		if (!is_dir($dir)) {
			require_once 'Fw/Page/Exception.php';
			throw new Fw_Page_Exception('Could not find tpl directory \'' . $dir . '\'');
		}
		$this->cacheDir = $dir; // for tpl base dir
		return $this;
	}
	
	/**
	 * Return template dir
	 * @return string
	 */
	public function getCacheDir ()
	{
		return $this->cacheDir;
	}
	
	/**
	 * Set page name (in subpage class)
	 * @access protected
	 * @param string $name
	 * @return Fw_Page
	 */
	public function setPageName ($name) 
	{
		$this->view->_page = $this->pageName = $name;
		return $this;
	}
	
	/**
	 * Return page name
	 * @access protected
	 * @return string
	 */
	public function getPageName () 
	{
		return $this->pageName;
	}
	
	/**
	 * Set template name (in subpage class)
	 * @access protected
	 * @param string $name
	 * @return Fw_Page
	 */
	public function setTemplate ($name) 
	{
		$this->tplName = $name;
		return $this;
	}
	
	/**
	 * Return template name
	 * @access protected
	 * @return string
	 */
	public function getTemplate () 
	{
		return $this->tplName;
	}
	
	/**
	 * Return template name
	 * @access protected
	 * @return string
	 */
	public function getView () 
	{
		return $this->view;
	}
	
	
	/**
	 * Return template name
	 * @access protected
	 * @return string
	 */
	public function setView ($key,$val)
	{
		return $this->view->$key=$val;
	}
	
	/**
	 * Used by subpages for fetching all request
	 * @access protected
	 * @param string $key
	 * @param mixed $value
	 * @return mixed 
	 */
	protected function param ($key = '', $value = null) 
	{
		return Fw_Util::param($key, $value);
	}
	
	/**
	 * Used by subpages for fetching all cookies
	 * @access protected
	 * @param string $key
	 * @param mixed $value
	 * @return mixed 
	 */
	protected function cookie ($key = '', $value = null) 
	{
		return Fw_Util::cookie($key, $value);
	}
	
	/**
	 * Used by subpages for fetching all sessions
	 * @access protected
	 * @param string $key
	 * @param mixed $value
	 * @return mixed 
	 */
	protected function session ($key = '', $value = null) 
	{
		return Fw_Util::session($key, $value);
	}
	
	/**
	 * Used by subpages for debug some infomation
	 * @access protected
	 * @param string $msg Message content
	 * @param string $label Message label string
	 * @param int $level Fw_Debug::LEVEL
	 * @return unknown
	 */
	protected function debug ($msg, $label = null, $level = Fw_Debug::DEBUG)
	{
		if ($this->_debug) {
			
//			if (!$label) {
				$reflection = new ReflectionClass('Fw_Debug');
				$levels = $reflection->getConstants();
				$label = '[' . array_search($level, $levels) . '] ' . $label;
//			}
			
			$this->_debug->debug($msg, $label, $level);
		}	
	}
	
	/**
	 * Do some initialization for page process
	 * Could be called by some class which need do page process manually
	 * @see Fw_App_Dispatcher
	 * @return unknown
	 */
	public function __prepare () 
	{
		// page execute time
		if (Fw_Debug::showDebug('time')) {
			$this->start_time = microtime(true);
		}
		
		// insert callback method
		$this->__before_prepare();
		
		// set page debug object
		$this->_debug = Fw_Debug::getInstance();
		$this->_debug->addWriter(new Fw_Debug_Writer_Html());
		
		// close debug infos in www
		if (!strcmp(__ENV, 'www')) {
			$this->_debug->setDebugLevel(Fw_Debug::INFO);
		}
		
		// set page tpl object
		$view_engine = defined('__TPL_ENGINE') ? __TPL_ENGINE : 'Smarty';
		$this->view = Fw_View::getInstance($view_engine, array(
			//'template_dir'	=> $this->tplDir . DIRECTORY_SEPARATOR . 'template',
			'template_dir'	=> $this->tplDir ,
			'compile_dir'	=> $this->cacheDir . DIRECTORY_SEPARATOR . 'template_c',
			'config_dir'	=> $this->tplDir . DIRECTORY_SEPARATOR . 'config',
			'cache_dir'		=> $this->cacheDir . DIRECTORY_SEPARATOR . 'cache',
		));
		
		// insert callback method
		$this->__before_process();
	}
	
	/**
	 * Assign prepared data into template and display
	 * Could be called by some class which need do page process manually
	 * @see Fw_App_Dispatcher
	 * @param string $tpl_name Passed template name
	 * @return unknown
	 */
	public function __display ($tpl_name = null) 
	{
		// display setted template
		if ($this->getTemplate()) {
			$tpl_name = $this->getTemplate();
		}
		
		// display passed template
		// TODO : Smarty 3 bug in caching
		if ($tpl_name && $this->view->templateExists($tpl_name)) {
			$this->view->display($tpl_name);
		}
		
		// page execute time
		if (Fw_Debug::showDebug('time')) {
			$this->end_time = microtime(true);
			$this->debug(__TPL_ENGINE . '_' . $this->view->getVersion(), '<span style="color:red">Template Engine Version >>></span>', Fw_Debug::INFO);
			$this->debug($this->end_time - $this->start_time, '<span style="color:red">Page Execute Time >>></span>', Fw_Debug::INFO);
			$this->debug($this->view->isCached($tpl_name), '<span style="color:red">Page Cached >>></span>', Fw_Debug::INFO);
		}
		
		// print debug msg
		$this->_debug->write();
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// implemented in sub class.
	
	/**
	 * Could be overload by subclasses
	 * For doing something before page preparation (page's objects initialization)
	 * @return unknown
	 */
	protected function __before_prepare () {}
	
	/**
	 * Could be overload by subclasses
	 * For doing something before page main process
	 * @return unknown
	 */
	protected function __before_process () {}
	
	/**
	 * Could be overload by subclasses
	 * For implement the main process of the subpages
	 * @return unknown
	 */
	protected function __process () {}
}
