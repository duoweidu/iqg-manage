<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Crypt
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @package Fw_Crypt
 */
abstract class Fw_Crypt
{
	/**
	 * Create crypt object
	 * @param string $engine Engine name (rsa)
	 * @return object
	 */
	public static function factory ($engine, $config = array())
	{
		$class_file = 'Fw/Crypt/' . ucfirst($engine) . '.php';
		$class_name = 'Fw_Crypt_' . ucfirst($engine);
		
		require_once $class_file; // include crypt class
		
		return new $class_name($config);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// abstract methods 
	
	abstract public function encrypt ($s);
	
	abstract public function decrypt ($s);
}