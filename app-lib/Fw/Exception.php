<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Exception
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';

/**
 * @package Fw_Exception
 */
class Fw_Exception extends Zend_Exception
{
	public function __construct($msg = '', $code = 0, Exception $e = null)
	{
		parent::__construct($msg, $code, $e);
	}
}