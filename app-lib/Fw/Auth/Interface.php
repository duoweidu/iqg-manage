<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Auth
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @package Fw_Auth
 */
interface Fw_Auth_Interface
{
    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Exception If authentication cannot be performed
     * @return Void / boolean / an object including result
     */
    public function authenticate();
}