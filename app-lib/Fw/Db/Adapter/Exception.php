<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Db
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id: james $
 */

/**
 * @see Fw_Db_Exception
 */
require_once 'Fw/Db/Exception.php';

/**
 * @package Fw_Db
 */
class Fw_Db_Adapter_Exception extends Fw_Db_Exception
{
}
