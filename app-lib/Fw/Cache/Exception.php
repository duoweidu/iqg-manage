<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Cache
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id: james $
 */
 
/**
 * @see Fw_Exception
 */
require_once 'Fw/Exception.php';

/**
 * @package Fw_Cache
 */
class Fw_Cache_Exception extends Fw_Exception
{
	
}