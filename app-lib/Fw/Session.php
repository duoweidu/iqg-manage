<?php
/**
 * Fw Framework
 *
 * @category   Fw
 * @package    Fw_Session
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */
 
/**
 * @see Zend_Session
 */
require_once 'Zend/Session.php';

/**
 * @package Fw_Session
 */
class Fw_Session extends Zend_Session
{
	
}