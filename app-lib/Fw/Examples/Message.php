<?php
/**
 * Fw Framework
 *
 * @ignore
 * @category   Examples
 * @package    Examples
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

require_once 'config.inc';

require_once 'Fw/Message.php';
require_once 'Fw/Util.php';

/**
 * @ignore
 * @package Examples
 */
class Examples_Message extends Fw_Message
{
	const MSG_ERROR		= 1;
	const MSG_NOTICE	= 2;
}

/**
 * @ignore
 * @package Examples
 */
class Examples_Message_Queue extends Fw_Message_Queue
{
	
}

/**
 * @ignore
 * @package Examples
 */
class Examples_Message_Handler extends Fw_Message_Handler
{
	public function doSend ()
	{
		// TODO : add send handler for sending message
	}
	
	public function doRecv ()
	{
		// Get message from queue
		$msg = $this->getMessage();
		
		// Do action by message type
		switch ($msg->getType()) {
			case Examples_Message::MSG_ERROR : 
				echo "[ERROR] Oh Shit ! " . $msg->getData() . "\n";
				exit;
			case Examples_Message::MSG_NOTICE : 
				echo "[NOTICE] Yeah ! " . $msg->getData() . "\n";
				break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// run demo

try {
	
	$mq = new Examples_Message_Queue();
	$mq->addHandler(new Examples_Message_Handler());
	
	if (!$mq->size()) {
		// init error messages
		foreach (range(1, 5) as $msg_id) {
			$msg = new Examples_Message();
			$msg->setType(Examples_Message::MSG_ERROR);
			$msg->setData("Error Message {$msg_id}");
			$msg = json_encode($msg); // json format data
			$mq->addMessage($msg);
			unset($msg);
		}
		// init notice messages
		foreach (range(1, 10) as $msg_id) {
			$msg = new Examples_Message();
			$msg->setType(Examples_Message::MSG_NOTICE);
			$msg->setData("Notice Message {$msg_id}");
			$msg = json_encode($msg); // json format data
			$mq->addMessage($msg);
			unset($msg);
		}
	}
	
	$mq->start();

} catch (Exception $e) {
	Fw_Util::trace($e);
	exit;
}
