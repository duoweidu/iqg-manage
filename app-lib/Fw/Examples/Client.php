<?php
/**
 * Fw Framework
 *
 * @ignore
 * @category   Examples
 * @package    Examples
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

require_once 'config.inc';

require_once 'Fw/Socket/Client.php';
require_once 'Fw/Util.php';

/**
 * @ignore
 * @package Examples
 */
class Examples_Client extends Fw_Socket_Client
{
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// run demo

try {
	
	$client = new Examples_Client();
	echo $client->test();
	
//	$client->shutdown();

} catch (Exception $e) {
	Fw_Util::trace($e);
	exit;
}
