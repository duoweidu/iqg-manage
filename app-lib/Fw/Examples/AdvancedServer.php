<?php
/**
 * Fw Framework
 *
 * @ignore
 * @category   Examples
 * @package    Examples
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

require_once 'config.inc';

require_once 'Fw/Socket/Server.php';
require_once 'Fw/Process.php';
require_once 'Fw/Util.php';

/**
 * @ignore
 * Server host constant
 */
define('SERVER_HOST', '127.0.0.1');

/**
 * @ignore
 * @package Examples
 */
class Examples_AdvancedServer extends Fw_Process
{
	public function __construct ($fr, $to)
	{
		parent::__construct(); // init shared space
		
		$this->ports = range($fr, $to);
		
		$this->setMaxProcess(count($this->ports));
	}
	
	public function __init ()
	{
		$this->__release();
	}
	
	public function run ()
	{
		$ports = $this->ports;
		$port = array_pop($ports);
		$this->ports = $ports;
		
		echo "Listening on : " . $port . "\n";
		
		$server = new Examples_Server(SERVER_HOST, $port);
		$server->daemon();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// implement class

/**
 * @ignore
 * @package Examples
 */
class Examples_Server extends Fw_Socket_Server
{
	public function __construct ($host, $port)
	{
		$this->host = $host;
		$this->port = $port;
	}
	
	/**
	 * This method could be called from the client
	 * 
	 * @return void
	 */
	public function test ()
	{
		return $this->host . ':' . $this->port . ' server call method : '.__METHOD__;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// run demo

try {
	
	$advancedServer = new Examples_AdvancedServer(11111, 11118);
	$advancedServer->start();

} catch (Exception $e) {
	Fw_Util::trace($e);
	exit;
}
