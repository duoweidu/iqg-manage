<?php
/**
 * Fw Framework
 *
 * @ignore
 * @category   Examples
 * @package    Examples
 * @author     - <->
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    $Id$
 */

require_once 'config.inc';

require_once 'Fw/Socket/Server.php';
require_once 'Fw/Util.php';

/**
 * @ignore
 * @package Examples
 */
class Examples_Server extends Fw_Socket_Server
{
	/**
	 * This method could be called from the client
	 * 
	 * @return void
	 */
	public function test ()
	{
		return 'server:ok';
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// run demo

try {
	
	$server = new Examples_Server();
	$server->debugMode(1);
	$server->daemon();

} catch (Exception $e) {
	Fw_Util::trace($e);
	exit;
}
