<?php
 

class Core_HushMQ { 

	/**
	 * Store the parts of the message
	 *
	 * @var array
	 */
	private $_parts = array();

	/**
	 * Socket to send and receive via
	 *
	 * @var ZMQSocket
	 */
	private $_socket;

	static private $Available;


	/*
		是否可用
	*/
	static public function Available(){
		if(!isset(self::$Available)){			
			try{
				//检查依赖amqp扩展是否存在？
				self::$Available= @class_exists('ZMQContext');		 	
			}catch(Exception $e){
				self::$Available=false;
			}
		}
		return self::$Available;
	}

	
	/*
		Core_HushMQ(ZMQ::SOCKET_DEALER,array(ZMQ::SOCKOPT_IDENTITY => $identity, ..))
	*/
	public function __construct( $socketType=null , $arrSockOpt=array()) {

		if(isset($socketType)){
			$context = new ZMQContext();		
			$this->_socket = new ZMQSocket($context, $socketType);	

			foreach($arrSockOpt as $optKey => $optVal){
				$this->_socket->setSockOpt($optKey, $optVal);
			}
			$this->connect();
		}
		
	}

	
	
	
	private function connect() {
		$this->_socket ->connect($_SERVER['hushmq']);//"tcp://192.168.1.89:8110"
	}


	public function setSocket(ZMQSocket $socket) {
		$this->_socket = $socket;
		$this->connect();
		return $this;
	}
	public function getSocket() {		
		return $this->_socket;
	}


	/*
		$identity = sprintf ("%04X", rand(0, 0x10000));	
		$zmq->setSockOpt(ZMQ::SOCKOPT_IDENTITY, $identity);	
	*/
	public function setSockOpt($optKey,$optVal) {
		$this->_socket->setSockOpt($optKey, $optVal);	
	}
	

	/**
	 * Formats 17-byte UUID as 33-char string starting with '@'
	 * Lets us print UUIDs as C strings and use them as addresses
	 *
	 * @param string $data 
	 * @return string
	 */
	public function encodeUUID($data) {
		return "@" . bin2hex($data);
	}

	/**
	 * Format the hex string back into a packed int. 
	 *
	 * @param string $data 
	 * @return string
	 */
	public function decodeUUID($data) {
		return pack("H*", substr($data, 1));
	}

	

	/**
	 *  Receive message from socket
	 *  Creates a new message and returns it
	 *  Blocks on recv if socket is not ready for input
	 * 
	 * @throws Exception if no socket present
	 * @return Zmsg
	 */
	public function recv() {
		if(!isset($this->_socket)) {
			throw new Exception("No socket supplied");
		}
		$this->_parts = array();
		while(true) {
			$this->_parts[] = $this->_socket->recv();
			if(!$this->_socket->getSockOpt(ZMQ::SOCKOPT_RCVMORE)) {
				break;
			}
		}
		return $this;
	}

	/**
	 * Send message to socket. Destroys message after sending.
	 *
	 * @throws Exception if no socket present
	 * @param boolean $clear
	 * @return Zmsg
	 */
	public function sendBody($clear = true) {
		if(!isset($this->_socket)) {
			throw new Exception("No socket supplied");
		}
		$count = count($this->_parts);
		$i = 1;
		foreach($this->_parts as $part) {
			$mode = $i++ == $count ? null : ZMQ::MODE_SNDMORE;
			$ret=$this->_socket->send($part, $mode);
			//var_dump($ret);
		}
		if($clear) {
			unset($this->_parts);
			$this->_parts = array();
		}
		return $this;
	}


	public function sendJson($value) {
		return $this->send(json_encode($value));		
	}

	public function send($string) {
		if(!isset($this->_socket)) {
			throw new Exception("No socket supplied");
		}
		$ret=$this->_socket->send($string, null);		 
		return $ret;
	}

	/**
	 * Report size of message
	 *
	 * @return int
	 */
	public function parts() {
		return count($this->_parts);
	}

    /**
     * Return the last part of the message
     *
     * @return string
     */
	public function last() {
		return $this->_parts[count($this->_parts)-1];
	}

	/**
	 * Set the last part of the message
	 *
	 * @param string $set 
	 */
	public function setLast($set) {
		$this->_parts[count($this->_parts)-1] = $set;
	}

	/**
	 * Return the body
	 *
	 * @return string
	 */
	public function body() {
		return $this->_parts[count($this->_parts) -1];
	}

	/**
	 * Set message body to provided string. 
	 *
	 * @param string $body 
	 * @return Zmsg
	 */
	public function bodySet($body) {
		$pos = count($this->_parts);
		if($pos > 0) {
			$pos = $pos - 1;
		}
		$this->_parts[$pos] = $body;
		return $this;
	}

	/**
	 * Set message body using printf format
	 *
	 * @return void
	 */
	public function bodyFmt() {
		$args = func_get_args();
		$this->bodySet(vsprintf(array_shift($args), $args));
		return $this;
	}

	/**
	 * Push message part to front
	 *
	 * @param string $part 
	 * @return void
	 */
	public function push($part) {
		array_unshift($this->_parts, $part);
	}

	/**
	 * Pop message part off front of message parts
	 *
	 * @return string
	 * @author Ian Barber
	 */
	public function pop() {
		return array_shift($this->_parts);
	}

	/**
	 * Return the address of the message
	 *
	 * @return void
	 * @author Ian Barber
	 */
	public function address() {
		$address = count($this->_parts) ? $this->_parts[0] : null;
		return (strlen($address) == 17 && $address[0] == 0) ? $this->encodeUUID($address) : $address;
	}

	/**
	 * Wraps message in new address envelope. 
	 * If delim is not null, creates two part envelope.
	 *
	 * @param string $address 
	 * @param string $delim 
	 * @return void
	 */
	public function wrap($address, $delim = null) {
		if($delim !== null) {
			$this->push($delim);
		}
		if($address[0] == '@' && strlen($address) == 33) {
			$address = $this->decodeUUID($address);
		}
		$this->push($address);
		return $this;
	}

	/**
	 * Unwraps outer message envelope and returns address
	 * Discards empty message part after address, if any
	 *
	 * @return void
	 * @author Ian Barber
	 */
	public function unwrap() {
		$address = $this->pop();
		if(!$this->address()) {
			$this->pop();
		}
		return $address;
	}

	/**
	 * Dump the contents to a string, for debugging and tracing.
	 *
	 * @return string
	 */
	public function __toString() {
		$string = "--------------------------------------" . PHP_EOL;
		foreach($this->_parts as $index => $part) {
			$len = strlen($part);
			if($len == 17 && $part[0] == 0) {
				$part = $this->encodeUUID($part);
				$len = strlen($part);
			}
			$string .= sprintf ("[%03d] %s %s",  $len, $part, PHP_EOL);
		}
		return $string;
	}

	 

	 
}

?>