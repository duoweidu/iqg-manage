<?php

/*
        注意：同域名下的的key值相通，不同域名下的的key值不相通
*/

//-----Memcache 内存临时缓存---------
define('MEMCACHE_HANDLE','__memcache__');


/*
        写入缓存
*/
function mc_set($name, $value , $ttl = 3600) {
        if(empty($name))return;
        $cache= mc_init();

        if(mc_isd()){
            Return	$cache->set(mc_prefix().$name, $value,  $ttl);  //memcached
        }else{
            Return  $cache->set(mc_prefix().$name, $value, false, $ttl);   //memcache
        }
}
/*
        读取缓存
*/
function mc_get($name, $default=null,$prefix=null,$cache_server=array()) {
        if(empty($name))return;
        $cache=mc_init('',$cache_server);
       
        $ret=$cache->get(mc_prefix($prefix).$name);
         
        Return $ret?$ret:$default;
}
/*
        清除缓存,允许跨域清除，但不允许跨域读写
        todo:  缓存服务器分布部署时会存在问题
*/
function mc_unset($name, $prefix = null,$cache_server=array()) {
	if(empty($name))return;
	$cache= mc_init('',$cache_server);
      if(mc_isd()){
          $cache->set(mc_prefix($prefix).$name, null, 0);   //memcached
      }else{
	      $cache->set(mc_prefix($prefix).$name, null, false, 0);  //memcache
      }
	$cache->delete(mc_prefix($prefix).$name);
}
function mc_isset() {
	//未用，暂不实现
}

function mc_inc($name,$val=1) {
	if(empty($name))return;
	$cache= mc_init();
	$ret = $cache->increment(mc_prefix().$name,$val);
	if($ret===false)        $cache->set(mc_prefix().$name,$val);
	Return $ret;
}
function mc_dec($name,$val=1 ) {
	if(empty($name))return;
	$cache= mc_init();
	Return  $cache->decrement(mc_prefix().$name,$val);
}
function mc_init($host='',$cache_server=array()) {
	$key = MEMCACHE_HANDLE.$host;
	if(!empty($cache_server))
	{
		$cache_server_key=$cache_server['server_area'];
		$key.=$cache_server_key;
	}
	if( !(isset($GLOBALS[$key]) && is_object($GLOBALS[$key])) ){
        
         if(mc_isd()){
            $cache = new Memcached();
         }else{
		    $cache = new Memcache();
         }
		$servers =  $_SERVER['memcache']; 
		if(!empty($cache_server)){
			$servers=$cache_server['cache_server'];
		}
		foreach($servers as $val)
		{
			$cache->addServer($val['host'], $val['port']);
		}
		$GLOBALS[$key] = $cache;
	}
	return $GLOBALS[$key];
}
/*
 关闭缓存连接
*/
function mc_close(){
	$key = MEMCACHE_HANDLE;
	if( (isset($GLOBALS[$key]) && is_object($GLOBALS[$key])) ){
		$cache=$GLOBALS[$key];
		$cache->close();
		unset($GLOBALS[$key]);
	}
}
/*
 设置缓存前缀
*/
function mc_prefix($prefix=null) {
	Return isset($prefix)  ? $prefix : strtolower($_SERVER['HTTP_HOST']);
}


/*
 检查当前mc扩展是否为 Memcached
*/
function mc_isd($prefix=null) {
	Return  class_exists('Memcached',FALSE);
}

?>