<?php


 

class Core_BesMQ {

   private $attr=array();

   static private $Available=false;
   static private $CurrentURL;
      
   private $hushmq;
   
   //默认为DIRECT , $queueName 不能包含"."(点号)
   function  __construct($queueName,$exchangeName){
		
	   //获取通道是否可用？
	    require_once('Switch.php');
		$isPass = Core_Switch::checkPass($queueName,$exchangeName);	

		//var_dump($queueName,$exchangeName,$isPass);
		//$isPass=true;		
	    if($isPass){
			$this->attr['queueName']=$queueName;
			$this->attr['exchangeName']=$exchangeName;
			 
			  
			//检查依赖 扩展是否存在？
			try{				
				self::$Available= Core_HushMQ::Available() ;// @class_exists('Core_HushMQ');
				
				if(self::$Available){
				
					$opt=array();
					//$identity = sprintf ("%08X", rand(0, 0x100000000));
					//$opt=array(ZMQ::SOCKOPT_IDENTITY => $identity);
					$this->hushmq= new Core_HushMQ(ZMQ::SOCKET_DEALER,$opt);
				}
				
					
			}catch(Exception $e){
				self::$Available=false;
			}
			
			
		}else{
			self::$Available=false;
		}

   }




	//发送消息
	public function sendDebug($message){	

		//特殊业务处理逻辑注入
		if(self::isFirstIn()){	
			$this->sendUserRequest();
		}

		
		//1.获取当前执行页面
		if(isset($_SERVER['REQUEST_URI']) ){
			$message.="\t".$_SERVER['REQUEST_URI'];
			$SERVER_ADDR = $_SERVER["SERVER_ADDR"];
		}else{
			$message.="\t".$_SERVER['PWD']."/".$_SERVER["SCRIPT_NAME"];
			$SERVER_ADDR = self::getServerIP() ;
		}

		//2.获取当前用户ID
		$message.="\t".self::getUserTag();	

		$message.="\t@". $SERVER_ADDR ;

		return $this->send($message);
	}



	private function sendUserRequest(){		
		//var_dump('isLogme:' .Core_Switch::isLogme());
		if(Core_Switch::isLogme()){		//1次检查：session和开关检查				

			$mq= new Core_BesMQ('usrreq','jtab.hush');		//2次检查：BesMQ内部还有一次针对front的开关检查		
			$req=array();	//  'string' =>  String 
			$req['userid']=self::getUserTag();		//v3前台的
			$req['sid']=session_id();		//v3前台的
			$req['uri']=$_SERVER['REQUEST_URI'];			
			$req['session']=json_encode($_SESSION);
			$req['get']=json_encode($_GET);
			$req['post']=json_encode($_POST);
			$req['cookie']=json_encode($_COOKIE);
			$req['server']=$_SERVER["SERVER_ADDR"];
			//var_dump(json_encode((object)$req));
			$ret=$mq->send(json_encode((object)$req));
			//s("send>",$ret);

		}
		
	}




	//通过amqpExchange发送消息
	public function send($message){
		$ret=false;
		//Add HushMQ
		switch($this->attr['exchangeName']){
			case 'log.hush':			
			default:
				$this->doSend($message,$this->attr['queueName'],$this->attr['exchangeName']);				
				break;
			
		}		
		return true;
	}
	
	

	
	public function doSend($msg,$type,$root){
		
		if(self::$Available){			
			$arr=array();
			$arr['r']=$root;
			$arr['t']=$type;
			$arr['d']=$msg;
			$this->hushmq->sendJson($arr);
		}
	}
	 

   	//获取各平台用户标识id
	static public function getUserTag(){
		if(isset($_SESSION['admin']['id']) ){		//v2后台的
			$ret=$_SESSION['admin']['id'];			
		}elseif(isset($_SESSION["userid"]))  {		//v1后台的  && v3前台的
			$ret=$_SESSION["userid"];			
		}elseif(isset($_COOKIE["visitorid"]))  {	//v1前端的 , visitorid=20111130104431420129
			$ret=$_COOKIE["visitorid"];			
		}else{										//默认空为0
			$ret="0";			
		}
		return $ret;
	}



	//是否首次调用进入
	static public function isFirstIn(){	
	   if(self::$CurrentURL==null){
		    self::$CurrentURL = $_SERVER['REQUEST_URI'];
			return true;
	   }else{			
			return false;
	   }

    }


	
	static public function getServerIP() {

		//windows server
		if(DIRECTORY_SEPARATOR==='\\') return '127.0.0.1';

		//linux server

		$preg = "/\A((([0-9]?[0-9])|(1[0-9]{2})|(2[0-4][0-9])|(25[0-5]))\.){3}(([0-9]?[0-9])|(1[0-9]{2})|(2[0-4][0-9])|(25[0-5]))\Z/";

		try{
			/*
		//获取操作系统为win2000/xp、win7的本机IP真实地址
			exec("ipconfig", $out, $stats);
			if (!empty($out)) {
				foreach ($out AS $row) {
					if (strstr($row, "IP") && strstr($row, ":") && !strstr($row, "IPv6")) {
						$tmpIp = explode(":", $row);
						if (preg_match($preg, trim($tmpIp[1]))) {
							return trim($tmpIp[1]);
						}
					}
				}
			}
			*/
		//获取操作系统为linux类型的本机IP真实地址
			exec("/sbin/ifconfig|grep 'inet addr:'", $out, $stats);
			//var_dump( $out);
			if (!empty($out)) {
				if (isset($out[0]) && strstr($out[0], 'addr:')) {
					$tmpArray = explode(":", $out[0]);
					$tmpIp = explode(" ", $tmpArray[1]);
					if (preg_match($preg, trim($tmpIp[0]))) {
						return trim($tmpIp[0]);
					}
				}
			}

		}catch(Exception $e){
			
		}
		return '127.0.0.1';
	}


}



?>