<?php


/*
	BesMQ 通道开关管理检查
	外部依赖： __APP__

*/


class Core_Switch {
   
   static private $APP;	
   
   //获取mq通道开关,默认开放
   static public function checkPass($queueName,$exchangeName){	

	    //针对交换机的例外处理策略
		switch($exchangeName){
			case 'track.hush':	//页面访问			
			case 'ops.hush':	//运维监控
			case 'batch.hush':	//优惠劵
				return self::isAllow('exception');
				break;
			default:
				if(!self::isAllow()){			 
					return false;
				}
				break;
		}

	  


		$ret=true;		
	   //检查针对不同平台，做一些特殊处理		 
		switch(self::getApp()){
			case 'frontend@v1':		//需更新 ihush-v1\ihush-v2\etc\global.setting.php	
			case 'frontend@v3':	
				if($exchangeName=='track.hush'){
					$ret=true;
				}else{
					switch($queueName){
						case 'sqlfront':	
						case 'sql':							
							//检查logme开关
							if(self::isLogme()){
								$ret=true;
							}else{
								//如果 $_COOKIE['cookieuserid'] = 1680602  则打开记录						
								if(isset($_COOKIE['cookieuserid']) && strpos(',1680602,593940,',$_COOKIE['cookieuserid']) ){
									$ret=true;
								}else{
									$ret=false;
								}	
							}
							break;
						case 'sqlfes':
							return true;
							break;		
						case 'sqlerr':
							return true;
							break;
						case 'usrreq':	//用户请求信息（配合sql进行debug）
							return (boolean)self::isLogme();
							break;
						case 'app':
							return true;
							break;
						default:
							$ret=false;
							break;
					}
				}
				break;
			case 'backend@v1':	
				$ret=true;
				break;
			case 'backend@v2':	
				$ret=true;
				break;
			default:
				$ret=true;
				break;
		}		
	
		return $ret;
	    
	} 



	//获取当前运行平台标识
   static public function getApp(){
	    if(!self::$APP){
			self::$APP = defined('__APP__') ? __APP__ : '';		
		}
		return self::$APP;
   }


	//获取当前会话logme开关
   static public function isLogme(){
	    if(session_id()!=""){
			return $_SESSION['please_log_me'];
		}else{
			return 0;
		}
   }

	
   
   static public function isAllow($tag='access'){	//access,exception,
		if(defined('__ETC')){
			$file=__ETC.'/switch.besmq.'.$tag;
		}
		$ret=true;
		if(file_exists($file)){
			$content=file_get_contents($file);
			$content=trim($content);
			//access文件内容为0时，关闭通道
			if($content!==false && $content=='0'){
				$ret=false;
			}
		}else{
			$ret=true;
		}

		return $ret;
   }


 



}



?>