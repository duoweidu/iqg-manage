<?php
class KpiRuleLog extends Db_Admin {

	public $_name = 'kpi_rule_log';
	public $_primarykey = 'krl_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'krl_id');                       //int(11)		
		self::fill_str($para,$data,'krl_key');                      //varchar(50)		
		self::fill($para,$data,'krl_value');                        //text		
		self::fill_int($para,$data,'krl_utime');                    //int(11)		
		self::fill_int($para,$data,'krl_uuid');                     //int(11)		
		return $data;
	}

} 
?>