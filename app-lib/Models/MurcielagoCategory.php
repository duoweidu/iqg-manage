<?php
class MurcielagoCategory extends Db_Iqg {

	public $_name = 'murcielago_category';
	public $_primarykey = 'catId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'catId');                        //int(11)		
		self::fill_int($para,$data,'parentId');                     //int(11)		
		self::fill($para,$data,'catPath');                          //text		
		self::fill_str($para,$data,'catName');                      //varchar(50)		
		self::fill_int($para,$data,'sortOrder');                    //int(11)		
		self::fill_int($para,$data,'isHidden');                     //int(11)		
		self::fill_int($para,$data,'cityId');                       //smallint(5) unsigned		
		return $data;
	}

} 
?>