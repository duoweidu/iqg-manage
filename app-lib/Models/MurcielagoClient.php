<?php
class MurcielagoClient extends Db_Iqg {

	public $_name = 'murcielago_client';
	public $_primarykey = 'clientId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'clientId');                     //int(11)		
		self::fill_int($para,$data,'clientLevel');                  //smallint(6)		
		self::fill_int($para,$data,'parentId');                     //int(11)		
		self::fill_str($para,$data,'username');                     //varchar(50)		
		self::fill_str($para,$data,'password');                     //varchar(50)		
		self::fill_date($para,$data,'beginDate');                   //date		
		self::fill_date($para,$data,'endDate');                     //date		
		self::fill_email($para,$data,'email');                      //varchar(200)		
		self::fill_str($para,$data,'companyName');                  //varchar(100)		
		self::fill_str($para,$data,'fullname');                     //varchar(50)		
		self::fill_str($para,$data,'gender');                       //varchar(10)		
		self::fill_str($para,$data,'telphone');                     //varchar(50)		
		self::fill_str($para,$data,'mobile');                       //varchar(50)		
		self::fill_int($para,$data,'enable');                       //tinyint(1)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'updateTime');                   //int(11)		
		return $data;
	}

} 
?>