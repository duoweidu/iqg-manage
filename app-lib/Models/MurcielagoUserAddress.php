<?php
class MurcielagoUserAddress extends Db_Iqg {

	public $_name = 'murcielago_user_address';
	public $_primarykey = 'addressId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'addressId');                    //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'address');                      //varchar(255)		
		self::fill_str($para,$data,'lat');                          //double(10,6)		
		self::fill_str($para,$data,'lng');                          //double(10,6)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>