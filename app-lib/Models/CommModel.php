<?php
/*
	伪MODEL，用于获取DB直接操作相应库
*/
class CommModel extends Db_Admin {	
	function prepareData($para){}
	
	
	public static function readOne($sql){
		$hotel= new CommModel();
		$rs = $hotel->execute($sql);
		return $hotel->fetch($rs);
	
	}
	
	public static function readAll($sql){		
		$hotel= new CommModel();		
		$rs = $hotel->execute($sql);
		return $hotel->fetchAll($rs);
		
	}

} 
?>