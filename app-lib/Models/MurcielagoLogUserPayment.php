<?php
class MurcielagoLogUserPayment extends Db_Iqg {

	public $_name = 'murcielago_log_user_payment';
	public $_primarykey = 'logUserPaymentId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logUserPaymentId');             //int(11)		
		self::fill_int($para,$data,'paymentType');                  //tinyint(2)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_float($para,$data,'amount');                     //float(9,2)		
		self::fill_str($para,$data,'title');                        //varchar(100)		
		self::fill($para,$data,'remark');                           //text		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>