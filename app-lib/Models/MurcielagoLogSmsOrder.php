<?php
class MurcielagoLogSmsOrder extends Db_Iqg {

	public $_name = 'murcielago_log_sms_order';
	public $_primarykey = 'logSmsOrderId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logSmsOrderId');                //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'orderSn');                      //varchar(50)		
		self::fill_str($para,$data,'mobile');                       //varchar(20)		
		self::fill($para,$data,'content');                          //text		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>