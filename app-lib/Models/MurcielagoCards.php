<?php
class MurcielagoCards extends Db_Iqg {

	public $_name = 'murcielago_cards';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'cardNumber');                   //varchar(16)		
		self::fill_int($para,$data,'money');                        //smallint(10) unsigned		
		self::fill_int($para,$data,'status');                       //tinyint(1) unsigned		
		self::fill_int($para,$data,'rechargeWay');                  //tinyint(1) unsigned		
		self::fill_int($para,$data,'categoryId');                   //int(10) unsigned		
		self::fill_int($para,$data,'isEnabled');                    //tinyint(1) unsigned		
		self::fill_int($para,$data,'batchId');                      //int(10) unsigned		
		return $data;
	}

} 
?>