<?php
class MurcielagoShopPhone extends Db_Iqg {

	public $_name = 'murcielago_shop_phone';
	public $_primarykey = 'phone';

	function prepareData($para) {
		$data=array();
		self::fill_str($para,$data,'phone');                        //varchar(20)		
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		return $data;
	}

} 
?>