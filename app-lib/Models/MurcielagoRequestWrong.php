<?php
class MurcielagoRequestWrong extends Db_Iqg {

	public $_name = 'murcielago_requestWrong';
	public $_primarykey = '';

	function prepareData($para) {
		$data=array();
		self::fill_str($para,$data,'ip');                           //varchar(15)		
		self::fill_int($para,$data,'count');                        //int(10) unsigned		
		self::fill_int($para,$data,'updateTime');                   //int(11) unsigned		
		return $data;
	}

} 
?>