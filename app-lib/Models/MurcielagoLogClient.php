<?php
class MurcielagoLogClient extends Db_Iqg {

	public $_name = 'murcielago_log_client';
	public $_primarykey = 'logClientId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logClientId');                  //int(11)		
		self::fill_int($para,$data,'clientId');                     //int(11)		
		self::fill_int($para,$data,'logType');                      //tinyint(4)		
		self::fill_str($para,$data,'subject');                      //varchar(200)		
		self::fill_str($para,$data,'remark');                       //blob		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>