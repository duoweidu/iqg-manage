<?php
class MurcielagoLogStock extends Db_Iqg {

	public $_name = 'murcielago_log_stock';
	public $_primarykey = 'logStockId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logStockId');                   //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_str($para,$data,'orderSn');                      //varchar(20)		
		self::fill_int($para,$data,'qty');                          //smallint(6)		
		self::fill($para,$data,'remark');                           //text		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>