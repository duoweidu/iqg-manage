<?php
class MurcielagoUser extends Db_Iqg {

	public $_name = 'murcielago_user';
	public $_primarykey = 'uid';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'username');                     //varchar(30)		
		self::fill_int($para,$data,'sex');                          //tinyint(1)		
		self::fill_str($para,$data,'alias');                        //char(30)		
		self::fill_email($para,$data,'email');                      //varchar(100)		
		self::fill_str($para,$data,'password');                     //varchar(100)		
		self::fill_str($para,$data,'countryCallingCode');           //varchar(5)		
		self::fill_str($para,$data,'mobile');                       //varchar(30)		
		self::fill_float($para,$data,'money');                      //float(9,2)		
		self::fill_str($para,$data,'paypassword');                  //varchar(50)		
		self::fill_str($para,$data,'uniqueId');                     //varchar(255)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'isForbidden');                  //tinyint(1)		
		self::fill_str($para,$data,'deviceToken');                  //char(64)		
		self::fill_int($para,$data,'cityId');                       //smallint(4) unsigned		
		self::fill_str($para,$data,'baiduPushUserId');              //varchar(256)		
		self::fill_int($para,$data,'comeFrom');                     //smallint(5) unsigned		
		return $data;
	}

} 
?>