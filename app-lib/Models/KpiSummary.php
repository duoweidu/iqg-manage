<?php
class KpiSummary extends Db_Admin {

	public $_name = 'kpi_summary';
	public $_primarykey = 'ksm_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'ksm_id');                       //int(11)		
		self::fill_int($para,$data,'ksm_uid');                      //int(11)		
		self::fill_int($para,$data,'ksm_islock');                   //tinyint(4)		
		self::fill_int($para,$data,'ksm_year');                     //smallint(6)		
		self::fill_int($para,$data,'ksm_month');                    //smallint(6)		
		self::fill_int($para,$data,'ksm_level');                    //tinyint(4)		
		self::fill_int($para,$data,'ksm_rulever');                  //int(11)		
		self::fill_int($para,$data,'ksm_achieve');                  //tinyint(4)		
		self::fill_int($para,$data,'ksm_shop_new');                 //int(11)		
		self::fill_int($para,$data,'ksm_shop_lt3m');                //int(11)		
		self::fill_int($para,$data,'ksm_shop_dyxx');                //int(11)		
		self::fill_int($para,$data,'ksm_shop_gtxm');                //int(11)		
		self::fill_int($para,$data,'ksm_shop_onsell');              //int(11)		
		self::fill_int($para,$data,'ksm_shop_total');               //int(11)		
		self::fill_int($para,$data,'ksm_profit_shop');              //int(11)		
		self::fill_int($para,$data,'ksm_total_money');              //int(11)		
		self::fill_int($para,$data,'ksm_total_money_alipay');       //int(11)		
		self::fill_int($para,$data,'ksm_total_money_balance');      //int(11)		
		self::fill_int($para,$data,'ksm_profit_money');             //int(11)		
		self::fill_int($para,$data,'ksm_profit_money_alipay');      //int(11)		
		self::fill_int($para,$data,'ksm_profit_money_balance');     //int(11)		
		self::fill_int($para,$data,'ksm_total_subordinate');        //int(11)		
		self::fill_int($para,$data,'ksm_achieve_sub');              //tinyint(4)		
		self::fill_int($para,$data,'ksm_total_subshop');            //int(11)		
		self::fill_int($para,$data,'ksm_profit_subshop');           //int(11)		
		self::fill_int($para,$data,'ksm_total_submoney');           //int(11)		
		self::fill_int($para,$data,'ksm_total_submoney_alipay');    //int(11)		
		self::fill_int($para,$data,'ksm_total_submoney_balance');   //int(11)		
		self::fill_int($para,$data,'ksm_profit_submoney');          //int(11)		
		self::fill_int($para,$data,'ksm_profit_submoney_alipay');   //int(11)		
		self::fill_int($para,$data,'ksm_profit_submoney_balance');  //int(11)		
		self::fill_int($para,$data,'ksm_profit_total');             //int(11)		
		self::fill_int($para,$data,'ksm_status');                   //tinyint(4)		
		self::fill_int($para,$data,'ksm_utime');                    //int(11)		
		return $data;
	}

} 
?>