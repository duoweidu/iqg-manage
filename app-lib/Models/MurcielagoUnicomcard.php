<?php
class MurcielagoUnicomcard extends Db_Iqg {

	public $_name = 'murcielago_unicomcard';
	public $_primarykey = '';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'cardNumber');                   //int(9) unsigned		
		self::fill_float($para,$data,'money');                      //float(9,2)		
		self::fill_str($para,$data,'mobile');                       //varchar(11)		
		return $data;
	}

} 
?>