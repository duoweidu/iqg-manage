<?php
class MurcielagoUserEcion extends Db_Iqg {

	public $_name = 'murcielago_user_ecion';
	public $_primarykey = 'uid';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'uid');                          //int(10) unsigned		
		self::fill_int($para,$data,'accumulativeTotal');            //int(10) unsigned		
		self::fill_int($para,$data,'total');                        //int(10)		
		self::fill_int($para,$data,'ranking');                      //int(10) unsigned		
		self::fill_int($para,$data,'countLogin');                   //int(10) unsigned		
		self::fill_int($para,$data,'countInvite');                  //int(10) unsigned		
		self::fill_int($para,$data,'countComment');                 //int(10) unsigned		
		self::fill_float($para,$data,'rankingPercent');             //float(3,2)		
		return $data;
	}

} 
?>