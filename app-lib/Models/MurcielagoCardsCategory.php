<?php
class MurcielagoCardsCategory extends Db_Iqg {

	public $_name = 'murcielago_cards_category';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'name');                         //varchar(100)		
		self::fill_int($para,$data,'addTime');                      //int(10) unsigned		
		self::fill_int($para,$data,'rechargeWay');                  //tinyint(1)		
		self::fill_int($para,$data,'money');                        //smallint(10) unsigned		
		self::fill_int($para,$data,'paymentNumbers');               //tinyint(4) unsigned		
		self::fill_int($para,$data,'expiredTime');                  //int(10) unsigned		
		return $data;
	}

} 
?>