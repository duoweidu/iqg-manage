<?php
class MurcielagoUserIdentification extends Db_Iqg {

	public $_name = 'murcielago_user_identification';
	public $_primarykey = '';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'uid');                          //int(10)		
		self::fill_str($para,$data,'mac');                          //varchar(17)		
		self::fill_str($para,$data,'idfa');                         //varchar(36)		
		self::fill_str($para,$data,'openudid');                     //varchar(40)		
		self::fill_str($para,$data,'source');                       //varchar(10)		
		return $data;
	}

} 
?>