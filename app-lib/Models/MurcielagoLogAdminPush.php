<?php
class MurcielagoLogAdminPush extends Db_Iqg {

	public $_name = 'murcielago_log_admin_push';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(11)		
		self::fill_int($para,$data,'adminId');                      //int(11)		
		self::fill_str($para,$data,'mobile');                       //varchar(50)		
		self::fill($para,$data,'content');                          //text		
		self::fill_int($para,$data,'status');                       //tinyint(1)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_datetime($para,$data,'addTime');                 //datetime		
		return $data;
	}

} 
?>