<?php
class MurcielagoGoodsShop extends Db_Iqg {

	public $_name = 'murcielago_goods_shop';
	public $_primarykey = 'shopId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'goodsId');                      //int(10) unsigned		
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		return $data;
	}

} 
?>