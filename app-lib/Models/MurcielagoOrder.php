<?php
class MurcielagoOrder extends Db_Iqg {

	public $_name = 'murcielago_order';
	public $_primarykey = 'orderId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'orderId');                      //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(10) unsigned		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'orderSn');                      //varchar(20)		
		self::fill_str($para,$data,'distributionNo');               //varchar(50)		
		self::fill_int($para,$data,'payway');                       //tinyint(1) unsigned		
		self::fill_float($para,$data,'payPrice');                   //float(9,2)		
		self::fill_int($para,$data,'payStatus');                    //tinyint(1)		
		self::fill_int($para,$data,'useStatus');                    //tinyint(1) unsigned		
		self::fill_str($para,$data,'fullname');                     //varchar(50)		
		self::fill_str($para,$data,'mobile');                       //varchar(50)		
		self::fill_email($para,$data,'email');                      //varchar(200)		
		self::fill_date($para,$data,'expiredDate');                 //date		
		self::fill_int($para,$data,'orderStatus');                  //tinyint(1)		
		self::fill_int($para,$data,'deleted');                      //tinyint(1)		
		self::fill_int($para,$data,'lat');                          //double(10,6)		
		self::fill_int($para,$data,'lng');                          //double(10,6)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'updateTime');                   //int(11)		
		self::fill_int($para,$data,'isCommented');                  //tinyint(1) unsigned		
		return $data;
	}

} 
?>