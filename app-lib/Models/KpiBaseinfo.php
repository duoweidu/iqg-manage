<?php
class KpiBaseinfo extends Db_Admin {

	public $_name = 'kpi_baseinfo';
	public $_primarykey = 'kb_shopid';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'kb_shopid');                    //int(10) unsigned		
		self::fill_str($para,$data,'kb_shopname');                  //varchar(100)		
		self::fill_int($para,$data,'kb_signer_sellerid');           //int(10) unsigned		
		self::fill_int($para,$data,'kb_holder_sellerid');           //int(10) unsigned		
		self::fill_int($para,$data,'kb_issuetime');                 //int(10) unsigned		
		self::fill_int($para,$data,'kb_offtime');                   //int(10) unsigned		
		self::fill_int($para,$data,'kb_catid');                     //int(10) unsigned		
		self::fill_int($para,$data,'kb_cityid');                    //int(10) unsigned		
		self::fill_int($para,$data,'kb_shop_pid');                  //int(10) unsigned		
		self::fill_str($para,$data,'kb_hqname');                    //varchar(100)		
		self::fill_int($para,$data,'kb_seller_pid');                //int(10) unsigned		
		self::fill_int($para,$data,'kb_seller_level');              //int(10) unsigned		
		self::fill_int($para,$data,'kb_goods_num');                 //int(11)		
		self::fill_int($para,$data,'kb_utime');                     //int(11)		
		self::fill_int($para,$data,'kb_uuid');                      //int(11)		
		return $data;
	}

} 
?>