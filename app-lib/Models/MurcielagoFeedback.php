<?php
class MurcielagoFeedback extends Db_Iqg {

	public $_name = 'murcielago_feedback';
	public $_primarykey = 'fid';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'fid');                          //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'subject');                      //varchar(200)		
		self::fill($para,$data,'content');                          //text		
		self::fill_str($para,$data,'mobile');                       //varchar(50)		
		self::fill_email($para,$data,'email');                      //varchar(50)		
		self::fill_int($para,$data,'replied');                      //tinyint(1)		
		self::fill($para,$data,'replyContent');                     //text		
		self::fill($para,$data,'lat');                              //double(10,6)		
		self::fill($para,$data,'lng');                              //double(10,6)		
		self::fill_int($para,$data,'enable');                       //tinyint(1)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_str($para,$data,'userAgent');                    //varchar(200)		
		return $data;
	}

} 
?>