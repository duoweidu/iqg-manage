<?php
class Shop extends Db_Manage {

	public $_name = 'shop';
	public $_primarykey = 'shopId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		self::fill_int($para,$data,'signerSellerId');               //int(10) unsigned		
		self::fill_int($para,$data,'holderSellerId');               //int(10) unsigned		
		self::fill_int($para,$data,'issueTime');                    //int(10) unsigned		
		self::fill_int($para,$data,'catId');                        //int(10) unsigned		
		return $data;
	}

} 
?>