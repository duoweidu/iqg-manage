<?php
class MurcielagoAd extends Db_Iqg {

	public $_name = 'murcielago_ad';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'imgUri');                       //varchar(200)		
		self::fill_int($para,$data,'beginTime');                    //int(10) unsigned		
		self::fill_int($para,$data,'endTime');                      //int(10) unsigned		
		self::fill_str($para,$data,'title');                        //varchar(100)		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		self::fill_int($para,$data,'width');                        //int(10)		
		self::fill_int($para,$data,'height');                       //int(10)		
		return $data;
	}

} 
?>