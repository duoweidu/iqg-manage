<?php
class MurcielagoClientShop extends Db_Iqg {

	public $_name = 'murcielago_client_shop';
	public $_primarykey = 'headquarterId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'clientId');                     //int(11)		
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		self::fill_date($para,$data,'beginDate');                   //date		
		self::fill_date($para,$data,'endDate');                     //date		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'headquarterId');                //int(10) unsigned		
		return $data;
	}

} 
?>