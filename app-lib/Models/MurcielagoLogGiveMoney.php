<?php
class MurcielagoLogGiveMoney extends Db_Iqg {

	public $_name = 'murcielago_log_giveMoney';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'mobile');                       //varchar(15)		
		self::fill_int($para,$data,'adminId');                      //int(10)		
		self::fill_int($para,$data,'money');                        //int(10)		
		self::fill_int($para,$data,'addTime');                      //int(10)		
		return $data;
	}

} 
?>