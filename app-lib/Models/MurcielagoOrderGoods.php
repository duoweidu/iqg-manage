<?php
class MurcielagoOrderGoods extends Db_Iqg {

	public $_name = 'murcielago_order_goods';
	public $_primarykey = 'orderGoodsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'orderGoodsId');                 //int(11)		
		self::fill_int($para,$data,'orderId');                      //int(11)		
		self::fill_int($para,$data,'shopId');                       //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_int($para,$data,'qty');                          //smallint(6)		
		self::fill_float($para,$data,'price');                      //float(9,2)		
		self::fill_float($para,$data,'subtotal');                   //float(9,2)		
		self::fill_int($para,$data,'paid');                         //tinyint(1)		
		self::fill_int($para,$data,'status');                       //tinyint(1)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>