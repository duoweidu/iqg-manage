<?php

/**
 * 数据库底层mysqli扩展操作类  
 * 外部依赖： 1.__ETC 常量    2. lib/Core/BesMQ.php
 * @package tc_db
 * @author 
 * @version 2013-08-18
 * @todo  n/a 
 */
class Mysqli2
{

    public static $debug = false;

    public static $report = true;

    const CONN_POOL_LEVEL = 2; // 1: 主机级共享主从连接池 (mysql连接占用少,性能略低)；2: 数据库级共享主从连接池(mysql连接占用多,性能略高)
                               
    // const MASTER_RATE_ON_SELECT = 0.0; //按1算,在主库上进行select操作的概率
    public static $IniDbConfArr = array(); // ini数据连接配置共享缓存
    private $_conf = array(
        'h' => '', // 主机ip
        'P' => '', // 端口
        'd' => '', // 库名
        'u' => '', // 用户名
        'p' => '', // 密码
        's' => array() // 从库配置,
        );

    public static $LinkConfArr = array();

    public static $LinkResArr = array(); // 全局共享连接对象
    private static $CacheLinkRes = array(); // 私有共享连接对象
    private $currLinkRes = 0; // 当前连接对象
    private $currLinkKey = 0;

    private $dbHashKey = 0;
    
    // 连接会话级相关设置
    private static $TransIDArr = array(); // 事务id，key值为$dbHashKey
    private static $IsTranProcessing = false; // 是否已进入事务处理
    private static $ReadWriteMode = 's-m-m'; // 数据读写模式（与事务相关）: 仅主、主从分离, select-update/delete-insert
    private static $LastQueryTime = array(); // 保存主从连接下最后一次sql起始执行时间
    private static $BesMQArr = array();

    private static $QueryCount = 0; // sql执行次数
    
    /**
     *
     *
     *
     * 构造函数：设置连接配置参数
     *
     * @param array $conf
     *            , 如 $conf=Mysqli2::getDefaultConfig($dbname)
     */
    function __construct($conf = null)
    {
        // 如果未传入配置，则加载默认配置数组
        if (empty($conf)) {
            $conf = self::getDefaultConfig();
        }
        
        if (isset($conf) && is_array($conf)) {
            $this->setConfig($conf[0]);
        }
    }

    private function setCacheLinkRes($slave, $link_res)
    {
        self::$CacheLinkRes[$this->dbHashKey][$slave] = $link_res;
    }

    private function getCacheLinkRes($slave)
    {
        return isset(self::$CacheLinkRes[$this->dbHashKey]) ? self::$CacheLinkRes[$this->dbHashKey][$slave] : null;
    }

    /**
     * 连接主从数据库函数：可根据CONN_POOL_LEVEL建立 连接池
     *
     * @param bool $slave
     *            , 是否选择从库[false即默认选择主库]
     * @return 当前连接对象
     */
    final public function connect($slave = false)
    {
        // 查找检查自己的连接池和是否可用
        $this->currLinkRes = $this->getCacheLinkRes($slave);
        
        if (is_object($this->currLinkRes)) {
            $available = true;
            if (microtime(true) - self::$LastQueryTime[$slave] > 5) { // sql间隔超过5秒?
                $available = mysqli_ping($this->currLinkRes); // 重新检查连接
            }
            if ($available) { // 判断连接是否可用
                if (self::$debug)
                    echo "CONNECT: 命中自己的连接池 " . ($slave ? 'slave' : 'master') . "……<br \\>\n";
                return $this->currLinkRes;
            } else {
                $this->currLinkRes = null;
            }
        } else {
            $this->currLinkRes = null;
        }
        
        // 设置当前连接唯一标识(与库名无关)
        $currlinkCfg = $this->getConfig($slave);
        $currCfg = '';
        if (self::CONN_POOL_LEVEL === 2) {
            $currCfg .= $currlinkCfg['d']; // 库名
        }
        $currCfg .= $currlinkCfg['h']; // 主机
        $currCfg .= $currlinkCfg['P']; // 端口
        $currCfg .= $currlinkCfg['c']; // 字符集
        $currCfg .= $currlinkCfg['u']; // 用户
        $currCfg .= $currlinkCfg['p']; // 密码
                                       // $currCfg['s'] = $currlinkCfg['s'];
        $currCfg .= (int) $slave;
        
        // 查找检查共享的连接池和是否可用（适用同主机不同库情况）
        if (! empty(self::$LinkConfArr)) {
            $key = array_search($currCfg, self::$LinkConfArr);
            if ($key !== false) {
                
                $tmpRes = self::$LinkResArr[$key];
                if (is_object($tmpRes) && mysqli_ping($tmpRes)) {
                    $this->currLinkRes = $tmpRes;
                    $this->currLinkKey = $key;
                    
                    // 存入自己的连接池
                    $this->setCacheLinkRes($slave, $this->currLinkRes);
                    if (self::$debug)
                        echo "CONNECT: 命中共享的连接池 " . ($slave ? 'slave' : 'master') . " $key:【" . $currCfg . "】<br \\>\n";
                } else {
                    unset(self::$LinkConfArr[$key]);
                    unset(self::$LinkResArr[$key]);
                }
                unset($tmpRes);
            }
        }
        
        // 当自己的连接池、共享的连接池都没有时，建立新连接并保存
        if (! is_object($this->currLinkRes)) {
            
            // 连接主/从数据库
            if (self::$debug) {
                echo "CONNECT: <b>连接" . ($slave ? 'slave' : 'master') . "库</b> > {$currlinkCfg['d']} , mysqli_connect( " . json_encode($currlinkCfg) . ")";
            }
            $this->currLinkRes = mysqli_connect($currlinkCfg['h'], $currlinkCfg['u'], $currlinkCfg['p'], null, $currlinkCfg['P']);
            if (self::CONN_POOL_LEVEL === 2) {
                mysqli_select_db($this->currLinkRes, $currlinkCfg['d']);
            }
            
            // 检查设置数据库字符集
            if (isset($currlinkCfg['c']) && ! empty($currlinkCfg['c'])) {
                mysqli_set_charset($this->currLinkRes, $currlinkCfg['c']);
            } else {
                mysqli_set_charset($this->currLinkRes, 'utf8');
            }
            
            // 连接成功后，将参数与连接号压入连接池
            if (is_object($this->currLinkRes)) {
                // 存入共享的连接池
                $key = count(self::$LinkResArr);
                self::$LinkConfArr[$key] = $currCfg;
                self::$LinkResArr[$key] = $this->currLinkRes;
                $this->currLinkKey = $key;
                
                // 存入自己的连接池
                $this->setCacheLinkRes($slave, $this->currLinkRes);
                
                if (self::$debug)
                    echo " ==>  连接成功，压入连接池。<br/>\n";
            } else {
                
                $this->halt("connect " . ($slave ? 'slave' : 'master') . " fail , check configure " . json_encode($currlinkCfg));
                // $this->halt("数据库繁忙请稍后访问。");
                return false;
            }
        }
        return $this->currLinkRes;
    }
    
    // =======start: 查询读数据相关操作===============================
    public function quote($val)
    {
        if (is_int($val)) {
            return $val;
        } else {
            if (! is_object($this->currLinkRes)) {
                $this->connect(true);
            }
            return "'" . mysqli_real_escape_string($this->currLinkRes, $val) . "'";
        }
    }

    /**
     * 查询主从数据库：主从选择可通过 $ReadWriteMode强制切换
     *
     * @param string $sql
     *            , 任意合法的sql语句
     * @return mixed , sql执行结果
     */
    public function query($sql)
    {
        if (empty($sql))
            return 0;
        
        switch (self::$ReadWriteMode) {
            case 's-m-m': // 读写分离
                          // 如果是select查询，则查从库 . || strtolower(substr(ltrim($sql), 0, 4))=='show'
                if (strtolower(substr(ltrim($sql), 0, 6)) == 'select') {
                    // 按照概率均衡主从库的select操作
                    // $slave= ( ceil(10*self::MASTER_RATE_ON_SELECT) >= rand(1,10) ) ? false : true ;
                    $slave = true;
                } else {
                    $slave = false; // update/insert/delete/...
                }
                break;
            case 'm-m-m': // 读写压主库
                $slave = false;
                break;
        }
        
        $currlinkCfg = $this->getConfig($slave);
        
        // 连库并设置、激活 $this->currLinkRes
        if (! $this->connect($slave))
            return false;
        
        if (self::$debug)
            echo ("Debug: connect " . ($slave ? 'slave' : 'master') . " [" . $currlinkCfg['d'] . "] <br \\>\n");
        
        if (self::CONN_POOL_LEVEL === 1 && ! mysqli_select_db($this->currLinkRes, $currlinkCfg['d']))         // && !empty($currlinkCfg['d'])
        {
            $this->halt("<font color='#ff0000'>无法选择数据库:{$currlinkCfg['d']}</font><BR>\n请检查：<BR>\n1、数据库是否存在<BR>\n2、您是否有相关操作的权限！");
        }
        
        if (self::$debug) {
            self::$QueryCount ++;
            echo ("Debug: " . ($this->isOnTran() ? '<b>Transaction</b> ' : '') . "query " . self::$QueryCount . "= {$sql}<br>\n");
        }
        
        self::$LastQueryTime[$slave] = microtime(true);
        
        // 执行sql语句，并返回结果
        $result = mysqli_query($this->currLinkRes, $sql);
        
        $useTime = microtime(true) - self::$LastQueryTime[$slave];
        
        // sql和耗时等送入日志队列
        self::sendDebug("{$useTime}\t{$sql}", $slave);
        
        if (self::$debug) {
            // $useTime = microtime(true) - self::$LastQueryTime[$slave];
            $color = $useTime > 0.5 ? 'red' : '';
            echo ("Time: <font color='$color'> {$useTime}</font><hr><br>\n");
        }
        
        if (! $result) {
            $this->halt("错误的SQL语句:[{$currlinkCfg['d']}] " . $sql);
            return false;
        }
        
        // Will return nada if it fails. That's fine.
        return $result;
    }

    /**
     *
     *
     *
     * 获取结果集单行数据，并自动指向下一行
     *
     * @param mysqli_result $result            
     * @param int $result_type
     *            , [默认字段名作为array键名]
     * @return array row, 一条记录
     */
    public function fetch($result, $result_type = MYSQLI_ASSOC)
    {
        if (is_object($result)) {
            return mysqli_fetch_array($result, $result_type);
        } else {
            Return array();
        }
    }

    /**
     *
     *
     *
     * 循环获取结果集所有行记录
     *
     * @param mysqli_result $result            
     * @param int $result_type            
     * @return array rowSet, 所有记录
     */
    public function fetchAll($result, $result_type = MYSQLI_ASSOC)
    {
        $rs = array();
        while ($row = $this->fetch($result, $result_type)) {
            $rs[] = $row;
        }
        return $rs;
    }

    /**
     *
     *
     *
     * 获取结果集所含行数
     *
     * @param mysqli_result $result            
     * @return int num ，行数
     */
    public function numRows($result)
    {
        if (is_object($result)) {
            return $result->num_rows;
        } else {
            Return false;
        }
    }

    /**
     *
     *
     *
     * 获取sql查询结果的单行或者单值
     *
     * @param string $sql            
     * @return mixed $val , [单行|单值]
     */
    public function scalar($sql)
    {
        $result = $this->query($sql);
        
        // 查询成功
        if ($result !== false) {
            $arr = $this->fetch($result);
            
            if ($arr && is_array($arr)) {
                return count($arr) > 1 ? $arr : array_shift($arr);
                ;
            } else {
                return null; // 无值
            }
        } else {
            // 查询失败
            return $result;
        }
    }
    
    // =======end: 查询读数据相关操作=============================
    
    // =======start: 更新写数据相关操作=============================
    
    /**
     *
     *
     *
     * 更新记录：含mysql escape过滤
     *
     * @param array $array
     *            , 变更字段
     * @param string $where_clause
     *            , WHERE条件语句
     * @param string $tablename
     *            , 表名
     * @return int $affectedRows , 影响行数
     */
    public function update($array, $where_clause, $tablename)
    {
        if (! is_array($array)) {
            $this->halt(" update_table( \$array, $where_clause, $tablename) 错误：第一个参数不是数组！");
        }
        
        // 主动建立主库连接，目的是为了 mysqli_real_escape_string $link
        $this->connect();
        
        foreach ($array as $k => $v) {
            $tem .= " `{$k}`='" . mysqli_real_escape_string($this->currLinkRes, $v) . "',";
            // $tem .= " `{$k}`='" . $v . "',";
        }
        $tem = rtrim($tem, ',');
        $sql = "UPDATE `{$tablename}` SET {$tem} WHERE {$where_clause}";
        if (self::$debug) {
            echo "更新数组语句：" . $sql . "<br>\n";
        }
        
        $result = $this->query($sql);
        if ($result) {
            return $this->affectedRows();
        } else {
            return false;
        }
    }

    /**
     *
     *
     *
     * 增加记录操作：含mysql escape过滤
     *
     * @param array $array
     *            , 变更字段
     * @param string $tablename
     *            , 表名
     * @return int $insertId , 自增id
     */
    public function insert($array, $tablename)
    {
        if (! is_array($array)) {
            $this->halt("方法 insert() 第1个参数不是数组错误， 表名为 {$tablename} ");
            return false;
        }
        
        // 主动建立主库连接，目的是为了 mysqli_real_escape_string $link
        $this->connect();
        
        $cols = $values = '';
        foreach ($array as $key => $val) {
            $values .= "'" . mysqli_real_escape_string($this->currLinkRes, $val) . "',";
            // $values .= "'" . $val . "',";
            $cols .= "`" . trim($key) . "`,";
        }
        
        $cols = rtrim($cols, ',');
        $values = rtrim($values, ',');
        $sql = "INSERT INTO `{$tablename}` ({$cols}) VALUES ({$values})";
        if (self::$debug) {
            echo "插入数组语句：" . $sql . "\n";
        }
        $result = $this->query($sql);
        
        if ($result) {
            return $this->lastInsertId();
        } else {
            return false;
        }
    }

    /**
     *
     *
     *
     * 获取最近更新影响行数：需紧跟update等更新后读取 （ INSERT, UPDATE, REPLACE or DELETE query）
     *
     * @return int $num ,
     */
    public function affectedRows()
    {
        // 连接主库 $this->connect();
        return mysqli_affected_rows($this->currLinkRes);
    }

    /**
     *
     *
     *
     * 获取最近新增记录id：需紧跟insert后读取
     *
     * @return int $insertId , 自增id
     */
    public function lastInsertId()
    {
        // 连接主库 $this->connect();
        return mysqli_insert_id($this->currLinkRes);
    }
    
    // =======end: 更新写数据相关操作=============================
    
    // =======start: 事务相关操作=============================
    
    /**
     *
     *
     *
     * 检查当前连接是否处于事务处理状态？
     *
     * @return bool Y/N
     */
    public function isOnTran()
    {
        return self::$IsTranProcessing;
    }

    /**
     *
     *
     *
     * 开始事务
     *
     * @return bool Y/N ，是否开启成功
     */
    public function begin()
    {
        self::$IsTranProcessing = true;
        // 建立主库连接，设置当前会话
        $this->connect();
        
        // 设置当前事务id
        $trid = self::getGUID();
        self::TransID($this->dbHashKey, $trid);
        $startTime = microtime(true);
        $succ = mysqli_autocommit($this->currLinkRes, false);
        // 送入日志队列
        self::sendDebug((microtime(true) - $startTime) . "\tTrans Start:{$trid}", false);
        
        if ($succ) {
            $this->selectMaster(); // 开始前，所有操作读写指向主库
        }
        return $succ;
    }

    /**
     *
     *
     *
     * 提交事务
     *
     * @return bool Y/N ，是否提交成功
     */
    public function commit()
    {
        // 建立主库连接，设置当前会话
        $this->connect();
        $startTime = microtime(true);
        $succ = mysqli_commit($this->currLinkRes);
        mysqli_autocommit($this->currLinkRes, true);
        // 送入日志队列
        self::sendDebug((microtime(true) - $startTime) . "\tTrans Commit:" . self::TransID($this->dbHashKey), false);
        
        // 结束后，恢复读写分离
        $this->selectSlaver();
        self::$IsTranProcessing = false;
        return $succ;
    }

    /**
     *
     *
     *
     * 回滚事务
     *
     * @return bool Y/N ，是否回滚成功
     */
    public function rollBack()
    {
        // 建立主库连接，设置当前会话
        $this->connect();
        $startTime = microtime(true);
        $succ = mysqli_rollback($this->currLinkRes);
        mysqli_autocommit($this->currLinkRes, true);
        // 送入日志队列
        self::sendDebug((microtime(true) - $startTime) . "\tTrans Rollback:" . self::TransID($this->dbHashKey), false);
        
        // 结束后，恢复读写分离
        $this->selectSlaver();
        self::$IsTranProcessing = false;
        return $succ;
    }

    /**
     * 设置、获取当前事务id
     *
     * @param unknown_type $key            
     * @param unknown_type $value            
     * @return boolean multitype:
     */
    private static function TransID($key, $value = null)
    {
        if ($value) { // set
            self::$TransIDArr[$key] = $value;
            return true;
        } else { // get
            return self::$TransIDArr[$key];
        }
    }

    /**
     * 生成唯一id
     *
     * @return string
     */
    public static function getGUID()
    {
        return md5(uniqid('T' . rand(), true));
    }
    
    // =======end: 事务相关操作=============================
    private function getConfig($slave)
    {
        if ($slave) {
            $tmp = $this->_conf['s'];
        } else {
            $tmp = $this->_conf;
            unset($tmp['s']);
        }
        return $tmp;
    }

    private function setConfig($cfg = null)
    {
        if (! is_array($cfg)) {
            $this->halt("connect parameter is not a array");
            return false;
        }
        $this->_conf = $cfg + $this->_conf;
        
        $this->dbHashKey = self::makeDbHashKey($this->_conf);
        
        return true;
    }

    static private function makeDbHashKey($cfg)
    {
        $s = implode(',', $cfg['s']);
        unset($cfg['s']);
        $s .= implode(',', $cfg);
        // s($s);s(md5($s),1);
        Return md5($s);
    }

    /**
     * 返回服务器的默认数据库配置
     *
     * @return array 配置数组
     */
    static public function getDefaultConfig($db = '', $charset = '')
    {
        if (__APP__ === 'frontend@v3') {
            $master = $_SERVER[$db]['master'];
            $slaver = $_SERVER[$db]['slave'];
        } else {
            
            $cfg = self::getIniDbConfig($db);
            $master = $cfg['WRITE']['1'];
            $slaver = $cfg['READ'];
        }
        $slaver = $slaver[array_rand($slaver)]; // 从库随即获取
                                                // s($master);s($slaver,1);
        
        $conf = array(
            array(
                'c' => $charset,
                'h' => $master['HOST'],
                'P' => $master['PORT'],
                'd' => $master['NAME'],
                'u' => $master['USER'],
                'p' => $master['PASS'],
                's' => array(
                    'c' => $charset,
                    'h' => $slaver['HOST'],
                    'P' => $slaver['PORT'],
                    'd' => $slaver['NAME'],
                    'u' => $slaver['USER'],
                    'p' => $slaver['PASS']
                )
            )
        );
        
        return $conf;
    }

    /**
     *
     *
     *
     * 根据库名读取连接文件配置信息
     *
     * @param string $db            
     * @return array $setting , 数据库ini连接配置参数信息
     */
    static private function getIniDbConfig($db)
    {
        $iniFile = __ROOT_ETC__ . "/database.{$db}.ini";
        
        if (! is_readable($iniFile)) {
            throw new Exception('Could not read db config ini file \'' . $iniFile . '\'');
        }
        
        if (array_key_exists($db, self::$IniDbConfArr)) {
            return self::$IniDbConfArr[$db];
        }
        
        // initialize the db links ini file into db pool
        $db_links = parse_ini_file($iniFile, true);
        $db_write_links = array();
        
        foreach ((array) $db_links as $k => $v) {
            $key_arr = explode('-', $k); // split by '-'
            $key_grp = isset($key_arr[0]) ? strtoupper(trim($key_arr[0])) : null;
            $key_num = isset($key_arr[1]) ? intval(trim($key_arr[1])) : 1; // start from 1
            if (! $key_grp)
                continue; // group key can not be empty
            self::$IniDbConfArr[$db][$key_grp][$key_num] = $v;
        }
        
        return self::$IniDbConfArr[$db];
    }
    
    // =======start: 调试检查相关操作=============================
    
    // 发送debug sql进入队列
    static private function sendDebug($sql, $isSelect = true)
    {
        return;
        switch (__APP__) {
            case 'frontend@v1':
            case 'frontend@v3':
                // 前端 sqlfes: front exclude select
                $queueName = $isSelect ? 'sqlfront' : 'sqlfes';
                break;
            default:
                // 后台
                $queueName = 'sql';
                break;
        }
        $besmq = self::getBesMQ($queueName);
        if ($besmq) {
            $besmq->sendDebug($sql);
        }
    }

    private static $BesArr = array();
    
    // 获取BesMQ对象资源,约定交换机为【log.hush】
    static private function getBesMQ($queueName)
    {
        return false;
        
        // 检查静态资源
        if (! is_object(self::$BesArr[$queueName])) {
            $file = dirname(__FILE__) . '/../../../../Core/BesMQ.php';
            if (file_exists($file)) {
                require_once ($file);
                self::$BesArr[$queueName] = new Core_BesMQ($queueName, 'log.hush');
            }
        }
        return self::$BesArr[$queueName];
    }

    /**
     * 高级开发人员使用
     * 数据模式: select-update/delete-insert
     * s-m-m: 默认的主从模式（从库略有延时）	实现
     * m-m-m: 全部都在主库上操作(实时)		实现
     * s-ms-m: 主从手动同步模式(insert有延时) 未实现
     */
    private function setRwMode($rwmode)
    {
        $arr = array(
            's-m-m',
            'm-m-m'
        );
        if (in_array($rwmode, $arr)) {
            self::$ReadWriteMode = $rwmode;
            Return true;
        } else {
            Return false;
        }
    }

    /**
     * 选择主库：select查询压至主库
     */
    public function selectMaster()
    {
        $this->setRwMode('m-m-m');
    }

    /**
     * 选择从库：select查询压至从库
     */
    public function selectSlaver()
    {
        $this->setRwMode('s-m-m');
    }

    /**
     * 设置debug模式：显示数据库操作详细信息
     *
     * @param bool $debug            
     */
    public function setDebug($debug = true)
    {
        self::$debug = $debug;
    }

    /**
     * 错误处理：显示提示信息,并记录log日志
     *
     * @param string $msg
     *            ,提示信息
     */
    public function halt($msg)
    {
        if (self::$report) {
            $this->haltmsg($msg);
        }        
        
        $besmq = self::getBesMQ("sqlerr");
        if (is_object($besmq)) { // 错误sql等送入日志队列
            $msg = "{$this->errno}\t{$this->error}\t" . str_replace("\t", " ", $msg);
            $besmq->sendDebug($msg);
        } elseif (defined('__ROOT_LOG__')) { // 写入日志
            $file = __ROOT_LOG__ . '/mysqli2_class.log';            
            error_log(date('[Y-m-d H:i:s] ') . "\t{$_SERVER[REQUEST_URI]}\t errno:{$this->errno}\t error:{$this->error}\t{$_SERVER[QUERY_STRING]}\t msg:{$msg}\n", 3, $file);
        }
        
        throw new Exception("<b>MySQL error</b>: {$this->errno} ({$this->error}) [please check log]<br><hr/>\n");
    }

    private function haltmsg($msg)
    {
        $this->error = mysqli_error($this->currLinkRes);
        $this->errno = mysqli_errno($this->currLinkRes);
        
        // if(SYS_RELEASE!='2'){
        // echo "<b>Database error:</b> {$msg}<br>\n";
        // echo "<b>MySQL error</b>: {$this->errno} ({$this->error})<br><hr/>\n";
        // }
    }

    /**
     * 关闭、释放当前连池中的主从连接对象
     */
    final public function close()
    {
        // 如果真正进行事务处理，则不允许关闭退出
        if (self::$IsTranProcessing)
            return false;
        
        if (is_object($this->getCacheLinkRes(false))) {
            mysqli_close($this->getCacheLinkRes(false));
        }
        if (is_object($this->getCacheLinkRes(true))) {
            mysqli_close($this->getCacheLinkRes(true));
        }
        
        unset($this->_conf);
        
        self::$LinkConfArr = null;
        self::$LinkResArr = null;
        
        return true;
    }
    
    // =======end: 调试检查相关操作=============================
}

/**
 * 注册页面结束前 关闭数据库的连接
 */
function mysqli_close_all_links_before_end()
{
    // 如果未指定参数，关闭所有连接
    if (! is_array(Mysqli2::$LinkConfArr)) {
        return 0;
    }
    
    foreach (Mysqli2::$LinkResArr as $link) {
        if (is_object($link)) {
            mysqli_close($link);
        }
    }
    
    Mysqli2::$LinkConfArr = null;
    Mysqli2::$LinkResArr = null;
    
    return true;
}
register_shutdown_function('mysqli_close_all_links_before_end');

?>