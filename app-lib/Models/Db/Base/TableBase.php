<?php
require_once 'core/Mysqli2.php';

/**
 * 该类用于操作分库中的表
 * 
 * @package tc_db
 * @author
 *
 */
abstract class Db_Base_TableBase
{

    protected $_dbname;

    protected $_charset;

    protected $_name;

    protected $_primarykey = 'id';

    protected $_op;

    protected $_adapter;

    protected $_sql;

    /**
     * 抽象方法：获取库名
     * 
     * @return string $dbname
     */
    abstract protected function getDbName();

    abstract protected function update($data, $where);

    abstract protected function delete($where);

    /**
     * 构造函数：设置 _dbname 库名
     */
    function __construct()
    {
        $this->_dbname = $this->getDbName();
    }

    /**
     *
     *
     * 初始实例化数据库操作对象
     * 
     * @param string $dbname            
     * @return object $adapter ,DB操作对象
     */
    final public function getAdapter($dbname = '')
    {
        // 读取库名
        if (! $this->_adapter) {
            $dbname = $this->getDbName();
            $conf = self::getDbConfig($dbname, (isset($this->_charset) ? $this->_charset : null));
            $this->_adapter = new Mysqli2($conf);
        }
        Return $this->_adapter;
    }

    final public function quote($val)
    {
        Return $this->getAdapter()->quote($val);
    }

    final private function sqlGenerater()
    {
        if (! is_object($this->_sql)) {
            require_once 'TableSelect.php';
            $this->_sql = new TableSelect($this);
        }
        return $this->_sql;
    }

    /**
     *
     *
     * Enter description here ...
     * 
     * @param
     *            array or string $columns
     * @return the selector
     */
    final public function select($columns)
    {
        if (is_string($columns)) {
            $columns = explode(',', $columns);
        }
        return $this->sqlGenerater()->select($columns);
    }

    /**
     *
     * @param array $clause            
     * @param bool $append
     *            = false
     * @return Ambigous <string, TableSelect>
     */
    final public function whereClause($clause, $append = false)
    {
        return $this->sqlGenerater()->whereClause($clause, $append);
    }

    final public function where($condition, $value = null, $isStr = true)
    {
        return $this->sqlGenerater()->where($condition, $value, $isStr);
    }

    /**
     *
     *
     * 根据主键获取行记录
     * 
     * @param mixed $id
     *            , 主键值
     * @param string $select_fields
     *            , 字段名单
     * @param boolean $lock
     *            , 是否锁行[事务处理]
     * @return array $row , 单行数组
     */
    final public function getRow($id, $select_fields = '*', $lock = false)
    {
        return $this->scalar($select_fields, "where `" . $this->_primarykey . "`='" . $id . "'", $lock);
    }

    /**
     *
     *
     * 根据条件直接获取所有行的单列记录
     * 
     * @param string $select_fields
     *            , 字段名单
     * @param string $where_orderby_clause
     *            where条件(需要带上Where关键字)
     * @param int $currpage
     *            , 当前页码[从 1 开始]
     * @param int $page_size
     *            , 页面记录大小
     * @param boolean $lock
     *            , 是否锁行[事务处理]
     * @return mixed $val_or_row , 一维数组
     */
    final public function getCol($select_fields, $where_orderby_clause, $currpage = '', $page_size = '', $lock = false)
    {
        $result = $this->query($select_fields, $where_orderby_clause, $currpage, $page_size, $lock);
        $resultArray = $this->fetchAll($result);
        $r = array();
        foreach ($resultArray as $row) {
            array_push($r, array_shift($row));
        }
        return $r;
    }

    /**
     *
     *
     * 根据条件直接获取所有行[注意性能,推荐query手动fetch]
     * 
     * @param string $select_fields
     *            , 字段名单
     * @param string $where_orderby_clause            
     * @param int $currpage
     *            , 当前页码[从 1 开始]
     * @param int $page_size
     *            , 页面记录大小
     * @param boolean $lock
     *            , 是否锁行[事务处理]
     * @return mixed $val_or_row , 单值、数组
     */
    final public function getAll($select_fields, $where_orderby_clause, $currpage = '', $page_size = '', $lock = false)
    {
        $result = $this->query($select_fields, $where_orderby_clause, $currpage, $page_size, $lock);
        return $this->fetchAll($result);
    }

    /**
     *
     *
     * 根据$where_clause计算总行数
     * 
     * @param string $where_orderby_clause            
     * @param boolean $lock
     *            , 是否锁行[事务处理]
     * @return int $count
     */
    final public function getCount($where_orderby_clause, $lock = false)
    {
        Return $this->scalar('count(*)', $where_orderby_clause, $lock);
    }

    /**
     *
     *
     * 获取指定 字段 的信息
     * 
     * @param string $select_fields
     *            , 字段名单
     * @param string $where_orderby_clause            
     * @param boolean $lock
     *            , 是否锁行[事务处理]
     * @return mixed $val_or_row , 单值、数组
     */
    final public function scalar($select_fields, $where_orderby_clause, $lock = false)
    {
        $sql = 'select  ' . $select_fields . ' from ' . $this->_name . ' ' . $where_orderby_clause . ' limit 1 ';
        if ($lock) { // $db->isOnTran()
            $sql .= ' for update';
        }
        Return $this->getAdapter()->scalar($sql);
    }

    /**
     *
     *
     * 查询获得记录集
     * 
     * @param string $select_fields            
     * @param string $where_orderby_clause            
     * @param int $currpage
     *            , 当前页码[从 1 开始]
     * @param int $page_size
     *            , 页面记录大小
     * @param boolean $lock
     *            , 是否锁行[事务处理]
     * @return mysql_result $result , 记录集对象
     */
    final public function query($select_fields, $where_orderby_clause, $currpage = '', $page_size = '', $lock = false)
    {
        $sql = 'select  ' . $select_fields . ' from ' . $this->_name . ' ' . $where_orderby_clause;
        if (! empty($currpage) && ! empty($page_size)) {
            $sql .= ' limit ' . ($currpage - 1) * $page_size . ',' . $page_size;
        }
        if ($lock) { // $db->isOnTran()
            $sql .= ' for update';
        }
        Return $this->getAdapter()->query($sql);
    }

    /**
     * 获取query后的单行记录
     * 
     * @param mysql_result $result            
     * @return array $row
     */
    final public function fetch($result)
    {
        if (is_object($result)) {
            return $this->getAdapter()->fetch($result);
        } else {
            Return null;
        }
    }

    /**
     *
     *
     * 获取所有记录
     * 
     * @param mysql_result $result            
     * @return array $rowSet
     */
    final public function fetchAll($result_or_sql)
    {
        if (is_object($result_or_sql)) {
            return $this->getAdapter()->fetchAll($result_or_sql);
        } elseif (is_string($result_or_sql)) {
            Return $this->executeFetchAll($result_or_sql);
        } else {
            return null;
        }
    }

    /**
     * 直接执行sql获取所有记录
     * 
     * @param unknown $sql            
     * @return Ambigous <multitype:, multitype:multitype: >|NULL
     */
    private function executeFetchAll($sql)
    {
        $result = $this->getAdapter()->query($sql);
        if (is_object($result)) {
            return $this->getAdapter()->fetchAll($result);
        } else {
            Return null;
        }
    }

    /**
     * 查询数据库键名、键值两字段，返回一个构造的数组
     *
     * @param unknown_type $key_field            
     * @param unknown_type $value_field            
     * @param unknown_type $where_clause            
     * @param unknown_type $orderby_clause            
     * @param unknown_type $limit            
     * @return unknown
     */
    final public function fetchHash($key_field, $value_field, $where_clause, $orderby_clause = '', $limit = null)
    {
        $db = $this->getAdapter();
        $sql = "select $key_field as _key , $value_field as _val  from " . $this->_name . " " . $where_clause . $orderby_clause . (is_int($limit) ? " limit " . $limit : '');
        
        $rs = $db->query($sql);
        
        while ($row = $db->fetch($rs)) {
            $arr[$row['_key']] = $row['_val'];
        }
        
        return $arr;
    }

    /**
     *
     *
     * 获取query后的记录数.
     * 
     * @param mysql_result $result            
     * @return int $num
     */
    final public function numRows($result)
    {
        if (is_object($result)) {
            return $this->getAdapter()->numRows($result);
        } else {
            Return false;
        }
    }
    
    /*
     * final public function getFieldNames() { //$key='PC:'.$this->_dbname.':'.$this->_name.':'.__FUNCTION__; //$ret=mc_get($key); //if(!$ret){ $rs= $this->fetchAll('desc '.$this->_name); foreach($rs as $row){	$fld[]=$row['Field'];	} $ret=implode(',',$fld); //mc_set($key,$ret); //} return 	$ret; }
     */
    
    /**
     *
     *
     * 开始事务
     * 
     * @return bool Y/N ，是否开启成功
     */
    final public function dbBeginTransaction()
    {
        return $this->getAdapter()->begin();
    }

    /**
     *
     *
     * 提交事务
     * 
     * @return bool Y/N ，是否提交成功
     */
    final public function dbCommit()
    {
        return $this->getAdapter()->commit();
    }

    /**
     *
     *
     * 回滚事务
     * 
     * @return bool Y/N ，是否回滚成功
     */
    final public function dbRollBack()
    {
        return $this->getAdapter()->rollBack();
    }

    /**
     * 设置进入debug调试模式，通常用于开发环境
     */
    final public function debug()
    {
        $this->getAdapter()->setDebug();
        return $this;
    }

    /**
     * 关闭db
     */
    final public function close()
    {
        $this->getAdapter()->close();
    }

    /**
     *
     *
     * 读取主从数据库的配置
     * 
     * @param string $database            
     * @param string $charset            
     * @return array $setting , 数据库连接配置参数
     */
    final static public function getDbConfig($database, $charset = '')
    {
        return Mysqli2::getDefaultConfig($database, $charset);
    }

    /**
     *
     *
     * 高级开发人员使用
     * 要注意当前连接的数据库,小心使用
     * 
     * @param string $sql
     *            , 任意sql语句
     * @return mixed $ret
     */
    final public function execute($sql)
    {
        Return $this->getAdapter()->query($sql);
    }

    /**
     * 高级开发人员使用：数据读写压至主库	*
     */
    final public function selectMaster()
    {
        $this->getAdapter()->selectMaster();
    }

    /**
     * 高级开发人员使用：切换到读写主从分离模式
     */
    final public function selectSlaver()
    {
        $this->getAdapter()->selectSlaver();
    }

/**
 * HS支持见: EcEsf\include\My\TableBase.php
 */
}

?>