<?php
require_once 'TableBase.php';

/**
 * 该类用于操作分库中的表
 * 
 * @package tc_db
 * @author
 *
 */
abstract class Db_Base_Table extends Db_Base_TableBase
{

    /**
     * 统一格式化: 插入、修改数据的hash数组,可用$_op检测insert/update操作
     * 
     * @param array $para            
     * @return array $data , 格式化后的hash的数组
     */
    abstract protected function prepareData($para);

    /**
     * 准备数据的辅助函数
     */
    final static public function fill($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = $para[$key];
    }

    final static public function fill_str($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = filter_var($para[$key], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    }

    final static public function fill_int($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = $para[$key] === '' ? '' : intval($para[$key]);
    }

    final static public function fill_float($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = $para[$key] === '' ? '' : floatval($para[$key]);
    }

    final static public function fill_email($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = filter_var($para[$key], FILTER_VALIDATE_EMAIL);
    }

    final static public function fill_md5($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = md5(trim($para[$key]));
    }

    final static public function fill_date($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = self::getDate($para[$key], 'Y-m-d');
    }

    final static public function fill_datetime($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = self::getDate($para[$key]);
    }

    final static public function fill_url($para, &$data, $key)
    {
        if (isset($para[$key]))
            $data[$key] = filter_var($para[$key], FILTER_VALIDATE_URL);
    }

    final static public function getDate($datetime = null, $format = 'Y-m-d H:i:s')
    {
        if (! isset($datetime)) {
            $datetime = time();
            return date($format, $datetime);
        } elseif (is_numeric($datetime)) {
            return date($format, $datetime);
        } elseif (! empty($datetime)) {
            $datetime = strtotime($datetime);
            return date($format, $datetime);
        } else {
            Return '';
        }
    }

    /**
     * 统一的增加、修改、删除记录处理 *
     * 
     * @param string $op            
     * @param array $para            
     * @param string $where            
     * @return int $num_or_id , 执行结果
     */
    final public function sync($op, $data, $where = null)
    {
        
        // 这里的id设置与模板的hidden pk设置相关，默认为表主键名，其次检查 id
        if (isset($data[$this->_primarykey])) {
            $id = $data[$this->_primarykey];
        } else {
            $id = isset($data['id']) ? $data['id'] : - 1;
        }
        
        $where = $where ? $where : $this->_primarykey . ' = "' . $id . '"';
        
        // s($where,1);
        switch ($op) {
            case 'update':
                $rows_affected = $this->update($data, $where);
                break;
            case 'insert':
                $rows_affected = $this->insert($data);
                break;
            case 'delete':
                // $rows_affected=$this->update(array('status'=>-1), $where);
                $rows_affected = $this->delete($where);
                break;
        }
        
        Return $rows_affected;
    }

    /**
     * 增加一条记录
     *
     * @param array $data            
     * @return int $insertID , 增加后的主键id
     */
    final public function insert($data)
    {
        $this->_op = 'insert';
        $data = $this->prepareData($data);
        $id = $this->getAdapter()->insert($data, $this->_name);
        Return $id;
    }

    /**
     * Extend the insert method of Zend Db
     * We can use this method to insert multiple line's data oncely
     * 
     * @param array $cols            
     * @param array $vals            
     * @return mixed
     */
    public function insertMultiRow($cols, $vals)
    {
        // param exception
        if (! $vals || ! $cols) {
            return false;
        }
        
        // extract and quote vals names from the array keys
        $cols_num = count($cols);
        $vals_sql = array();
        foreach ($vals as $bind) {
            if (! is_array($bind) || $cols_num != count($bind)) {
                continue;
            }
            foreach ($bind as $k => $v) {
                $bind[$k] = $this->quote($v);
            }
            $vals_sql[] = '(' . implode(', ', $bind) . ')';
        }
        
        // build the statement
        $sql = "INSERT INTO " . $this->_name . ' (' . implode(', ', $cols) . ') VALUES ' . implode(', ', $vals_sql);
        
        // execute the statement and return the number of affected rows
        
        return $this->getAdapter()->query($sql);
    }

    /**
     * 修改记录
     *
     * @param array $data            
     * @param string $where            
     * @return int $affectedRows ,影响行数
     */
    final public function update($data, $where)
    {
        $this->_op = 'update';
        $data = $this->prepareData($data);
        $where = str_ireplace('where', '', $where);
        $affectedRows = $this->getAdapter()->update($data, $where, $this->_name);
        Return $affectedRows;
    }

    /**
     * 删除记录
     *
     * @param string $where            
     * @return int $affectedRows ,影响行数
     */
    final public function delete($where)
    {
        $where = str_ireplace('where', '', $where);
        $db = $this->getAdapter();
        $db->query("delete from " . $this->_name . ' where ' . $where);
        $affectedRows = $db->affectedRows();
        Return $affectedRows;
    }
}
?>