<?php

/**
 * 
 * Enter description here ...
 * @author leon
 *
 */
class TableSelect
{

    const JOIN_LEFT = 'LEFT JOIN';

    const JOIN_RIGHT = 'RIGHT JOIN';

    const JOIN_INNER = 'INNER JOIN';

    /**
     *
     * Enter description here ...
     *
     * @var the object of Com_Model_Xxx
     */
    private $tableModel = null;

    /**
     *
     * Enter description here ...
     *
     * @var string
     */
    private $mainTable = '';

    /**
     * Enter description here ...
     *
     * @var array
     */
    private $mainTableColumns = array();

    private $joinTableColumns = array();

    /**
     * Enter description here ...
     *
     * @var string
     */
    private $join = array();

    private $where = null;

    private $order = null;

    private $group = null;

    private $limit = null;

    /**
     * Enter description here .
     *
     *
     * ..
     */
    public function __construct($tableModel)
    {
        $this->tableModel = $tableModel;
        $this->mainTable = $this->tableModel->_name;
    }

    /**
     *
     *
     * Enter description here ...
     *
     * @param array $columns            
     * @return the object of Com_Psp_Lister
     */
    public function select($columns)
    {
        return $this->_columns($columns, $this->mainTable);
    }

    /**
     *
     *
     * Enter description here ...
     *
     * @param array $columns            
     * @param string $prefix            
     * @return the object of Com_Psp_Lister
     */
    private function _columns($columns, $prefix)
    {
        if (is_array($columns)) {
            foreach ($columns as $column) {
                if ($prefix === $this->mainTable) {
                    // array_push($this->mainTableColumns, '`' . $prefix . '`.' . $column);
                    array_push($this->mainTableColumns, $column);
                } else {
                    array_push($this->joinTableColumns, '`' . $prefix . '`.' . $column);
                }
            }
        }
        return $this;
    }

    /**
     * Enter description here .
     * ..
     *
     * @param string $condition            
     * @param mixed $value            
     * @param boolean $isStr            
     * @return the object of Com_Psp_Lister
     */
    public function where($condition, $value = null, $isStr = true)
    {
        if (! isset($value)) {
            $this->where .= ' AND ' . $condition;
        } else {
            // array(a,b) => "a,b"
            if (is_array($value)) {
                if ($isStr) {
                    $value = '"' . implode('","', $value) . '"';
                } else {
                    $value = implode('","', $value);
                }
            } else {
                if ($isStr) {
                    $value = '"' . $value . '"';
                }
            }
            
            $this->where .= ' AND ' . str_replace('?', $value, $condition);
        }
        
        return $this;
    }

    /**
     * array(
     * 'id' => array(1,2,3),
     * 'year' => '2012'
     * )
     * output: WHERE `id` IN ('1','2','3') AND `year`='2012'
     *
     * @param array $clause            
     * @return string
     */
    public function whereClause($clause, $append = false)
    {
        if (! is_array($clause) || empty($clause)) {
            return '';
        }
        $r = '';
        $map = array(
            'e' => '=',
            'lt' => '<',
            'lte' => '<=',
            'gt' => '>',
            'gte' => '>=',
            'ne' => '!=',
            'like' => ' LIKE '
        );
        foreach ($map as $str => $math) {
            if (! isset($clause[$str]) || empty($clause[$str])) {
                continue;
            }
            foreach ($clause[$str] as $key => $value) {
                if (is_array($value)) {
                    $tmp = array();
                    foreach ($value as $one) {
                        $tmp[] = $this->tableModel->quote($one);
                    }
                    $part_1 = $key . ' IN (' . implode(',', $tmp) . ') ';
                } else {
                    $part_1 = $key . $math . $this->tableModel->quote($value);
                }
                if (! empty($r)) {
                    $r .= ' AND ';
                }
                $r .= $part_1;
            }
        }
        if (! empty($r)) {
            if ($append) {
                $this->where = $this->where . ' AND ' . $r;
            } else {
                $this->where = ' AND ' . $r;
            }
        }
        
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param string $order            
     * @return the object of Com_Psp_Lister
     */
    public function order($order)
    {
        if (is_array($order)) {
            $this->order = implode(",", $order);
        } else {
            $this->order = $order;
        }
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param string $group            
     * @return the object of Com_Psp_Lister
     */
    public function group($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     *
     * @param number $currpage
     *            , start from 1
     * @param number $pagesize            
     * @return TableSelect
     */
    public function page($currpage = 1, $pagesize = 20)
    {
        $this->limit(($currpage - 1) * $pagesize, $pagesize);
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param int $offset            
     * @param int $count            
     * @return the object of Com_Psp_Lister
     */
    public function limit($offset = 0, $count = 0)
    {
        $offset = intval($offset);
        $count = intval($count);
        if ($offset > 0 && $count === 0)
            $this->limit = $offset;
        elseif ($offset > 0 && $count > 0)
            $this->limit = $offset . ',' . $count;
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param string $condtions            
     * @param mixed $value            
     * @param boolean $isStr            
     * @return the object of Com_Psp_Lister
     */
    public function orWhere($condition, $value, $isStr = true)
    {
        if (is_array($value)) {
            if ($isStr)
                $value = '"' . implode('","', $value) . '"';
            else
                $value = implode('","', $value);
        } else {
            if ($isStr)
                $value = '"' . $value . '"';
        }
        
        $this->where .= ' OR ' . str_replace('?', $value, $condition);
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param array $alias2table            
     * @param string $condition            
     * @param array $columns            
     */
    private function join($alias2table, $condition, $columns, $type)
    {
        if (is_array($alias2table)) {
            $alias = key($alias2table);
            $tableName = $alias2table[$alias];
            if ($alias) {
                // $alias = $alias;
            } else {
                $alias = $tableName;
            }
        } else {
            $alias = $alias2table;
            $tableName = $alias2table;
        }
        
        $this->_columns($columns, $alias);
        
        array_push($this->join, $type . ' `' . $tableName . '` AS `' . $alias . '` ON ' . $condition);
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param array $alias2table            
     * @param string $condition            
     * @param array $columns            
     * @return the object of Com_Psp_Lister
     */
    public function joinLeft($alias2table, $condition, $columns = array())
    {
        $this->join($alias2table, $condition, $columns, self::JOIN_LEFT);
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param array $alias2table            
     * @param string $where            
     * @param array $columns            
     * @return the object of Com_Psp_Lister
     */
    public function joinRight($alias2table, $condition, $columns = array())
    {
        $this->join($alias2table, $condition, $columns, self::JOIN_RIGHT);
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @param array $alias2table            
     * @param string $where            
     * @param array $columns            
     * @return the object of Com_Psp_Lister
     */
    public function joinInner($alias2table, $condition, $columns = array())
    {
        $this->join($alias2table, $condition, $columns, self::JOIN_INNER);
        return $this;
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @return int
     */
    public function count()
    {
        $sql = 'SELECT COUNT(*)' . ' FROM ' . $this->mainTable;
        if (sizeof($this->join) > 0)
            $sql .= ' ' . implode(',', $this->join);
        if (is_null($this->where) === false)
            $sql .= ' WHERE 1 ' . $this->where;
        if (is_null($this->group) === false)
            $sql .= ' GROUP BY ' . $this->group;
        $result = $this->tableModel->fetch($this->tableModel->execute($sql));
        return $result['COUNT(*)'];
    }

    /**
     *
     *
     *
     *
     *
     * Enter description here ...
     *
     * @return string
     */
    private function generate()
    {
        if (empty($this->mainTableColumns)) {
            $this->_columns(array(
                '*'
            ), $this->mainTable);
        }
        
        $sql = 'SELECT ' . implode(',', array_merge($this->mainTableColumns, $this->joinTableColumns)) . ' FROM ' . $this->mainTable;
        if (sizeof($this->join) > 0)
            $sql .= ' ' . implode(' ', $this->join);
        if (is_null($this->where) === false)
            $sql .= ' WHERE 1 ' . $this->where;
        if (is_null($this->group) === false)
            $sql .= ' GROUP BY ' . $this->group;
        if (is_null($this->order) === false)
            $sql .= ' ORDER BY ' . $this->order;
        if (is_null($this->limit) === false)
            $sql .= ' LIMIT ' . $this->limit;
            // echo $sql;
        return $sql;
    }

    /**
     *
     *
     *
     *
     * Enter description here ...
     *
     * @return string
     */
    public function sql()
    {
        return $this->generate();
    }

    /**
     *
     *
     *
     * Enter description here ...
     *
     * @return array list
     */
    public function fetchAll()
    {
        return $this->tableModel->fetchAll($this->tableModel->execute($this->generate()));
    }

    public function scalar()
    {
        return $this->tableModel->getAdapter()->scalar($this->generate());
    }

    public function update($data)
    {
        if (is_null($this->where) === false) {
            return $this->tableModel->update($data, 'WHERE 1 ' . $this->where);
        } else {
            return false;
        }
    }

    public function delete()
    {
        if (is_null($this->where) === false) {
            return $this->tableModel->delete('WHERE 1 ' . $this->where);
        } else {
            return false;
        }
    }
}