<?php
//设置a
ini_set('display_errors','On');	error_reporting(E_ALL ^ E_NOTICE | E_PARSE);
define("__ROOT_DIR__",dirname(dirname(dirname(__FILE__))));
define("__ETC", realpath(__ROOT_DIR__."/../../app-env/etc"));
define('__ROOT_ETC__',__ETC);
//var_dump(__ROOT_DIR__ ,dirname(dirname(__FILE__)), __ETC);//exit;

//初始化
set_include_path( 	__ROOT_DIR__. PATH_SEPARATOR	. get_include_path() );
function __autoload($class){   include str_replace('_','/',$class).".php"; } 
if(!file_exists('models'))mkdir('models');


//选择数据库生成库表model文件
class MyModel extends Db_Admin {function prepareData($para){} }


//获取所有表名
$tab =new MyModel();
//$tab->debug();
$rs= $tab->execute("show tables like '%'");
$tabArr = $tab->fetchAll($rs);
//print_r($tables);
$i=0;
foreach($tabArr as $tabname){
	//if($i==0){var_dump($tabname);}
	$tabname= array_pop($tabname);//$tabname['Tables_in_travel'];  //t_order
	$rs= $tab->execute("desc $tabname");
	
	$dbname =$tab->getDbName();	
	$dbclass = ucfirst($dbname);
	$classname =str_replace(" ","",ucwords(str_replace("_"," ",$tabname)));


	$code  = "<?php\n";
	$code .="class {$classname} extends Db_{$dbclass} {\n\n\t";
	$code .='public $_name = '."'{$tabname}';";


	$colms = $tab->fetchAll($rs);
	//print_r($colms);

	$str_prepareData=get_str_prepareData($colms,$dbname,$tabname);
	

	$code .=$str_prepareData;

	$code .="\n\n} \n?>";
	//echo $code ;

	$code = iconv('gbk',"utf-8", $code);

	file_put_contents("Models/{$classname}.php",$code);
	$i++;

	echo "\n $i\t done: Models/{$classname}.php" ;
	//break;


}
echo "\n finish ! ";




//=======页面函数==============================

function get_str_prepareData($colms,$dbname,$tabname){
	$str="\n\t".'function prepareData($para) {';
	$str.="\n\t\t".'$data=array();';

	$primaryField="";

	foreach($colms as $col){
		$type=$col['Type'];
		$field=$col['Field'];
		$pkkey=$col['Key'];
		if($pkkey=="PRI"){
			$primaryField=$field;
		}

		//int
		if(strpos($type,"int")!==false){
			$fillname = "fill_int";
		}
		//float
		if(strpos($type,"float")!==false){
			$fillname = "fill_float";
		}
		//char
		if(strpos($type,"char")!==false){
			$fillname = "fill_str";
		}
		//varchar
		if(strpos($type,"varchar")!==false){
			$fillname = "fill_str";
		}
		//text
		if(strpos($type,"text")!==false){
			$fillname = "fill";
		}

		//date
		if($type=="date"){
			$fillname = "fill_date";
		}
		//datetime
		if($type=="datetime"){
			$fillname = "fill_datetime";
		}
		//===============
		//email
		if(strpos($field,"email")!==false){
			$fillname = "fill_email";
		}
		//url
		if(strpos($field,"url")!==false){
			$fillname = "fill_url";
		}

		$str_commet = get_field_commet($dbname,$tabname,$field);

		//var_dump($str_commet);		exit;
		
		$str_field="self::".$fillname.'($para,$data,'."'$field');";
		$str_field= str_pad($str_field,60).'//'.$type."\t\t".$str_commet;


		$str.="\n\t\t";
		$str.=$str_field;
		

	}
	$str.="\n\t\t";
	$str.='return $data;';

	$str.="\n\t".'}';


	$str_pri= "\n\t".'public $_primarykey = '."'{$primaryField}';\n";

	return $str_pri . $str;

}


function get_field_commet($dbname,$tabname,$field){
	global $tab;

	$sql="SELECT COLUMN_COMMENT FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$tabname' AND table_schema = '$dbname' AND column_name LIKE '$field'";
	echo $sql;  
	$row = $tab->fetch($tab->execute($sql));
	//var_dump( $row);  
	//return  $row['COLUMN_COMMENT'];
	return iconv("utf-8",'gbk', $row['COLUMN_COMMENT']);
}


?>