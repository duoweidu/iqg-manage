<?php
class MurcielagoGoods extends Db_Iqg {

	public $_name = 'murcielago_goods';
	public $_primarykey = 'goodsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'deleted_warehouseId');          //int(10) unsigned		
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		self::fill_int($para,$data,'catId');                        //int(11)		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		self::fill_date($para,$data,'beginDate');                   //date		
		self::fill_date($para,$data,'beginTime');                   //time		
		self::fill_date($para,$data,'endDate');                     //date		
		self::fill_date($para,$data,'endTime');                     //time		
		self::fill_date($para,$data,'shopLat');                     //double(10,6)		
		self::fill_date($para,$data,'shopLng');                     //double(10,6)		
		self::fill_str($para,$data,'goodsName');                    //varchar(255)		
		self::fill_float($para,$data,'goodsPrice');                 //float(9,2)		
		self::fill_float($para,$data,'currentPrice');               //float(9,2)		
		self::fill_float($para,$data,'marketPrice');                //float(9,2)		
		self::fill_float($para,$data,'minPrice');                   //float(9,2) unsigned		
		self::fill_float($para,$data,'buyPointPrice');              //float(7,2)		
		self::fill_float($para,$data,'bargainRange');               //float(9,2) unsigned		
		self::fill_float($para,$data,'costPrice');                  //float(9,2)		
		self::fill_int($para,$data,'isNew');                        //tinyint(1)		
		self::fill_str($para,$data,'thumbUrl');                     //varchar(255)		
		self::fill_str($para,$data,'imgUrl');                       //varchar(255)		
		self::fill_str($para,$data,'imgId');                        //char(32)		
		self::fill($para,$data,'goodsBriefDesc');                   //text		
		self::fill($para,$data,'goodsDesc');                        //mediumtext		
		self::fill_int($para,$data,'goodsStocks');                  //smallint(6)		
		self::fill_int($para,$data,'goodsNumber');                  //int(11)		
		self::fill_int($para,$data,'favorites');                    //int(11)		
		self::fill_str($para,$data,'shopName');                     //varchar(100)		
		self::fill_str($para,$data,'shopTitle');                    //varchar(100)		
		self::fill_str($para,$data,'shopTel');                      //varchar(100)		
		self::fill_str($para,$data,'shopOpeningHours');             //varchar(100)		
		self::fill_int($para,$data,'expiredDays');                  //tinyint(4)		
		self::fill_str($para,$data,'shopLogo');                     //varchar(200)		
		self::fill_str($para,$data,'shopAddress');                  //varchar(200)		
		self::fill($para,$data,'goodsOptions');                     //text		
		self::fill($para,$data,'promotions');                       //text		
		self::fill_str($para,$data,'promotionPoster');              //varchar(200)		
		self::fill($para,$data,'linePromotion');                    //text		
		self::fill_int($para,$data,'goodsStatus');                  //tinyint(1)		
		self::fill($para,$data,'tags');                             //text		
		self::fill_int($para,$data,'sequence');                     //int(11)		
		self::fill_int($para,$data,'enable');                       //tinyint(1)		
		self::fill_int($para,$data,'countBargainAll');              //int(10) unsigned		
		self::fill_int($para,$data,'countBargainToday');            //int(10) unsigned		
		self::fill_int($para,$data,'countClickAll');                //int(10) unsigned		
		self::fill_int($para,$data,'countClickToday');              //int(10) unsigned		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'updateTime');                   //int(11)		
		self::fill_str($para,$data,'adTitle');                      //varchar(50)		
		self::fill_str($para,$data,'adUri');                        //varchar(1000)		
		self::fill_str($para,$data,'adUriTarget');                  //varchar(10)		
		self::fill_int($para,$data,'isDeleted');                    //tinyint(1) unsigned		
		self::fill_int($para,$data,'type');                         //tinyint(3) unsigned		
		self::fill_int($para,$data,'isMultiShop');                  //tinyint(1) unsigned		
		self::fill_int($para,$data,'telecomId');                    //int(10) unsigned		
		self::fill($para,$data,'yimaConfig');                       //text		
		self::fill_int($para,$data,'isNeedBooking');                //tinyint(1) unsigned		
		self::fill_int($para,$data,'isNotAllowedTakeOut');          //tinyint(1) unsigned		
		return $data;
	}

} 
?>