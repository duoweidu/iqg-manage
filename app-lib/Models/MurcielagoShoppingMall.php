<?php
class MurcielagoShoppingMall extends Db_Iqg {

	public $_name = 'murcielago_shopping_mall';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'address');                      //varchar(255)		
		self::fill_str($para,$data,'lat');                          //double(10,6) unsigned		
		self::fill_str($para,$data,'lng');                          //double(10,6) unsigned		
		self::fill_int($para,$data,'addTime');                      //int(10) unsigned		
		self::fill_int($para,$data,'sortOrder');                    //tinyint(3) unsigned		
		self::fill_int($para,$data,'isHidden');                     //tinyint(1) unsigned		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		return $data;
	}

} 
?>