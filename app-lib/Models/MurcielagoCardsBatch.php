<?php
class MurcielagoCardsBatch extends Db_Iqg {

	public $_name = 'murcielago_cards_batch';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'name');                         //varchar(100)		
		return $data;
	}

} 
?>