<?php
class LtChecking extends Db_Admin {

	public $_name = 'lt_checking';
	public $_primarykey = 'lc_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'lc_id');                        //int(11)		
		self::fill_int($para,$data,'lc_lpid');                      //int(11)	
		self::fill_int($para,$data,'lc_prize');                      //tinyint(4)
		self::fill_str($para,$data,'lc_openid');                    //varchar(50)		
		self::fill_str($para,$data,'lc_mobile');                    //int(20)		
		self::fill_int($para,$data,'lc_regist');                    //tinyint(4)		
		self::fill_str($para,$data,'lc_tmp');                       //varchar(20)		
		self::fill_int($para,$data,'lc_ctime');                     //int(11)		
		self::fill_int($para,$data,'lc_utime');                     //int(11)		
		self::fill_int($para,$data,'lc_status');                    //tinyint(4)		
		return $data;
	}

} 
?>