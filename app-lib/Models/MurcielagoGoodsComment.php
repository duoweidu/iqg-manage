<?php
class MurcielagoGoodsComment extends Db_Iqg {

	public $_name = 'murcielago_goods_comment';
	public $_primarykey = 'goodscommentId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'goodscommentId');               //int(11)		
		self::fill_str($para,$data,'orderSn');                      //varchar(20)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill($para,$data,'content');                          //text		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'orderId');                      //int(10) unsigned		
		self::fill_int($para,$data,'helpfulCount');                 //int(10) unsigned		
		self::fill_int($para,$data,'isApproved');                   //tinyint(1) unsigned		
		self::fill_int($para,$data,'itemRating');                   //tinyint(1) unsigned		
		self::fill_int($para,$data,'attitudeRating');               //tinyint(1) unsigned		
		return $data;
	}

} 
?>