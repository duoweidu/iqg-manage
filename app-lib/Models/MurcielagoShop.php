<?php
class MurcielagoShop extends Db_Iqg {

	public $_name = 'murcielago_shop';
	public $_primarykey = 'shopId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'shopId');                       //int(11)		
		self::fill_int($para,$data,'cityId');                       //smallint(4) unsigned		
		self::fill_int($para,$data,'parentId');                     //int(11)		
		self::fill_int($para,$data,'buyCycle');                     //int(10) unsigned		
		self::fill_str($para,$data,'shopName');                     //varchar(100)		
		self::fill_str($para,$data,'shopTitle');                    //varchar(100)		
		self::fill($para,$data,'shopDesc');                         //text		
		self::fill_str($para,$data,'shopAddress');                  //varchar(255)		
		self::fill_str($para,$data,'shopTel');                      //varchar(100)		
		self::fill_str($para,$data,'shopLat');                      //double(10,6)		
		self::fill_str($para,$data,'shopLng');                      //double(10,6)		
		self::fill_str($para,$data,'shopLogo');                     //varchar(200)		
		self::fill_str($para,$data,'logoId');                       //char(32)		
		self::fill_int($para,$data,'isHidden');                     //tinyint(1)		
		self::fill_int($para,$data,'ecodeMakeType');                //tinyint(1) unsigned		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill($para,$data,'yimaConfig');                       //text		
		self::fill_int($para,$data,'isUseYima');                    //tinyint(1) unsigned		
		self::fill_int($para,$data,'ecodeValidateType');            //tinyint(1) unsigned		
		self::fill_str($para,$data,'ecodeValidateTypeCheckbox');    //varchar(20)		
		self::fill_str($para,$data,'businessHoursOpen');            //varchar(5)		
		self::fill_str($para,$data,'businessHoursClose');           //varchar(5)		
		self::fill_int($para,$data,'isDeleted');                    //tinyint(1) unsigned		
		return $data;
	}

} 
?>