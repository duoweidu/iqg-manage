<?php
class MurcielagoNotice extends Db_Iqg {

	public $_name = 'murcielago_notice';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(11) unsigned		
		self::fill_int($para,$data,'uid');                          //int(10) unsigned		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		self::fill_int($para,$data,'startTime');                    //int(10) unsigned		
		self::fill_int($para,$data,'expireTime');                   //int(11)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_str($para,$data,'title');                        //varchar(50)		
		self::fill_str($para,$data,'content');                      //varchar(200)		
		return $data;
	}

} 
?>