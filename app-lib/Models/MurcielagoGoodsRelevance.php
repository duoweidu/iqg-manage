<?php
class MurcielagoGoodsRelevance extends Db_Iqg {

	public $_name = 'murcielago_goods_relevance';
	public $_primarykey = 'relevanceGoodsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'goodsId');                      //int(10) unsigned		
		self::fill_int($para,$data,'relevanceGoodsId');             //int(10) unsigned		
		return $data;
	}

} 
?>