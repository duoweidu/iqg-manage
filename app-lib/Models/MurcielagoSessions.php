<?php
class MurcielagoSessions extends Db_Iqg {

	public $_name = 'murcielago_sessions';
	public $_primarykey = 'session_id';

	function prepareData($para) {
		$data=array();
		self::fill_str($para,$data,'session_id');                   //varchar(40)		
		self::fill_str($para,$data,'ip_address');                   //varchar(16)		
		self::fill_str($para,$data,'user_agent');                   //varchar(120)		
		self::fill_int($para,$data,'last_activity');                //int(10) unsigned		
		self::fill($para,$data,'user_data');                        //text		
		return $data;
	}

} 
?>