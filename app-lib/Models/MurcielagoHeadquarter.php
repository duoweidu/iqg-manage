<?php
class MurcielagoHeadquarter extends Db_Iqg {

	public $_name = 'murcielago_headquarter';
	public $_primarykey = 'headquarterId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'headquarterId');                //int(11)		
		self::fill_str($para,$data,'headquarterName');              //varchar(100)		
		self::fill_str($para,$data,'headquarterTitle');             //varchar(100)		
		self::fill_str($para,$data,'headquarterLogo');              //varchar(200)		
		self::fill_str($para,$data,'logoId');                       //char(32)		
		self::fill_str($para,$data,'headquarterTel');               //varchar(50)		
		self::fill_str($para,$data,'headquarterAddress');           //varchar(200)		
		self::fill_str($para,$data,'headquarterLat');               //double(10,6)		
		self::fill_str($para,$data,'headquarterLng');               //double(10,6)		
		self::fill_str($para,$data,'headquarterURL');               //varchar(255)		
		self::fill($para,$data,'headquarterDESC');                  //text		
		self::fill_int($para,$data,'isBrander');                    //tinyint(1)		
		self::fill_int($para,$data,'enable');                       //tinyint(1)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_str($para,$data,'usernamePrefix');               //varchar(50)		
		self::fill_int($para,$data,'isDeleted');                    //tinyint(1) unsigned		
		self::fill_str($para,$data,'ecodeValidatePassword');        //char(4)		
		return $data;
	}

} 
?>