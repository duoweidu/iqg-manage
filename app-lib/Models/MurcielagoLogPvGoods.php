<?php
class MurcielagoLogPvGoods extends Db_Iqg {

	public $_name = 'murcielago_log_pv_goods';
	public $_primarykey = 'logPVGoodsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logPVGoodsId');                 //int(11)		
		self::fill_int($para,$data,'shopId');                       //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_int($para,$data,'logData');                      //blob		
		self::fill_int($para,$data,'lat');                          //double(10,6)		
		self::fill_int($para,$data,'lng');                          //double(10,6)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>