<?php
class MurcielagoNoticeStatus extends Db_Iqg {

	public $_name = 'murcielago_notice_status';
	public $_primarykey = 'uid';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'noticeId');                     //int(10) unsigned		
		self::fill_int($para,$data,'uid');                          //int(10) unsigned		
		self::fill_int($para,$data,'isRead');                       //tinyint(1) unsigned		
		return $data;
	}

} 
?>