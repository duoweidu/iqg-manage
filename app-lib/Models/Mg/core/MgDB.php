<?php

/*
 * 错误日志文件路径  
 */
define('MONGODB_LOG_PATH', __ETC . '/../dat/log/MgDB.log');

/**
 * 前提已初始化db，查询帮助 http://www.mongodb.org/display/DOCS/Querying
 * mongoDB对dever过于灵活，对高级特性做了一定限制
 * @author 
 *
 */
abstract class MgDB {
	
	//引入辅助过滤函数
	//use Trait_Filter;

	protected  $_name;				//*必填：表名	
	protected  $_hasExtra=false;		//选填：增加cdate/udate字段信息
	protected  $_hasSeqID=false;		//选填：是否使用int类型自增seqid？ 否:则为mongoid

	private    $_connected=false;		//

	private    $_op;			//操作类型:insert/update
	private    $_dbname;		//getDbName获得
	
	//static private $Mongo; //Mongo		
	private	   $_mongo; //Mongo	
	private    $_col;	//MongoCollection	
	
	abstract protected function getDbName();
	abstract protected function prepareData($para);
	
	/**
	 * 
	 * 对象初始化
	 * @throws Exception
	 */
	public function __construct(){		
		//成功连接时
		if($this->connect()){
			$this->_dbname	= $this->getDbName();			
			$this->_col		= $this->_mongo->selectDB($this->_dbname)->selectCollection($this->_name);	//当前col			
		}else{
			//throw new Exception('mongo connect fail');
		}
	}

	public function __destruct(){
	   $this->close();
	}
	
	/**
	 * 分页查询
	 * $strFields不能有空格, return  MongoCursor;	
	 * @param string $strFields
	 * @param array $where
	 * @param array $orderby
	 * @param int $pagesize
	 * @param int $currpage
	 */
	final public function query($strFields,array $where=array(), array $orderby=array(),$pagesize=0,$currpage=0) {
		return $this->find( $where, self::convertFields($strFields),$orderby,$pagesize,$currpage);
	}
	/**
	 * mongo分页查询
	 * where like: array('f' => new MongoRegex("/.*bar.* /i")); 
	 * orderby：值为int 1=asc ,-1|0=desc
	 * @param array $where
	 * @param array $fields
	 * @param array $orderby
	 * @param unknown_type $pagesize
	 * @param unknown_type $currpage
	 */
	final public function find( array $where=array(),array $fields=array() ,  array $orderby=array(), $pagesize=0,$currpage=0 ) {
		try{			
			if(!$this->_connected) return false;

			$cursor = $this->_col->find($where,$fields);
			//1.排序			
			$cursor = $cursor->sort($orderby);	//array("a" => 1)
			//2.分页
			if($currpage)	$cursor = $cursor->skip(($currpage-1)*$pagesize);
			if($pagesize)	$cursor = $cursor->limit($pagesize);
			Return $cursor;

		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}

	}
	/**
	 * 取单行或单值
	 * 注意$strFields不能有\t \n \r等空白字符
	 * @param unknown_type $strFields
	 * @param array $where
	 */
	final public function scalar( $strFields, array $where=array()) { 
		return $this->findOne($where , self::convertFields($strFields));
	}
	
	/**
	 * mongo取单行或单值 
	 * @param array $where
	 * @param array $fields
	 */
	final public function findOne(array $where=array(),array $fields=array()) {
		try{		
			if(!$this->_connected) return false;

			$one = $this->_col->findOne($where,$fields);
			if(count($fields)===1){
				Return $one[array_pop($fields)];
			}else{
				if($fields && !in_array('_id',$fields)){
					unset($one['_id']);
				}
				Return $one;
			}
		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}
	}
	/**
	 * 查询匹配行总数
	 * @param array $where
	 */
	final public function getCount(array $where=array()) {	
		try{	
			if(!$this->_connected) return false;

			Return $this->_col->find($where)->count();
		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}
	}
	
	/**	 
	 * 获取distinct字段值
	 * @param string $fieldname
	 * @param array $where
	 */
	final public function distinct($fieldname, array $where ){
		if(!$this->_connected) return false;

		$command=array();
		$command['distinct']=$this->_name;
		$command['key']=$fieldname;
		$command['query']=$where;	
		return $this->_col->db->command($command);
	}

	/**
	 * 获取游标中下一行记录.
	 * @param unknown_type $cursor
	 */
	final public function  fetch($cursor,$timeoutMs=100){
		try{
			if(!$this->_connected) return false;

			if(is_object($cursor) && $cursor->hasNext()){					
				$cursor->timeout(100);
				return $cursor->getNext();
			
			}else{
				Return null;
			}
		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}
	}
	
	/**
	 * 数据更新操作统一接口
	 * Enter description here ...
	 * @param unknown_type $op
	 * @param unknown_type $data
	 * @param unknown_type $where
	 */
	final public function sync($op,array $data,array $where=array()) {
		try{			
			if( ($op=='update' || $op=='delete') && !$where){
				$id = isset($data['_id']) ? $data['_id'] : -1;
				$id = $this->_hasSeqID ? $id : new MongoId($id);
				$where = array( '_id' => $id );
			}
			switch($op){
				case 'update':					
					$ret=$this->update($data, $where);
					break;
				case 'insert':
					$ret=$this->insert($data);
					break;
				case 'delete':					
					$ret=$this->delete($where);
					break;
			}
			Return $ret;
		} catch(Exception $e) {
			$numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}

	}
	/**
	 * 增加记录 ,可选opt：safe|fsync （fsync数据安全、性能差）
	 * @param array $data
	 * @param array $opt
	 */
	final public function insert(array $data, array $opt=array("safe"=>true)) {
		try{
			if(!$this->_connected) return false;

			$this->_op='insert';
			$data=$this->prepareData($data);			
			if($this->_hasExtra){			
				$data['cdate']	=	time();	//new MongoDate()
				$data['udate']	=	$data['cdate'];
			}
			if($this->_hasSeqID){
				$data['_id']	=	$this->getSeqID($this->_name);	
			}
			$ret = $this->_col->insert($data, $opt);
			
			if($ret['ok']){
				return $this->_hasSeqID ?  $data['_id']: $data['_id']->__toString();
			}else{
				return null;
			}
		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}
	}
	/**
	 * 【推荐使用】根据ID更新一条记录
	 * @param unknown_type $data
	 * @param unknown_type $id
	 */
	final public function updateByID(array $data,$id) { 		
		$id = $this->_hasSeqID ? $id : new MongoId($id);		
		Return $this->updateOne($data,array('_id'=>$id));
	}
	/**
	 * 更新一条(不支持shared)
	 * @param unknown_type $data
	 * @param unknown_type $where
	 */
	final public function updateOne(array $data,array $where) { 
		Return $this->update($data,$where,false);	
	}
	
	/**
	 * 根据where更新多条记录
	 * @param unknown_type $data
	 * @param unknown_type $where
	 * @param unknown_type $multiple
	 */
	final public function update(array $data,array $where,$multiple=true,$timeoutMS=1000) { 
		if(!$this->_connected) return false;

		$this->_op='update';
		unset($data['_id']); 
					
		$data=$this->prepareData($data);
		if($this->_hasExtra){			
			$data['udate']	=	time();	//new MongoDate()
		}
		$ret =  $this->doUpdate(array('$set'=>$data),$where,array("multiple"=>$multiple, "safe"=>true ,'timeout'=>$timeoutMS) ); 	
		if(isset($ret['ok'])){
			Return $ret['ok'] ? $ret['n'] : 0; //正常时返回影响行数
		}else{
			Return false;	//异常时返回false
		}
	}
	
	/*
	//【不推荐用】增加或修改: 返回结果有两种 ，SeqID存在问题
	final public function upsert(array $data,array $where=array() ) { 		
		$data=$this->prepareData($data);
		return $this->doUpdate($data,$where,array("upsert"=>true, "safe"=>true) ); 
	}*/
	
	/**
	 * 【慎用】mongo更新操作
	 * 注意更新操作符: $inc、$set、$unset、$push、$pushAll、$addToSet、$pop、$pull、$pullAll、$（自己）	 
	 * @param array $data
	 * @param array $where
	 * @param array $options
	 */
	final public function doUpdate(array $data,array $where=array(),array $options=array("multiple"=>false,"safe"=>true,'timeout'=>1000) ) {	
		try{			
			if(!$this->_connected) return false;

			Return $this->_col->update($where,$data,$options);
		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}
	}	
	/**
	 * 【推荐使用】根据id删除记录	
	 * @param unknown_type $id
	 */
	final public function deleteByID($id) {	
		$id = $this->_hasSeqID ? $id : new MongoId($id);	
		Return $this->delete(array('_id' =>$id));
	}
	/**
	 * 【慎用】mongo删除记录,可选opt： justOne|safe|fsync
	 * @param array $where
	 * @param array $opt
	 */
	final public function delete(array $where ,array $opt=array("justOne" => true) ) {	
		try{			
			if(!$this->_connected) return false;

			$ret = $this->_col->remove($where, $opt);	
			Return $ret;
		} catch(Exception $e) {
			 $numargs = func_get_args();
			 self::printErr($e,$numargs);
			 return false;
		}
	}
	/**
	 * 转换为mongo字段选择数组类型
	 * @param unknown_type $strFileds
	 */
	final public static function convertFields($strFileds){		
		if($strFileds=='*'){
			return array();
		}else{
			return explode(',',str_replace(' ','',$strFileds));

		}
	}
	/**
	 * 模糊查询正则表达式 : "/.*中文.* /i"
	 * @param unknown_type $exp
	 */
	final public static function getRegex($exp){
		return new MongoRegex($exp);		
	}	
	
	/**
	 * $_hasSeqID=true时从当前db中的auto_increment_id集合获取相应col自增id
	 * （auto_increment_id会自动创建）
	 * @param unknown_type $seqname
	 * @param array $option
	 */
	final public function getSeqID($seqname, array $option = array()){  	   
	   $option += array( 'init' => 1, 'step' => 1); 	 
	   $command = array(  
		   'findandmodify' => 'auto_increment_id',	//集合表
		   'query'  => array('_id'=>$seqname),							//field _id 序列名，同时作为唯一id
		   'update' => array('$inc'=>array('id'=>$option['step'])),		//field id  自增id值		  
		   'new' => true, 
		   'upsert' => false	//采用手动检查
		   ); 
	   $row = $this->_col->db->command($command); 
	   if(isset($row['value']['id'])){
			return $row['value']['id']; 
	   }else{
			$this->_col->db->selectCollection('auto_increment_id')->insert(array('_id' => $seqname, 'id' => $option['init'] )); 
			return $option['init']; 
	   }
	} 
	
	/**
	 * 随机初始化，并连接mongo
	 */
	final private function connect(){
		$succConn=false;
		$index=false;

		//检查是否启用
		if(self::isAllow()){
			//连接mongodb://localhost:27017,localhost:27018 , mongodb://${username}:${password}@localhost
			$servers = $_SERVER["mongodb"]; 
			
			//检查并共享mongo连接
			while(!$succConn && $servers){								
				//检查mongo实例是否存在？
				if(!is_object($this->_mongo)){	
					$index=rand(0,count($servers)-1);  //随机选取
					$this->_mongo = new Mongo('mongodb://'.$servers[$index] );  //,array('persist' => 'db'.$index)	
				}	
				$succConn=$this->_mongo->connect();
				if($succConn){	//成功，跳出循环检查				
					break;
				}else{	//失败，移除当前服务节点
					$this->_mongo = null; //重置，触发后继重新实例化
					if($index!==false){
						unset($servers[$index]);
					}
				}
			}
		}
		$this->_connected=$succConn;
		return $succConn;
	}
	
	
	
	
	
	
	
	/**【高级开发使用】mapReduce
	 * db.runCommand(
		 { mapreduce : <collection>,
		   map : <mapfunction>,		归类
		   reduce : <reducefunction>	统计
		   [, query : <query filter object>]
		   [, sort : <sorts the input objects using this key. Useful for optimization, like sorting by the emit key for fewer reduces>]
		   [, limit : <number of objects to return from collection, not supported with sharding>]
		   [, out : <see output options below>]	注意shard模式需'sharded'=>true
		   [, keeptemp: <true|false>]
		   [, finalize : <finalizefunction>]
		   [, scope : <object where fields go into javascript global scope >]
		   [, jsMode : true]	注意受js内存大小和500K uid限制
		   [, verbose : true]
		 }
		);
	 * http://www.mongodb.org/display/DOCS/MapReduce
	 * @param MongoCode $mapfunction
	 * @param MongoCode $reducefunction
	 * @param array $where
	 * @param unknown_type $outCol
	 * @param array $opt
	 */
	final public function mapReduce(MongoCode $mapfunction, MongoCode $reducefunction, array $where , $outCol ,array $opt=array()){	
		if(!$this->_connected) return false;

		$command=array();
		$command['mapreduce']=$this->_name;
		$command['map']= $mapfunction;
		$command['reduce']= $reducefunction;
		$command['query']= $where;		
		$command['out']= $outCol;					
		$command += $opt;
		return $this->_col->db->command($command);
	}
	
	
	/*
	//【高级开发使用】直接执行mongo command命令。
	final public function dbRunCommand(array $command){  	   	   
	   return  $this->_col->db->command($command); 
	}
	*/
	
	
	/**
	 * [高级应用]
	 * @param array $keys	,分组字段 array("category" => 1)
	 * @param unknown_type $initial		,聚合条目结果初始值 array("items" => array())
	 * @param unknown_type $reduce		,逐条归纳js函数 "function (obj, prev) { prev.items.push(obj.name); }"
	 * @param unknown_type $options  , condition|finalize
	 */
	final public function groupWithoutSharded (array $keys, array $initial, $reduce , array $options = array()){
		$ret=$this->_col->group($keys, $initial, $reduce,$options);
		return $ret;				
	}
	
	
	
	final public function reconnect(){
		return $this->_mongo->connect();
	}

	final public function close(){
		if($this->_connected){
			return $this->_mongo->close();
		}
	}
	
	
	
	final static public function fill($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= $para[$key];
	}
	final static public function fill_str($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= filter_var($para[$key],FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
	}
	final static public function fill_int($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= $para[$key]===''? '': intval($para[$key]);
	}
	final static public function fill_float($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= $para[$key]===''? '': floatval($para[$key]);
	}	
	final static public function fill_email($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= filter_var($para[$key],FILTER_VALIDATE_EMAIL);
	}
	final static public function fill_md5($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= md5(trim($para[$key]));
	}
	final static public function fill_date($para,&$data,$key) {
		if(isset($para[$key]) )	$data[$key]	= self::getDate($para[$key],'Y-m-d');
	}
	final static public function fill_datetime($para,&$data,$key) {
		if(isset($para[$key]) )	$data[$key]	= self::getDate($para[$key]);
	}	
	final static public function fill_url($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= filter_var($para[$key],FILTER_VALIDATE_URL);
	}	
	final static public function fill_array($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= is_array($para[$key])? $para[$key] : null;
	}
	final static public function getDate($datetime=null,$format='Y-m-d H:i:s'){	
		if(!isset($datetime)){
			$datetime=time();
			return date($format,$datetime);
	    }elseif(is_numeric($datetime)){
			return date($format,$datetime);
		}elseif(!empty($datetime)){
			$datetime=strtotime($datetime);
			return date($format,$datetime);
	    }else{
			Return '';
		} 
	}
	
	
	/**
	 * 异常错误输出
	 * @param unknown_type $e
	 * @param unknown_type $args
	 */
	final public static function printErr($e,$args){
		if(__ENV__=='prod'){			
			error_log(date('[Y-m-d H:i:s] ') . "\t{$_SERVER[REQUEST_URI]}\t args:".json_encode($args)."\t err:".$e->getMessage()."\n",3,MONGODB_LOG_PATH);			
		}else{
			var_dump($e,$args);
			echo '<hr/>';
		}
	}


	
	static public function isAllow(){
		if(defined('__ETC')){
			$file=__ETC.'/switch.mongodb.access';
		}else{
			$file=defined('SWITCH_MONGODB_ACCESS')? SWITCH_MONGODB_ACCESS : '';			
		}
		
		$ret=true;
		if(file_exists($file)){
			$content=file_get_contents($file);
			$content=trim($content);
			//access文件内容为0时，关闭通道
			if($content!==false && $content=='0'){
				$ret=false;
			}
		}else{
			$ret=true;
		}

		return $ret;
   }


}

