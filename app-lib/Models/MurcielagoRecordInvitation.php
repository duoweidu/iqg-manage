<?php
class MurcielagoRecordInvitation extends Db_Iqg {

	public $_name = 'murcielago_record_invitation';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_int($para,$data,'fromUserId');                   //int(10) unsigned		
		self::fill_str($para,$data,'countryCallingCode');           //varchar(5)		
		self::fill_str($para,$data,'mobile');                       //varchar(20)		
		self::fill_str($para,$data,'originalMobile');               //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(10) unsigned		
		self::fill_int($para,$data,'isAccepted');                   //tinyint(1) unsigned		
		return $data;
	}

} 
?>