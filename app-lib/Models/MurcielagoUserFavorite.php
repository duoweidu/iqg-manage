<?php
class MurcielagoUserFavorite extends Db_Iqg {

	public $_name = 'murcielago_user_favorite';
	public $_primarykey = 'goodsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_int($para,$data,'shopId');                       //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'goodsPrice');                   //decimal(10,2)		
		self::fill($para,$data,'remark');                           //text		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>