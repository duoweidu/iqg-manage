<?php
class MurcielagoLog extends Db_Iqg {

	public $_name = 'murcielago_log';
	public $_primarykey = 'userLogId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'userLogId');                    //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'subject');                      //varchar(200)		
		self::fill($para,$data,'remark');                           //text		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_datetime($para,$data,'addTime');                 //datetime		
		return $data;
	}

} 
?>