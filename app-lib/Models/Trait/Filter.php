<?php

//php 5.4

trait Trait_Filter {

	final static public function fill($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= $para[$key];
	}
	final static public function fill_str($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= filter_var($para[$key],FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
	}
	final static public function fill_int($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= $para[$key]===''? '': intval($para[$key]);
	}
	final static public function fill_float($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= $para[$key]===''? '': floatval($para[$key]);
	}	
	final static public function fill_email($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= filter_var($para[$key],FILTER_VALIDATE_EMAIL);
	}
	final static public function fill_md5($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= md5(trim($para[$key]));
	}
	final static public function fill_date($para,&$data,$key) {
		if(isset($para[$key]) )	$data[$key]	= self::getDate($para[$key],'Y-m-d');
	}
	final static public function fill_datetime($para,&$data,$key) {
		if(isset($para[$key]) )	$data[$key]	= self::getDate($para[$key]);
	}	
	final static public function fill_url($para,&$data,$key) {
		if(isset($para[$key]))	$data[$key]	= filter_var($para[$key],FILTER_VALIDATE_URL);
	}
	
	final static public function getDate($datetime=null,$format='Y-m-d H:i:s'){	
		if(!isset($datetime)){
			$datetime=time();
			return date($format,$datetime);
	    }elseif(is_numeric($datetime)){
			return date($format,$datetime);
		}elseif(!empty($datetime)){
			$datetime=strtotime($datetime);
			return date($format,$datetime);
	    }else{
			Return '';
		} 
	}


}


?>