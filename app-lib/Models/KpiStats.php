<?php
class KpiStats extends Db_Admin {

	public $_name = 'kpi_stats';
	public $_primarykey = 'ks_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'ks_id');                        //int(11)		
		self::fill_int($para,$data,'ks_shopid');                    //int(11)		
		self::fill_int($para,$data,'ks_year');                      //smallint(6)		
		self::fill_int($para,$data,'ks_month');                     //smallint(6)		
		self::fill_int($para,$data,'ks_onsell');                    //tinyint(4)		
		self::fill_int($para,$data,'ks_onsell_days');               //int(11)		
		self::fill_int($para,$data,'ks_goods_total');               //int(11)		
		self::fill_int($para,$data,'ks_money_total');               //int(11)		
		self::fill_int($para,$data,'ks_goods_num');                 //int(11)		
		self::fill_int($para,$data,'ks_money_alipay');              //int(11)		
		self::fill_int($para,$data,'ks_money_balance');             //int(11)		
		self::fill_int($para,$data,'ks_utime');                     //int(11)		
		return $data;
	}

} 
?>