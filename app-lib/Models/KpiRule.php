<?php
class KpiRule extends Db_Admin {

	public $_name = 'kpi_rule';
	public $_primarykey = 'kr_key';

	function prepareData($para) {
		$data=array();
		self::fill_str($para,$data,'kr_key');                       //varchar(50)		
		self::fill($para,$data,'kr_value');                         //text		
		self::fill_int($para,$data,'kr_utime');                     //int(11)		
		self::fill_int($para,$data,'kr_uuid');                      //int(11)		
		return $data;
	}

} 
?>