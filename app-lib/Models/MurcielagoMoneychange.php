<?php
class MurcielagoMoneychange extends Db_Iqg {

	public $_name = 'murcielago_moneychange';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_float($para,$data,'money');                      //float(9,2)		
		self::fill_str($para,$data,'remark');                       //varchar(50)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_float($para,$data,'balance');                    //float(9,2)		
		self::fill_str($para,$data,'cardNumber');                   //varchar(20)		
		return $data;
	}

} 
?>