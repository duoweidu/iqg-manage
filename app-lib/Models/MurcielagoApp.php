<?php
class MurcielagoApp extends Db_Iqg {

	public $_name = 'murcielago_app';
	public $_primarykey = 'appId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'appId');                        //int(10) unsigned		
		self::fill_str($para,$data,'appKey');                       //char(32)		
		self::fill_int($para,$data,'versionCode');                  //int(11)		
		self::fill_str($para,$data,'versionName');                  //varchar(20)		
		self::fill_str($para,$data,'appType');                      //enum('win8','ios','bios','android')		
		self::fill_str($para,$data,'appUrl');                       //varchar(255)		
		self::fill_str($para,$data,'updateMode');                   //enum('compulsory','optional')		
		self::fill_float($para,$data,'updateProbability');          //float(9,1)		
		self::fill_int($para,$data,'hasTutorial');                  //tinyint(1)		
		self::fill($para,$data,'updateInfo');                       //text		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		self::fill_int($para,$data,'stability');                    //enum('dev','stable')		
		return $data;
	}

} 
?>