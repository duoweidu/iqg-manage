<?php
class LtPincode extends Db_Admin {

	public $_name = 'lt_pincode';
	public $_primarykey = 'lp_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'lp_id');                        //int(10) unsigned		
		self::fill_str($para,$data,'lp_batch');                     //varchar(20)		
		self::fill_str($para,$data,'lp_pin');                       //varchar(20)		
		self::fill_int($para,$data,'lp_prize');                     //tinyint(4)		
		self::fill_str($para,$data,'lp_value');                     //varchar(50)		
		self::fill_date($para,$data,'lp_start');                    //date		
		self::fill_date($para,$data,'lp_end');                      //date		
		self::fill_str($para,$data,'lp_memo');                      //varchar(50)		
		self::fill_int($para,$data,'lp_check');                     //tinyint(4)		
		self::fill_int($para,$data,'lp_ctime');                     //int(11)		
		self::fill_int($para,$data,'lp_utime');                     //int(11)		
		self::fill_int($para,$data,'lp_uuid');                      //int(11)		
		self::fill_int($para,$data,'lp_status');                    //tinyint(4)		
		return $data;
	}

} 
?>