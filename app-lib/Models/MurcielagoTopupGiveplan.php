<?php
class MurcielagoTopupGiveplan extends Db_Iqg {

	public $_name = 'murcielago_topup_giveplan';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_float($para,$data,'money');                      //float(9,2) unsigned		
		self::fill_float($para,$data,'give');                       //float(9,2) unsigned		
		self::fill_int($para,$data,'beginTime');                    //int(10) unsigned		
		self::fill_int($para,$data,'endTime');                      //int(10) unsigned		
		self::fill_str($para,$data,'name');                         //varchar(50)		
		self::fill_int($para,$data,'id');                           //int(10)		
		self::fill_int($para,$data,'stop');                         //tinyint(1) unsigned		
		return $data;
	}

} 
?>