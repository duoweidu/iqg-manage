<?php
class MurcielagoLogSms extends Db_Iqg {

	public $_name = 'murcielago_log_sms';
	public $_primarykey = 'logSmsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logSmsId');                     //int(11)		
		self::fill_int($para,$data,'smsType');                      //tinyint(4)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_str($para,$data,'countryCallingCode');           //varchar(5)		
		self::fill_str($para,$data,'mobile');                       //varchar(20)		
		self::fill_str($para,$data,'ucode');                        //varchar(20)		
		self::fill($para,$data,'content');                          //text		
		self::fill_int($para,$data,'enable');                       //tinyint(1)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>