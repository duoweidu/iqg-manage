<?php
class MurcielagoLogCvGoods extends Db_Iqg {

	public $_name = 'murcielago_log_cv_goods';
	public $_primarykey = 'logCVGoodsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logCVGoodsId');                 //int(11)		
		self::fill_int($para,$data,'shopId');                       //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_int($para,$data,'logData');                      //blob		
		self::fill_int($para,$data,'lat');                          //double(10,6)		
		self::fill_int($para,$data,'lng');                          //double(10,6)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>