<?php
class MurcielagoEcionChange extends Db_Iqg {

	public $_name = 'murcielago_ecion_change';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_int($para,$data,'uid');                          //int(10) unsigned		
		self::fill_int($para,$data,'transactionAmount');            //int(10)		
		self::fill_str($para,$data,'remark');                       //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(10) unsigned		
		self::fill_int($para,$data,'type');                         //tinyint(1) unsigned		
		self::fill_int($para,$data,'isSystem');                     //tinyint(1) unsigned		
		self::fill_int($para,$data,'adminId');                      //int(10) unsigned		
		return $data;
	}

} 
?>