<?php
class MurcielagoLogTodayselling extends Db_Iqg {

	public $_name = 'murcielago_log_todayselling';
	public $_primarykey = 'logId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logId');                        //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'marketPrice');                  //decimal(10,2)		
		self::fill_int($para,$data,'goodsPrice');                   //decimal(10,2)		
		self::fill_int($para,$data,'currentPrice');                 //decimal(10,2)		
		self::fill_int($para,$data,'bargainRange');                 //decimal(10,2)		
		self::fill_int($para,$data,'goodsNumber');                  //int(11)		
		self::fill_int($para,$data,'countBargainPrev');             //smallint(6)		
		self::fill_int($para,$data,'countBargainAll');              //bigint(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>