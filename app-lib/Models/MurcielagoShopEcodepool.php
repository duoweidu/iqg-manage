<?php
class MurcielagoShopEcodepool extends Db_Iqg {

	public $_name = 'murcielago_shop_ecodepool';
	public $_primarykey = 'isMultiShop';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		self::fill_int($para,$data,'poolId');                       //int(10) unsigned		
		self::fill_int($para,$data,'overplus');                     //int(10) unsigned		
		self::fill_int($para,$data,'isMultiShop');                  //tinyint(1) unsigned		
		return $data;
	}

} 
?>