<?php
class MurcielagoLogAppdown extends Db_Iqg {

	public $_name = 'murcielago_log_appdown';
	public $_primarykey = 'logId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logId');                        //int(11)		
		self::fill_int($para,$data,'logType');                      //enum('android','ios')		
		self::fill($para,$data,'logUserAgent');                     //text		
		self::fill($para,$data,'logReferer');                       //text		
		self::fill($para,$data,'logData');                          //text		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_datetime($para,$data,'addTime');                 //datetime		
		return $data;
	}

} 
?>