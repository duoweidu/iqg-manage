<?php
class MurcielagoTodaysell extends Db_Iqg {

	public $_name = 'murcielago_todaysell';
	public $_primarykey = 'sellTime';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'sellTime');                     //int(10) unsigned		
		return $data;
	}

} 
?>