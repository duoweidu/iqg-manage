<?php
class MurcielagoAdmin extends Db_Iqg {

	public $_name = 'murcielago_admin';
	public $_primarykey = 'adminId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'adminId');                      //int(11)		
		self::fill_int($para,$data,'adminLevel');                   //smallint(6)		
		self::fill_int($para,$data,'adminLocked');                  //tinyint(1)		
		self::fill_int($para,$data,'actionList');                   //blob		
		self::fill_str($para,$data,'username');                     //varchar(50)		
		self::fill_str($para,$data,'password');                     //varchar(128)		
		self::fill_int($para,$data,'enable');                       //tinyint(1)		
		self::fill_int($para,$data,'nlogins');                      //int(11)		
		self::fill_str($para,$data,'lastLoginIp');                  //varchar(20)		
		self::fill_datetime($para,$data,'lastLoginDate');           //datetime		
		self::fill_str($para,$data,'creationIp');                   //varchar(20)		
		self::fill_datetime($para,$data,'creationDate');            //datetime		
		self::fill_datetime($para,$data,'updateDate');              //datetime		
		self::fill_datetime($para,$data,'actionLists');             //blob		
		return $data;
	}

} 
?>