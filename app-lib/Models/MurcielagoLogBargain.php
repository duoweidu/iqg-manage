<?php
class MurcielagoLogBargain extends Db_Iqg {

	public $_name = 'murcielago_log_bargain';
	public $_primarykey = 'logsId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'logsId');                       //int(11)		
		self::fill_int($para,$data,'shopId');                       //int(11)		
		self::fill_int($para,$data,'goodsId');                      //int(11)		
		self::fill_int($para,$data,'uid');                          //int(11)		
		self::fill_float($para,$data,'bargainRange');               //float(9,2)		
		self::fill_float($para,$data,'currentPrice');               //float(9,2)		
		self::fill_float($para,$data,'prevPrice');                  //float(9,2)		
		self::fill_float($para,$data,'goodsPrice');                 //float(9,2)		
		self::fill_float($para,$data,'lat');                        //double(10,6)		
		self::fill_float($para,$data,'lng');                        //double(10,6)		
		self::fill_str($para,$data,'addIp');                        //varchar(20)		
		self::fill_int($para,$data,'addTime');                      //int(11)		
		return $data;
	}

} 
?>