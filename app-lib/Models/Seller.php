<?php
class Seller extends Db_Manage {

	public $_name = 'seller';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'name');                         //varchar(10)		
		self::fill_int($para,$data,'jobTitleId');                   //tinyint(3) unsigned		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		self::fill_int($para,$data,'parentId');                     //int(10) unsigned		
		self::fill_int($para,$data,'isDeleted');                    //tinyint(1) unsigned		
		self::fill_email($para,$data,'email');                      //varchar(40)		
		return $data;
	}

} 
?>