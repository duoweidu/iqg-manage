<?php
class MurcielagoCity extends Db_Iqg {

	public $_name = 'murcielago_city';
	public $_primarykey = 'cityId';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'cityId');                       //smallint(4) unsigned		
		self::fill_str($para,$data,'cityName');                     //varchar(50)		
		self::fill_str($para,$data,'address');                      //varchar(50)		
		self::fill_str($para,$data,'lng');                          //double		
		self::fill_str($para,$data,'lat');                          //double		
		self::fill_int($para,$data,'sortOrder');                    //int(11)		
		self::fill_int($para,$data,'isHidden');                     //tinyint(1)		
		return $data;
	}

} 
?>