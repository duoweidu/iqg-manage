<?php
class MurcielagoOrderPayment extends Db_Iqg {

	public $_name = 'murcielago_order_payment';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_int($para,$data,'uid');                          //int(10) unsigned		
		self::fill_str($para,$data,'paymentSn');                    //varchar(20)		
		self::fill_str($para,$data,'orderSn');                      //varchar(20)		
		self::fill_float($para,$data,'amount');                     //float(9,2)		
		self::fill_int($para,$data,'addTime');                      //int(10) unsigned		
		self::fill_int($para,$data,'isPaid');                       //tinyint(1) unsigned		
		self::fill_int($para,$data,'payway');                       //tinyint(4) unsigned		
		return $data;
	}

} 
?>