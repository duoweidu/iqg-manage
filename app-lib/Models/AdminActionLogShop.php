<?php
class AdminActionLogShop extends Db_Manage {

	public $_name = 'admin_action_log_shop';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_int($para,$data,'adminId');                      //int(10) unsigned		
		self::fill_int($para,$data,'shopId');                       //int(10) unsigned		
		self::fill_int($para,$data,'addTime');                      //int(10) unsigned		
		self::fill_str($para,$data,'ip');                           //varchar(39)		
		self::fill_int($para,$data,'actionId');                     //tinyint(3) unsigned		
		self::fill_str($para,$data,'actionDesc');                   //varchar(100)		
		return $data;
	}

} 
?>