<?php
class MurcielagoCbd extends Db_Iqg {

	public $_name = 'murcielago_cbd';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10) unsigned		
		self::fill_str($para,$data,'name');                         //varchar(50)		
		self::fill_str($para,$data,'address');                      //varchar(100)		
		self::fill_str($para,$data,'lat');                          //double(10,6) unsigned		
		self::fill_str($para,$data,'lng');                          //double(10,6) unsigned		
		self::fill_int($para,$data,'cityId');                       //int(10) unsigned		
		self::fill_int($para,$data,'sortOrder');                    //int(10) unsigned		
		return $data;
	}

} 
?>