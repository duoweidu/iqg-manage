<?php
namespace PEAR2;
class Str
{
    private function __construct()
    {
    }
   
    public static function firstToUpper($s)
    {
        $first = substr ( $s, 0, 1 );
        $left = substr ( $s, 1 );
        return strtoupper ( $first ) .$left;
    }
   
    public static function firstToLower($s)
    {
        $first = substr ( $s, 0, 1 );
        $left = substr ( $s, 1 );
        return strtolower ( $first ) .$left;
    }
}
?>
