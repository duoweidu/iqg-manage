<?php
namespace PEAR2\sql\maker;
final class Mysql extends Base
{
    protected $char = array(
        'identifier_quote' => '`',
        'value_quote' => '\'',
    );

    /**
     * sql mode
     * @var  array
     */
    private static $sql_mode = array(
        'ansi' => array(
            'identifier_quote' => '"',
            'value_quote' => '\'',
            'end' => ';',
        ),
    );

    protected $conf = array(
        'api' => 'pdo',
        'tableName' => '',
    );

    public function __construct($db_connent, array $conf = array())
    {
        parent::__construct($db_connent, $conf);
        //mysql default sql_mode is empty
        if(isset($conf['sql_mode'])&&!empty($conf['sql_mode'])) {
            if(isset($this->sql_mode[$conf['sql_mode']])) {
                $this->char = $this->sql_mode[$conf['sql_mode']];
            }
        }
    }

    public function limit($limit_start, $limit_size)
    {
        return ' LIMIT ' . intval($limit_start) . ',' . intval($limit_size);
    }

    protected function quote($data)
    {
        switch ($this->conf['api']) {
            case 'pdo':
                return $this->db->quote($data);
                break;
            case 'mysqli':
                return $this->char['value_quote'] . $this->db->real_escape_string($data) . $this->char['value_quote'];
                break;
            default :
                throw new SQL_Maker_Exception();
        }
    }

    public function quoteColumn($data)
    {
        return $this->quoteIdentifier($data);
    }

    /**
     * @todo escape indentifier
     */
    public function quoteIdentifier($data)
    {
        return $this->char['identifier_quote'] . $data . $this->char['identifier_quote'];
    }
    
    public function quoteTableName()
    {
        return $this->quoteIdentifier($this->conf['tableName']);
    }
}
?>
