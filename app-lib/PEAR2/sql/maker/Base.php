<?php
namespace PEAR2\sql\maker;
abstract class Base
{
    protected $char = array();
    protected $conf;
    protected $db;

    public function __construct($db_connection, array $conf = array())
    {
        if(!empty($conf)) {
            //can't use array_merge, because $this->conf may be empty
            foreach($conf as $k=>$v) {
                $this->conf[$k] = $v;
            }
        }
        $this->db = $db_connection;
    }

    abstract public function limit($limit_start, $limit_size);

    abstract protected function quote($data);

    abstract protected function quoteColumn($data);

    abstract public function quoteIdentifier($data);

    abstract public function quoteTableName();
    
    /**
     * column array to string, 用于select和insert
     *
     * @param mixed $data
     *
     * @return   string
     * @throws   SQL_Maker_Exception
     */
    public function implodeToColumn($data)
    {
        if(empty($data)) {
            throw new SQL_Maker_Exception();
        }
        if($data=='*') {
            return '*';
        }
        if(is_array($data)) {
            $tmp = array();
            foreach($data as $one) {
                if(strpos($one, '(') !== false) {
                    $tmp[] = $one; //eg. point(location)
                } else {
                    $tmp[] = $this->quoteColumn($one);
                }
            }
            $r = implode(',', $tmp); //eg. `id`,`name`,`desc`
        }
        else {
            if(strpos($data, '(') !== false) {
                $r = $data;
            } else {
                $r = $this->quoteColumn($data); //eg. 'name'
            }
        }
        return $r;
    }

    public function implodeToRowValues($data)
    {
        if(is_array($data)) {
            $tmp = array();
            foreach($data as $one) {
                $tmp[] = $this->quote($one);
            }
            $a = implode(',', $tmp);
        }
        else {
            $a = $this->quote($data);
        }
        $b = '(' . $a . ')';
        return $b;
    }

    /**
     * input: array(
     *  array(1, 'jim'),
     *  array(2, 'lucy'),
     * )
     * output: ('1','jim'),('2','lucy')
     * @param array $data
     * @return string
     */
    public function implodeToRowsValues($data) {
        foreach($data as $value) {
            $tmp = $this->implodeToRowValues($value);
            $row_array[] = $tmp;
        }
        $rows = implode(',',$row_array);
        return $rows;
    }

    /**
     * input: array(
     *      'id' => array(
     *          1,2,3
     *      ),
     *      'year' => '2012'
     * )
     * output: WHERE `id` IN ('1','2','3') AND `year`='2012'
     * @param array $data
     * @return string
     */
    public function implodeToWhere($data)
    {
        if(!is_array($data) || empty($data)) {
            return '';
        }
        $r = '';
        $map = array(
            'e' => '=',
            'lt' => '<',
            'lte' => '<=',
            'gt' => '>',
            'gte' => '>=',
            'ne' => '!=',
            'o' => 'OR',
        );
        foreach($map as $str=>$math) {
            if(!isset($data[$str]) || empty($data[$str])) {
                continue;
            }
            foreach($data[$str] as $key=>$value) {
                if(is_array($value)) {
                    $tmp = array();
                    foreach($value as $one) {
                        $tmp[] = $this->quote($one);
                    }
                    $part_1 =  $this->quoteIdentifier($key) . ' IN (' . implode(',', $tmp) . ') ';
                } else {
                    if ( $math == 'OR' ){
                        $part_1 = $this->quoteIdentifier($key) . ' ' . $value ;
                    }else{
                        $part_1 = $this->quoteIdentifier($key) . $math . $this->quote($value);
                    }
                }

                if(!empty($r)) {
                    if ( $math == 'OR' ){
                        $r .= ' OR ';
                    }else{
                        $r .= ' AND ';
                    }
                }
                $r .= $part_1;
            }
        }
        if(empty($r)) {
            return '';
        } else {
            return ' WHERE ' . $r;
        }
    }

    /**
     * input: array(
     *      'name' => 'jim',
     *      'year' => '2012'
     * )
     * output: `name`='jim',`year`='2012'
     * @param array $data
     * @return string
     */
    public function implodeToUpdate($data) {
        if(empty($data)) {
            throw new SQL_Maker_Excption();
        }
        $r = '';
        /*if(isset($data['add'])) {
            foreach($data['add'] as $column=>$value) {
                if(!empty($r)) {
                    $r .= ',';
                }
                $r .= $this->quoteIdentifier($column) . '=' . $this->quoteIdentifier($column) . '+' . $this->quote($value);
            }
        }
        unset($data['add']);*/
        foreach($data as $column=>$value) {
            if(!empty($r)) {
                $r .= ',';
            }
            $r .= $this->quoteIdentifier($column) . '=' . $this->quote($value);
        }
        return $r;
    }
}
?>
