<?php
namespace PEAR2\sql;
$this_dir = dirname(__FILE__) . '/';
class Maker
{
    private $syntex;

    /**
     * if user mysql, conf like array(
     *      'api' => 'pdo', //or 'mysqli', dafault 'pdo'
     *      'sql_mode' => '', // or 'ansi', default ''
     *      'tableName' => '', // not null
     * )
     * if user pgsql, conf like array(
     *      'api' => 'pdo', //or 'pgsql', dafault 'pdo'
     *      'schema_name' => 'public', //default 'public'
     *      'tableName' => '', // not null
     * )
     *
     * @param string    $dbProduct     eg. mysql,postgresql
     * @param mixed     $db_connection  eg. PDO OBJ, mysqli OBJ, pg_connect    
     * @param array     $conf
     */
    public function __construct($dbProduct, $db_connection, array $conf)
    {
        switch ($dbProduct) {
            case 'mysql' :
                $this->syntex = new \PEAR2\sql\maker\Mysql($db_connection, $conf);
                break;
            case 'pgsql' :
                $this->syntex = new \PEAR2\sql\maker\Pgsql($db_connection, $conf);
                break;
            default :
                throw new SQL_Maker_Exception();
                break;
        }
    }

    public function delete($where='')
    {
        return 'DELETE FROM ' . $this->syntex->quoteTableName() . $this->syntex->implodeToWhere($where);
    }
    
    public function insertRow($data)
    {
        return 'INSERT INTO ' . $this->syntex->quoteTableName() . ' (' . $this->syntex->implodeToColumn(array_keys($data)) . ') VALUES ' . $this->syntex->implodeToRowValues(array_values($data));
    }

    public function insertRows($data) {
        $data_for_query = array();
        $columns = array_keys($data[0]);
        foreach($data as $one_row) {
            $data_for_query[] = array_values($one_row);
        }
        return 'INSERT INTO ' . $this->syntex->quoteTableName() . ' (' . $this->syntex->implodeToColumn(array_values($columns)) . ') VALUES ' . $this->syntex->implodeToRowsValues($data_for_query);
    }

    /**
     * 因为经常会查询SELECT * FROM article where id=?，这时候只传第一个参数即可。
     * 所以where放第一位。
     */
    public function select($where='', $column='*', $limit_start=0, $limit_size='', $group_by='', $order_by='')
    {
        $sql = 'SELECT ' . $this->syntex->implodeToColumn($column) . ' FROM ' . $this->syntex->quoteTableName() . $this->syntex->implodeToWhere($where);
        if(!empty($group_by)) {
            $sql .= ' GROUP BY ' . $this->syntex->implodeToColumn($group_by);
        }
        if(!empty($order_by)) {
            if(is_array($order_by)) {
                $tmp = array();
                foreach($order_by as $column => $sc) {
                    $tmp[] = $this->syntex->quoteColumn($column) . ' ' . strtoupper($sc);
                }
                $sql .= ' ORDER BY ' . implode(',', $tmp);
            } else {
                if(strpos(trim($order_by), ' ') !== false) {
                    $tmp = explode(' ', trim($order_by));
                    $sql .= ' ORDER BY ' . $this->syntex->quoteColumn($tmp[0]) . ' ' . strtoupper($tmp[1]);
                } else {
                    $sql .= ' ORDER BY ' . $this->syntex->quoteColumn($order_by);
                }
            }
        }
        if(!empty($limit_size)) {
            $sql .= $this->syntex->limit($limit_start, $limit_size);
        }
        return $sql;
    }

    public function selectCount($where='', $column='*')
    {
        if($column!='*') {
            $sql = 'SELECT COUNT(' . $this->syntex->quoteColumn() . ') as count';
        }   
        else {
            $sql = 'SELECT COUNT(*) as count';
        }   
        //$sql .= empty($group_by) ? '' : ',' . $group_by;
        $sql .= ' FROM ' . $this->syntex->quoteTableName();
        $sql .= $this->syntex->implodeToWhere($where);
        //$sql = !empty($group_by) ? $sql . ' GROUP BY ' . $group_by : $sql;
        return $sql;
    }
    
    public function update($data, $where='')
    {
        return 'UPDATE ' . $this->syntex->quoteTableName() . ' SET ' . $this->syntex->implodeToUpdate($data) . $this->syntex->implodeToWhere($where);
    }
}
?>
