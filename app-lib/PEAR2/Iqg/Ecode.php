<?php
namespace PEAR2\Iqg;
class Ecode
{
    private $conf;

    /**
     * 
     * @param array $conf array(
            'uriPrefix' => 'http://ecode.dev.intra.iqianggou.com/',
            'version' => '1.0.3',
        );
     */
    public function __construct($conf)
    {
        $this->conf = $conf;
    }
    
    private function request($httpMethod, $api, $data=array())
    {
        switch($httpMethod) {
        case 'get' :
            $uri = $this->conf['uriPrefix'] . $api . '?' . http_build_str($data);
            $request = new \HttpRequest($uri);
            $request->setMethod(HTTP_METH_GET);
            break;
        case 'post' :
            $request = new \HttpRequest($this->conf['uriPrefix'] . $api);
            $request->setMethod(HTTP_METH_POST);
            $request->addPostFields($data);
            break;
        case 'put' :
            $request = new \HttpRequest($this->conf['uriPrefix'] . $api);
            $request->setMethod(HTTP_METH_PUT);
            $request->addPutData($data);
        }
        $request->addHeaders(array('Accept' => 'application/json; version=' . $this->conf['version']));
        $request->send();
        if($request->getResponseCode() != 200) {
            throw new \Exception('http not 200');
        }
        return json_decode($request->getResponseBody(), true);
    }

    /** 
     * 新增一个号码池
     * curl -i -d 'resource=auto' -H 'Accept: application/json; version=trunk' 'http://ecode.dev.intra.iqianggou.com/pools/'
     * @param string resource 来源。auto为我们的自动兑换码，custom为自定义
     * @return array array(
        'id' => 3
       )
     */
    public function addPool($resource, $isMultiShop=0)
    {
        $data = array(
            'resource' => $resource,
            'isMultiShop' => $isMultiShop,
        );
        $r = $this->request('post', 'pools/', $data);
        return $r;
    }

    /**
     * 获得一批未用的号
     */
    public function getUsable($poolId, $amount)
    {
        $data = array(
            'amount' => $amount,
        );
        $r = $this->request('get', 'pools/' . $poolId . '/', $data);
        return $r;
    }
 
    /**
     * 发放一个号
     */
    public function allot($poolId)
    {
        $r = $this->request('post', 'pools/' . $poolId . '/');
        return $r;
    }

    /**
     * 录入一批号
     */
    public function addCustomEcodes($poolId, $ecodes)
    {
        $data = array(
            'action' => 'add',
            'ecodes' => json_encode($ecodes),
        );
        $r = $this->request('post', 'pools/' . $poolId . '/', $data);
        return $r;
    }
    
    /**
     * 录入一批号
     */
    public function setCustomEcodes($poolId, $ecodes)
    {
        $data = 'ecodes=' . json_encode($ecodes);
        $r = $this->request('put', 'pools/' . $poolId . '/', $data);
        return $r;
    }
}
?>
