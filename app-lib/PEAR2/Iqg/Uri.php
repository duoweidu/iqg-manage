<?php
namespace PEAR2\Iqg;
class Uri
{
    //这些域名都是cname到七牛云存储。
    private static $conf = array(
        'img-agc' => 'http://img-agc.iqianggou.com/',
        'img-ugc' => 'http://img-ugc.iqianggou.com/',
        'dl' => 'http://dl.iqianggou.com/',
    );
    
    //如果七牛云存储故障，则切换至下面的配置即可，无需修改dns。
    //private static $conf = array(
    //    'img-agc' => 'http://static.iqianggou.com/img-agc/',
    //    'img-ugc' => 'http://static.iqianggou.com/img-ugc/',
    //    'dl' => 'http://static.iqianggou.com/dl/',
    //);

    public static function getImg($bucket, $imgId)
    {
        if(empty($bucket) || empty($imgId)) {
            return '';
        }
        return self::$conf[$bucket] . $imgId;
    }
}
?>
