<?php

/*
 * 
 * 用途： 统计本月【活动】门店的 销售信息
 * 
 */

include_once 'inc/header.php';


l("sale-stats: start ...",'kpi');

$base= st('KpiBaseinfo');
 

$nowTime= Model_Stats::getCurrTime();
$monthBegin = strtotime(get_date($nowTime,'Y-m-01 00:00:00'));
$monthEnd = strtotime('+ 1 month',$monthBegin);  // 自然月

//var_dump($startTime,get_date(time(),'Y-m-01 00:00:00'));

l("total-num:". $base->getCount(""),'kpi');

//1. 在售未下线的门店
$clause= " kb_issuetime<=$monthEnd ";			//上线时间在本月底前的
$clause .=" and ( kb_offtime>$monthBegin or kb_offtime=0 )" ;	//下线时间在本月，或者未准备下线（继续合作）的门店
//$clause .=" and kb_shopid=11324 ";
//$base->debug();
$baseData=$base->getAll("*","where $clause");

l("ative-num:" . count($baseData) . ' ['. get_date($monthBegin) .'-' . get_date($monthEnd) .']','kpi');



//======prepare===========
$statsData=array();
$statsData['ks_year']=get_date($nowTime,'Y');
$statsData['ks_month']=get_date($nowTime,'m');

/*
//随机修改门店下线时间
foreach ($baseData as $shop){		
	$tmp['kb_offtime']= $shop['kb_issuetime']  + rand(0, 9)* 30 * 86400;
	$base->update($tmp, "where kb_shopid= $shop[kb_shopid] ");	
}
*/

$i=1;

foreach ($baseData as $shop){
	$kb_shopid = $shop['kb_shopid'];
	$statsData['ks_shopid'] = $kb_shopid ;
	$statsData['ks_onsell']= Model_KShop::isOnSell($shop['kb_offtime']);
	$statsData['ks_onsell_days'] = Model_KShop::getSellDay($shop['kb_issuetime'], $shop['kb_offtime']);	
	$statsData['ks_goods_total']= Model_KShop::getGoodsTotal($kb_shopid,$monthBegin,$monthEnd);
	
	//1支付宝,2余额,3UPS,4PayPal,5SmoovPay,
	$statsData['ks_money_alipay']= Model_KShop::getMoney($kb_shopid,$monthBegin,$monthEnd,1);
	$statsData['ks_money_balance']= Model_KShop::getMoney($kb_shopid,$monthBegin,$monthEnd,2);
	$statsData['ks_money_total']=  $statsData['ks_money_alipay'] + $statsData['ks_money_balance'];
	
	Model_KShop::saveStats( $statsData);
	
	
	//门店商品种类数，含即将上线的  beginDate > time()
	$biData=array();
	$biData['kb_goods_num']= Model_KShop::getCurrGoodsNum($kb_shopid);
	$biData['kb_shopname']= Model_Shop::getShop($kb_shopid,"shopName");
	$biData['kb_hqname']= Model_Shop::getHQ($shop['kb_shop_pid'],"headquarterName");
	$base->update($biData,"where kb_shopid=$kb_shopid");
	
	l("{$i}-shop-{$kb_shopid}:". get_attrs( $statsData) . "\t base:". get_attrs($biData) ,'kpi');		
	$i++;
	//break;
}



l("sale-stats: end !",'kpi');
 



