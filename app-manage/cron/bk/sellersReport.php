<?php
require_once dirname(__FILE__) . '/../../OA/Db.php';
class sellersReport {
    private $manageDb;
    private $iqg2012Db;
    private $jobTitles;

    public function __construct() {
        $thisDir = dirname(__FILE__) . '/';
        require $thisDir . '../../conf/db.conf';
        require $thisDir . '../../conf/job.conf';
        $this->conf['db'] = $conf['db'];
        $this->manageDb = new Db('iqg_manage');
        $this->iqg2012Db = new Db('iqg2012');
        $this->jobTitles = $conf['jobTitles'];
    }

    /**
     * 删除垃圾数据
     */
    public function clear() {
        $sql = 'select count(*) as cnt from shop';
        $r = $this->manageDb->queryAndFetch($sql);
        $cnt = $r[0]['cnt'];
        for($i=0; $i<$cnt; $i+=100) {
            $sql = 'select shopId from shop limit ' . $i . ',100';
            $r = $this->manageDb->queryAndFetch($sql);
            $shopsIds = array();
            foreach($r as $one) {
                $shopsIds[] = $one['shopId'];
            }

            $sql = 'select shopId from murcielago_shop where shopId in (' . implode(',', $shopsIds) . ')';
            $r2 = $this->iqg2012Db->queryAndFetch($sql);
            $realShopsIds = array();
            foreach($r2 as $one) {
                $realShopsIds[] = $one['shopId'];
            }

            $diff = array_diff($shopsIds, $realShopsIds);
            echo '删除' . "\n";
            var_dump($diff);
            if(!empty($diff)) {
                $sql = 'delete from shop where shopId in (' . implode(',', $diff) . ')';
                $r = $this->manageDb->query($sql);
            }
        }
        return true;
    }

    /**
     * 每个商品的月销售额提成
     */
    public function doGoodsMonthlyReward($yearMonth, $payway=null) {
        echo '每个商品的月销售额提成' . "\n";
        $startTime = strtotime($yearMonth . '-01');
        $endTime = strtotime($yearMonth . '-01' . ' +1 month');

        //重跑本月报表，需要先删除本月报表
        $sql = 'delete from goodsMonthlyReward where yearMonth=\'' . $yearMonth . '-01\' AND payway=' . intval($payway);
        $r = $this->manageDb->query($sql);
        $sql = 'delete from goodsMonthlyParentReward where yearMonth=\'' . $yearMonth . '-01\' AND payway=' . intval($payway);
        $r = $this->manageDb->query($sql);

        $sql = 'select * from categoryGainsharing';
        $r = $this->manageDb->queryAndFetch($sql);
        foreach($r as $one) {
            $categoryGainsharing[$one['catId']][$one['jobTitleId']] = $one;
        }

        $sql = 'select * from shop';
        $shops = $this->manageDb->queryAndFetch($sql);

        foreach($shops as $shop) {
            echo $shop['shopId'] . "\n";
            //一个店的多个商品 可能属于不同的 分类
            $sql = 'select catId,goodsId from murcielago_goods where goodsId in (select goodsId from murcielago_order_goods where shopId=' . $shop['shopId'] . ')';
            $goods = $this->iqg2012Db->queryAndFetch($sql);
            if(empty($goods)) {
                continue;
            }

            $sql = 'select * from seller where id=' . $shop['holderSellerId'];
            $r = $this->manageDb->queryAndFetch($sql);
            $seller = $r[0];
            $jobTitleId = $r[0]['jobTitleId'];

            $parent =array();
            $parentSellerId = $seller['parentId'];

            foreach($goods as $product) {
                $parentSellerId = $seller['parentId'];
                $catId = $product['catId'];
                if(empty($catId)) {
                    echo 'error: goodsId ' . $product['goodsId'] . ', catId empty:' . $product['catId'] . "\n";
                    continue;
                }

                $salesRewardPercent = $categoryGainsharing[$catId][$jobTitleId]['salesRewardPercent'];

                $sql = 'select sum(o.payPrice) as salesAmount, count(o.orderId) as salesCount from murcielago_order_goods as og, murcielago_order as o where o.orderId=og.orderId AND og.goodsId=' . $product['goodsId'] . ' AND o.orderStatus in (1,2) AND o.addTime>=' . $startTime . ' AND o.addTime<' . $endTime;
                if(!empty($payway)) {
                    $sql .= ' AND o.payway = ' . $payway;
                }
                $r = $this->iqg2012Db->queryAndFetch($sql);
                $salesAmount = floatval($r[0]['salesAmount']);
                //这个月没卖掉的商品
                if(bccomp($salesAmount, 0, 3) == 0) {
                    continue;
                }
                $salesCount = intval($r[0]['salesCount']);

                $salesReward = bcmul($salesAmount, $salesRewardPercent, 3);
                //业务员 的 上级经理提成，经理的上级总监也从业务员提成。
                while(!empty($parentSellerId)) {
                    $sql = 'select * from seller where id=' . $parentSellerId;
                    $r = $this->manageDb->queryAndFetch($sql);
                    $parentJobTitleId = $r[0]['jobTitleId'];
                    $parentRewardPercent = $categoryGainsharing[$catId][$parentJobTitleId]['subSalesRewardPercent'];
                    $parentReward = bcmul($salesAmount, $parentRewardPercent, 3);
                    $sql = 'INSERT INTO goodsMonthlyParentReward (goodsId,shopId,salesAmount,yearMonth,parentSellerId,parentRewardPercent,parentReward,payway) VALUES (' . $product['goodsId'] . ',' . $shop['shopId'] . ',' . $salesAmount . ',\'' . $yearMonth . '-01\',' . $parentSellerId . ',' . $parentRewardPercent . ',' . $parentReward . ',' . intval($payway) . ')';
                    echo $sql . "\n";
                    $r2 = $this->manageDb->query($sql);
                    $parentSellerId = $r[0]['parentId'];
                }

                $sql = 'INSERT INTO goodsMonthlyReward (goodsId,shopId,salesAmount,salesCount,salesRewardPercent,yearMonth,holderSellerId,salesReward,payway) VALUES (' . $product['goodsId'] . ',' . $shop['shopId'] . ',' . $salesAmount . ',' . $salesCount . ',' . $salesRewardPercent . ',\'' . $yearMonth . '-01\',' . $shop['holderSellerId'] . ',' . $salesReward . ',' . intval($payway) . ')';
                echo $sql . "\n";
                $r = $this->manageDb->query($sql);
            }
        }
    }

    /**
     * 每月新店提成
     */
    public function doNewShopMonthlyReward($yearMonth) {
        echo '每月新店提成' . "\n";
        $startTime = strtotime($yearMonth . '-01');
        $endTime = strtotime($yearMonth . '-01' . ' +1 month');

        $sql = 'delete from newShopMonthlyReward where yearMonth=\'' . $yearMonth . '-01\'';
        $r = $this->manageDb->query($sql);
        $sql = 'delete from newShopMonthlyParentReward where yearMonth=\'' . $yearMonth . '-01\'';
        $r = $this->manageDb->query($sql);

        $sql = 'select * from categoryGainsharing';
        $r = $this->manageDb->queryAndFetch($sql);
        foreach($r as $one) {
            $categoryGainsharing[$one['catId']][$one['jobTitleId']] = $one;
        }

        $sql = 'select * from shop where issueTime>=' . $startTime . ' AND issueTime<' . $endTime . ' AND catId !=0';
        $shops = $this->manageDb->queryAndFetch($sql);

        foreach($shops as $shop) {
            echo $shop['shopId'] . "\n";
            $sql = 'select * from seller where id=' . $shop['signerSellerId'];
            $r = $this->manageDb->queryAndFetch($sql);
            $seller = $r[0];
            $jobTitleId = $r[0]['jobTitleId'];

            $parentSellerId = $seller['parentId'];

            $catId = $shop['catId'];

            $newShopRewardAmount = $categoryGainsharing[$catId][$jobTitleId]['newShopRewardAmount'];
            $reward = $newShopRewardAmount;
            while(!empty($parentSellerId)) {
                $sql = 'select * from seller where id=' . $parentSellerId;
                $r = $this->manageDb->queryAndFetch($sql);
                $parentJobTitleId = $r[0]['jobTitleId'];
                $parentReward = $categoryGainsharing[$catId][$parentJobTitleId]['subNewShopRewardAmount'];
                $sql = 'REPLACE INTO newShopMonthlyParentReward (shopId,yearMonth,parentSellerId,parentReward) VALUES (' . $shop['shopId'] . ',\'' . $yearMonth . '-01\',' . $parentSellerId . ',' . $parentReward . ')';
                //echo $sql . "\n";
                $r2 = $this->manageDb->query($sql);
                $parentSellerId = $r[0]['parentId'];
            }

            $sql = 'REPLACE INTO newShopMonthlyReward (shopId,yearMonth,signerSellerId,reward) VALUES (' . $shop['shopId'] . ',\'' . $yearMonth . '-01\',' . $shop['signerSellerId'] . ',' . $reward . ')';
            //echo $sql . "\n";
            $r = $this->manageDb->query($sql);
        }
    }

    public function doShopReward($yearMonth) {
        $startTime = strtotime($yearMonth . '-01');
        $endTime = strtotime($yearMonth . '-01' . ' +1 month');

        $sql = 'select * from categoryGainsharing';
        $r = $this->manageDb->queryAndFetch($sql);
        foreach($r as $one) {
            $categoryGainsharing[$one['catId']][$one['jobTitleId']] = $one;
        }

        $sql = 'select * from shop';
        $shops = $this->manageDb->queryAndFetch($sql);

        foreach($shops as $shop) {
            $sql = 'select catId from murcielago_goods where goodsId = (select goodsId from murcielago_order_goods where shopId=' . $shop['shopId'] . ' limit 1)';
            $r = $this->iqg2012Db->queryAndFetch($sql);
            if(!isset($r[0]['catId'])) {
                continue;
            }
            $catId = $r[0]['catId'];

            $sql = 'select * from seller where id=' . $shop['holderSellerId'];
            $r = $this->manageDb->queryAndFetch($sql);
            $seller = $r[0];
            $jobTitleId = $r[0]['jobTitleId'];

            $salesRewardPercent = $categoryGainsharing[$catId][$jobTitleId]['salesRewardPercent'];

            $sql = 'select sum(o.payPrice) as salesAmount from murcielago_order_goods as og, murcielago_order as o where o.orderId=og.orderId AND og.shopId=' . $shop['shopId'] . ' AND o.orderStatus in (1,2) AND o.addTime>=' . $startTime . ' AND o.addTime<' . $endTime;
            $r = $this->iqg2012Db->queryAndFetch($sql);
            $salesAmount = $r[0]['salesAmount'];

            $salesReward = bcmul($salesAmount, $salesRewardPercent, 3);


            $sql = 'REPLACE INTO sellerShopReward (shopId,salesAmount,yearMonth,holderSellerId,salesRewardPercent,salesReward) VALUES (' . $shop['shopId'] . ',' . $salesAmount . ',\'' . $yearMonth . '-01\',' . $shop['holderSellerId'] . ',' . $salesRewardPercent . ',' . $salesReward . ')';
            //echo $sql . "\n";
            $r = $this->manageDb->query($sql);

            $sql = 'SELECT count(*) as cnt FROM sellerReward WHERE sellerId=' . $seller['id'] . ' AND  yearMonth=\'' . $yearMonth . '-01\'';
            //echo $sql . "\n";
            $r = $this->manageDb->queryAndFetch($sql);
            if($r[0]['cnt'] == 0) {
                $sql = 'INSERT INTO sellerReward (yearMonth,sellerId) VALUES (\'' . $yearMonth . '-01\',' . $shop['holderSellerId'] . ')';
                $r = $this->manageDb->query($sql);
            }
            $sql = 'UPDATE sellerReward set salesAmount=salesAmount+' . $salesAmount . ',salesReward=salesReward+' . $salesReward . ',newShopCount=newShopCount+0,newShopReward=0 WHERE sellerId=' . $seller['id'] . ' AND  yearMonth=\'' . $yearMonth . '-01\'';
            $r = $this->manageDb->query($sql);

            //修改上级的数据
            if(!empty($seller['parentId'])) {
                $sql = 'select * from seller where id=' . $seller['parentId'];
                $r = $this->manageDb->queryAndFetch($sql);
                $parent = $r[0];

                $sql = 'SELECT count(*) as cnt FROM sellerReward WHERE sellerId=' . $seller['parentId'] . ' AND  yearMonth=\'' . $yearMonth . '-01\'';
                $r = $this->manageDb->queryAndFetch($sql);
                if($r[0]['cnt'] == 0) {
                    $sql = 'INSERT INTO sellerReward (yearMonth,sellerId) VALUES (\'' . $yearMonth . '-01\',' . $seller['parentId'] . ')';
                    $r = $this->manageDb->query($sql);
                }
                $sql = 'UPDATE sellerReward set subSalesReward=subSalesReward+' . bcmul($salesAmount, $categoryGainsharing[$catId][$parent['jobTitleId']]['subSalesRewardPercent'], 3) . ' WHERE sellerId=' . $parent['id'] . ' AND  yearMonth=\'' . $yearMonth . '-01\'';
                //echo $sql . "\n";
                $r = $this->manageDb->query($sql);
            }

        }
    }
}
if(isset($argv[1])) {
    $yearMonths = array(
        date('Y-m', strtotime($argv[1] . '-01')),
    );
} else {
    $yearMonths = array(
        date('Y-m', time()), //这个月的
    );
    //每月第一天 统计上个月的
    if(date('d', time()) == '01') {
        $yearMonths[] = date('Y-m', strtotime(' -1 month', time())); //上个月的
    }
}

foreach($yearMonths as $yearMonth) {
    echo '开始计算' . $yearMonth . '的数据' . "\n";
    $o = new sellersReport();
    $o->clear();

    $tmp = parse_ini_file(dirname(__FILE__) . '/../../conf/order.php', true);
    //按支付网关计算
    foreach($tmp['payway'] as $payway=>$v) {
        if(!empty($payway)) {
            $o->doGoodsMonthlyReward($yearMonth, $payway);
        }
    }

    //计算所有支付网关合计的
    $o->doGoodsMonthlyReward($yearMonth);

    //新店提成
    $o->doNewShopMonthlyReward($yearMonth);
    echo '计算完成' . "\n";
}
?>