<?php

/*
 * 
 * 用途： 同步原manage库的用户和门店基础数据
 * 
 */

include_once 'inc/header.php';


l("sale-sync: start ...",'sync');

$base= st('KpiBaseinfo');
$seller= st('Seller');
$shop= st('Shop');
$user= st('AclUser');
$ext= st('AclUserExt');
$userrole= st('AclUserRole');
$mshop= st('MurcielagoShop');


//$user->debug();
//$seller->debug();
#================1. 初始化系统用户  ======================
/* 
insert into iqg_manage_sys.acl_user (id,realname,name,email,department,pass) 
		select id, name ,email,email as m2 , '销售部','6f07588a49bfcaec584719433a3cb086' from iqg_manage.seller ;
#7 销售部 
insert into iqg_manage_sys.acl_user_ext (uid,realname,level,cityid, pid, depid) 
	 	select id, name , jobtitleid , cityid,parentid  ,7  from iqg_manage.seller ;
#6 销售人员
insert into iqg_manage_sys.acl_user_role (user_id,role_id)  select id,6  from iqg_manage.seller ;
#总监设置为： 7 销售主管 
insert into iqg_manage_sys.acl_user_role (user_id,role_id)  select id,7  from iqg_manage.seller  where parentId=0;
*/


$where="";
//$where="where id=13";
$rowArr= $seller->getAll("*",$where);

l("total-user:". count($rowArr),'sync');

$i=1;
foreach ($rowArr as $row){
	$uid=$row['id'];
	$email=$row['email'];
	
	if($uid<13){continue;} //管理员帐号禁止同步
	
	//if(empty($email)){continue;} 
	
	//id-name 需保持对应，检查是否存在？ 强制同步！！！
	if(!Model_Sync::userExists($uid, $email)){		
		
		$logdata=array();
		
		$user->delete("where id=$uid");
		$data=array();
		$data['id']=$row['id'];
		$data['realname']=$row['name'];
		$data['name']=$row['email'];
		$data['email']=$row['email'];
		$data['department']='销售部';
		$data['pass']='6f07588a49bfcaec584719433a3cb086'; //dwd10000		
		$user->insert($data);		
		$logdata+=$data;
		
		$ext->delete("where uid=$uid");
		$data=array();
		$data['uid']=$row['id'];
		$data['realname']=$row['name'];
		$data['level']=$row['jobTitleId'];
		$data['cityid']=$row['cityId'];
		$data['pid']=$row['parentId'];
		$data['depid']=7;  //7 销售部 
		$ext->insert($data);		
		$logdata+=$data;
		
		$userrole->delete("where user_id=$uid");
		$data=array();
		$data['user_id']=$row['id'];
		$data['role_id']=6; //6 销售人员
		$userrole->insert($data);
		
		if($row['parentId']==0){
			$data=array();
			$data['user_id']=$row['id'];
			$data['role_id']=7; // 7 销售主管 
			$userrole->insert($data);
		}
		
		l("{$i}-user-{$uid}:". get_attrs( $logdata) ,'sync');
		$i++;
	}
	
	
}



#================2. 初始化:kpi_baseinfo  ======================
/*
 * insert into iqg_manage.kpi_baseinfo (kb_shopid,kb_signer_sellerid,kb_holder_sellerid,kb_issuetime,kb_catid)  
 * 				select shopId,signerSellerId,holderSellerId,issueTime,catId from iqg_manage.shop; 
update iqg_manage.kpi_baseinfo  set kb_cityid=(select shop.cityid from iqg_dev.murcielago_shop as shop where shop.shopid= kb_shopid);
update iqg_manage.kpi_baseinfo  set kb_shop_pid=(select shop.parentid from iqg_dev.murcielago_shop as shop where shop.shopid= kb_shopid);
update iqg_manage.kpi_baseinfo  set kb_seller_pid =(select tt.parentid from iqg_manage.seller as tt where tt.id= kb_holder_sellerid);
update iqg_manage.kpi_baseinfo  set kb_seller_level =(select tt.jobtitleid from iqg_manage.seller as tt where tt.id= kb_holder_sellerid);
 * 
 */

$shoplog= new AdminActionLogShop();

$where="";
//$where="where shopId=12156 ";
$rowArr= $shop->getAll("*",$where);
l("total-shop:". count($rowArr),'sync');

$i=1;
$j=0;
foreach ($rowArr as $row){
	$shopid=$row['shopId'];
	
	//检查是否存在？ 增量同步！！！
	if(!Model_Sync::shopExists($shopid)){
		
		$sellerid = $row['holderSellerId'];
		
		$data=array();
		$data['kb_shopid']=$shopid;
		$data['kb_signer_sellerid']=$row['signerSellerId'];
		$data['kb_holder_sellerid']=$sellerid;
		$data['kb_issuetime']=$row['issueTime'];
		$data['kb_catid']=$row['catId']; 
		
		$data['kb_cityid']=$mshop->scalar('cityid',"where shopid= $shopid ");
		$data['kb_shop_pid']=$mshop->scalar('parentid',"where shopid= $shopid ");
		$data['kb_seller_pid']=$seller->scalar('parentid',"where id= $sellerid ");
		$data['kb_seller_level']=$seller->scalar('jobtitleid',"where id= $sellerid ");	
		
		$data['kb_goods_num']= Model_KShop::getCurrGoodsNum($shopid);
		$data['kb_shopname']= Model_Shop::getShop($shopid,"shopName");
		$data['kb_hqname']= Model_Shop::getHQ($data['kb_shop_pid'],"headquarterName");
		
		$base->insert($data);
		
		l("{$i}-shop-{$shopid}:". get_attrs( $data) ,'sync');
		$i++;
	}
	
	
	//从日志同步上下架时间 
	$kb_issuetime = $base->scalar('kb_issuetime',"where kb_shopid=$shopid");
	$ontime = $shoplog->scalar('max(addTime)',"where shopId=$shopid and actionId=3");//actionId=3  上架
	$offtime = $shoplog->scalar('max(addTime)',"where shopId=$shopid and actionId=1");//actionId=1 下架
		
	if($ontime<$kb_issuetime){ //检查上线时间是否有效
		$ontime=$kb_issuetime;
	}	
	if($ontime>=$offtime){ //重新上线
		$offtime=0;
	}	
	 
	//$base->debug();
	//s($kb_issuetime,$ontime,$offtime,'--');
	$data=array();	
	if($ontime>$kb_issuetime){	//重新上线
		$data['kb_issuetime']=$ontime;	
	}	
	$data['kb_offtime']=$offtime;
	$base->update($data,"where kb_shopid=$shopid");	
	
	l("{$j}-shoptime-{$shopid}:". get_attrs( $data) ,'sync');
	$j++;
	
	 
		
	
}



l("sale-sync: end !",'sync');
 



