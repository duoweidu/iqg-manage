<?php

/*
 * 
 * 用途： 汇总本月销售人员提成售信息
 * 
 */

include_once 'inc/header.php';


l("sale-summary: start ...",'kpi');

$base= st('KpiSummary');

$nowTime=  Model_Stats::getCurrTime();
$monthBegin = strtotime(get_date($nowTime,'Y-m-01 00:00:00'));
$monthEnd = strtotime('+ 1 month',$monthBegin);  // 自然月

//获取所有用户
$sellerData= Model_KUser::getSellers();
l("total-num:". count($sellerData),'kpi');

//======prepare===========
$summData=array();
$ksm_year=get_date($nowTime,'Y');
$ksm_month=get_date($nowTime,'m');

$summData['ksm_year']=$ksm_year;
$summData['ksm_month']=$ksm_month;

//规则
$ruleLevel['1']= Model_Rule::getLevelValue(1);
$ruleLevel['2']= Model_Rule::getLevelValue(2);
//规则版本
$ruleVer['1']= Model_Rule::getLevelVersion(1);
$ruleVer['2']= Model_Rule::getLevelVersion(2);

$i=1;

foreach ($sellerData as $user){
	
	
	$uid = $user['uid'];
	$level = $user['level'] ;
	$rule=$ruleLevel[$level];
	
	$summData['ksm_uid'] = $uid ;
	$summData['ksm_islock'] = $user['islock'] ; //是否离职禁用
	$summData['ksm_level']= $user['level'];
	$summData['ksm_rulever']= $ruleVer[$level]; 
	
	
	#---1。----职员 / 经理----------------------------------------------------------	
	$shop_new=Model_Stats::getShopNew($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month); //新签门店
	$shop_lt3m= Model_Stats::getShopLT3M($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);  //未满三个月下线
	$shop_dyxx= Model_Stats::getShopDYXX($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);
	//单店上线时间超过 x月，奖励
	$shop_gtxm= Model_Stats::getShopGTXM($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month, $rule );
	$shop_onsell= Model_Stats::getShopONSELL($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);
	$shop_total= Model_Stats::getShopTotal($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);
	//-----门店数---
	$summData['ksm_shop_new']= count($shop_new);
	$summData['ksm_shop_lt3m']= count($shop_lt3m);
	$summData['ksm_shop_dyxx']= count($shop_dyxx);	
	$summData['ksm_shop_gtxm']= count($shop_gtxm);	
	$summData['ksm_shop_onsell']= $shop_onsell;	
	$summData['ksm_shop_total']= $shop_total;	 
	
	//s($shop_new,1);
	//-----销售提成-----
	$realZB= intval(get_arr_value($rule, 'ZB.XQMD'));
	$ksm_achieve =  $summData['ksm_shop_new'] >= $realZB ? 1 : 0; 
	$summData['ksm_achieve'] = $ksm_achieve ;
	
	
	$summData['ksm_total_money'] = Model_Stats::getTotalMoney($uid,$ksm_year,$ksm_month ,'ks_money_total');
	$summData['ksm_total_money_alipay'] = Model_Stats::getTotalMoney($uid,$ksm_year,$ksm_month ,'ks_money_alipay');
	$summData['ksm_total_money_balance'] = Model_Stats::getTotalMoney($uid,$ksm_year,$ksm_month ,'ks_money_balance');
	$summData['ksm_profit_money'] = 0;
	$summData['ksm_profit_money_alipay'] = 0;
	$summData['ksm_profit_money_balance'] = 0;
	
	//【大于6月数 】 * 【15】
	$money_cqhz = count($shop_gtxm) * get_arr_value($rule,'JL.CQHZ.JE');  
	if($ksm_achieve===1){
		//达标：  (新店 - 本月下线 - 未满3月 ) * 【门店提成 】   +  销售额 * 【5%】   +  【大于6月数 】 * 【15】
		$shopIdRows = array_diff($shop_new, $shop_dyxx, $shop_lt3m );
		$money1 = Model_Stats::getProfitShop($shopIdRows, $rule);  //计算门店提成			
		$summData['ksm_profit_shop'] = $money1 + $money_cqhz;			
		//销售额 * 【5%】 
		$GRTC= get_arr_value($rule,'JL.GRTC')/100;
		$summData['ksm_profit_money'] = floor($summData['ksm_total_money'] * $GRTC);
		$summData['ksm_profit_money_alipay'] = floor($summData['ksm_total_money_alipay'] * $GRTC);
		$summData['ksm_profit_money_balance'] = floor($summData['ksm_total_money_balance'] * $GRTC);
	}else{
		//未达标：   (新店 - 本月下线 - 指标) * 【100 】   -  未满3月 * 【门店提成 】  +  【大于6月数 】 * 【15】
		$money1 = ($summData['ksm_shop_new'] - $summData['ksm_shop_dyxx'] - $realZB ) * get_arr_value($rule,'KF.GRWWC') ;
		//$shop_lt3m= Model_Stats::getShopONSELL2($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);
		$money2 =  Model_Stats::getProfitShop($shop_lt3m, $rule);  //计算未满3月 门店提成
		//s($money2,1);
		$summData['ksm_profit_shop'] = $money1  - $money2  + $money_cqhz;
		$summData['ksm_profit_money'] = 0; //没有了
	}
	
	
	//初始化默认值
	$summData['ksm_total_subordinate']=0;
	$summData['ksm_total_subshop']=0;
	$summData['ksm_profit_subshop']=0;
	$summData['ksm_total_submoney']= 0;
	$summData['ksm_total_submoney_alipay']= 0;
	$summData['ksm_total_submoney_balance']= 0;
	$summData['ksm_profit_submoney']= 0;
	$summData['ksm_profit_submoney_alipay']= 0;
	$summData['ksm_profit_submoney_balance']= 0;
	
	#---2.----经理下属提成----------------------------------------------------------	
	
	if ($level=="2"){		
		#---经理--此时职员级别应该已经处理结束---
		$subrule = $ruleLevel['1'];
		$subordinate =  Model_KUser::getSellers($uid);  //下属人员	
		$subordinateNum=count($subordinate);
		$summData['ksm_total_subordinate'] =  $subordinateNum;
		
		if($subordinateNum>0){
			//下属总指标
			$subRealZB = $subordinateNum * get_arr_value($subrule, 'ZB.XQMD');  
			
			$subid=array();
			foreach ($subordinate as $sub){	 $subid[]=$sub['uid'];	}
			
			$subshop_dyxx= Model_Stats::getCountShopDYXX($subid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);
			$ksm_total_subshop=Model_Stats::getCountShopNew($subid,$monthBegin,$monthEnd,$ksm_year,$ksm_month); //新签门店
			$summData['ksm_total_subshop']= $ksm_total_subshop;  //下属新签门店数
			//下属销售总额
			$summData['ksm_total_submoney'] = Model_Stats::getTotalMoney($subid,$ksm_year,$ksm_month ,'ks_money_total'); 
			$summData['ksm_total_submoney_alipay'] = Model_Stats::getTotalMoney($subid,$ksm_year,$ksm_month  ,'ks_money_alipay'); 
			$summData['ksm_total_submoney_balance'] = Model_Stats::getTotalMoney($subid,$ksm_year,$ksm_month ,'ks_money_balance');  
			//-----销售提成-------------		
			$sub_achieve =  $summData['ksm_total_subshop'] >= $subRealZB ? 1 : 0;				
			$summData['ksm_achieve_sub']=$sub_achieve;
			if($sub_achieve===1){
				//达标：  【1000】 + 销售额 * 【5%】 +  下属销售额 * 【3%】
				$summData['ksm_profit_subshop']= get_arr_value($rule, 'JL.TDWC'); //下属新店：  完成月指标，奖励 
				//计算下属门店提成			 	
				$TDTC = get_arr_value($rule,'JL.TDTC') /100; 		
				$summData['ksm_profit_submoney']= floor($summData['ksm_total_submoney'] * $TDTC );
				$summData['ksm_profit_submoney_alipay']= floor($summData['ksm_total_submoney_alipay'] * $TDTC );
				$summData['ksm_profit_submoney_balance']= floor($summData['ksm_total_submoney_balance'] * $TDTC );
			}else{
				//未达标：   (下属新店 - 本月下线 - 指标) * 【100 】   -  未满3月 * 【门店提成 】  
				$money1 = ($ksm_total_subshop - -$subshop_dyxx - $subRealZB ) * get_arr_value($rule,'KF.TDWWC');
				$subshop_lt3m = Model_Stats::getShopLT3M($uid,$monthBegin,$monthEnd,$ksm_year,$ksm_month);  //未满三个月下线
				$money2 =  Model_Stats::getProfitShop($subshop_lt3m, $subrule);  //计算未满3月 门店提成
				//s($money2,1);
				$summData['ksm_profit_subshop']= $money1 - $money2 ;  //负值 ，扣罚
				$summData['ksm_profit_submoney']= 0;  //没有了
				$summData['ksm_profit_submoney_alipay']= 0;  //没有了
				$summData['ksm_profit_submoney_balance']= 0;  //没有了
			}		
		} 
	 	
	}
	
	
	//总提成
	$summData['ksm_profit_total']= $summData['ksm_profit_shop'] + $summData['ksm_profit_money'] +$summData['ksm_profit_subshop']+$summData['ksm_profit_submoney'];
	
		 
	
	Model_Stats::saveSummary($summData);
	
	
	l("{$i}-sales-{$uid}:". get_attrs($summData)  ,'kpi');	
	$i++;
	
	//var_dump($summData);
	//break;
	 
}



l("sale-summary: end !",'kpi');
 



