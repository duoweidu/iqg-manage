<?php

/**
 * @package Model
 */
class Model_KShop {
	
	
	
	
	public static function isOnSell($kb_offtime) {
		//s(time(), $kb_offtime,1);
		$kb_offtime= intval($kb_offtime);
		if ($kb_offtime > 0) {
			$ret = $kb_offtime <= time () ? 0 : 1;
		} else {
			$ret = 1;
		}
		return $ret;
	}
	
	
	
	public static function getSellDay($start, $end) {
		$end = $end>0 ? $end : time();	
		$diff = $end-$start; //在线时间		
		if($diff>0){
			return floor( $diff/86400);
		}else{
			return 0;
		}
	}
	
	//1支付宝,2余额,3UPS,4PayPal,5SmoovPay,
	public static function getMoney($shopid, $start, $end , $payWay="") {
		
		$ord=st('MurcielagoOrder');
		$ordgood=st('MurcielagoOrderGoods');
		//$ord->debug();
		//1 先取shop orderid
		$tmp=array();
		$ordArr= $ordgood->getAll("orderId","where shopId=$shopid");	
		foreach ($ordArr as $row){
			$tmp[]=$row['orderId'];
		}	
		$orderid = implode(',', $tmp);

		if(empty( $orderid)) {
			return 0;
		}
		//s($orderid,1);
		//2 再汇总order
		$where = "where payStatus=1  and orderStatus in (1,2) and  orderId in ($orderid) ";  //已支付，, 1=正常(已支付未兑换) , 2已领用
		$where .= $payWay!="" ? " and payway=$payWay" : "" ;
		$where .= ' AND  addTime>=' . $start . ' AND addTime<' . $end ;		
		$ret = $ord->scalar("sum(payPrice)", $where);
		
		return $ret ? $ret : 0 ;
	}
	
	
	
	public static function getGoodsTotal($shopid, $start, $end ) {
	
		$ord=st('MurcielagoOrder');
		$ordgood=st('MurcielagoOrderGoods');
		//$ord->debug();
		//1 先取shop orderid
		$tmp=array();
		$ordArr= $ordgood->getAll("orderId","where shopId=$shopid");
		foreach ($ordArr as $row){
			$tmp[]=$row['orderId'];
		}
		$orderid = implode(',', $tmp);
	
		if(empty( $orderid)) {
			return 0;
		}
		//s($orderid,1);
		//2 再汇总order
		$where = "where orderStatus in (1,2) and  orderId in ($orderid) ";  		
		$where .= ' AND  addTime>=' . $start . ' AND addTime<' . $end ;
		$ret = $ord->scalar("count(*)", $where);
	
		return $ret ? $ret : 0 ;
	}
	
	
	//门店商品种数(含即将上线的)  beginDate > time()
	public static function getCurrGoodsNum($shopid) {
		$gshop=st('MurcielagoGoodsShop');
		$goods=st('MurcielagoGoods');
		
		//1 先取 goodsid
		$tmp=array();
		$ordArr= $gshop->getAll("goodsid","where shopId=$shopid");
		foreach ($ordArr as $row){
			$tmp[]=$row['goodsid'];
		}
		$goodsid = implode(',', $tmp);	
		if(empty( $goodsid)) {
			return 0;
		}
		 
		//2 再汇总  g.`isDeleted`=0 AND g.`enable`=1 AND g.`beginDate` <= CURDATE() AND g.`endDate` >= CURDATE()
		$where = "where  goodsId in ($goodsid) ";
		$where .=" AND isDeleted=0 AND  enable=1 ";		
		$where .=" AND endDate >= CURDATE() ";  //	beginDate <= CURDATE() AND 
		$ret = $goods->scalar("count(*)", $where);
	
		return $ret ? $ret : 0 ;
	}
	
	 
	
	public static function saveStats($data) {
		$stat= st('KpiStats');
		$data['ks_utime']=time();
		$where= "where ks_shopid=$data[ks_shopid] and ks_year=$data[ks_year] and ks_month=$data[ks_month]";
		
		$cnt= $stat->getCount($where);
		if($cnt==1){
			$stat->update($data, $where);
		}else{
			$stat->insert($data);
		}
	
	}
	
	
	public static function getBaseByIDS($shopids){
		$base= new KpiBaseinfo();
		//$base->debug();
		$sql= $base->select(array("*"))->where("kb_shopid in ($shopids )");
		return $sql->fetchAll();
	}
	
	
}
	
