<?php

/**
 * @package Model
 */
class Model_KUser {
	 
	 
	
	
	//注意排序： 先职员 ，后经理，以便统计
	public static function getSellers($parentid=null){
		$depid = self::getDepID('销售');
		$mck=__CLASS__.__FUNCTION__.$parentid;
		$ret=mc_get($mck);
		if(!$ret){
			$tab = new AclUserExt (); 
			$where_dep= $parentid ? " and pid ='$parentid' " : " ";			
			$ret = $tab->getAll ( '*', "where depid='$depid' and isdel=0 and level<3 {$where_dep} order by level asc " );
			
			
			foreach ($ret as $k=>$user){
				$islock = st('AclUser')->scalar('islock',"where id='{$user[uid]}'");
				$ret[$k]['islock']=$islock;
				
				if($parentid){ //去除已离职的下属
					if($islock){
						unset($ret[$k]);  //去除离职的用户
					}
				}
			}			
			
			
			unset ( $tab );
			mc_set($mck, $ret, 300);
		}
		return $ret;
	
	}
	 
	
	public static function getDepID($name){
		$mck=__CLASS__.__FUNCTION__ .$name; 
		$depid= mc_get($mck);
		if(!$depid){
			$depid= st('Department')->scalar("department_id","where department_name like '%$name%' and department_condition=0");
			mc_set($mck, $depid);
		}
		return $depid;
	}
	
	 
	
	 
}