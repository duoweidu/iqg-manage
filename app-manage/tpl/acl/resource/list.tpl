{include file="frame/head.tpl"}

<div class="maintop">
<img src="{$_root}img/icon_arrow_right.png" class="icon" /> 权限(ACL)资源列表
</div>

<div class="mainbox">

<table class="tlist" >
	<thead>
		<tr class="title">
			<th align="left">&nbsp;ID</th>
			<th align="left">资源名</th>
			<th align="left">全局资源</th>
			<th align="left">资源说明</th>
			<th align="left">角色权限</th>
			<th align="right">操作&nbsp;</th>
		</tr>  
	</thead>
	<tbody>
		{foreach $resourceList as $resource}
		<tr>
			<td align="left">{$resource.id}</td>
			<td align="left"><a href='resourceedit?id={$resource.id}'><u>{$resource.name}</u></a></td>
			<td align="left">{if $resource.app_id eq '0'}<img src="{$_root}img/icon_right.png" class="icon" />{/if}</td>
			<td align="left">{$resource.description}</td>
			<td align="left">{$resource.role}</td>
			<td align="right">
				<a href="resourceedit?id={$resource.id}">编辑</a>
				{if $alc_sa} | <a href="javascript:$.form.confirm('resourcedel?id={$resource.id}', '确认删除权限资源“{$resource.name}”？');">删除</a>{/if}
			</td>
		</tr>
		{/foreach}
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
				<button type="button" class="btn1s" onclick="javascript:location.href='resourceadd';">新增资源</button>
			</td>
		</tr>
	</tfoot>
</table>



</div>

<div style="alignment-adjust:central">
<pre>
权限控制模型： 应用-角色-资源

第一级显示控制：
1. 可用菜单管理【应用菜单-角色】 ,检查实现在 $page->authenticate();

第二级逻辑控制：
1.控制器【角色->应用页面】属性设置
  相较资源控制，该方法简单高效；适用于角色权限清晰明确的场景。 使用如：protected $check_role = 'XSZG' ，或手动 $page->checkRole('SA,AM');
2.最小粒度的资源管理【资源->(角色-应用)】
  该方法设置略微复杂，但较灵活应对角色权限的变化；适用于模块和角色权限不确定的、需精细控制的场景。 
  使用如：protected $check_res = 'SaleManage' (默认为控制器名) ，模版内$_acl->isAllowed($_admin.role, 'acl_user_passwd')

</pre>
</div>

{include file="frame/foot.tpl"}