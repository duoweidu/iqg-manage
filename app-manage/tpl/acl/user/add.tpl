{include file="frame/head.tpl"}
<link href="{$_root}css/UserShade.css" rel="stylesheet" type="text/css" />
<script src="{$_root}js/Shade.js" language="javascript" type="text/javascript"></script>
<script>
function rdl_doAdd(){
var sText  = $("#DepartmentName").val();
var m = 0;
if(sText == ""){
   alert("请输入新增部门的名称！");
   return false;
}
$("select > option").each(function(){      
      if(this.value == sText && sText != ""){      
      	  m = 1;
      }});
      if(m){
          alert("该部门已经存在，您可以在所属部门的下拉框中选择此部门！");
          return false;
      }{
          var oOption = document.createElement("option");
          document.getElementById("oSelect").options.add(oOption);
          oOption.innerHTML=oOption.Value = sText;
          oOption.selected=true;
          closeWindow();
      }  
}
function validate(){
   var pass=$("#pass").val();
   var oSelectValue = $("#oSelect").val();
   var level = $("input[name='level']:checked").val();
   if(!level){
	   alert('请选择用户级别');		
	   return false;
   }else if(level == 1 ){ //|| level == 2
		var pid = $('#parent td select').val();
		if(!pid){
			alert('请选择直属上级');
			return false;
		}
   }
   {literal}
   var patrn= /^(\w){8,}$/ ;
   {/literal}   
   if(!patrn.exec(pass)){
      alert("密码必须由8位或8位以上的数字和英文字母组成");
      return false ;

   }
   var preg1=/[0-9]/;
   var preg2=/[a-zA-Z]/;
   if(!(preg1.test(pass)&&preg2.test(pass))){
	alert("密码必须同时包含字母和数字！");
	return false;
   }

   if(oSelectValue == ""){
   alert("请选择所属部门！");
   return false;
   }
   $("#frm").submit();
   return true;
}
   $(document).ready(function() {
		$("input[name='level']").click(function(){
			var v = $(this).val();
			if(v == 1 || v == 2){
				$('#parent').show();
			}else{
				$('#parent').hide();
				$('#parent td select option').removeAttr('selected');
			}
		});
  });
</script>
<div class="maintop">
<img src="{$_root}img/icon_arrow_right.png" class="icon" /> 新增用户信息
</div>

<div class="mainbox" style="height:680px">
{include file="frame/error.tpl"}
<form method="post" id="frm">
<table class="titem">
	<tr>
		<td class="field">用户名 *</td>
		<td class="value"><input class="common" type="text" name="name" value="{$user.name}" /></td>
	</tr>
	<tr>
		<td class="field">用户密码 *</td>
		<td class="value"><input class="common" id="pass" type="text" name="pass" value="{$user.pass}" /></td>
	</tr>
        <tr>
		<td class="field">真实姓名 *</td>
		<td class="value"><input class="common" type="text" name="realname" value="{$user.realname}" /></td>
	</tr>
        <tr>
		<td class="field">联系电话 *</td>
		<td class="value"><input class="common" maxlength="16" type="text" name="phone" value="{$user.phone}" /></td>
	</tr>
    <tr>
		<td class="field">电子邮件 *</td>
		<td class="value"><input class="common" type="text" name="email" value="{$user.email}" /></td>
	</tr>
    <tr>
		<td class="field">所属城市 *</td>
		<td class="value">
			{foreach $userCity as $ukey=>$ulist}
			<input type='radio' name='cityid' value='{$ukey}'/> {$ulist}&nbsp;&nbsp;
			{/foreach}
		</td>
	</tr>
     <tr>
		<td class="field">所属部门 *</td>
		<td class="value">
                <select name="department" id='oSelect'>
                <option value=""></option>
                {foreach $alldepartment as $department}
                <option {if $user.department eq $department.department_name}selected="selected"{/if} value = '{$department.department_name}'>{$department.department_name}</option>
                {/foreach}
                </select>{if $alc_sa}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#none" style='color:gray;' onclick="testMessageBox(event);">新增部门</a>{/if}
	</tr>
	<tr>
		<td class="field">用户级别 *</td>
		<td class="value">
			{foreach $userLevel as $ukey=>$ulist}
			<input type='radio' name='level' value='{$ukey}'/> {$ulist}&nbsp;&nbsp;
			{/foreach}
		</td>
	</tr>
	<tr id='parent' style='display:none'>
		<td class="field">直属上级 *</td>
		<td class="value">
		<select id="pid" name="pid" size="15" style="width: 290px;height:100px;">
				{foreach $leaderList as $llist}
				<option value="{$llist.uid}">{$llist.realname}</option>
				{/foreach}
		</select>
		</td>
	</tr>
   
    <tr>
		<td class="field">首次登录强制修改密码 *</td>
		<td class="value"><input  type="radio" name="passedit" value="1" checked="checked"/>&nbsp;是&nbsp;&nbsp;
                <input  type="radio" name="passedit" value="0" />&nbsp;否</td>
	</tr>
        <tr>
		<td class="field">每日可查看的用户信息数 *</td>
		<td class="value"><input class="common" type="text" name="lookup_information_nums" value="{if $user.lookup_information_nums eq 0}0{else}{$user.lookup_information_nums}{/if}"</td>
	</tr>
	<tr>
		<td class="field">角色选择 *</td>
		<td class="value">
			{include file="acl/forms/roles_add.tpl"}
		</td>
	</tr>
	<tr>
		<td class="submit" colspan="2">
			<input type="button" value="提交" onclick="validate();"/>
			<input type="button" value="返回" onclick="javascript:history.go(-1);" />
		</td>
	</tr>
</table>
</form>

</div>

{include file="frame/foot.tpl"}