{include file="frame/head.tpl"}

<div class="maintop">
<img src="{$_root}img/icon_arrow_right.png" class="icon" /> 新增用户信息
</div>

<div class="mainbox">

{include file="frame/error.tpl"}

<form method="post" enctype="multipart/form-data"‎>
<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
<table class="titem">
	<tr>
		<td class="value">用户列表文件(CSV) <input class="common" type="file" name="userfile" value="" /></td>
	</tr>
	<tr>
		<td class="value">* CSV 文件必须包含 3 列 ID USERNAME PASSWORD ISDELETE 间隔为 \t</td>
	</tr>
	{if $errorUserIds}
	<tr>
		<td class="error">请检查以下出错用户的Id：<br/>{$errorUserIds}</td>
	</tr>
	{/if}
	<tr>
		<td class="submit">
			<input type="submit" value="提交" />
			<input type="button" value="返回" onclick="javascript:history.go(-1);" />
		</td>
	</tr>
</table>
</form>

</div>

{include file="frame/foot.tpl"}