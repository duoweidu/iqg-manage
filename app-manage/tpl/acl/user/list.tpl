{include file="frame/head.tpl"}

<div class="maintop">
<img src="{$_root}img/icon_arrow_right.png" class="icon" /> 后台用户列表
</div>

<div class="mainbox">

<table class="tlist" >
	<thead>
		<tr class="title">
			<th align="left" style="width:100px;">&nbsp;ID</th>
			<th align="left" style="width:300px;">用户名&nbsp;<input type="text" class="filter_input" /></th>
            <th align="left">姓名</th>
			<th align="left">所属角色列表</th>
            <th align="left">所属部门</th>
            <th align="left">职位</th>
			<th align="right">操作&nbsp;</th>
		</tr>  
	</thead>
	<tbody>
		{foreach $userList as $user}
		<tr class="filter_rows">
			<td align="left">{$user.id}</td>
			<td align="left" {if $user.islock} class="strike" {/if} ><a href='useredit?id={$user.id}'><u class="filter_name">{$user.name}</u></a></td>
            <td align="left" {if $user.islock} class="strike" {/if} > {$user.realname} </td>
			<td align="left">{$user.role}</td>
            <td align="left">{$user.department}</td>
            <td align="left">{$userLevel[$user.level]}</td>
			<td align="right">
				<a href="useredit?id={$user.id}">编辑</a>
				{if $alc_sa} |
				<a href="javascript:$.form.confirm('userdel?id={$user.id}', '确认删除用户“{$user.name}”？');">删除</a>
				{/if}
			</td>
		</tr>
		{/foreach}
	</tbody>
	{if $_acl->isAllowed($_admin.role, 'acl_user_add')}
	<tfoot>
		<tr>
			<td colspan="4">
				<button type="button" class="btn1s" onclick="javascript:location.href='useradd';">新增用户</button>
				<button type="button" class="btn1s" onclick="javascript:location.href='userimport';">导入用户(CSV)</button>
			</td>
		</tr>
	</tfoot>
	{/if}
</table>

{*include file="frame/page.tpl"*}

</div>

<script type="text/javascript">
$.form.filter({
	filter_input : $('.filter_input'),
	filter_rows : $('.filter_rows'),
	filter_val : $('.filter_name')
});
</script>

{include file="frame/foot.tpl"}