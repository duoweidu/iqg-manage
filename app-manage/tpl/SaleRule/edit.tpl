{include file="frame/head_vm.tpl"}


<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 销售部</a></li>
  <li class="active" >修改提成管理</li>
</ol>



 
<div class="mainbox" style="height: 680px">

{include file="frame/error.tpl"}

<form method="post" id="frm">
<table class="table ">

	<tr>
		<td class="field" align="right"  width="15%">职位： </td>
		<td class="value">
        {foreach $userLevel as $ukey=>$ulist} 
        <input type='radio' {if $level eq $ukey}checked='checked' {/if} name='kr_level' value='{$ukey}' 
        	onclick="javascript:ChangeLevel('{$ukey}')" /> {$ulist}&nbsp;&nbsp; 
        {/foreach}
        &nbsp;&nbsp; (规则版本号： {$version}  ， 更新时间： {$verdate})
        </td>
	</tr>
	 
    
	 
    <tr>
		<td class="field" align="right" >新店： </td>
		<td class="value">1 . 指标 <input class="common" type="text" name="rule[ZB][XQMD]" id="ZB_XQMD" value="{$rule.ZB.XQMD|default:0}" /> 家门店/月</td>
	</tr>
    
     <tr>
		<td class="field" >  </td>
		<td class="value">
        
        	<table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
            <th>编号</th>
            <th>分类</th>
            <th>奖励(元/店)</th>
            </tr>
            </thead>
            <tbody>
            
            {foreach $catidOpts as $key=>$value}
            <tr>
            <td>{$key}</td>
            <td>{$value}</td>
            <td><input class="common" type="text" name="rule[JL][MDFL][{$key}]" id="JL_MDFL_{$key}" value="{$rule['JL']['MDFL'][$key]}" /></td>
            </tr>
            {/foreach}
             
            </tbody>
            </table>
			 
		</td>
	</tr>
     <tr>
		<td class="field"> </td>
		<td class="value">2 . 未完成指标扣 <input class="common" type="text" name="rule[KF][GRWWC]" id="KF_GRWWC" value="{$rule.KF.GRWWC}" /> 元/店</td>
	</tr>
    
    {if $level=="1"}{/if}
	<tr>
		<td class="field" align="right">老店：</td>
		<td class="value">单店上线时间超过<input class="common" type="text" name="rule[JL][CQHZ][YS]" id="JL_CQHZ_YS" value="{$rule.JL.CQHZ.YS}"   />月，奖励
        <input class="common" type="text" name="rule[JL][CQHZ][JE]" id="JL_CQHZ_JE" value="{$rule.JL.CQHZ.JE}"   /> 元/店 
        </td>
	</tr>
    
    
    
   
	
	<tr>
		<td class="field" align="right">销售提成： </td>
		<td class="value"><input class="common" maxlength="16" type="text" name="rule[JL][GRTC]" id="JL_GRTC" value="{$rule.JL.GRTC}" />% </td>
	</tr>
    
    {if $level=="2"}
    <tr>
		<td class="field" align="right">下属销售提成： </td>
		<td class="value"><input class="common" maxlength="16" type="text" name="rule[JL][TDTC]" id="JL_TDTC" value="{$rule.JL.TDTC}" />% </td>
	</tr>
    {/if}
    
    {if $level=="2"}
    <tr>
		<td class="field" align="right">下属新店： </td>
		<td class="value">1 . 完成月指标，奖励 <input class="common" type="text" name="rule[JL][TDWC]" id="JL_TDWC" value="{$rule.JL.TDWC}" /> 元</td>
	</tr>
     <tr>
		<td class="field">  </td>
		<td class="value">2 . 未完成月指标，扣 <input class="common" type="text" name="rule[KF][TDWWC]" id="KF_TDWWC" value="{$rule.KF.TDWWC}" />  元/店</td>
	</tr>
    {/if}
   
	<tr>
        <td class="field"> </td>
		<td class="submit"  ><input type="button" value="提交" onclick="validate();" /> <input type="button" value="返回" onclick="javascript:goback()" /></td>
	</tr>
</table>
</form>

</div>


<script>

function goback(){
	location.href="./index";
}

function ChangeLevel(level){
	location.href="./edit?level="+level;
}

function validate(){
	
	var ready=true;
	
	$('#frm input[type="text"]').each(function(index, element) {
        
		var v= $(element).val();
		$(element).parent().parent().css('color','');
		$(element).focus();
		
		if(v==''){ //不是数字
			$(element).parent().parent().css('color','red');
			alert('请输入数字');
			ready=false;
			return false;
		}
		
		if(isNaN(v)){ //不是数字
			$(element).parent().parent().css('color','red');
			alert('请输入数字');
			ready=false;
			return false;
		}
		
    });
	
	if(ready){
		$('#frm').submit();
	}
	
	
}




</script>

{include file="frame/foot.tpl"}
