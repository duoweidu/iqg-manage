{include file="frame/head_vm.tpl"}
 

<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 销售部</a></li>
  <li class="active">提成管理</li>
</ol>





<div class="mainbox">

 
 



<table class="table table-bordered" >
	<thead>
		<tr class="title">
			<th align="left" >&nbsp;职位</th>
			<th align="left" >新店提成</th>
            <th align="left">老店提成</th>
            <th align="left">销售提成</th>
			<th align="left">下属销售提成</th>
             
			<th align="right">操作&nbsp;</th>
		</tr>  
	</thead>
	<tbody>
		 
		<tr >
			<td>业务员      <br/>  <br/>版本号：{$V1}
            </td>
			<td>
            	<ul>
                <li>个人指标：  {$L1.ZB.XQMD}  家</li>
                 <li>--------------------------</li>
                {foreach $catidOpts as $key=>$value}
                <li>{$value} ：  {$L1['JL']['MDFL'][$key]}  元/店</li>
                {/foreach}
                <li>--------------------------</li>
                <li>未完成： - {$L1.KF.GRWWC}   元/店</li>
                
                </ul>
            </td>
			<td valign="middle">超过  {$L1.JL.CQHZ.YS} 个月， {$L1.JL.CQHZ.JE} 元/店</td>
            <td valign="middle">{$L1.JL.GRTC} % </td>
			<td valign="middle">无</td>
           
			<td valign="middle" >
				<a href="edit?level=1">编辑</a>
			</td>
		</tr>
        
        
        <tr >
			<td>主管经理	 <br/> <br/>版本号：{$V2}</td>
			<td>
            	<ul>
                <li>个人指标：  {$L2.ZB.XQMD}  家</li>
                <li>团队完成：  {$L2.JL.TDWC}  元</li>
                <li>--------------------------</li>
                {foreach $catidOpts as $key=>$value}
                <li>{$value} ：  {$L2['JL']['MDFL'][$key]}  元/店</li>
                {/foreach}
                <li>--------------------------</li>
                <li>未完成： - {$L2.KF.GRWWC}   元/店</li>
                
                </ul>
            </td>
			<td valign="middle">
            {if  $L2.JL.CQHZ.JE==0}
            	无
            {else}
           		超过  {$L2.JL.CQHZ.YS} 个月， {$L2.JL.CQHZ.JE} 元/店
            {/if}	
             </td>
            <td valign="middle">{$L2.JL.GRTC} % </td>
			<td valign="middle">{$L2.JL.TDTC} % </td>
           
			<td >
				<a href="edit?level=2">编辑</a>
				 
			</td>
		</tr>
		
	</tbody>
	
</table>

 

</div>

 

{include file="frame/foot.tpl"}