{include file="frame/head_vm.tpl"  ngcontroller="$_page"}
   


<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 销售部</a></li>
  <li class="active" > 销售个人报表 </li>
</ol>
 

 
<div class="mainbox" >
 
 <h4> Hi {$ext.realname}   <small  style="padding-left:20px"> 地区: {$ext.cityid2} ， 职位: {$ext.level2} ， 上次登录: {$user.last_login2} ， 
 当前规则版本：{$version} </small> </h4> 
 <input type="hidden" ng-init="userid='{$ext.uid}'" />  
 <input type="hidden" ng-model="userid" />
 
 <input type="hidden" ng-init="level='{$ext.level}'" />  
 <input type="hidden" ng-model="level" />

 <hr/>
 
{literal}
<div class="search" >
	 
     <div class="title"> </div>	
    
    
    <select ng-model="selectYearmonth"  ng-options="m.name for m in yearmonthModel" >
	  </select> 
       
    <select ng-model="selectPayway"  ng-options="m.name for m in paywayModel"  >
    	<option value="">所有支付方式</option>
	  </select> 
      
	<input type="button" class="btn-large" value="搜索"  id="btn_search" ng-click="refresh($event)"/>
     
     
</div>

<br/>
 
 
<h5> 个人指标{{summ.ksm_achieve=='1' ? '已' : '未'}}完成，本月总奖金额：  {{summ.my_total}} 元 
 <small style="padding-left:20px">  (  销售额提成: {{summ.my_money}} ， 门店提成： {{summ.my_shop}}   
 
 <span ng-show="level==2">， 团队指标{{summ.ksm_achieve_sub=='1' ? '已' : '未'}}完成 </span>  ， 汇总时间： {{summ.ksm_utime}} 
 ，  汇总规则版本： {{summ.ksm_rulever}} ) </small>
  </h5> 
 
<!-- {{query | json}}  -->
 
<my-tabs>
      <my-pane title="本月门店" type="dymd">
      
      		<table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr> <th>编号</th> <th>分店名称</th> <th>商户类型</th> <th>上线时间</th> <th>下线时间</th> <th>奖励奖金</th></tr>
            </thead>
            <tbody>             
            <tr ng-repeat="row in tab_dymd">
                <td>{{row.kb_shopid}}</td>
                <td>{{row.shopname}}</td>
                <td>{{row.catname}}</td>
                <td>{{row.kb_issuetime}}</td>
                <td>{{row.kb_offtime}}</td>
                <td>{{row.profit}}</td>
            </tr> 
            <tr >
                <td colspan="5" align="right">合计 </td>
                <td> {{getTotal('dymd','profit')}} </td>
            </tr>                 
            </tbody>
            </table>
       
      </my-pane>
       <my-pane title="本月销售" type="dyxs">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr> <th>编号</th> <th>总部名称</th> <th>商户类型</th> <th>销量</th> <th>销售额</th> <th>奖金</th></tr>
            </thead>
            <tbody>             
            <tr ng-repeat="row in tab_dyxs">
                <td>{{row.kb_shop_pid}}</td>
                <td>{{row.hqname}}</td>
                <td>{{row.catname}}</td>
                <td>{{row.gtotal}}</td>
                <td>{{row.mtotal}}</td>
                <td>{{row.profit}}</td>
            </tr>   
            <tr >
                <td colspan="3" align="right">合计 </td>
                <td> {{getTotal('dyxs','gtotal')}} </td>
                <td> {{getTotal('dyxs','mtotal')}} </td>
                <td> {{getTotal('dyxs','profit')}} </td>
            </tr>   
            </tbody>
            </table>
           
      </my-pane>
      <my-pane title="下属新店提成" type="xsxdtc" visible="{{level==2}}">
         <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr> <th>编号</th> <th>下属</th> <th>总部名称</th> <th>门店数</th>  <th>商户类型</th> <th>当月上线时间</th> </tr>
            </thead>
            <tbody>             
            <tr ng-repeat="row in tab_xsxdtc">
                <td>{{row.kb_holder_sellerid}}</td>
                <td>{{row.username}}</td>
                <td>{{row.hqname}}</td>
                <td>{{row.shopnum}}</td>
                <td>{{row.catname}}</td>
                <td>{{row.issuetime}}</td>
            </tr>   
            <tr >
                <td colspan="3" align="right">合计 </td>
                <td colspan="3"> {{getTotal('xsxdtc','shopnum')}} </td>
               
            </tr>   
            </tbody>
            </table>
      </my-pane>
      <my-pane title="下属销售提成" type="xsxstc" visible="{{level==2}}">
       	<table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr> <th>编号</th> <th>下属</th> <th>总部名称</th> <th>商户类型</th> <th>销量</th> <th>销售额</th> <th>奖金</th></tr>
            </thead>
            <tbody>             
            <tr ng-repeat="row in tab_xsxstc">           		
                <td>{{row.kb_holder_sellerid}}</td>
                <td>{{row.username}}</td>
                <td>{{row.hqname}}</td>
                <td>{{row.catname}}</td>
                <td>{{row.gtotal}}</td>
                <td>{{row.mtotal}}</td>
                <td>{{row.profit}}</td>
            </tr>   
            <tr >
                <td colspan="4" align="right">合计 </td>
                <td> {{getTotal('xsxstc','gtotal')}} </td>
                <td> {{getTotal('xsxstc','mtotal')}} </td>
                <td> {{getTotal('xsxstc','profit')}} </td>
            </tr>   
            </tbody>
            </table>
      </my-pane>
      <my-pane title="未满3月下线" type="lt3m">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr> <th>编号</th> <th>分店名称</th> <th>商户类型</th> <th>上线时间</th> <th>下线时间</th> </tr>
            </thead>
            <tbody>             
            <tr ng-repeat="row in tab_lt3m">
                <td>{{row.kb_shopid}}</td>
                <td>{{row.shopname}}</td>
                <td>{{row.catname}}</td>
                <td>{{row.kb_issuetime}}</td>
                <td>{{row.kb_offtime}}</td>
            </tr>   
            </tbody>
            </table>
      </my-pane>
 </my-tabs>
 
 
  <input type="button" class="btn-large" value="导出Excel"  id="btn_search" ng-click="dowload($event)"/>
      

{/literal}


<script>
          angular.element(document).ready(function(){   
			// $('#btn_search').click();
			 //console.log($('#btn_search'));
          });
 </script>
    

</div>

{include file="frame/foot.tpl"}
