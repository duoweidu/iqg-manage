<table width="100%" border="1" style="margin-top: 15px; font-size: 12px;" cellpadding="0" cellspacing="0">
	<tr height="30">
		<td colspan="4">&nbsp;&nbsp;<b>操作记录</b></td>
	</tr>
	<tr align="center">
		<td height="30"><strong>操作用户</strong></td>
		<td height="30"><strong>操作内容</strong></td>
		<td height="30"><strong>操作时间</strong></td>
		<td height="30"><strong>操作备注</strong></td>
	</tr>
	{foreach $oploglist as $p}
	<tr align="center">
		<td height="40">{$p.eo_username}</td>
		<td height="40">{$p.eo_action}</td>
		<td height="40">{"Y-m-d H:i:s"|date:$p.eo_createtime}</td>
		<td height="40">{$p.eo_memo}</td>
	</tr>
	{/foreach}
</table>
