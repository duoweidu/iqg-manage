<!doctype html>
<html ng-app="{$ngapp|default:"myApp"}"  {if $ngcontroller}  ng-controller="{$ngcontroller}"  {/if}>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage System</title>
<link href="{$_root}css/main.css" rel="stylesheet" type="text/css" />

<script src="{$_root}js/string.js" language="javascript" type="text/javascript"></script>

<!-- 最新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="{$_root}vm/bootstrap/3.0.3/css/bootstrap.min.css">
<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet" href="{$_root}vm/bootstrap/3.0.3/css/bootstrap-theme.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="{$_root}vm/bootstrap/jquery-1.11.1.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="{$_root}vm/bootstrap/3.0.3/js/bootstrap.min.js"></script>
 
<script src="{$_root}js/ifrw.js" language="javascript" type="text/javascript"></script>
<script src="{$_root}js/common.js" language="javascript" type="text/javascript"></script>

<link rel="stylesheet" href="{$_root}vm/bootstrap/fix.css">

{if $ngcontroller} 
 <script src="/vm/angular/angularjs-1.3.0-beta.js"></script> 
 <script src="/vm/controller/{$ngcontroller}.js"></script>  
{/if}

 
 
<script>
function visit(url){
	location.href=url;
}
</script>
	
</head>
<body>
<div class="main" >
{include file="frame/tab_left.tpl"}



