
{assign var="curr_page" value="1"}
{if $smarty.request.currpage =="" || $smarty.request.currpage < 1}
		{assign var="curr_page" value="1"}
{elseif $smarty.request.currpage > $total_page}
		{assign var="curr_page" value=$total_page}
{else}
		{assign var="curr_page" value=$smarty.request.currpage}
{/if}

{if $smarty.request.direction =="" || $smarty.request.direction =="asc"}
	{assign var="direction" value="desc"}
{else}
	{assign var="direction" value="asc"}
{/if}

<style type="text/css" media="all">
@import "/css/x/xcomm.css";
@import "/css/x/xstyle.css";
</style>
	
	<input name="selectedData" id="selectedData" type="hidden" value="{$pk_field_value}" />
	
	<div  class="list_table">	 
    
 	<table class="tabular" id="tabular" cellspacing="0" summary="Class Schedule" width="98%">	
    <tr >
		{if $pk_field} <th  >&nbsp;</th>	{/if}	
		{foreach key=key item=item from=$fields}
			
			{if $nosort == false && $item.nosort == false}
				 <th   {$item.style} >	
					{if $smarty.request.orderby == $key}
						<a href="javascript:list_tpl_orderby('{$key}','{$direction}')" class="sort{$direction}">&nbsp;{$item.title}</a>
					{else}
						<a href="javascript:list_tpl_orderby('{$key}','{$direction}')" class="nosort">&nbsp;{$item.title}</a>
					{/if}
				 </th>
			{else}
				<th  {$item.style}>&nbsp;{$item.title}</th>
			{/if}
		{/foreach}
	</tr>
{if !$rows}
	 {assign var="fields_count" value=$fields|@count}
     {assign var="fields_count" value=$fields_count+1}
	<tr><td colspan="{$fields_count}" >暂无相关记录</td>
    </tr>
{else}
    <!--start loop row-->	 
    {section name=row loop=$rows}
        
        {assign var="pk_value" value=$rows[row][$pk_field]}
         
        <tr  onclick="javascript:list_tpl_set_row('row{$pk_value}');" >
        {if $pk_field}
            <td  width="10px">	
            {if $type == "checkbox"}
                <input name="{$pk_field}[{$pk_value}]" id="row{$pk_value}" type="checkbox" class="radiobox" value="{$pk_value}"		
                onclick="javascript:list_tpl_set_row('row{$pk_value}');"
                {if $pk_field_value[$pk_value] == $pk_value} checked{/if}	{$rows[row].style[$pk_field]} />	
            {else}
                <input name="{$pk_field}" id="row{$pk_value}" type="radio" class="radiobox" value="{$pk_value}"
                onclick="javascript:list_tpl_set_row('row{$pk_value}');"
                {if $pk_field_value == $pk_value} checked{/if}		{$rows[row].style[$pk_field]} />
            {/if}
            </td>
        {/if}
            {assign var="field_index" value=0}
            {foreach key=key item=item from=$fields}
                {assign var="field_index" value=$field_index+1}
               
		<td {$rows[row].style[$key]}><SPAN id="row{$pk_value}_{$field_index}"  >{$rows[row][$key]|default:'-'}</SPAN>&nbsp;</td>
            {/foreach}	  
        </tr>
    {/section}
    <!--end loop row-->	
{/if}	 
 	</table>
	 </div>

{if $nopager == false  && $total_page > 1}
	<div class="pagger">
    共有{$total_rows}条，
    {if $curr_page != 1}
    [<a href="javascript:list_tpl_change2page(1)">首页</a>]
    [<a href="javascript:list_tpl_change2page({math equation="(x - y)" x=$curr_page y=1})">上一页</a>]
    {/if}  
    
    {section name=loop start=$curr_page-2 loop=$curr_page}   
    {if $smarty.section.loop.index >= 1}
    	[<a href="javascript:list_tpl_change2page({$smarty.section.loop.index})">{$smarty.section.loop.index}</a>]
    
    {/if}
    {/section}    
    
   [ {$curr_page} ]
    
    {section name=loop2 start=$curr_page+1 loop=$curr_page+3}   
    {if $smarty.section.loop2.index <= $total_page}
    	[<a href="javascript:list_tpl_change2page({$smarty.section.loop2.index})">{$smarty.section.loop2.index}</a>]
    
    {/if}    
    {/section}
    
     
    	
	{if $curr_page != $total_page}
    [<a href="javascript:list_tpl_change2page({math equation="(x + y)" x=$curr_page y=1})">下一页</a>]
    [<a href="javascript:list_tpl_change2page({$total_page})">尾页</a>]
    {/if}
    
     当前第
     <!--{literal}
     <select name="page" id="page">
    	{section name=loop loop=$total_page}
			<option value="{$smarty.section.loop.index+1}" {if $curr_page == $smarty.section.loop.index+1} selected="selected" {/if}> {$smarty.section.loop.index+1}</option> 
		{/section}
    </select>
    {/literal}-->
    <input name="page" id="page" type="text" onfocus="javascript:this.select();" onkeypress="javascript:list_tpl_check_key(event);" value="{$curr_page}" size="1" maxlength="3" style="width:30px"/>
    /{$total_page}页
    
    <input type="button" value="Go" class="btn" onclick="javascript:list_tpl_change2page(document.getElementById('page').value)"/>
	</div>
{/if}

	
	<script language="javascript">
	//alert(document.getElementById('page').value);
	    var list_tpl_form,list_tpl_checkboxName="{$pk_field}[";
		
		window.onload=function()
		{
			list_tpl_form={$submit_form|default:"document.getElementById('search')"};
		}
		

		 //alert($("#tabular tr").length);
		
		 var tabular_tr_index=0;		
		$("#tabular tr:odd").attr('class','odd_out');	
		$("#tabular tr:even").attr('class','even_out');		
		$("#tabular tr:even").hover(function(){
			tabular_tr_index= $("#tabular tr").index( $(this));
			 if(tabular_tr_index>0)
			  $(this).attr('class','even_over');	  
			},
			function(){
			 $(this).attr('class','even_out');
			}); 
		

		$("#tabular tr:odd").hover(function(){
			  $(this).attr('class','odd_over');
			},
			function(){
			 $(this).attr('class','odd_out');	
			}); 
		
		 
		
		function list_tpl_set_row(id)
		{		
			try
			{
				var currunt=document.getElementById(id).checked;	//false;//
				document.getElementById(id).checked=!currunt;
				if(!currunt){
					document.getElementById('selectedData').value=document.getElementById(id).value;
				}else{
					document.getElementById('selectedData').value="";
				}
			}catch(err)
			{
			}
		}

		function list_tpl_checkedCount(checkBoxName)
	    {
			var cnt=0;
			var frm=document.getElementById('selectedData').form;
			
			if(checkBoxName==null || checkBoxName=="" || checkBoxName=="undefined" )		
					checkBoxName=list_tpl_checkboxName;
			//alert(checkBoxName);
			for (var i=0; i < frm.elements.length; i++)
			if (frm.elements[i].name.indexOf(checkBoxName) !=-1 )
			{  
				 if(frm.elements[i].checked)	cnt++;			 
			}	 
			return cnt;
	   }
		
		function list_tpl_check_key(event)
		{
			//alert(event.srcElement.value);
			event=window.event||event;  
			element=event.srcElement||event.target;  
			 
			if(event.keyCode==13 || (event.keyCode > 47 && event.keyCode < 58) )
			{
				if(event.keyCode==13)list_tpl_change2page(element.value);
				//alert(event.keyCode);
				
			}else
			{
				event.returnValue=false
			}
		
		}
		
		function list_tpl_change2page(val)
	{
			list_tpl_form.{$currpage_field|default:"currpage"}.value=val;
			list_tpl_form.submit();
	}
		 
		
		
		function list_tpl_orderby(field,direct)	 
	{
			list_tpl_form.{$orderby_field|default:"orderby"}.value=field;
			list_tpl_form.{$direction_field|default:"direction"}.value=direct;
			list_tpl_form.submit();
	}
	
		function list_selected_data()
	{
			return document.getElementById('selectedData').value;
	}		
	</script>
 