{include file="frame/head_vm.tpl"}

<style type="text/css" media="all">
.modal-dialog {
    margin: 120px auto;
    width: 600px;
}
</style>



<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 系统提示 </a></li>
   
</ol>


<div class="mainbox">



<div class="modal show">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">系统提示</h4>
      </div>
      <div class="modal-body">
        	<div class="alert alert-{$message_type}">{$message}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"  onclick="javascript:goback()">确定</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--
 
<div class="alert alert-{$message_type|default:'info'}">{$message}</div>
 
<button type="button" class="btn btn-default" onclick="javascript:goback()">返回</button>

-->




<script>

var url="{$message_url}";

function goback(){
	if(url==""){
		history.go(-1);
	}else{
		location.href=url;
	}
	
}

</script>



 

</div>

 

{include file="frame/foot.tpl"}