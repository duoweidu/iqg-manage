{include file="frame/head_vm.tpl"}
 

<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 市场部</a></li>
  <li class="active">生成验证码</li>
</ol>





<div class="mainbox">


 
  <div class="box-body" style="padding-left:20px">
                                                    <!--validation-->
                                                    <form id="form-validate" class="form-horizontal" novalidate="novalidate" method="post">
                                                        <fieldset>
                                                            <legend>生成10位验证码</legend>
                                                            <div class="control-group">
                                                                <label for="required" class="control-label">请输入两个字符作为前缀，如：AA</label>
                                                                <div class="controls">
                                                                    <input type="text" id="prefix" name="prefix" 
                       data-validate="{ required: true, minlength: 2,maxlength: 2, messages:{ required:'请输入两个字符' } }" class="grd-white text-error helper-font-small"  value="{$prefix}">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="control-group">
                                                                <label for="date" class="control-label">有效日期 </label>
                                                                <div class="controls">
                                                                    <input type="text" id="start" name="start" 
                                                                    data-validate="{ required: true, dateISO:true, messages:{ required:'Please enter field date', date:'Please enter a valid date'}}" class="grd-white text-error helper-font-small" value="{$start}" > 
                                                                    到
                                                                    <input type="text" id="end" name="end" 
                                                                    data-validate="{ required: true, dateISO:true, messages:{ required:'Please enter field date', date:'Please enter a valid date'}}" class="grd-white text-error helper-font-small" value="{$end}"> 
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                             <div class="control-group">
                                                                <label for="number" class="control-label">数量，理论最大6W</label>
                                                                <div class="controls">
                                                                    <input type="text" id="number" name="number" data-validate="{ required: true, number:true, max: 999999, messages:{ required:'必填项', number:'请填写数字' } }" class="grd-white text-error helper-font-small" value="{$number|default:1000}"> 
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="control-group">
                                                                <label for="number" class="control-label">起始数值，可用于分批处理</label>
                                                                <div class="controls">
                                                                    <input type="text" id="startNum" name="startNum" data-validate="{ required: true, number:true, max: 60000, messages:{ required:'必填项', number:'请填写数字' } }" class="grd-white text-error helper-font-small" value="{$startNum|default:1}"> 
                                                                </div>
                                                            </div>
           {literal}                                                 
                                                            <!--
                                                            <div class="control-group">
                                                                <label for="minlength" class="control-label">Min Length</label>
                                                                <div class="controls">
                                                                    <input type="text" id="minlength" name="minlength" data-validate="{required: true, minlength: 2, messages:{required:'Please enter field min length', minlength:'Please enter at least 2 characters.'}}" class="grd-white text-error helper-font-small">
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="maxlength" class="control-label">Max Length</label>
                                                                <div class="controls">
                                                                    <input type="text" id="maxlength" name="maxlength" data-validate="{required: true, maxlength: 6, messages:{required:'Please enter field max length', maxlength:'Please enter a maximum of 6 characters.'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="email" class="control-label">Email</label>
                                                                <div class="controls">
                                                                    <input type="text" id="email" name="email" data-validate="{required: true, email:true, messages:{required:'Please enter field email', email:'Please enter a valid email address'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="url" class="control-label">URL</label>
                                                                <div class="controls">
                                                                    <input type="text" id="url" name="url" data-validate="{required: true, url:true, messages:{required:'Please enter field url', url:'Please enter a valid url'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="date" class="control-label">Date</label>
                                                                <div class="controls">
                                                                    <input type="text" id="date" name="date" data-validate="{required: true, date:true, messages:{required:'Please enter field date', date:'Please enter a valid date'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="mins" class="control-label">Min</label>
                                                                <div class="controls">
                                                                    <input type="text" id="mins" name="mins" data-validate="{required: true, number:true, min: 5, messages:{required:'Please enter field min', number:'Please enter a valid number', min:'Please enter a number greater than or equal to 5'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="maxs" class="control-label">Max</label>
                                                                <div class="controls">
                                                                    <input type="text" id="maxs" name="maxs" data-validate="{required: true, number:true, max: 5, messages:{required:'Please enter field max', number:'Please enter a valid number', min:'Please enter a number less than or equal to 5'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="number" class="control-label">Number</label>
                                                                <div class="controls">
                                                                    <input type="text" id="number" name="number" data-validate="{required: true, number:true, messages:{required:'Please enter field number', number:'Please enter a valid number'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="password" class="control-label">Password</label>
                                                                <div class="controls">
                                                                    <input type="password" id="password" name="password" data-validate="{required: true, messages:{required:'Please enter field password'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label for="cpassword" class="control-label">Confirm Password</label>
                                                                <div class="controls">
                                                                    <input type="password" id="cpassword" name="cpassword" data-validate="{required: true, equalTo: '#password', messages:{required:'Please enter field confirm password', equalTo: 'confirmation password does not match the password'}}" class="grd-white text-error helper-font-small"> 
                                                                </div>
                                                            </div>
                                                            --> {/literal}
                                                            <div class="form-actions">
                                                                <button class="btn btn-primary" type="submit">生成验证码</button>
                                                               
                                                            </div>
                                                            
                                                        </fieldset>
                                                    </form><!--/validation-->
                                                </div>




</div>

<script src="/js/validate/jquery.validate.js"></script>
<script src="/js/validate/jquery.metadata.js"></script>
<script>
 // validate form
                $('#form-validate').validate();
</script>
 

{include file="frame/foot.tpl"}