{include file="frame/head.tpl"}
<script>
function validate(){
    var pass=$("#pass").val();
    if($("#oldpass").val()==""){
	alert("请输入原密码！");
	return false;
    }
    if(pass==""){
	alert("请输入新密码！");
	return false;
    }
   {literal}
   var patrn= /^(\w){8,}$/ ; 
   {/literal}
   if(!patrn.exec(pass)){
      alert("密码必有由8位或8位以上的数字和英文字母组成");
      return     false ;
   }
   if(!patrn.exec(pass)){
      alert("密码必有由8位或8位以上的数字和英文字母组成");
      return     false ;
   }
   var preg1=/[0-9]/;
   var preg2=/[a-zA-Z]/;
   if(!(preg1.test(pass)&&preg2.test(pass))){
	alert("密码必须同时包含字母和数字！");
	return false;
   }
   if($("#pass").val()!=$("#passwords").val()){
    	alert("两次密码输入不一致！");
	return false;
    }
    $("#frm").submit();
    return true;
}
</script>
<div class="maintop">
<img src="{$_root}img/icon_arrow_right.png" class="icon" /> {if $user.passedit}<font color='red'>第一次登录系统需要修改您的默认密码</font>{else}个人信息修改{/if}
</div>

<div class="mainbox">

{include file="frame/error.tpl"}

<form method="post" id="frm">
<table class="titem" >
	<tr>
		<td class="field">用户名 *</td>
		<td class="value">{$user.name} <input class="common" type="hidden" name="name" value="{$user.name}" readonly /></td>
	</tr>
	<tr>
		<td class="field">原密码</td>
		<td class="value"><input class="common" type="password" name="oldpass" value="" /></td>
	</tr>
	<tr>
		<td class="field">新密码</td>
		<td class="value"><input class="common" type="password" id="pass" name="pass" value="" /><font color="#acacac">密码必有由8位或8位以上的数字和英文字母组成</font></td>
	</tr>
	<tr>
		<td class="field">确认密码</td>
		<td class="value"><input class="common" type="password" id="passwords" name="passwords" value="" /></td>
	</tr>
	<tr>
		<td class="submit" colspan="2">
			<input type="button" value="提交" onclick="validate();" />
			<input type="button" value="返回" onclick="javascript:history.go(-1);" />
		</td>
	</tr>
</table>
</form>

</div>

{include file="frame/foot.tpl"}