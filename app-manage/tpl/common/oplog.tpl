<table width="90%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF" 
style="margin-top:10px; font-size:12px; " >
    
    <tr align="center"> 
      <td >
      	<strong>操作用户</strong>      
      </td>
      <td >
      	<strong>操作内容</strong>      
      </td>
      <td >
      	<strong>操作时间</strong>      
      </td>
      <td >
      	<strong>操作备注</strong>      
      </td>
    </tr>
    {foreach $oploglist as $p}
     <tr align="center">
	<td >{$p.to_username}</td>
        <td >{$p.to_action}</td>        
        <td >{"Y-m-d H:i:s"|date:$p.to_createtime}</td>
        <td >{$p.to_memo}</td>
     </tr>
    {/foreach}
  </table>