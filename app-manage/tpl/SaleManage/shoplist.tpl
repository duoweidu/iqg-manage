
{include file="frame/head_vm.tpl"}
 

<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 销售部</a></li>
  <li class="active" >门店管理</li>
</ol>


 
<div class="mainbox"> 


<div class="search" >
	 
     <div class="title"> </div>	
      <select name="s_cityid" id="s_cityid" >	 
     		<option value="">所有城市</option>
	 		{html_options options=$cityidOpts selected=$smarty.request.cityid}
	  		</select>
            
       <select name="s_catid" id="s_catid" >	 
     		<option value="">所有分类</option>
	 		{html_options options=$catidOpts selected=$smarty.request.catid}
	  		</select>
            
       <select name="s_shoppid" id="s_shoppid" >	 
     		<option value="">所有总店</option>
	 		{html_options options=$shoppidOpts selected=$smarty.request.shoppid}
	  		</select>
            
      <select name="s_sellerid" id="s_sellerid" >	 
     		<option value="">所有销售</option>
	  		{html_options options=$selleridOpts selected=$smarty.request.sellerid}
	  		</select>
     
      <select  id="my_sellerid" style="display:none" >	
	  		{html_options options=$selleridOpts selected=$smarty.request.sellerid}
	  		</select>
      
     
        <!--
       <div class="title">状态：</div>
      {html_radios  id=s_status  name=s_status options=$statusOpts selected=$request.status|default:1}
        -->
      
	<input name="search2" type="button" class="btn-large" onClick="javascript:dosearch(false)" value="搜索" />
     
     
	<script>
    function dosearch(tocsv){
		var frm=document.getElementById('search');
		 
		frm.cityid.value=document.getElementById('s_cityid').value;
		frm.catid.value=document.getElementById('s_catid').value;
		frm.shoppid.value=document.getElementById('s_shoppid').value;
		frm.sellerid.value=document.getElementById('s_sellerid').value;
		
		//frm.roles.value=document.getElementById('s_roles').value;
		//frm.status.value=$('input[@name=s_status][@checked]').val();
		 
		
		//frm.act.value=tocsv?'tocsv':'';
		
		frm.submit();
	}
	
	
	
	function  editSeller(el){
			var shopid= $(el).data('shopid');
			var sellerid= $(el).data('sellerid');
			
			
			var id='#seller_'+shopid;
			
			$(id).append("</br>");
			
			var tmp_id = 'seller_select_'+shopid;
			$('#my_sellerid').clone().show().attr('id',tmp_id).appendTo(id);
			$('#'+tmp_id+" option[value='"+sellerid+"']").attr("selected", true); 
			
			var org=$(id).html(); 
			
			org+= '<button  onclick="setSeller(' + shopid + ');">确定</button>';
			
			$(id).html(org);
			
			$(el).attr('id','changebtn_'+shopid);			
			$(el).attr("disabled",true);
	}
		
	function  setSeller(shopid){
			var id='#seller_'+shopid;
			var selid ='#seller_select_'+shopid;
			var btn='#changebtn_'+shopid;
			
		    var sellerid= $(selid).val();
			
			var checkText=$(selid).find("option:selected").text(); 
			  
			$.post("./changeSeller", { 'shopid':shopid,'sellerid':sellerid },
			function(data){     
			 if(data=='succ'){ 
			 	 $(id).css('color','green').css('font-weight','bold');
				
				 $(id).text(checkText);	 
				 $(btn).attr("disabled",false);	
				 $(btn).parent().parent().parent().addClass("success");	 
			 }else{
				 alert(data);
			 }
		   });
		    
	}
	
	/*
	function resetForm(){
		var frm=document.getElementById('search');
		frm.act.value='';
	}
	setInterval("resetForm()",1000);
	*/
	
    </script> 
</div>
 
 
	{include file="frame/xlist_template.tpl" rows=$rows fields=$fields pk_field="" pk_field_value="" total_page=$total_page}
 




<form name="search" id="search" method="get" action="">
	<input name="currpage" type="hidden" value="{$smarty.request.currpage}" />
	<input name="orderby" type="hidden" value="{$smarty.request.orderby}" />
	<input name="direction" type="hidden" value="{$smarty.request.direction}" />
    
	<input name="cityid" type="hidden" value="{$smarty.request.cityid}" />
    <input name="catid" type="hidden" value="{$smarty.request.catid}" />
    <input name="shoppid" type="hidden" value="{$smarty.request.shoppid}" /> 
    <input name="sellerid" type="hidden" value="{$smarty.request.sellerid}" />
    
    <input name="act" type="hidden" value="" />
    
	</form>
    
     <div class="buttomToolbar hide">     
     	<input name="" type="button" value="编辑选中记录" onclick="do_edit();" />		 
      	<input name="" type="button" value="添加一条新记录" onclick="do_add();" />	 
      	<input name="" type="button" value="删除选中记录" onclick="do_delete();" />	 
     </div>
     
       
 <script language="javascript">
 
function do_add(){
  
 	visit('./add' );
 
 }
  function do_edit()
 {
 	var id=list_selected_data();
	
	if(id){
	
		visit('./edit?id='+id);
	}else{
		alert('未选中操作对象');
	}
}
 function do_delete()
 {
 	var id=list_selected_data();
	
	if(id){
		var tip= $('span#row'+id+"_2").text()+"  "+$('span#row'+id+"_3").text();		
		
		if(confirm("是否确定删除\""+tip+"\"?"))
		visit('./delete?id='+id);
	}else{
		alert('未选中操作对象');
	}
}

 
 </script> 





</div>
                        
                        
{include file="frame/foot.tpl"}