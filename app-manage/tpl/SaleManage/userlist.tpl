{include file="frame/head_vm.tpl"}
 

<ol class="breadcrumb">
  <li><img src="{$_root}img/icon_arrow_right.png" class="icon" />  <a href="#"> 销售部</a></li>
  <li class="active">人员管理</li>
</ol>





<div class="mainbox">

<div style="float:right; margin-right:20px">

{if $_acl->isAllowed($_admin.role, 'acl_user_add')}
	
				<button type="button" class="btn1s" onclick="javascript:location.href='useradd';">新增销售</button>
	
	{/if} </div>
</div>


<form action="" method='get' id = 'fm'>
<table >
    <tr>
        <td>
        <select name='cityid' id='cityid'>
        <option value ='' >所有地区</option>
        {foreach $userCity as $k => $v}
        <option value ='{$k}' {if $k == $cityid}selected{/if}>{$v}</option>
        {/foreach}
        </select>
        
        <select name='pid' id='pid'>
        <option value ='' >所有上级</option>
        {foreach $leaderList as $k => $v}
        <option value ='{$v.uid}' {if $v.uid == $pid}selected{/if}>{$v.realname}</option>
        {/foreach}
        </select>
        
         <input name='keyword' id='keyword' value='{$keyword}' type='text'   placeholder="姓名"> 
         <input type="submit" class="narrowButton" id='Btn' value=' 搜 索 '/>   </td>
    </tr>
</table>
</form>




<table class="tlist" >
	<thead>
		<tr class="title">
			<th align="left" style="width:100px;">&nbsp;ID</th>
			<th align="left" style="width:300px;">姓名&nbsp;</th>
            <th align="left">地区</th>
            <th align="left">职位</th>
			<th align="left">上级</th>
             <th align="left">帐号</th>
            <th align="left">门店</th>
			<th align="right">操作&nbsp;</th>
		</tr>  
	</thead>
	<tbody>
		{foreach $userList as $user}
		<tr class="filter_rows">
			<td align="left">{$user.id}</td>
			<td align="left" ><a href='useredit?id={$user.id}'><u class="filter_name">{$user.realname}</u></a></td>
			<td align="left">{$user.cityid_name}</td>
            <td align="left">{$user.level_name}</td>
			<td align="left">{$user.pid_name|default:'-'}</td>
             <td align="left" {if $user.islock} class="strike" {/if} >{$user.name}</td>
            <td align="left"><a href="shop?sellerid={$user.id}"> {$user.shop_num} </a></td>
			<td align="right">
				<a href="useredit?id={$user.id}">编辑</a>
				{if $user.shop_num >0}
                 <span style="color:gray">删除 </span>
                {else}
                <a href="javascript:$.form.confirm('userdel?id={$user.id}', '确认删除用户“{$user.realname}”？');">删除</a>
                {/if}
				
			</td>
		</tr>
		{/foreach}
	</tbody>
	
</table>

{*include file="frame/page.tpl"*}

</div>

 

{include file="frame/foot.tpl"}