<?php
class Comm_Share{

	#创建上传目录
	public function createDir($path) {
		if (!file_exists($path)) {
			$this->createDir(dirname($path));
			mkdir($path, 0777);
		}
	}
	
	/**
	 *
	 * @global type $strUploadFilePath
	 * @param type $strName
	 * @param type $strNewFileName
	 * @return string
	 */
	public function uploadImage($strUploadFilePath, $strName, $strNewFileName="", $restrict=array()) {
		$this->createDir($strUploadFilePath);
		$strUploadFile = $_FILES[$strName]['tmp_name'];
		$strFileType = $_FILES[$strName]['type'];
		$strstr = '.jpg';
		if ($strFileType == "image/pjpeg" || $strFileType == "image/gif" || $strFileType == "image/png" || $strFileType == "image/jpeg") {
			if ($strUploadFile != "") {
				if ($restrict) {
					$imageInfo = getimagesize($strUploadFile);
					if ($imageInfo[0] != $restrict['width'] || $imageInfo[1] != $restrict['height']) {
						echo "<script>alert('您上传的图片尺寸有误，请重新上传！');history.go(-1);</script>";
						exit;
					}
				}
				if ($strNewFileName == "") {
					$strUploadFileName = $_FILES[$strName]['name'];
				} else {
					$strUploadFileName = $strNewFileName . $strstr;
				}
				$strFilePathName = $strUploadFilePath . $strUploadFileName;
				if (!move_uploaded_file($strUploadFile, $strFilePathName)) {
					echo $_FILES[$strName]['name'] . ("上传文件失败");
					exit;
				}
				@unlink($strUploadFile);
				return $strUploadFileName;
			}
		}
	}

	/**
	 *
	 * @global type $strUploadFilePath
	 * @param type $strName
	 * @param type $strNewFileName
	 * @return string
	 */
	public function uploadImageAll($strUploadFilePath, $strName, $strNewFileName="", $restrict=array()) {
		$this->createDir($strUploadFilePath);
		#删除空路径
		foreach($_FILES[$strName]['name'] as $k => $v){
			if(empty($v)){
				unset($_FILES[$strName]['name'][$k]);
				unset($_FILES[$strName]['type'][$k]);
				unset($_FILES[$strName]['tmp_name'][$k]);
				unset($_FILES[$strName]['error'][$k]);
				unset($_FILES[$strName]['size'][$k]);
			}
		}
		$strUploadFile = $_FILES[$strName]['tmp_name'];
		$file_name = array();
		$flag = true;
		foreach($_FILES[$strName]['type'] as $k => $v){
			if($v != "image/pjpeg" && $v != "image/gif" && $v != "image/png" && $v != "image/jpeg"){
				$flag = false;
				continue;
			}
		}
		if ($flag) {
			foreach ($_FILES[$strName]["error"] as $key => $error) {
				if ($error == UPLOAD_ERR_OK) {
					$tmp_name = $strUploadFile[$key];
					$name    = $strNewFileName.uniqid().substr($_FILES[$strName]["name"][$key],-4,4);
					$uploadfile = $strUploadFilePath.$name;
					$file_name[] = $name ;
					move_uploaded_file($tmp_name, $uploadfile);
				}
			}
			@unlink($strUploadFile);
			return $file_name;
		}else{
			echo "<script>alert('Type Error ');history.go(-1);</script>";
		}
	}

	/**
	 *
	 * @global type $strUploadFilePath
	 * @param type $strName
	 * @param type $strNewFileName
	 * @return string
	 */
	public function uploadFile($strUploadFilePath, $strName, $strNewFileName="") {
		$this->createDir($strUploadFilePath);
		$strUploadFile = $_FILES[$strName]['tmp_name'];
		$old_file_name = $_FILES[$strName]["name"];
		if(!$_FILES[$strName]['error']){
			$strFileType = strtolower(substr($old_file_name,strrpos($old_file_name,".")));
			if (in_array($strFileType, array('.docx','.doc','.xls','.xlsx','.jpg','.jpeg'))) {
				if ($strUploadFile != "") {
					if ($strNewFileName == "") {
						$strUploadFileName = $_FILES[$strName]['name'];
					} else {
						$strUploadFileName = $strNewFileName . $strFileType;
					}
					$strFilePathName = $strUploadFilePath . $strUploadFileName;
					if (!move_uploaded_file($strUploadFile, $strFilePathName)) {
						echo $_FILES[$strName]['name'] . ("上传文件失败");
						exit;
					}
					@unlink($strUploadFile);
					return $strFilePathName;
				}
			}else{
				self::alert('上传失败：仅支持WORD,EXCEL,JPG文件');
			}
		}else{
			return false;
		}
	}

	/**
	 * CURL 方式POST数据
	 * Timeout：10S
	 * @param $remote_server
	 * @param $post_array
	 */
	public function request_by_curl($remote_server,$post_array){
		$ch = curl_init();
		$post_array['password'] = '816bcde8a271caf027a622eabb67fc17'; //auth
		$post_array = http_build_query($post_array);
		curl_setopt($ch,CURLOPT_URL,$remote_server);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$post_array);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	/**
	 * 弹出框
	 * @param $str
	 * @param $url
	 */
	public function alert($str,$url=''){
		$go_url = empty($url)?'history.go(-1)':"location.href='{$url}'";
		$alert = <<<EOF
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>提示信息</title>
		<script language='javascript' type='text/javascript' charset='utf-8'>alert('{$str}');{$go_url};</script>
		</head>
		<body>
		</body>
		</html> 
EOF;
		die($alert);
	}

	/**
	 *
	 * 重定向
	 * @param str $url
	 */
	public function redirect($url = ''){
		$go_url = empty($url)?'history.go(-1)':"location.href='{$url}'";
		die("<script language='javascript' type='text/javascript' charset='utf-8'>{$go_url};</script>");
	}

	/**
	 *
	 * 下载EXCEL文件
	 * @param array $downList
	 * @author  
	 */
	public function downloadFile($downList){
		$filename 		= $downList['filename']; // string
		$title_list 	= $downList['title_list']; // array
		$content_list 	= $downList['content_list']; // array
		if(empty($filename)){
			self::alert('请设置EXCEL的文件名');
		}else if(empty($title_list)){
			self::alert('请设置EXCEL的字段名');
		}else if(empty($content_list)){
			self::alert('没有可供下载的信息');
		}
		$titleArr = $fildArr = array();
		foreach($title_list as $tlkey=>$tllist){
			$fildArr[] = $tlkey;
			$titleArr[] = $tllist;
		}
		// build xls
		$objPHPExcel = new PHPExcel();
		$commonStyle = array('font' => array('size' => 10),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000'))));
		$headStyle = array_merge_recursive($commonStyle, array('font' => array('bold' => true),'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCEE'))));
		$kk = 0;
		foreach ($titleArr as $tkey=>$tlist){
			$cur_asc = $kk + 65;
			if($cur_asc <= 90){
				$letter = chr($cur_asc);
			}else if($cur_asc < 116){
				$letter = 'A'.chr($cur_asc-26);
			}else{
				self::alert('The field is too much to work!');
				exit;
			}
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setWidth(20);
			$objPHPExcel->getActiveSheet()->SetCellValue($letter.'1', $tlist);
			$objPHPExcel->getActiveSheet()->getStyle($letter.'1')->applyFromArray($headStyle);
			$kk++;
		}
		$lineno = 2;
		$cur_asc = $mm = 0;
		foreach ((array)$content_list as $ckey => $clist) {
			$cur_asc = $mm + 65;
			if($cur_asc <= 90){
				$letter = chr($cur_asc);
			}else if($cur_asc < 116){
				$letter = 'A'.chr($cur_asc-26);
			}else{
				self::alert('The field is too much to work!');
				exit;
			}
			$nn = 0;
			foreach ($titleArr as $takey=>$talist){
				$asc = $nn + 65;
				if($asc <= 90){
					$letters = chr($asc);
				}else if($asc < 116){
					$letters = 'A'.chr($asc-26);
				}else{
					self::alert('The field is too much to work!');
				}
				$objPHPExcel->getActiveSheet()->SetCellValue($letters.$lineno, $clist[$fildArr[$takey]]);
				$objPHPExcel->getActiveSheet()->getStyle($letters.$lineno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle($letters.$lineno)->applyFromArray($commonStyle);
				$nn++;
			}
			$lineno++;
			$mm++;
		}
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.$filename.'.xls');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}

	/**
	 *
	 * 导出财务报表,多Sheet
	 * @param array $downList
	 * @author  
	 */
	public function exportReport($downList){
		$filename 		= $downList['filename']; // string
		$title_list 	= $downList['title_list']; // array
		$content_list 	= $downList['content_list']; // array
		if(empty($filename)){
			self::alert('请设置EXCEL的文件名');
		}
		// build xls
		$objPHPExcel = new PHPExcel();
		$filedStyle = array('font' => array('size' => 10),'borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$commonStyle = array_merge_recursive($filedStyle,array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
		$headStyle = array_merge_recursive($commonStyle, array('font' => array('bold' => true),'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCEE'))));
		$titleStyle = array_merge_recursive($commonStyle, array('font' => array('bold' => true),'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'EEEEEE'))));
		foreach($content_list as $ckey=>$clist){
			if($ckey)$objPHPExcel->createSheet();
			$objPHPExcel->getSheet($ckey)->setTitle($clist['ed_batch_sysno']);
			$objPHPExcel->getSheet($ckey)->SetCellValue('A1', '批次号：'.$clist['ed_batch_sysno']);
			$objPHPExcel->getSheet($ckey)->SetCellValue('B1', '事业部：'.$GLOBALS['DEPARTMENTS'][$clist['ed_bu']]);
			$objPHPExcel->getSheet($ckey)->SetCellValue('C1', '品牌负责人：'.$clist['ed_bm_name']);
			$objPHPExcel->getSheet($ckey)->SetCellValue('D1', '上线时间：'.$clist['edp_online']);
			$objPHPExcel->getSheet($ckey)->SetCellValue('E1', '下线时间：'.$clist['edp_offline']);
			$objPHPExcel->getSheet($ckey)->SetCellValue('B2', '预   计');
			$objPHPExcel->getSheet($ckey)->SetCellValue('C2', '实   际');
			$objPHPExcel->getSheet($ckey)->mergeCells('B33:C33');
			$objPHPExcel->getSheet($ckey)->mergeCells('B34:C34');
			$objPHPExcel->getSheet($ckey)->mergeCells('B35:C35');
			$objPHPExcel->getSheet($ckey)->mergeCells('B36:C36');
			for($i = 1;$i < 39;$i++){
				$style= in_array($i,array(2,20,22,26,32,37))?$headStyle:$commonStyle;
				$aStyle = in_array($i,array(2,20,22,26,32,37))?$headStyle:$filedStyle;
				if($i == 1)$aStyle = $style = $titleStyle;
				$objPHPExcel->getSheet($ckey)->getStyle('A'.$i)->applyFromArray($aStyle);
				$objPHPExcel->getSheet($ckey)->getStyle('B'.$i)->applyFromArray($style);
				$objPHPExcel->getSheet($ckey)->getStyle('C'.$i)->applyFromArray($style);
				$objPHPExcel->getSheet($ckey)->getStyle('D'.$i)->applyFromArray($style);
				$objPHPExcel->getSheet($ckey)->getStyle('E'.$i)->applyFromArray($style);
			}
			$m = 3;
			foreach ($clist['filed_info'] as $fkey=>$flist){
				$objPHPExcel->getSheet($ckey)->SetCellValue('A'.$m, $fkey);
				$objPHPExcel->getSheet($ckey)->SetCellValue('B'.$m, $flist[0]);
				$objPHPExcel->getSheet($ckey)->SetCellValue('C'.$m, $flist[1]);
				$m++;
			}
			$objPHPExcel->getSheet($ckey)->getColumnDimension('A')->setWidth(30);
			$objPHPExcel->getSheet($ckey)->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getSheet($ckey)->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getSheet($ckey)->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getSheet($ckey)->getColumnDimension('E')->setWidth(30);
		}
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.$filename.'.xls');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}

	/**
	 *
	 * 获取字符串首字母
	 * @param array $downList
	 * @author  
	 */
	public function getLetter($input)
	{
		$letters=array(
	        'A'=>0xB0C4,
	        'B'=>0xB2C0,
	        'C'=>0xB4ED,
	        'D'=>0xB6E9,
	        'E'=>0xB7A1,
	        'F'=>0xB8C0,
	        'G'=>0xB9FD,
	        'H'=>0xBBF6,
	        'J'=>0xBFA5,
	        'K'=>0xC0AB,
	        'L'=>0xC2E7,
	        'M'=>0xC4C2,
	        'N'=>0xC5B5,
	        'O'=>0xC5BD,
	        'P'=>0xC6D9,
	        'Q'=>0xC8BA,
	        'R'=>0xC8F5,
	        'S'=>0xCBF9,
	        'T'=>0xCDD9,
	        'W'=>0xCEF3,
	        'X'=>0xD188,
	        'Y'=>0xD4D0,
	        'Z'=>0xD7F9,
		);
		$input = iconv('UTF-8', 'GBK', $input);
		$str = substr($input, 0, 1);
		if ($str >= chr(0x81) && $str <= chr(0xfe))    {
			$num = hexdec(bin2hex(substr($input, 0, 2)));
			foreach ($letters as $k=>$v){
				if($v>=$num)
				break;
			}
			return $k;
		}
		else{
			return $str;
		}
	}
}