<?php
class Comm_Util {
	
	
	
	static function getOptFromArr($arr, $fldKey ,$fldValue) {
		$rt = array ();
		foreach ( $arr as $k => $a ) {
			$rt [$a[$fldKey]] = $a[$fldValue];
		}
		return $rt;
	}
	/**
	 * 从2维数组中获取某一列数据
	 * 
	 * @param array $arr        	
	 * @param string $col        	
	 * @return array
	 */
	static function getColFromArr($arr, $col) {
		$rt = array ();
		foreach ( $arr as $k => $a ) {
			$rt [$k] = $a [$col];
		}
		return $rt;
	}
	/**
	 * 转变数组的key
	 * 
	 * @param
	 *        	$arr
	 * @param
	 *        	$field
	 */
	static function changeArrKey(&$arr, $field = 'id') {
		foreach ( ( array ) $arr as $key => $v ) {
			$arr [$v [$field]] = $v;
			unset ( $arr [$key] );
		}
		
		return $arr;
	}
	
	static function getClientIP() {
		if (getenv ( "HTTP_X-Real-IP" )) {
			$ip = getenv ( "HTTP_X-Real-IP" );
		} elseif (getenv ( "HTTP_X_FORWARDED_FOR" )) {
			$ip_arr = explode ( ',', getenv ( "HTTP_X_FORWARDED_FOR" ) );
			$ip = $ip_arr [0];
		} elseif ($_SERVER ['REMOTE_ADDR']) {
			return $_SERVER ['REMOTE_ADDR'];
		} else {
			$ip = "unknown";
		}
		return $ip;
	}
	
	/**
	 * 合并3维数组
	 */
	static function merage_arr($arr) {
		$rt = array ();
		
		foreach ( $arr as $a ) {
			$rt = array_merge ( $rt, $a );
		}
		
		return $rt;
	}
	/**
	 * 过滤数组
	 * 
	 * @param array $arr        	
	 * @param string $fn
	 *        	null,
	 * @return array
	 */
	static function array_filter($arr, $fn = 'null') {
		if ($fn == 'null') {
			return array_filter ( $arr, array (
					"array_filter_null" 
			) );
		}
	}
	/**
	 * 过滤掉数组中值为空的元素
	 */
	function array_filter_null($var) {
		return ($var == '') ? false : true;
	}
	
	static function format_date($time, $format = "%Y/%m/%d %H:%M:%S") {
		$format = ($format == 'date' ? "%m-%d-%Y" : $format);
		$format = ($format == 'datetime' ? "%m-%d-%Y %H:%M:%S" : $format);
		$format = ($format == 'mysql-date' ? "%Y-%m-%d" : $format);
		$format = ($format == 'mysql-datetime' ? "%Y-%m-%d %H:%M:%S" : $format);
		
		return strftime ( $format, $time );
	}
	/**
	 * 得到有距离结束还有多少小时多少分多少秒
	 * 
	 * @param int $time
	 *        	时间搓
	 */
	static function intervalTime($time) {
		$rt = array ();
		if ($time == 0) {
			$rt = array (
					0,
					0,
					0 
			);
		} else {
			$day = self::intervalDay ( $time ); // 多少天
			$hour = self::intervalHour ( $time );
			$minute = self::intervalMinute ( $time );
			$second = self::intervalSecond ( $time );
			
			$rt = array (
					$day,
					$hour,
					$minute,
					$second 
			);
		}
		return $rt;
	}
	static function intervalDay($time) {
		return self::zero ( floor ( $time / 86400 ) ); // 多少天
	}
	static function intervalHour($time) {
		return self::zero ( floor ( $time / 3600 ) % 24 ); // 多少小时
	}
	static function intervalMinute($time) {
		return self::zero ( floor ( $time / 60 ) % 60 ); // 多少分钟
	}
	static function intervalSecond($time) {
		return self::zero ( $time % 60 ); // 多少秒
	}
	static function zero($n) {
		if ($n > 0) {
			if ($n <= 9) {
				$n = "0" . $n;
			}
			return $n;
		} else {
			return "00";
		}
	}
	static function send_curl($url, $field) {
		$ch = curl_init (); // 初始化CURL句柄
		curl_setopt ( $ch, CURLOPT_URL, $url ); // 设置请求的URL
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 ); // 设为TRUE把curl_exec()结果转化为字串，而不是直接输出
		curl_setopt ( $ch, CURLOPT_POST, 1 ); // 启用POST提交
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $field ); // 设置POST提交的字符串
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 25 ); // 超时时间
		//curl_setopt ( $ch, CURLOPT_USERAGENT, $user_agent ); // HTTP请求User-Agent:头
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
				'Accept-Language: zh-cn',
				'Connection: Keep-Alive',
				'Cache-Control: no-cache' 
		) ); // 设置HTTP头信息
		$document = curl_exec ( $ch ); // 执行预定义的CURL
		$info = curl_getinfo ( $ch ); // 得到返回信息的特性
		return $document;
	}
	/**
	 * 发送短信
	 * 
	 * @param string $mobile        	
	 * @param string $content        	
	 * @param string $logname        	
	 */
	static function sendSms($mobile, $content, $logname = '') {
		$result = Com_Sms_Api::send ( $mobile, $content );
		
		// $field = http_build_query(array('mobiles'=>$mobile,'smsbody'=>$content));
		// $result = Comm_Util::send_curl(SMS_ADDR, $field);
		
		// $stats = json_decode($result);
		// $str = "mobile:{$mobile}\t content:{$content}\tdate:".date("Y-m-d H:i:s")."\tresult:".($stats->result?'TRUE':'FALSE')."\r\n";
		$str = "mobile:{$mobile}\t content:{$content}\tdate:" . date ( "Y-m-d H:i:s" ) . "\tresult:" . ($result) . "\r\n";
		Comm_Util::log ( $str, $logname );
		
		return $result;
	}
	static function log($str, $filename = 'travel_subscibe') {
		$fp = fopen ( LOG_SMS_SUBSCIBE . date ( "Ymd" ) . "_{$filename}.log", 'a+' );
		flock ( $fp, LOCK_EX );
		fwrite ( $fp, $str );
		flock ( $fp, LOCK_UN );
		fclose ( $fp );
	}
	static function getWeekDay($time) {
		$week_day = '';
		$day = date ( 'N', $time );
		switch ($day) {
			case 1 :
				$week_day = '周一';
				break;
			case 2 :
				$week_day = '周二';
				break;
			case 3 :
				$week_day = '周三';
				break;
			case 4 :
				$week_day = '周四';
				break;
			case 5 :
				$week_day = '周五';
				break;
			case 6 :
				$week_day = '周六';
				break;
			case 7 :
				$week_day = '周日';
				break;
			default :
				break;
		}
		return $week_day;
	}
	
	public static function shortUrl($url) {
		$base32 = array (
				'a',
				'b',
				'c',
				'd',
				'e',
				'f',
				'g',
				'h',
				'i',
				'j',
				'k',
				'l',
				'm',
				'n',
				'o',
				'p',
				'q',
				'r',
				's',
				't',
				'u',
				'v',
				'w',
				'x',
				'y',
				'z',
				'0',
				'1',
				'2',
				'3',
				'4',
				'5' 
		);
		$hex = md5 ( $input );
		$hexLen = strlen ( $hex );
		$subHexLen = $hexLen / 8;
		$output = array ();
		for($i = 0; $i < $subHexLen; $i ++) {
			$subHex = substr ( $hex, $i * 8, 8 );
			$int = 0x3FFFFFFF & (1 * ('0x' . $subHex));
			$out = '';
			for($j = 0; $j < 6; $j ++) {
				$val = 0x0000001F & $int;
				$out .= $base32 [$val];
				$int = $int >> 5;
			}
			$output [] = $out;
		}
		return $output;
	}
	static function ip_limit($client_ip) {
		if (in_array ( $client_ip, Const_Main::$allow_ips )) {
			return true;
		}
		return false;
	}
	static function getDataByCache($key) {
		$ret = mc_get ( $key );
		if (! is_array ( $ret ) || empty ( $ret ['data'] ))
			return false;
		return $ret ['data'];
	}
	static function setCache($key, $data, $expire) {
		if (! empty ( $data )) {
			$rs ['data'] = $data;
			$rs ['last_upd_time'] = date ( 'Y-m-d H:i:s', time () );
			mc_set ( $key, $rs, $expire );
		}
		// return true;
	}
}

?>