<?php

include 'PHPExcel/PHPExcel.php';
class Comm_Excel
{

    public static function createDir($path)
    {
        if (! file_exists($path)) {
            self::createDir(dirname($path));
            mkdir($path, 0777);
        }
    }

    public static function readRows($pathName, $startRowNum = 1)
    {
        $objReader = PHPExcel_IOFactory::createReader('Excel2007'); // use excel2007 for 2007 format
        // $objReader =PHPExcel_IOFactory::createReader('Excel5'); 
        
        if (! $objReader->canRead($pathName)) {
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            if (! $objReader->canRead($pathName)) {
                unlink($pathName);
                self::alert('上传文件不是excel');
                exit();
            }
        }
        
        $objPHPExcel = $objReader->load($pathName);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow(); // 取得总行数
        $highestColumn = $sheet->getHighestColumn(); // 取得总列数
        
        //起始行
        $j = $startRowNum;
        
        for ($j; $j <= $highestRow; $j ++) {
            for ($k = 'A', $x = 0; $k <= $highestColumn; $k ++, $x ++) {
                // $arrGoods[$j-2][$x] = iconv('gbk','utf-8//IGNORE',trim($objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue()));
                $arrGoods[$j - 2][$x] = trim($objPHPExcel->getActiveSheet()
                    ->getCell("$k$j")
                    ->getValue());
            }
        }
        return $arrGoods;
    }

    public static function uploadExcel($filename, $upPath, $upFileName)
    {
        $sourceName = $_FILES[$filename]['name'];
        $sourceFileName = $_FILES[$filename]['tmp_name'];
        $sourceFileType = $_FILES[$filename]['type'];
        
        $path_info = pathinfo($sourceName);
        $extension = $path_info['extension'];
        $fName = $path_info['filename'];
        $extension = "." . $extension;
        
        if ($_FILES[$filename]['name'] == "") {
            // echo "<script>alert('文件不存在！');history.go(-1);</script>";
            self::alert('文件不存在！');
            exit();
        }
        
        if (! strstr($sourceName, ".xls")) {
            // echo "<script>alert('文件不正确！');history.go(-1);</script>";
            self::alert('文件不正确！');
            exit();
        }
        
        if (file_exists($upPath . $upFileName . ".xls") || file_exists($upPath . $upFileName . ".xlsx")) {
            // echo "<script>alert('文件已存在！');history.go(-1);</script>";
            self::alert('文件已存在！');
            exit();
        }
        
        if (! move_uploaded_file($sourceFileName, $upPath . $upFileName . $extension)) {
            // echo "<script>alert('文件上传失败！');history.go(-1);</script>";
            self::alert('文件上传失败！');
            exit();
        }
        return $upFileName . $extension;
    }

    /**
     * 弹出框
     * 
     * @param
     *            $str
     * @param
     *            $url
     */
    public static function alert($str, $url = '')
    {
        $go_url = empty($url) ? 'history.go(-1)' : "location.href='{$url}'";
        $alert = <<<EOF
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>提示信息</title>
		<script language='javascript' type='text/javascript' charset='utf-8'>alert('{$str}');{$go_url};</script>
		</head>
		<body>
		</body>
		</html> 
EOF;
        die($alert);
    }

    /**
     * 下载EXCEL文件
     * 
     * @param string $filename            
     * @param array $title_list            
     * @param array $content_list            
     */
    public static function downloadFile($filename, $title_list, $content_list)
    {
        set_time_limit(600);
        ini_set('max_execution_time', 600);
        // $filename = $downList['filename']; // string
        // $title_list = $downList['title_list']; // array
        // $content_list = $downList['content_list']; // array
        if (empty($filename)) {
            self::alert('请设置EXCEL的文件名');
        } else 
            if (empty($title_list)) {
                self::alert('请设置EXCEL的字段名');
            } else 
                if (empty($content_list)) {
                    self::alert('没有可供下载的信息');
                }
        $titleArr = $fildArr = array();
        foreach ($title_list as $tlkey => $tllist) {
            $fildArr[] = $tlkey;
            $titleArr[] = $tllist;
        }
        // build xls
        $objPHPExcel = new PHPExcel();
        $commonStyle = array(
            'font' => array(
                'size' => 10
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '000000'
                    )
                )
            )
        );
        $headStyle = array_merge_recursive($commonStyle, array(
            'font' => array(
                'bold' => true
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array(
                    'rgb' => 'CCCCEE'
                )
            )
        ));
        $kk = 0;
        
        foreach ($titleArr as $tkey => $tlist) {
            $cur_asc = $kk + 65;
            if ($cur_asc <= 90) {
                $letter = chr($cur_asc);
            } else 
                if ($cur_asc < 116) {
                    $letter = 'A' . chr($cur_asc - 26);
                } else {
                    self::alert('The field is too much to work!');
                    exit();
                }
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($letter)
                ->setWidth(20);
            $objPHPExcel->getActiveSheet()->SetCellValue($letter . '1', $tlist);
            $objPHPExcel->getActiveSheet()
                ->getStyle($letter . '1')
                ->applyFromArray($headStyle);
            $kk ++;
        }
        
        $lineno = 2;
        
        foreach ((array) $content_list as $ckey => $clist) {
            
            $nn = 0;
            foreach ($titleArr as $takey => $talist) {
                $asc = $nn + 65;
                if ($asc <= 90) {
                    $letters = chr($asc);
                } else 
                    if ($asc < 116) {
                        $letters = 'A' . chr($asc - 26);
                    } else {
                        self::alert('The field is too much to work!');
                        exit();
                    }
                $objPHPExcel->getActiveSheet()->SetCellValue($letters . $lineno, $clist[$fildArr[$takey]]);
                // $objPHPExcel->getActiveSheet()->getStyle($letters.$lineno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                // $objPHPExcel->getActiveSheet()->getStyle($letters.$lineno)->applyFromArray($commonStyle);
                $nn ++;
            }
            $lineno ++;
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $filename . '.xls');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }
}