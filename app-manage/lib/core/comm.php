<?php

/**
 * Singleton 返回单例
 * @param unknown $tabModel
 * @return unknown
 */
function st($class) {
	$class = trim ( $class );
	if (! isset ( $GLOBALS ['_singleton_'] )) {
		$GLOBALS ['_singleton_'] = array ();
	}
	if (! isset ( $GLOBALS ['_singleton_'] [$class] )) {
		$GLOBALS ['_singleton_'] [$class] = new $class ();
	}
	return $GLOBALS ['_singleton_'] [$class];
}


function header_cache($seconds=86400) {
	header('Cache-Control: max-age='.$seconds);
	header('Last-Modified: '.gmdate('D, d M Y 01:01:01',time()).' GMT');
	header('Pragma: public');
}

function get_uid() {
	$user=get_session ( 'admin' );
	Return $user['id'];
}
function get_session($key = '') {
	Return $key ? (isset ( $_SESSION [$key] ) ? $_SESSION [$key] : null) : $_SESSION;
}
function set_session($key, $val = '') {
	if (is_array ( $key )) {
		foreach ( $key as $k => $v ) {
			$_SESSION [$k] = $v;
		}
	} else {
		$_SESSION [$key] = $val;
	}
}
function unset_session($key) {
	$_SESSION [$key] = null;
	unset ( $_SESSION [$key] );
}
function clear_session() {
	// unset($_SESSION);
	// $_SESSION=null;
	session_unset ();
}
function daddslashes($string, $force = 0) {
	! defined ( 'MAGIC_QUOTES_GPC' ) && define ( 'MAGIC_QUOTES_GPC', get_magic_quotes_gpc () );
	if (! MAGIC_QUOTES_GPC || $force) {
		if (is_array ( $string )) {
			foreach ( $string as $key => $val ) {
				$string [$key] = daddslashes ( $val, $force );
			}
		} else {
			$string = addslashes ( $string );
		}
	}
	return $string;
}
function get_elapsed_time() {
	return microtime ( true ) - $_SERVER ['START_TIME'];
}
function s() {
	//header ( "Content-Type:text/html; charset=UTF-8" );
	$para = func_get_args ();
	$para = is_array ( $para ) ? $para : array ();
	$exit = array_pop ( $para );
	foreach ( $para as $val ) {
		$val = $val === '' ? time () : $val;
		var_dump ( $val );
		echo ("<hr>");
	}
	if ($exit === 1) {
		echo ("int 1 => Exit!!!");
		exit ();
	} else {
		var_dump ( $exit );
		echo ("<hr>");
	}
}

function html_btn($title, $click, $attrs = array()) {
	Return "<button  class='btn-small' onclick='javascript:{$click};' " . get_attrs ( $attrs ) . " >$title</button>";
}
function html_link($title, $url, $attrs = array()) {
	Return "<a href='$url' " . get_attrs ( $attrs ) . " >$title</a>";
}
function html_img($scr, $attrs = array()) {
	Return "<img  src='$scr' " . get_attrs ( $attrs ) . "/>";
}
function get_attrs($attributes = array()) {
	if (is_array ( $attributes )) {
		$t = '';
		foreach ( $attributes as $key => $value ) {
			$t .= " $key=" . '"' . $value . '"';
		}
		return $t;
	}
}

function get_opts($arr ,$keyfld , $valfld) {
	$opts=array();
	if (is_array ( $arr )) {
		foreach ( $arr as $value ) {
			$opts[$value[$keyfld]] = $value[$valfld] ;
		}
	}
	return $opts;
}

function set_arr_value(&$arr, $path, $val) {
	$it = &$arr;
	$pathes = explode ( ".", $path );
	foreach ( $pathes as $p ) {
		$it = &$it [$p];
	}
	$it = $val;
}
function get_arr_value($arr, $path) {
	$it = $arr;
	$pathes = explode ( ".", $path );
	foreach ( $pathes as $p ) {
		$it = $it [$p];
	}
	Return $it; // $it=$val;
}


//直接获取过滤后的 GET + POST
function req_int($name,$default=0){
	return flt_int(req_get($name,$default));
}
function req_str($name,$default=null){	
	return flt_str(req_get($name,$default));	
}
function req_email($name,$default=null){
	return flt_email(req_get($name,$default));
}
function req_url($name,$default=null){
	return flt_url(req_get($name,$default));
}
function req_float($name,$default=null){
	return flt_float(req_get($name,$default));
}

function req_get($name,$default=null){
	$_REQ=$_GET+$_POST;
	$val=$_REQ[$name];
	if(!isset($val) || empty($val) ) {
		$val= $default;
	}
	return $val;
}


/**
 * filter过滤输入变量
 */
function flt_int($str) {
	Return filter_var($str,FILTER_VALIDATE_INT);
}
function flt_str($str) {
	Return filter_var($str,FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
}
function flt_email($str) {
	Return filter_var($str,FILTER_VALIDATE_EMAIL);
}
function flt_url($str) {
	Return filter_var($str,FILTER_VALIDATE_URL);
}
function flt_float($str) {
	Return filter_var($str,FILTER_VALIDATE_FLOAT);
}
function flt_ip($str) {
	Return filter_var($str,FILTER_VALIDATE_IP);
}


function get_date($datetime=null,$format='Y-m-d H:i:s'){

	if(!isset($datetime)){
		$datetime=time();
		return date($format,$datetime);
	}elseif(is_numeric($datetime)){
		return date($format,$datetime);
	}elseif(!empty($datetime)){
		$datetime=strtotime($datetime);
		return date($format,$datetime);
	}else{
		Return '';
	}
}


function l($msg,$file="commom.log"){	
	error_log(date('[Y-m-d H:i:s] ') . "\t$msg\n",3, __LOG_DIR.'/'.$file);
}



/**
 *	清除select field 的前缀表名,如a.user =>user 和 a.user as u => u
 */
function prepare_select_fields(&$fields) {
	//$ret=implode(',',array_keys($fields) );
	$flds=array();
	$data=array();
	foreach($fields as $key=>$val) {
		//转换 select  a as b 
		$fieldname=trim($key);
		$fieldname=explode(' ',$fieldname);
		$fieldname = array_pop($fieldname);
		if(strpos($fieldname,'.')!==false){
			$fieldname = trim(strstr($fieldname, '.'),'.');
		}
		$data[$fieldname]=$val;
		unset($fields[$key]);
		
		//处理favke
		if(!$val['fake']){
			$flds[]=$key;
		}else{
			$data[$fieldname]['nosort']=1; //假选列无排序
		}
		
	}

	$fields=$data;
	
	//s($fields,1);

	Return implode(',',$flds);
}



?>