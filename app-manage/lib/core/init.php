<?php



define ( '__ETC', __ROOT_DIR__ . '/etc' );
define ( '__LIB_DIR', __ROOT_DIR__ . '/lib' ); //系统类库
define ( '__SRC_DIR', __ROOT_DIR__ . '/src' );// 业务代码


define ( '__CACHE_DIR', realpath ( __ROOT_DIR__ . '/../app-env/cache/' ) . '/' ); // 缓存目录
define ( '__UPLOAD_DIR', realpath ( __ROOT_DIR__ . '/../app-env/upload/' ) . '/' ); // 上传目录
define ( '__LOG_DIR', realpath ( __ROOT_DIR__ . '/../app-env/log/' ) . '/' ); // 上传目录
define ( '__ROOT_LIB_DIR', realpath ( __ROOT_DIR__ . '/../app-lib' ) ); // 系统根类库
define ( '__ROOT_ETC__', realpath ( __ROOT_DIR__ . '/../app-env/etc' ) );


include __ROOT_ETC__.'/global.setting.php';	//root-etc
include __ETC . '/global.setting.php';  //local-etc

// 运行环境:'dev', 'test', 'prod'
switch (__ENV__) {
	case "dev" :
		ini_set ( 'display_errors', 'On' );
		error_reporting ( E_ALL ^ E_NOTICE );
		break;
	case "test" :
		ini_set ( 'display_errors', 'On' );
		error_reporting ( E_ALL ^ E_NOTICE ^ E_STRICT );
		break;
	case "prod" :
	default :
		ini_set ( 'display_errors', 'Off' );
		break;
}
////. __ROOT_DIR__ . '/lib/PHPExcel/' . PATH_SEPARATOR
set_include_path ( '.' . PATH_SEPARATOR . __SRC_DIR . PATH_SEPARATOR. __LIB_DIR . PATH_SEPARATOR 
. __LIB_DIR . '/Models/' . PATH_SEPARATOR . __ROOT_DIR__ . '/lib/' . PATH_SEPARATOR 
. __ROOT_DIR__ . '/lib/Models/' . PATH_SEPARATOR 
. __ROOT_LIB_DIR . '/Models/' . PATH_SEPARATOR . __ROOT_LIB_DIR . PATH_SEPARATOR 
. __ROOT_DIR__.'/lib/Page'. PATH_SEPARATOR
. get_include_path () );
function __autoload($classname) {    
	$path = explode ( '_', $classname );
	$phpfile = str_replace ( array('_','\\'), '/', $classname ) . ".php";	
	switch ($path [0]) {
		case 'Core' :
		case 'Com' :
			include __ROOT_LIB_DIR . '/' . $phpfile;
			break;
		default :
		    
			include $phpfile;
			break;
	}
}

// =====================================

/**
 * URL relative constants
 * TODO : could be changed by yourself's settings !!!
 */
define ( '__HTTP_HOST', 'http://' . $_SERVER ['HTTP_HOST'] );
define ( '__HTTP_ROOT', '/' );

/**
 * DB config ini file
 * TODO : could be changed by yourself's settings !!!
 */
define ( '__DB_INI_FILE', realpath ( __ROOT_DIR__ . '/../app-env/etc/database.admin.ini' ) );

/**
 * Error's messages ini file
 * TODO : could be changed by yourself's settings !!!
 */

define ( '__MAP_INI_FILE', realpath ( __ETC . '/backend.mapping.ini' ) );
define ( '__MSG_INI_FILE', realpath ( __ETC . '/backend.errors.ini' ) );

/**
 * Template relevant settings
 * TODO : could be changed by yourself's settings !!!
 */
define ( '__TPL_ENGINE', 'Smarty' );
define ( '__TPL_LIB_PATH', 'Smarty_3' );
define ( '__TPL_SMARTY_PATH', __ROOT_DIR__ . '/tpl/' );
define ( '__TPL_CACHE_PATH', __CACHE_DIR . '/' . __APP__ . '/' );

  

?>