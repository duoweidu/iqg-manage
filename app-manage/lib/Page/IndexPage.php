<?php
require_once 'MyFw/App/Backend/Page.php';

/**
 *
 * @package MyFw_App_Backend
 */
class IndexPage extends MyFw_App_Backend_Page {
	public function indexAction() {
		
		// get app menu list
		$appDao = new MyFw_Model_App (); // $this->dao->load('MyFw_App');
		$appList = $appDao->getAppListByRole ( $this->admin ['role'] );
		
		$this->view->appList = $appList;
	}
}