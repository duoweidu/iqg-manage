<?php
require_once 'MyFw/App/Backend/Page.php';
require_once 'Zend/Validate.php';

/**
 *  欢迎、修改密码
 * @package MyFw_App_Backend
 */
class CommonPage extends MyFw_App_Backend_Page {
	public function _init() {
				 
	}
	public function indexAction() {
		$this->authenticate ();		 
	}
	
	public function personalAction() {
		! empty ( $_SESSION ['admin'] ) ? $_SESSION ['adminForEdit'] = $_SESSION ['admin'] : "";
		$aclUserDao =new MyFw_Model_AclUser() ;//$this->dao->load ( 'MyFw_AclUser' );
		$userId = $_SESSION ['adminForEdit'] ['id'] ? $_SESSION ['adminForEdit'] ['id'] : 0;
		$user = $aclUserDao->read ( $_SESSION ['adminForEdit'] ['id'] );
		// do post
		if ($_POST) {
			// validation
			if (! $userId) {
				$this->addError ( 'common.notempty', 'User Id' );
			}
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'User name' );
			}
			if (! Zend_Validate::is ( $this->param ( 'pass' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Pass' );
			}
			if (! Zend_Validate::is ( $this->param ( 'passwords' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Passwords' );
			}
			if (Fw_Util::md5 ( $_POST ["oldpass"] ) != $user ["pass"]) {
				$this->addError ( 'common.error', 'Old Pass' );
			}
			if ($_POST ["pass"] != $_POST ["passwords"]) {
				$this->addError ( 'common.notequal', 'Pass', 'passwords' );
			}
			if ($this->noError ()) {
				$data ['name'] = $this->param ( 'name' );
				if ($this->param ( 'pass' )) {
					$data ['pass'] = Fw_Util::md5 ( $this->param ( 'pass' ) );
				}
				// check pass was edited
				$directly = 0;
				if ($user ['passedit']) {
					$data ['passedit'] = 0;
					$directly = 1;
				}
				// do update
				if ($userId) {
					$aclUserDao->update ( $data, 'id=' . $userId );
					$this->addErrorMsg ( 'Personal Infomation updated successfully' );
				}
				$this->session ( 'admin', $_SESSION ['adminForEdit'] );
				// destroy session adminForEdit
				unset ( $_SESSION ['adminForEdit'] );
				// redirect to homepage
				$directly ? $this->forward ( '/index' ) : "";
			}
		}
		
		$this->view->user = $user;
	}
}
