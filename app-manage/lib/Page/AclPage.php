<?php
require_once 'MyFw/App/Backend/Page.php';
require_once 'Zend/Validate.php';

/**
 *	整个系统管理模块
 * @package MyFw_App_Backend
 */
class AclPage extends MyFw_App_Backend_Page {
	
	protected $check_role = 'SA,AM';
	
	public function __init() {
		parent::__init (); // overload parent class's method	
		
		// check The user in role sa
		$this->view->alc_sa = $this->acl_sa = in_array ( "1", $this->admin['role'] ) ?  1 : 0;
		
	}
	public function __done() {
		$this->view->max_role_size = max ( count ( $this->view->allroles ), count ( $this->view->selroles ), 5 );
	}
	
	public function indexAction() {
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////
	// User actions
	public function userListAction() {
		// var_dump( $this->session('admin') );
		$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
		$this->view->userList = $aclUserDao->getUserList ();
		$this->view->userLevel = $GLOBALS ['USER_LEVEL'];
		$this->render ( 'acl/user/list.tpl' );
	}
	
	public function userAddAction() {
		$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
		
		$this->view->userCity = $GLOBALS['CITY'];
		$this->view->userLevel = $GLOBALS ['USER_LEVEL'];
		$this->view->leaderList = $this->getTableList ( 'AclUserExt', 'uid,realname', 'where level in(2,3,4)' );
		$tab = new AclUserExt ();
		// $tab->debug();
		// do post
		if ($_POST) {
			// validation
			$this->view->userCacheRoles = $aclUserDao->getRoleNameByRoleId ( $_POST ['roles'] );
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '用户名' );
			}
			if (! Zend_Validate::is ( $this->param ( 'pass' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '用户密码' );
			}
			if (! Zend_Validate::is ( $this->param ( 'realname' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '真实姓名' );
			}
			// if (!Zend_Validate::is($this->param('phone'), 'NotEmpty')) {
			// $this->addError('common.notempty', '联系电话');
			// }
			// else if (!preg_match("/((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/", $this->param('phone'))) {
			// $this->addError('common.formaterror', '联系电话');
			// }
			if (! Zend_Validate::is ( $this->param ( 'email' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '电子邮件' );
			} else if (! preg_match ( "/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $this->param ( 'email' ) )) {
				$this->addError ( 'common.formaterror', '电子邮件' );
			}
			if (! Zend_Validate::is ( $this->param ( 'lookup_information_nums' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '每日可查看的用户信息数' );
			}
			if (! $this->param ( 'roles' )) {
				$this->addError ( 'common.notempty', '角色选择' );
			}
			if ($this->_checkUserExists ( null, $this->param ( 'name' ) )) {
				$this->addError ( 'common.exists', '用户名' );
			}
			
			if ($this->noError ()) {
			    $department = trim( $this->param ( 'department' ));
			    if(!is_numeric($department)){
			    	$ttt = new MyFw_Model_Department (); //
			    	$department= $ttt->checkDepartment($department,$this->admin['name']);
			    }
				$department_name = st ( 'Department' )->scalar ( "department_name", "where department_id=" . $department );
				// prepare data
				$data ['name'] = $this->param ( 'name' );
				$data ['realname'] = $this->param ( 'realname' );
				$data ['phone'] = $this->param ( 'phone' );
				$data ['email'] = $this->param ( 'email' );
				$data ['department'] = $department_name; // $this->param('department');
				$data ['passedit'] = $this->param ( 'passedit' );
				$data ['lookup_information_nums'] = $this->param ( 'lookup_information_nums' );
				$data ['pass'] = Fw_Util::md5 ( $this->param ( 'pass' ) );
				// do create
				$userId = $aclUserDao->create ( $data );
				if ($userId) {
					$aclUserDao->updateRoles ( $userId, $this->param ( 'roles' ) );
				}
				 
				// update user extend info
				$level = $this->param ( 'level' );
				$userExtData ['pid'] = (in_array ( $level, array (1,2 ) )) ? $this->param ( 'pid' ) : 0;
				$userExtData ['realname'] = $this->param ( 'realname' );
				$userExtData ['level'] = $level;
				$userExtData ['cityid'] = $this->param ( 'cityid' );
				$userExtData ['uid'] = $userId;
				$userExtData ['depid'] = $department;
				
				$this->insertIntoTable ( 'AclUserExt', $userExtData );
				
				$this->forward ( 'userlist' );
			}
		}
		
		// default data
		$this->view->user = $_POST;
		// select department
		$aclDepartmentDao = new MyFw_Model_Department (); // $this->dao->load('MyFw_Department');
		$this->view->alldepartment = $aclDepartmentDao->getAllDP ();
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllPrivs ( $this->admin ['role'] );
		$this->view->selroles = $aclRoleDao->getRoleByUserId ( 1, $this->getRoleIds ( $this->view->allroles ) );
		$this->render ( 'acl/user/add.tpl' );
	}
	
	public function userDelAction() {
		if (! $this->acl_sa) {
			echo "You don't have permission to access";
			exit ();
		}
		if ($this->param ( 'id' )) {
			$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
			                                         // $aclUserDao->delete($this->param('id'));
			$aclUserDao->deleteUser ( $this->param ( 'id' ) ); // logically delete user
			$aclUserDao->updateRoles ( $this->param ( 'id' ) );
		}
		$this->forward ( 'userlist' );
	}
	
	public function userEditAction() { 
		
		$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
		
		$this->view->yesno = $GLOBALS['YES_NO'];
		$this->view->userCity = $GLOBALS['CITY'];
		$this->view->userLevel = $GLOBALS['USER_LEVEL'];
		$this->view->leaderList = $this->getTableList ( 'AclUserExt', 'uid,realname', 'where level in (2,3,4) and uid != ' . $this->param ( 'id' ) );
		$this->view->curLevel = $this->getRowRecord ( 'AclUserExt', '*', 'where uid = ' . $this->param ( 'id' ) );
		//s($this->view->curLevel);
		$user = $aclUserDao->read ( $this->param ( 'id' ) );
		// var_dump($this->view->curLevel,$this->param('department'));
		// select department
		$aclDepartmentDao = new MyFw_Model_Department (); // $this->dao->load('MyFw_Department');
		$this->view->alldepartment = $aclDepartmentDao->getAllDP ();
		// do post
		if ($_POST) {
			// merged roles
			$roles = $this->mergeRoles ( $this->param ( 'roles_' ), $this->param ( 'roles' ) );
			// validation
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '用户名' );
			}
			if (! Zend_Validate::is ( $this->param ( 'realname' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '真实姓名' );
			}
			// if (!Zend_Validate::is($this->param('phone'), 'NotEmpty')) {
			// $this->addError('common.notempty', '联系电话');
			// }
			// else if (!preg_match("/((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/", $this->param('phone'))) {
			// $this->addError('common.formaterror', '联系电话');
			// }
			if (! Zend_Validate::is ( $this->param ( 'email' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '电子邮件' );
			} else if (! preg_match ( "/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $this->param ( 'email' ) )) {
				$this->addError ( 'common.formaterror', '电子邮件' );
			}
			if (! Zend_Validate::is ( $this->param ( 'lookup_information_nums' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', '每日可查看的用户信息数' );
			}
			if (! $roles) {
				$this->addError ( 'common.notempty', 'Role list' );
			}
			if ($this->noError ()) {
			    $department = trim( $this->param ( 'department' ));			    
			    if(!is_numeric($department)){
			         $ttt = new MyFw_Model_Department (); //
			         $department= $ttt->checkDepartment($department,$this->admin['name']);
			    }
				$department_name = st ( 'Department' )->scalar ( "department_name", "where department_id=" . intval ( $department ) );
				// prepare data
				$data ['name'] = $this->param ( 'name' );
				$data ['realname'] = $this->param ( 'realname' );
				$data ['phone'] = $this->param ( 'phone' );
				$data ['email'] = $this->param ( 'email' );
				$data ['department'] = $department_name; // $this->param('department');
				$data ['lookup_information_nums'] = $this->param ( 'lookup_information_nums' );
				$data ['islock'] = $this->param ( 'islock' );
				if ($this->param ( 'pass' )) {
					$data ['pass'] = Fw_Util::md5 ( $this->param ( 'pass' ) );
				}
				//st('AclUserExt')->debug();
				//var_dump($roles);
				// do update
				if ($this->param ( 'id' )) {
					$aclUserDao->update ( $data, 'where id=' . $this->param ( 'id' ) );
					$aclUserDao->updateRoles ( $this->param ( 'id' ), $roles );
					// Check Department Exists
					//s($_POST['department'],1);
					
					                                     // update user extend info
					$level = $this->param ( 'level' );
					$userExtData ['pid'] = (in_array ( $level, array (1,2 ) )) ? $this->param ( 'pid' ) : 0;
					$userExtData ['realname'] = $this->param ( 'realname' );
					$userExtData ['level'] = $level;
					$userExtData ['cityid'] = $this->param ( 'cityid' );
					$userExtData ['depid'] = $department;
					
					$count = $this->getTableCount ( 'AclUserExt', 'where uid = ' . $this->param ( 'id' ) );
					if ($count) {
						$this->updateTable ( 'AclUserExt', $userExtData, 'where uid = ' . $this->param ( 'id' ) );
					} else {
						$userExtData ['uid'] = $this->param ( 'id' );
						$this->insertIntoTable ( 'AclUserExt', $userExtData );
					}
					$this->forward ( 'userlist' );
				}
			}
		}
		
		// default data
		$this->view->user = $user;
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllPrivs ( $this->admin ['role'] );
		$this->view->selroles = $aclRoleDao->getRoleByUserId ( $this->param ( 'id' ), $this->getRoleIds ( $this->view->allroles ) );
		$this->view->oldroles = $this->buildRoles ( $this->filterOldRoles ( $this->view->selroles ) );
		
		if($showbydep){
			$this->render ( 'acl/user/edit_bydep.tpl' );
		}else {
			$this->render ( 'acl/user/edit.tpl' );
		}
		
	}
	
	public function userImportAction() {
		if (! empty ( $_FILES )) {
			$checkUserArr = array ();
			$errorUserArr = array ();
			$uploadFile = 'userimport.csv';
			$uploadPath = __UPLOAD_DIR . '/be-acl-userimport';
			if (! file_exists ( $uploadPath ))
				mkdir ( $uploadPath, 0777 );
			$uploadRes = Fw_Util::upload ( 'userfile', $uploadPath, $uploadFile );
			if ($uploadRes) {
				$csvText = file_get_contents ( $uploadPath . '/' . $uploadFile );
				if ($csvText) {
					$parseRes = Fw_Util::parse_csv ( $csvText );
					foreach ( ( array ) $parseRes as $row ) {
						// invalid user data
						if (count ( $row ) != 4 || ! is_numeric ( $row [0] ) || ! is_string ( $row [1] ) || ! is_string ( $row [2] ) || ! is_numeric ( $row [3] )) {
							$errorUserArr [] = $row [0] . ' (Format Error)';
							continue;
						}
						// exists user data
						if ($this->_checkUserExists ( $row [0], $row [1] )) {
							$errorUserArr [] = $row [0] . ' (ID or Name Exists)';
							continue;
						}
						// passed user data
						$checkUserArr [] = $row;
					}
				}
			}
			if (count ( $errorUserArr )) {
				$this->addError ( 'common.file.formaterr' );
				$this->view->errorUserIds = implode ( '<br/>', $errorUserArr );
			}
			if (! count ( $checkUserArr )) {
				$this->addError ( 'common.notempty', 'Valid User List' );
			}
			if ($this->noError ()) {
				$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
				$aclUserRes = $aclUserDao->importUser ( $checkUserArr );
				if (! $aclUserRes) {
					$this->addError ( 'common.db.inserterr' );
				} else {
					$this->forward ( 'userlist' );
				}
			}
		}
		
		$this->render ( 'acl/user/import.tpl' );
	}
	protected function _checkUserExists($uid, $uname) {
		if (is_numeric ( $uid ) || strlen ( $uname )) {
			if (! $this->userIdHash || ! $this->userNameHash) {
				$this->userIdHash = array ();
				$this->userNameHash = array ();
				$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
				$userRes = $aclUserDao->getAllUsers ();
				foreach ( $userRes as $user ) {
					$this->userIdHash [$user ['id']] = 1;
					$this->userNameHash [$user ['name']] = 1;
				}
			}
			$idExists = array_key_exists ( $uid, $this->userIdHash );
			$nameExists = array_key_exists ( $uname, $this->userNameHash );
			return $idExists || $nameExists;
		}
		return false;
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////
	// Role actions
	public function roleListAction() {
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->roleList = $aclRoleDao->getRoleList ();
		$this->render ( 'acl/role/list.tpl' );
	}
	public function roleAddAction() {
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		                                         
		// do post
		if ($_POST) {
			// validation
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Role name' );
			}
			if (! Zend_Validate::is ( $this->param ( 'alias' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Alias name' );
			}
			if ($this->noError ()) {
				// prepare data
				$data ['name'] = strtoupper ( $this->param ( 'name' ) );
				$data ['alias'] = $this->param ( 'alias' );
				// do create
				$roleId = $aclRoleDao->create ( $data );
				if ($roleId) {
					$aclRoleDao->updatePrivs ( $roleId, $this->param ( 'privs' ) );
				}
				$this->forward ( 'rolelist' );
			}
		}
		
		// default data
		$this->view->role = $_POST;
		
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllRoles ();
		
		$this->render ( 'acl/role/add.tpl' );
	}
	public function roleEditAction() {
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		
		$role = $aclRoleDao->read ( $this->param ( 'id' ) );
		
		// do post
		if ($_POST) {
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Role name' );
			}
			if (! Zend_Validate::is ( $this->param ( 'alias' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Alias name' );
			}
			if ($this->noError ()) {
				$data ['name'] = strtoupper ( $this->param ( 'name' ) );
				$data ['alias'] = $this->param ( 'alias' );
				if ($this->param ( 'id' )) {
					$aclRoleDao->update ( $data, 'where id=' . $this->param ( 'id' ) );
					$aclRoleDao->updatePrivs ( $this->param ( 'id' ), $this->param ( 'privs' ) );
					$this->forward ( 'rolelist' );
				}
			}
		}
		
		// default data
		$this->view->role = $role;
		
		// fill role select box
		// $aclRoleDao = $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllRoles ();
		$this->view->selroles = $aclRoleDao->getPrivByRoleId ( $this->param ( 'id' ) );
		
		$this->render ( 'acl/role/edit.tpl' );
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////
	// Resource actions
	public function resourceListAction() {
		$aclResDao = new MyFw_Model_AclResource (); // $this->dao->load('MyFw_AclResource');
		$this->view->resourceList = $aclResDao->getResourceList ();
		$this->render ( 'acl/resource/list.tpl' );
	}
	public function resourceAddAction() {
		$aclResDao = new MyFw_Model_AclResource (); // $this->dao->load('MyFw_AclResource');
		                                            
		// do post
		if ($_POST) {
			// validation
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Resource name' );
			}
			if (! Zend_Validate::is ( $this->param ( 'description' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'Resource description' );
			}
			if (! $this->param ( 'roles' )) {
				$this->addError ( 'common.notempty', 'Role list' );
			}
			if ($this->noError ()) {
				// prepare data
				$data ['name'] = $this->param ( 'name' );
				$data ['app_id'] = $this->param ( 'app_id' );
				$data ['description'] = $this->param ( 'description' );
				// do create
				$resourceId = $aclResDao->create ( $data );
				if ($resourceId) {
					$aclResDao->updateRoles ( $resourceId, $this->param ( 'roles' ) );
				}
				$this->forward ( 'resourcelist' );
			}
		}
		
		// default data
		$this->view->resource = $_POST;
		
		// fill app select list
		$this->view->appopts = $this->getAppOpts ();
		
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllPrivs ( $this->admin ['role'] );
		
		$this->render ( 'acl/resource/add.tpl' );
	}
	public function resourceDelAction() {
		if ($this->param ( 'id' )) {
			$aclResDao = new MyFw_Model_AclResource (); // $this->dao->load('MyFw_AclResource');
			$aclResDao->delete ( $this->param ( 'id' ) );
			$aclResDao->updateRoles ( $this->param ( 'id' ) );
		}
		$this->forward ( 'resourcelist' );
	}
	public function resourceEditAction() {
		$aclResDao = new MyFw_Model_AclResource (); // $this->dao->load('MyFw_AclResource');
		
		$user = $aclResDao->read ( $this->param ( 'id' ) );
		
		// do post
		if ($_POST) {
			// merged roles
			$roles = $this->mergeRoles ( $this->param ( 'roles_' ), $this->param ( 'roles' ) );
			// validation
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'User name' );
			}
			if (! $roles) {
				$this->addError ( 'common.notempty', 'Role list' );
			}
			if ($this->noError ()) {
				// prepare data
				$data ['name'] = $this->param ( 'name' );
				$data ['app_id'] = $this->param ( 'app_id' );
				$data ['description'] = $this->param ( 'description' );
				// do update
				if ($this->param ( 'id' )) {
					$aclResDao->update ( $data, 'id=' . $this->param ( 'id' ) );
					$aclResDao->updateRoles ( $this->param ( 'id' ), $roles );
					$this->forward ( 'resourcelist' );
				}
			}
		}
		
		// default data
		$this->view->resource = $user;
		
		// fill app select list
		$this->view->appopts = $this->getAppOpts ();
		
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllPrivs ( $this->admin ['role'] );
		$this->view->selroles = $aclRoleDao->getRoleByResourceId ( $this->param ( 'id' ), $this->getRoleIds ( $this->view->allroles ) );
		$this->view->oldroles = $this->buildRoles ( $this->filterOldRoles ( $this->view->selroles ) );
		
		$this->render ( 'acl/resource/edit.tpl' );
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////
	// Menu actions
	public function appListAction() {
	    //st ( 'App' )->debug();
		$appDao = new MyFw_Model_App (); // $this->dao->load('MyFw_App');
		$this->view->appTree = $appDao->getAppTree ();
		// Fw_Util::dump($this->view->appTree);
		$this->render ( 'acl/app/list.tpl' );
	}
	public function appAddAction() {
		$appDao = new MyFw_Model_App (); // $this->dao->load('MyFw_App');
		                                 
		// do post
		if ($_POST) {
			// validation
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'App / Menu name' );
			}
			if (! Zend_Validate::is ( $this->param ( 'pid' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'App / Menu level' );
			}
			if (! $this->param ( 'roles' )) {
				$this->addError ( 'common.notempty', 'Role list' );
			}
			if ($this->noError ()) {
				// prepare data
				$data ['pid'] = $this->param ( 'pid' );
				$data ['name'] = $this->param ( 'name' );
				$data ['path'] = $this->param ( 'path' );
				$data ['is_app'] = $this->param ( 'is_app' );
				$data ['order'] =   isset($_POST['order']) ? $this->param ( 'order' ) : 10;;
				// do create
				$appId = $appDao->create ( $data );
				if ($appId) {
					$appDao->updateRoles ( $appId, $this->param ( 'roles' ) );
				}
				$this->forward ( 'applist' );
			}
		}
		
		// default data
		$this->view->app = $_POST;
		
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllPrivs ( $this->admin ['role'] );
		
		$this->render ( 'acl/app/add.tpl' );
	}
	public function appDelAction() {
		if ($this->param ( 'id' )) {
			$appDao = new MyFw_Model_App (); // $this->dao->load('MyFw_App');
			$appDao->delete ( $this->param ( 'id' ) );
			$appDao->updateRoles ( $this->param ( 'id' ) );
		}
		$this->forward ( 'applist' );
	}
	public function appEditAction() {
		$appDao = new MyFw_Model_App (); // $this->dao->load('MyFw_App');
		
		$app = $appDao->read ( $this->param ( 'id' ) );
		$app_parent = $appDao->read ( $app ['pid'] );
		
		// do post
		if ($_POST) {
			// merged roles
			$roles = $this->mergeRoles ( $this->param ( 'roles_' ), $this->param ( 'roles' ) );
			// validation
			if (! Zend_Validate::is ( $this->param ( 'name' ), 'NotEmpty' )) {
				$this->addError ( 'common.notempty', 'App / Menu name' );
			}
			if (! $roles) {
				$this->addError ( 'common.notempty', 'Role list' );
			}
			if ($this->noError ()) {
				// prepare data
				$data ['name'] = $this->param ( 'name' );
				$data ['path'] = $this->param ( 'path' );
				$data ['order'] = isset($_POST['order']) ? $this->param ( 'order' ) : 10;
				if ($this->param ( 'pid' )) {
					$data ['pid'] = $this->param ( 'pid' );
				}
				// do update
				if ($this->param ( 'id' )) {
					$appDao->update ( $data, ' where id=' . $this->param ( 'id' ) );
					$appDao->updateRoles ( $this->param ( 'id' ), $roles );
					$this->forward ( 'applist' );
				}
			}
		}
		
		// default data
		$this->view->app = $app;
		$this->view->app_parent = $app_parent;
		
		// fill role select box
		$aclRoleDao = new MyFw_Model_AclRole (); // $this->dao->load('MyFw_AclRole');
		$this->view->allroles = $aclRoleDao->getAllPrivs ( $this->admin ['role'] );
		$this->view->selroles = $aclRoleDao->getRoleByAppId ( $this->param ( 'id' ), $this->getRoleIds ( $this->view->allroles ) );
		$this->view->oldroles = $this->buildRoles ( $this->filterOldRoles ( $this->view->selroles ) );
		
		$this->render ( 'acl/app/edit.tpl' );
	}
	public function appAjaxAction() {
		// get ajax options
		$this->view->opt = $this->param ( 'opt' );
		$this->view->sel = $this->param ( 'sel' );
		
		switch ($this->param ( 'opt' )) {
			case 'menu' :
				$this->view->appopts = $this->getAppOpts ( 1 );
				break;
			case 'app' :
				$this->view->appopts = $this->getAppOpts ( 23 );
				break;
		}
		
		$this->render ( 'acl/app/ajax.tpl' );
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////
	// Common methods
	protected function getAppOpts($level = 0) {
		$appDao = new MyFw_Model_App (); // $this->dao->load('MyFw_App');
		$appTreeList = $appDao->getAppTree ();
		
		// Fw_Util::dump($appTreeList);
		
		switch ($level) {
			case 1 :
				$appTreeList = $this->makeOpt ( $appTreeList, 'id', 'name' );
				break;
			case 2 :
				foreach ( ( array ) $appTreeList as $topList ) {
					if (! $topList ['id'])
						continue;
					$menuOpts = $this->makeOpt ( $topList ['list'], 'id', 'name' );
					$appTreeList [$topList ['id']] ['list'] = $menuOpts;
				}
				break;
			case 23 :
				foreach ( ( array ) $appTreeList as $topList ) {
					if (! $topList ['id'])
						continue;
						// Fw_Util::dump($topList);exit;
					
					$optArr = array ();
					foreach ( $topList ['list'] as $id => $d1 ) {
						$optArr [$d1 ['id']] = $d1 ['name'];
						
						// 涓夌骇鑿滃崟
						foreach ( $d1 ['list'] as $id2 => $d2 ) {
							$optArr [$d2 ['id']] = ' --' . $d2 ['name'];
						}
					}
					
					$menuOpts = $optArr; // $this->makeOpt($optArr, 'id', 'name');
					$appTreeList [$topList ['id']] ['list'] = $menuOpts;
				}
				break;
			case 3 :
			default :
				foreach ( ( array ) $appTreeList as $topList ) {
					if (! $topList ['id'])
						continue;
					foreach ( ( array ) $topList ['list'] as $menuList ) {
						if (! $menuList ['id'])
							continue;
						$menuOpts = $this->makeOpt ( $menuList ['list'], 'id', 'name' );
						$appTreeList [$topList ['id']] ['list'] [$menuList ['id']] ['list'] = $menuOpts;
					}
				}
				break;
		}
		
		// Fw_Util::dump($appTreeList);
		
		return $appTreeList;
	}
	protected function getRoleIds($roles = array()) {
		$role_ids = array ();
		foreach ( ( array ) $roles as $role ) {
			if ($role ['id'])
				$role_ids [] = $role ['id'];
		}
		return $role_ids;
	}
	protected static function filterOldRoles($roles = array()) {
		$role_ids = array ();
		foreach ( ( array ) $roles as $role ) {
			if ($role ['id'] && $role ['readonly'])
				$role_ids [] = $role ['id'];
		}
		return $role_ids;
	}
	protected static function buildRoles($roles = array()) {
		return is_array ( $roles ) ? implode ( ',', $roles ) : $roles;
	}
	protected static function mergeRoles($old_roles, $new_roles) {
		if (! is_array ( $old_roles ))
			$old_roles = explode ( ',', $old_roles );
		if (! is_array ( $new_roles ))
			$new_roles = explode ( ',', $new_roles );
		$merged = array_merge ( $old_roles, $new_roles );
		
		return array_filter ( $merged ); // skip empty item
	}
}
