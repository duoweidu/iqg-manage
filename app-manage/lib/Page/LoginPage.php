<?php
require_once 'MyFw/App/Backend/Page.php';

/**
 * 登录，注销
 * 
 * @package MyFw_App_Backend
 */
class LoginPage extends MyFw_App_Backend_Page
{

    protected $check_acl = false;

    public function __init()
    {
        parent::__init(); // overload parent class's method
        
        $this->logLoginDao = new MyFw_Model_LogLogin(); // $this->dao->load('MyFw_LogLogin');
    }

    public function indexAction()
    {
        if ($this->param('user') && $this->param('ocip')) {
            $this->addError('login.occupied', base64_decode($this->param('user')), long2ip($this->param('ocip')));
        }
    }

    public function loginAction()
    {
        $failed_limit = 10;
        $locked_times = 30 * 60;
        // form validation
        /*
         * if (!$this->param('username') || !$this->param('password') || !$this->param('securitycode')) { $this->addError('login.notempty'); }
         */
        if (! $this->param('username') || ! $this->param('password')) {
            $this->addError('login.notempty');
        } elseif (strcasecmp($this->param('sessionnum'), 'iqg')) { // $this->session ( 'sessionnum' )
            $this->addError('common.scodeerr');
        }
        
        // login process
        if ($this->noError()) {
            $aclUserDao = new MyFw_Model_AclUser();
            $admin = $aclUserDao->authenticate($this->param('username'), $this->param('password'));
            // login failed
            if (! is_array($admin)) {
                // no user
                if (! $admin) {
                    $this->addError('login.nouser');
                } elseif (is_numeric($admin)) { // password error
                    $failed_times = $this->logLoginDao->getFailedTimes($admin, $locked_times);
                    if ($failed_times >= $failed_limit) {
                        $this->addError('login.locked');
                    } else {
                        $try_times = intval($failed_limit - $failed_times - 1);
                        if ($try_times > 0) {
                            $this->addError('login.failed', $try_times);
                        } else {
                            $this->addError('login.locked');
                        }
                        $this->logLogin($admin, true);
                    }
                }
            }             // login ok
            else {
                $failed_times = $this->logLoginDao->getFailedTimes($admin['id'], $locked_times);
                if ($failed_times >= $failed_limit) {
                    $this->addError('login.locked');
                } else {
                    // whether super admin
                    $admin['sa'] = strcasecmp($admin['name'], $this->sa) ? false : true;
                    // store admin into session
                    // $this->session('admin', $admin);
                    // add login log
                    $this->logLogin($admin['id'], false);
                    // store admin into session
                    $this->session('admin', $admin);
                    // whether passEdit
                    $passedit = $aclUserDao->getPassEditType($this->param('username'));
                    if ($passedit) {
                        $this->session('adminForEdit', $admin);
                        $this->forward("/common/personal");
                    } else {
                        // redirect to homepage
                        $this->forward('/index');
                    }
                }
            }
        }
        
        // also use index template
        $this->render('login/index.tpl');
    }

    public function logoutAction()
    {
        if ($admin = $this->session('admin')) {
            // delete session from login log
            // $this->logLoginDao->delete($admin['id'], 'user_id');
            $this->logLoginDao->delete(session_id(), 'session_id');
            // clear admin from session
            $this->session('admin', '');
        }
        
        $this->forward($this->root);
    }
    
    // Add login log
    private function logLogin($user_id, $failed = true)
    {
        $is_failed = ($failed) ? 'YES' : 'NO';
        $this->logLoginDao->create(array(
            'ip' => ip2long(Fw_Util::clientip()),
            'user_id' => $user_id,
            'session_id' => session_id(),
            'is_failed' => $is_failed
        ));
    }

    function imgcodeAction()
    {
        session_start();
        
        // /var_dump(__DIR__);
        
        $captcha = new Com_Captcha();
        // $captcha->resourcesPath= __ROOT_LIB__ .'/resourse/';
        $captcha->session_var = 'sessionnum';
        // $captcha->imageFormat = 'png';
        $captcha->lineWidth = 1;
        // $captcha->scale = 3; $captcha->blur = true;
        $captcha->width = 140;
        $captcha->height = 50;
        $captcha->minWordLength = 5;
        $captcha->maxWordLength = 5;
        
        $captcha->CreateImage();
    }
}
