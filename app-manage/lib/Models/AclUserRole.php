<?php
class AclUserRole extends Db_Admin {

	public $_name = 'acl_user_role';
	public $_primarykey = 'role_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'user_id');                      //int(10)		
		self::fill_int($para,$data,'role_id');                      //int(10)		
		return $data;
	}

} 
?>