<?php
class AclUser extends Db_Admin {

	public $_name = 'acl_user';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10)		
		self::fill_str($para,$data,'name');                         //varchar(100)		
		self::fill_str($para,$data,'pass');                         //varchar(32)		
		self::fill_int($para,$data,'isdel');                        //int(2)		
		self::fill_int($para,$data,'passedit');                     //smallint(1)		
		self::fill_str($para,$data,'realname');                     //varchar(20)		
		self::fill_str($para,$data,'phone');                        //varchar(20)		
		self::fill_email($para,$data,'email');                      //varchar(50)		
		self::fill_str($para,$data,'department');                   //varchar(30)		
		self::fill_int($para,$data,'lookup_information_nums');      //int(8)		
		self::fill_int($para,$data,'last_login');                   //int(11)		
		self::fill_int($para,$data,'islock');                       //tinyint(4)		
		return $data;
	}

} 
?>