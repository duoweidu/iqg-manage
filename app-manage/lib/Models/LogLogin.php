<?php
class LogLogin extends Db_Admin {

	public $_name = 'log_login';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(11)		
		self::fill_int($para,$data,'ip');                           //bigint(20)		
		self::fill_int($para,$data,'user_id');                      //int(11)		
		self::fill_str($para,$data,'session_id');                   //varchar(50)		
		self::fill_str($para,$data,'is_failed');                    //enum('YES','NO')		
		self::fill_str($para,$data,'log_time');                     //timestamp		
		return $data;
	}

} 
?>