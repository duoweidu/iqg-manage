<?php
class AclRole extends Db_Admin {

	public $_name = 'acl_role';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10)		
		self::fill_str($para,$data,'name');                         //varchar(50)		
		self::fill_str($para,$data,'alias');                        //varchar(50)		
		return $data;
	}

} 
?>