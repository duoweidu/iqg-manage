<?php
class AclResourceRole extends Db_Admin {

	public $_name = 'acl_resource_role';
	public $_primarykey = 'role_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'resource_id');                  //int(10)		
		self::fill_int($para,$data,'role_id');                      //int(10)		
		return $data;
	}

} 
?>