<?php
class AclUserExt extends Db_Admin {

	public $_name = 'acl_user_ext';
	public $_primarykey = 'ue_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'ue_id');                        //int(10)		
		self::fill_int($para,$data,'uid');                          //int(10)		
		self::fill_str($para,$data,'realname');                     //varchar(20)		
		self::fill_int($para,$data,'level');                        //tinyint(1)		
		self::fill_int($para,$data,'pid');                          //int(10)		
		self::fill_int($para,$data,'depid');                        //int(11)		
		self::fill_int($para,$data,'cityid');                       //int(11)		
		self::fill_int($para,$data,'isdel');                        //tinyint(4)		
		return $data;
	}

} 
?>