<?php
class App extends Db_Admin {

	public $_name = 'app';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10)		
		self::fill_str($para,$data,'name');                         //varchar(50)		
		self::fill_str($para,$data,'path');                         //varchar(50)		
		self::fill_int($para,$data,'pid');                          //int(10)		
		self::fill_int($para,$data,'order');                        //int(10)		
		self::fill_int($para,$data,'is_app');                       //enum('YES','NO')		
		return $data;
	}

} 
?>