<?php
class AclResource extends Db_Admin {

	public $_name = 'acl_resource';
	public $_primarykey = 'id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'id');                           //int(10)		
		self::fill_int($para,$data,'app_id');                       //int(10)		
		self::fill_str($para,$data,'name');                         //varchar(50)		
		self::fill_str($para,$data,'description');                  //varchar(100)		
		return $data;
	}

} 
?>