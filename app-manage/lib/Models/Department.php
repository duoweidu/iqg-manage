<?php
class Department extends Db_Admin {

	public $_name = 'department';
	public $_primarykey = 'department_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'department_id');                //tinyint(3) unsigned		
		self::fill_str($para,$data,'department_name');              //varchar(80)		
		self::fill_str($para,$data,'department_desc');              //varchar(500)		
		self::fill_str($para,$data,'department_createtime');        //timestamp		
		self::fill_str($para,$data,'department_create_user');       //varchar(20)		
		self::fill_str($para,$data,'department_update_user');       //varchar(20)		
		self::fill_int($para,$data,'department_condition');         //tinyint(3)		
		self::fill_int($para,$data,'department_updatetime');        //timestamp		
		return $data;
	}

} 
?>