<?php
class AclRolePriv extends Db_Admin {

	public $_name = 'acl_role_priv';
	public $_primarykey = 'priv_id';

	function prepareData($para) {
		$data=array();
		self::fill_int($para,$data,'role_id');                      //int(10)		
		self::fill_int($para,$data,'priv_id');                      //int(10)		
		return $data;
	}

} 
?>