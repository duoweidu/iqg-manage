<?php
/**
 * TC Acl
 *
 * @category   TC
 * @package    MyFw_Acl
 * @author     
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'Fw/Acl.php';

/**
 *
 * @package MyFw_Acl
 */
class MyFw_Acl extends Fw_Acl {
	/**
	 * Judge the privilege is allowed
	 * 
	 * @param mixed $roles        	
	 * @param string $resource        	
	 * @param string $privilege        	
	 * @return bool
	 */
	public function isAllowed($roles = null, $resource = null, $privilege = null) {	    
	    /*
	    //如果资源部存在，则允许
	    if(!parent::has($resource)){
	    	return true;
	    }*/
	    
		if (! is_array ( $roles )) {
			return parent::isAllowed ( $roles, $resource, $privilege );
		}
		foreach ( ( array ) $roles as $role ) {
			if (parent::isAllowed ( $role, $resource, $privilege )) {
				return true;
			}
		}
		return false;
	}
}
