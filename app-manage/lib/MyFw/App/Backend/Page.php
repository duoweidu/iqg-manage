<?php
require_once 'MyFw/App/Backend.php';
require_once 'MyFw/Acl/Backend.php';

/**
 *
 * @package MyFw_App_Backend
 */
class MyFw_App_Backend_Page extends MyFw_App_Backend {
	protected $check_role = '';
	protected $check_res = '';
	protected $check_acl = true;
	protected $check_acl_already = false;
	
	/**
	 * Do something before dispatch
	 *
	 * @see Fw_App_Dispatcher
	 */
	public function __init() {		 
		
		// Setting acl control object
		$this->view->_acl = $this->acl = MyFw_Acl_Backend::getInstance ();
		
		//业务控制器初始化： 可用于提前控制访问权限
		if(method_exists($this,'_init')){
			$this->_init();
		}
		
		 
		if ($this->check_acl) {
			//检查是否登录和菜单资源
			$this->authenticate();
			
			//检查是否拥有对应角色
			if ($this->check_role) {
				$this->checkRole();
			}
			
			//检查是否拥有可用资源
			if (empty($this->check_res)) {
			    $this->check_res = $this->getPageName();
			}			
			$this->checkResourse($this->check_res);
			
			
		}
		
		
		
		
	}
	
	/**
	 *  $role='SA,XSRY'; 性能快
	 * @param string $role
	 * @return boolean
	 */
	public function checkRole($role="") {
		$role = $role ? $role : $this->check_role;			
		if(empty($role)){ return true; }
		
		$role= trim($role).",SA";
		
		$appRoles = explode(',', $role);		
		if(count($appRoles)==0){ 
			return true; 
		}
		
		$myRoles =  $this->admin['roleName'];
		//strcasecmp — 二进制安全比较字符串（不区分大小写）
		$tmp= array_uintersect($myRoles, $appRoles, "strcasecmp");
		
		if(count($tmp)>0){
			return true;
		}else{
			$this->alert("WARNING: You don't have permission to access!",  '/login/logout', 'danger');
			return false;
		}
		
	}
	
	/**
	 * 
	 * @param arr or string $resourse
	 * @param arr $roles
	 * @return boolean
	 */
	public function checkResourse($resourse ,$roles=array()) {	
		$resourse = $resourse ? $resourse : $this->check_res;	
		$roles = $roles ? $roles : $this->admin['role'];
		
		if(empty($resourse)){ return true; }
		
		$resArr = is_array($resourse) ? $resourse : array($resourse);		
		//s($resArr);
		//循环检查，只要包含其中任一资源即满足成功返回
		foreach ($resArr as $resname){
		    if($this->acl->has($resname)){
		        if ($this->acl->isAllowed($roles, $resname)) {
		        	return true;
		        }
		    }else{
		        //没有设置资源的时候，默认允许
    			return true;
		    }
		}
		$this->alert("WARNING: You don't have permission to access!",  '/login/logout', 'danger');
		return false;
	}
	
	
	/**
	 * See if the user is logined
	 *
	 * @uses subclasses redirect to login page if user is not logined
	 * @return unknown
	 */
	public function authenticate() {
		if ($this->check_acl_already){
			return true;
		}
		
		$this->check_acl_already = true;
		// check if login
		if (! $this->session ( 'admin' )) {
			$this->forward ( $this->root . 'login/' );
		}
		
		// set admin info object
		$this->view->_admin = $this->admin = $this->session ( 'admin' );
		
		//s($this->admin,1);
		//s($this->admin['roleName'],1);
		
		
		// check if user logined in other place
		$this->logLoginDao = new MyFw_Model_LogLogin(); // $this->dao->load('MyFw_LogLogin');
		$loginRes = $this->logLoginDao->getLastLogin ( $this->admin ['id'] );
		$loginSessionId = $loginRes ['session_id'] ? $loginRes ['session_id'] : null;
		if ($loginSessionId != session_id ()) {
			$this->forward ( $this->root . 'login/?user=' . base64_encode ( $this->admin ['name'] ) . '&ocip=' . $loginRes ['ip'] );
		}
		
		// check if this menu path is accessable
		$path = parse_url ( $_SERVER ['REQUEST_URI'] );
		if ($this->acl->has ( $path ['path'] )) {
			if (! $this->acl->isAllowed ( $this->admin ['role'], $path ['path'] )) {
				// redirect to referer page if has
				if (isset ( $_SERVER ['HTTP_REFERER'] ) && strlen ( $_SERVER ['HTTP_REFERER'] )) {
					$this->forward ( $_SERVER ['HTTP_REFERER'] );
				}
				// redirect to default page
				$this->forward ( $this->root . 'common/' );
			}
		}
	}
	
	
	public function display($hash, $tpl) {
		foreach ( $hash as $key => $val ) {
			$this->view->$key = $val;
		}
		
		$this->view->CDN_FILE = CDN_FILE;
		
		$this->render ( $tpl );
	}
	
	/**
	 * 
	 * @param unknown $msg
	 * @param string $type	success,info,warning,danger
	 * @param string $url
	 */
	public function alert($msg, $url="" ,$type="info" ) {
		
		$this->view->message = $msg;
		$this->view->message_type = $type;
		$this->view->message_url = $url;
		
		//var_dump($this->view);		
		$this->render ("frame/message.tpl" );
		
		$this->setInvoke(false);		
		return false;
	}
	
	


	public function jsonResulte($errno , $msg, $data=array() ,$ttl=0) {
	    $ret=array();
	    $ret['errno']=$errno;
	    $ret['msg']=$msg;
	    $ret['data']=$data;
	    
		switch ($ttl){
			case 0:
				break;
			case -1:
				header_cache(86400);
				break;
			default:
				header_cache($ttl);
				break;
		}
	
		exit(json_encode($ret));
	
	}
	
	public function json($array ,$ttl=0 ) {
		switch ($ttl){			
			case 0:				
				break;
			case -1:
				header_cache(86400);
				break;
			default:
				header_cache($ttl);
				break;
		}
		
		exit(json_encode($array));
	
	}
	
	// ######################### DB Operate/Created on 2011-10-08/@Author  ##########################
	/**
	 * Insert into table
	 *
	 * @param string $modelName        	
	 * @param array $data;        	
	 * @return int $insertid
	 */
	public function insertIntoTable($modelName, $data) {
		if (count ( $data ) != 0) {
			$tab = new $modelName ();
			$insertid = $tab->insert ( $data );
			unset ( $tab );
		}
		return $insertid;
	}
	
	/**
	 * update table
	 *
	 * @param string $modelName        	
	 * @param array $data;        	
	 * @param string $where        	
	 * @return int $affectedRows
	 */
	public function updateTable($modelName, $data, $where) {
		$tab = new $modelName ();
		// $tab->debug();
		$affectedRows = $tab->update ( $data, $where );
		unset ( $tab );
		Return $affectedRows;
	}
	
	/**
	 *
	 * @param string $modelName        	
	 * @param string $select_fields
	 *        	, 字段名单
	 * @param string $where_orderby_clause        	
	 * @param int $currpage
	 *        	, 当前页码[从 1 开始]
	 * @param int $page_size
	 *        	, 页面记录大小
	 * @param boolean $lock
	 *        	, 是否锁行[事务处理]
	 * @return array $arr
	 */
	public function getTableList($modelName, $select_fields, $where_orderby_clause, $currpage = '', $page_size = '', $lock = false) {
		$tab = new $modelName ();
		$arr = $tab->getAll ( $select_fields, $where_orderby_clause, $currpage, $page_size, $lock );
		unset ( $tab );
		return $arr;
	}
	
	/**
	 * 获取指定 字段 的信息
	 *
	 * @param string $modelName        	
	 * @param string $select_fields
	 *        	, 字段名单
	 * @param string $where_orderby_clause        	
	 * @param boolean $lock
	 *        	, 是否锁行[事务处理]
	 * @return mixed $val_or_row , 单值、数组
	 */
	public function getRowRecord($modelName, $select_fields, $where_orderby_clause, $lock = false) {
		$tab = new $modelName ();
		$val_or_row = $tab->scalar ( $select_fields, $where_orderby_clause, $lock );
		unset ( $tab );
		return $val_or_row;
	}
	
	/**
	 * 删除记录
	 *
	 * @param string $modelName        	
	 * @param string $where        	
	 * @return int $affectedRows ,影响行数
	 */
	public function deleteRecord($modelName, $where) {
		$tab = new $modelName ();
		$affectedRows = $tab->delete ( $where );
		unset ( $tab );
		Return $affectedRows;
	}
	
	/**
	 *
	 *
	 *
	 * 根据$where_clause计算总行数
	 *
	 * @param string $modelName        	
	 * @param string $where_orderby_clause        	
	 * @param boolean $lock
	 *        	, 是否锁行[事务处理]
	 * @return int $count
	 */
	public function getTableCount($modelName, $where_orderby_clause, $lock = false) {
		$tab = new $modelName ();
		$count = $tab->getCount ( $where_orderby_clause, $lock );
		unset ( $tab );
		Return $count;
	}
}