<?php
/**
 * TC App
 *
 * @category   TC
 * @package    MyFw_App
 * @author     
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'Fw/Exception.php';

/**
 *
 * @package MyFw_App
 */
class MyFw_App_Exception extends Fw_Exception {
	public function __construct($msg = '', $code = 0, Exception $e = null) {
		parent::__construct ( $msg, $code, $e );
	}
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}
}