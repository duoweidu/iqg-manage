<?php
/**
 * TC Dao
 *
 * @category   TC
 * @package    MyFw_Dao
 * @author     
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'Fw/Db/Dao.php';

/**
 *
 * @abstract
 *
 * @package MyFw_Dao
 */
class MyFw_Dao extends Fw_Db_Dao {
	/**
	 * Autoload TC Daos
	 *
	 * @param string $dao        	
	 * @return MyFw_Dao
	 */
	public static function load($class_name, $charset = 'utf8') {
		static $_model = array ();
		if (! isset ( $_model [$class_name] )) {
			require_once 'MyFw/Dao/' . str_replace ( '_', '/', $class_name ) . '.php';
			$_model [$class_name] = new $class_name ();
			$_model [$class_name]->charset ( $charset );
		}
		return $_model [$class_name];
	}
}