<?php
/**
 * TC Cache
 *
 * @category   TC
 * @package    MyFw_Cache
 * @author     
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'Fw/Cache.php';

/**
 *
 * @package MyFw_Cache
 */
class MyFw_Cache {
	/**
	 *
	 * @var Fw_Cache
	 */
	public static $_cache = null;
	
	/**
	 * Factory method for cache building
	 *
	 * @param string $type
	 *        	Cache type supported 'File', 'Memcached' now
	 * @return Fw_Cache
	 */
	public static function factory($type = 'File', $life_time = 0) {
		if ($life_time) {
			Fw_Cache::setLifeTime ( $life_time );
		}
		switch ($type) {
			case 'Memcached' :
				return Fw_Cache::factory ( 'Memcached', array (
						'memcache_host' => __MEMCACHE_HOST,
						'memcache_port' => __MEMCACHE_PORT 
				) );
			default :
				return Fw_Cache::factory ( 'File', array (
						'cache_dir' => __CACHE_DIR 
				) );
		}
	}
	
	/**
	 * Singleton method for getting cache instance
	 *
	 * @param string $type
	 *        	Cache type supported 'File', 'Memcached' now
	 * @return Fw_Cache
	 */
	public static function getInstance($type) {
		if (! self::$_cache) {
			self::$_cache = self::factory ( $type );
		}
		return self::$_cache;
	}
}