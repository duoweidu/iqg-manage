<?php

/**
 * @package MyFw_Dao_Admin
 */
class MyFw_Model_App {
	/**
	 *
	 * @static
	 *
	 */
	const TABLE_NAME = 'app';
	
	/**
	 * Initialize
	 */
	public function __construct() {
		$this->t1 = self::TABLE_NAME;
		$this->t2 = "acl_role"; // MyFw_AclRole::TABLE_NAME;
		$this->rsh = "app_role"; // MyFw_AppRole::TABLE_NAME;
	}
	public function read($id) {
		return st ( 'App' )->getRow ( $id );
	}
	public function update($data, $where) {
		return st ( 'App' )->update ( $data, $where );
	}
	public function delete($id) {
		return st ( 'App' )->delete ( "where id= $id " );
	}
	public function create($data) {
		return st ( 'App' )->insert ( $data );
	}
	
	/**
	 * Get all applications from track_app
	 * 
	 * @return array
	 */
	public function getAllApps($is_app = false) {
		$sql = st ( 'App' )->select ( array (
				"*" 
		) );
		
		if ($is_app)
			$sql->where ( "{$this->t1}.is_app = ?", 'YES' );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get acl data from relative tables
	 * For app's acl controls
	 * 
	 * @see MyFw_Acl_Backend
	 * @return array
	 */
	public function getAclApps() {
		$sql = st ( 'App' )->select ( array (
				"{$this->t1}.path as path",
				"{$this->t2}.id as role" 
		) )->joinInner ( $this->rsh, "{$this->t1}.id = {$this->rsh}.app_id", null )->joinInner ( $this->t2, "{$this->t2}.id = {$this->rsh}.role_id", null );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get all app data from track_acl_app
	 * Only for backend acl tools
	 * 
	 * @return array
	 */
	public function getAppList() {
	   
		$sql = st ( 'App' )->select ( array (
				"{$this->t1}.*",
				"group_concat({$this->t2}.name) as role" 
		) )->joinLeft ( $this->rsh, "{$this->t1}.id = {$this->rsh}.app_id", null )->joinLeft ( $this->t2, "{$this->t2}.id = {$this->rsh}.role_id", null )->group ( "{$this->t1}.id" );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get all app tree's data from track_acl_app
	 * Only for backend acl tools
	 * 
	 * @return array
	 */
	public function getAppTree() {
		return $this->getAppListByRole ( null );
	}
	
	/**
	 * Get application list by role
	 * For getting menus for whole application
	 * 
	 * @param int $role_id        	
	 * @return array
	 */
	public function getAppListByRole($role_id) {
	    $app =new App();
		$sql = $app->select ( array (
				"{$this->t1}.*",
				"group_concat({$this->t2}.name) as role" 
		) )->joinLeft ( $this->rsh, "{$this->t1}.id = {$this->rsh}.app_id", null )->joinLeft ( $this->t2, "{$this->t2}.id = {$this->rsh}.role_id", null );
		
		if ($role_id) {
			if (is_array ( $role_id )) {
				$sql->where ( "{$this->rsh}.role_id in (?)", $role_id );
			} else {
				$sql->where ( "{$this->rsh}.role_id = ?", $role_id );
			}
		}
		
		$sql->group ( "{$this->t1}.id" )->order ( array (
				"{$this->t1}.is_app desc",	
				"{$this->t1}.pid",
				"{$this->t1}.order",				
				"{$this->t1}.id" 
		) );
		
		//echo($sql->sql());
		$rawAppList = $sql->fetchAll ();
		
		// build app tree
		require_once 'Fw/Tree.php';
		$tree = new Tree ();
		foreach ( $rawAppList as $app ) {
			// s($app);
			$tree->setNode ( $app ['id'], $app ['pid'], $app );
		}
		
		// get top
		$topAppList = array ();
		$topAppListIds = $tree->getChild ( 0 );
		foreach ( $topAppListIds as $id ) {
			$topAppList [$id] = $tree->getValue ( $id );
		}
		
		// get all list
		$allAppList = $topAppList;
		foreach ( $topAppListIds as $tid ) {
			$groupList = array (); // group list
			$groupListIds = $tree->getChild ( $tid );
			foreach ( $groupListIds as $gid ) {
				$groupAppList = array (); // group app list
				$groupList [$gid] = $tree->getValue ( $gid );
				$appListIds = $tree->getChild ( $gid );
				foreach ( $appListIds as $aid ) {
					$groupAppList [$aid] = $tree->getValue ( $aid );
					
					$subgroupAppList = array (); // subgroup app list
					$subappListIds = $tree->getChild ( $aid );
					foreach ( $subappListIds as $subaid ) {
						$subgroupAppList [$subaid] = $tree->getValue ( $subaid );
					}
					$groupAppList [$aid] ['list'] = $subgroupAppList;
					// if($aid==6)break;
				}
				$groupList [$gid] ['list'] = $groupAppList;
			}
			$allAppList [$tid] ['list'] = $groupList;
		}
		
		//Fw_Util::dump($allAppList);
		
		return $allAppList;
	}
	
	/**
	 * Update all app role from track_app_role
	 * 
	 * @param int $id
	 *        	App ID
	 * @param array $roles
	 *        	Role ID's array
	 * @return bool
	 */
	public function updateRoles($id, $roles = array()) {
		if ($id) {
			st ( 'AppRole' )->delete ( "where app_id = $id" );
		} else {
			return false;
		}
		
		if ($roles) {
			$cols = array (
					'app_id',
					'role_id' 
			);
			$vals = array ();
			foreach ( ( array ) $roles as $role ) {
				$vals [] = array (
						$id,
						$role 
				);
			}
			if ($cols && $vals) {
				st ( 'AppRole' )->insertMultiRow ( $cols, $vals );
				return true;
			}
		} else {
			return true;
		}
		
		return false;
	}
}