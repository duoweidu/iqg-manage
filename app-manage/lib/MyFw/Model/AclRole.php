<?php

/**
 * @package MyFw_Dao_Admin
 */
class MyFw_Model_AclRole {
	/**
	 *
	 * @static
	 *
	 */
	const TABLE_NAME = 'acl_role';
	
	/**
	 * Initialize
	 */
	public function __construct() {
		$this->t1 = self::TABLE_NAME;
		$this->rsh1 = 'acl_user_role'; // MyFw_AclUserRole::TABLE_NAME;
		$this->rsh2 = 'app_role'; // MyFw_AppRole::TABLE_NAME;
		$this->rsh3 = 'acl_resource_role'; // MyFw_AclResourceRole::TABLE_NAME;
		$this->rsh4 = 'acl_role_priv'; // MyFw_AclRolePriv::TABLE_NAME;
	}
	public function read($id) {
		return st ( 'AclRole' )->getRow ( $id );
	}
	
	/**
	 * Get user's role data by user id
	 * Include the user's roles
	 * 
	 * @param int $id
	 *        	User ID
	 * @param array $privs
	 *        	Allowed privileges
	 * @return array
	 */
	public function getRoleByUserId($id, $privs = array()) {
		$sql = st ( 'AclRole' )->select ( array (
				"{$this->t1}.*" 
		) )->joinInner ( $this->rsh1, "{$this->t1}.id = {$this->rsh1}.role_id", null )->where ( "{$this->rsh1}.user_id=?", $id );
		
		$res = $sql->fetchAll ();
		
		foreach ( ( array ) $res as $k => $v ) {
			if (! in_array ( $v ['id'], $privs ))
				$res [$k] ['readonly'] = true;
		}
		
		return $res;
	}
	
	/**
	 * Get app's role data by app id
	 * Include the app's roles
	 * 
	 * @param int $id
	 *        	App ID
	 * @param array $privs
	 *        	Allowed privileges
	 * @return array
	 */
	public function getRoleByAppId($id, $privs = array()) {
		$sql = st ( 'AclRole' )->select ( array (
				"{$this->t1}.*" 
		) )->joinInner ( $this->rsh2, "{$this->t1}.id = {$this->rsh2}.role_id", null )->where ( "{$this->rsh2}.app_id=?", $id );
		
		$res = $sql->fetchAll ();
		
		foreach ( ( array ) $res as $k => $v ) {
			if (! in_array ( $v ['id'], $privs ))
				$res [$k] ['readonly'] = true;
		}
		
		return $res;
	}
	
	/**
	 * Get resource's role data by resource id
	 * Include the resource's roles
	 * 
	 * @param int $id
	 *        	Resource ID
	 * @param array $privs
	 *        	Allowed privileges
	 * @return array
	 */
	public function getRoleByResourceId($id, $privs = array()) {
		$sql = st ( 'AclRole' )->select ( array (
				"{$this->t1}.*" 
		) )->joinInner ( $this->rsh3, "{$this->t1}.id = {$this->rsh3}.role_id", null )->where ( "{$this->rsh3}.resource_id=?", $id );
		
		$res = $sql->fetchAll ();
		
		foreach ( ( array ) $res as $k => $v ) {
			if (! in_array ( $v ['id'], $privs ))
				$res [$k] ['readonly'] = true;
		}
		
		return $res;
	}
	
	/**
	 * Get all roles data from track_acl_role
	 * 
	 * @return array
	 */
	public function getAllRoles() {
		$sql = st ( 'AclRole' )->select ( array (
				"*" 
		) );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get all roles data from track_acl_role
	 * Only for backend acl tools
	 * 
	 * @return array
	 */
	public function getRoleList() {
		// Join self demo !!!
		$sql = st ( 'AclRole' )->select ( array (
				"{$this->t1}.*",
				"group_concat({$this->t1}_2.name) as role" 
		) )->joinLeft ( $this->rsh4, "{$this->t1}.id = {$this->rsh4}.role_id", null )->joinLeft ( array (
				$this->t1 . "_2" => $this->t1 
		), "{$this->t1}_2.id = {$this->rsh4}.priv_id", null )->group ( "{$this->t1}.id" );
		
		// s($sql->sql());
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get all role's priv data
	 * Only for privilege management
	 * 
	 * @param array $roles
	 *        	Role ID array (from session)
	 * @return array
	 */
	public function getAllPrivs($roles) {
		if (! $roles || ! sizeof ( $roles ))
			return array ();
		
		$sql = st ( 'AclRole' )->select ( array (
				"{$this->t1}.*" 
		) )->joinInner ( $this->rsh4, "{$this->t1}.id = {$this->rsh4}.priv_id", null )->where ( "{$this->rsh4}.role_id IN (?)", implode ( ',', $roles ) );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get user's priv data by role id
	 * That is getting user accessed role list
	 * 
	 * @param int $id
	 *        	Role ID
	 * @return array
	 */
	public function getPrivByRoleId($id) {
		$sql = st ( 'AclRole' )->select ( array (
				"{$this->t1}.*" 
		) )->joinInner ( $this->rsh4, "{$this->t1}.id = {$this->rsh4}.priv_id", null )->where ( "{$this->rsh4}.role_id=?", $id );
		
		return $sql->fetchAll ();
	}
	public function update($data, $where) {
		return st ( 'AclRole' )->update ( $data, $where );
	}
	public function create($data) {
		return st ( 'AclRole' )->insert ( $data );
	}
	/**
	 * Update all role priv from track_acl_role_priv
	 * 
	 * @param int $id
	 *        	Role ID
	 * @param array $privs
	 *        	Role's privs array (that is also role id)
	 * @return bool
	 */
	public function updatePrivs($id, $privs = array()) {
		if ($id) {
			st ( 'AclRolePriv' )->delete ( "where role_id = $id " );
		} else {
			return false;
		}
		
		if ($privs) {
			
			foreach ( ( array ) $privs as $priv ) {
				
				$data = array ();
				$data ['role_id'] = $id;
				$data ['priv_id'] = $priv;
				st ( 'AclRolePriv' )->insert ( $data );
			}
			return true;
		} else {
			return true;
		}
		
		return false;
	}
}