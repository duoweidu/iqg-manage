<?php

/**
 * @package MyFw_Dao_Admin
 */
class MyFw_Model_AclUser {
	const TABLE_NAME = 'acl_user';
	public function __construct() {
		$this->t1 = self::TABLE_NAME;
		$this->t2 = "acl_role"; // MyFw_AclRole::TABLE_NAME;
		$this->rsh = "acl_user_role"; // MyFw_AclUserRole::TABLE_NAME;
		$this->ext = "acl_user_ext"; // MyFw_AclUserExt::TABLE_NAME;
			                             // st('AclUser')->debug();
	}
	public function read($id) {
		return st ( 'AclUser' )->getRow ( $id );
	}
	public function update($data, $where) {
		return st ( 'AclUser' )->update ( $data, $where );
	}
	
	/**
	 * Login function
	 *
	 * @uses Used by user login process
	 * @param string $user        	
	 * @param string $pass        	
	 * @return bool or array
	 */
	public function authenticate($user, $pass) {
		$tab = st ( 'AclUser' );
		$user = $tab->scalar ( "*", "where name ='$user' and islock=0 " );
		
		if (! $user){
			return false;
		}
		
		$userExt = st ( 'AclUserExt' )->scalar ( "*", "where uid ='$user[id]'  " );
		unset ( $userExt ['id'] );
		$user = array_merge ( $user, $userExt );
		
		if (! $user ['id'] || ! $user ['pass']){
			return false;
		}
		
		if (strcmp ( $user ['pass'], Fw_Util::md5 ( $pass ) ))
			return $user ['id'];
		
		$sql = "select * from acl_role t2 join acl_user_role rsh on t2.id= rsh.role_id ";
		$sql .= " where rsh.user_id = '$user[id]'";
		
		$roles = $tab->fetchAll ( $sql );
		//s($roles,1);
		if (! sizeof ( $roles ))
			return false;
		
		foreach ( $roles as $role ) {
			
			$user ['role'] [] = $role ['id'];
			$user ['priv'] [] = $role ['alias'];
			$user ['roleName'] [] = $role ['name'];
		}
		
		$tab->update ( array('last_login'=>time()), "where id ='$user[id]'  " );
		
		return $user;
	}
	
	/**
	 * Get user passedit from acl_user
	 *
	 * @see MyFw_Acl
	 * @return bool
	 */
	public function getPassEditType($user) {
		$user = st ( 'AclUser' )->scalar ( "*", "where name='$user' " );
		// s($user,1);
		return $user ['passedit'];
	}
	/**
	 * Get all user data from track_acl_user
	 *
	 * @see MyFw_Acl
	 * @return array
	 */
	public function getAllUsers() {
		return st ( 'AclUser' )->getAll ( "*" );
	}
	
	/**
	 * Get cached user list
	 *
	 * @return array
	 */
	public function getCachedUserNames() {
		require_once 'MyFw/Cache.php';
		$cache = MyFw_Cache::factory ( 'File', 60 );
		$cache_id = __CLASS__ . '_getCachedUserNames';
		if (! ($cached_users = $cache->load ( $cache_id ))) {
			
			$res = st ( 'AclUser' )->getAll ( "id,name" );
			foreach ( $res as $row ) {
				$cached_users [$row ['id']] = $row ['name'];
			}
			$cache->save ( $cached_users, $cache_id );
		}
		return $cached_users;
	}
	
	/**
	 * Get all user data from track_acl_user
	 * Only for backend acl tools
	 *
	 * @return array
	 */
	public function getUserList($depid = null, $wheres = array()) {
		$tab = new AclUser ();
		// $ts=new TableSelect();
		$ts = $tab->select ( array (
				"{$this->t1}.*",
				"group_concat({$this->t2}.name) as role" 
		) );
		$sql = $ts->joinLeft ( $this->rsh, "{$this->t1}.id = {$this->rsh}.user_id", null )->joinLeft ( $this->t2, "{$this->t2}.id = {$this->rsh}.role_id", null )->where ( "{$this->t1}.isdel = 0" )->group ( "{$this->t1}.id" );
		
		$sql->joinLeft ( $this->ext, "{$this->ext}.uid = {$this->t1}.id", array (
				"*" 
		) );
		
		if ($depid) {			
			$sql->where ( "{$this->ext}.depid=$depid" );
		}
		
		if (is_array ( $wheres )) {			
			foreach ( $wheres as $wh ) {				
				$sql->where ( $wh );
			}
		}
		
		$sql->order(" islock asc" );
		
		//s($sql->sql());
		 
		return $sql->fetchAll ();
	}
	
	/**
	 * Update all user role from track_acl_user_role
	 *
	 * @param int $id
	 *        	User ID
	 * @param array $roles
	 *        	Role ID's array
	 * @return bool
	 */
	public function updateRoles($id, $roles = array()) {
		if ($id) {
			st ( 'AclUserRole' )->delete ( "where user_id = $id" );
		} else {
			return false;
		}
		
		if ($roles) {
			
			foreach ( ( array ) $roles as $role ) {
				
				$data = array ();
				$data ['user_id'] = $id;
				$data ['role_id'] = $role;
				st ( 'AclUserRole' )->insert ( $data );
			}
			
			return true;
		} else {
			return true;
		}
		
		return false;
	}
	
	/**
	 * get roleArray by roleId
	 *
	 * @param array $roles        	
	 * @return array
	 */
	public function getRoleNameByRoleId($roles) {
		if (empty ( $roles )) {
			$rolest = array ();
		} else {
			$tab = new AclRole ();
			$rolest = $tab->select ( array (
					"alias",
					"id",
					"name" 
			) )->where ( "id in (?)", $roles )->fetchAll ();
		}
		return $rolest;
	}
	
	/**
	 * Import multi-line data
	 *
	 * @param array $data        	
	 * @return bool
	 */
	public function importUser($data) {
		if (is_array ( $data ) && count ( $data )) {
			
			return st ( 'AclUser' )->insertMultiRow ( array (
					'id',
					'name',
					'pass',
					'isdel' 
			), $data );
		}
		return false;
	}
	
	/**
	 * Logically delete user
	 *
	 * @param int $uid
	 *        	User id to be deleted
	 * @return bool
	 */
	public function deleteUser($uid) {
		if (is_numeric ( $uid )) {
			st ( 'AclUser' )->update ( array (
					'isdel' => 1 
			), ' where id=' . $uid );
			
			st ( 'AclUserExt' )->update ( array (
			'isdel' => 1
			), ' where uid=' . $uid );
			
			return true;
		}
		return false;
	}
	
	/**
	 * Check the department was selected by User
	 *
	 * @param string $partment_name        	
	 * @return bool
	 */
	public function check_department_selected($partment_name) {
		return st ( 'AclUser' )->getCount ( "where department = '$partment_name'" );
	}
	public function create($data) {
		return st ( 'AclUser' )->insert ( $data );
	}
}