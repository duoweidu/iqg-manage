<?php

/**
 * @package MyFw_Model_Admin
 */
class MyFw_Model_LogLogin {
	/**
	 *
	 * @static
	 *
	 */
	const TABLE_NAME = 'log_login';
	
	/**
	 * Initialize
	 */
	public function __construct() {
		$this->t1 = self::TABLE_NAME;
	}
	/**
	 * Get login failed times
	 * For limiting user's login trying times
	 *
	 * @param int $time_limit
	 *        	Limit time (seconds), default is 5 mimutes
	 * @return int
	 */
	public function getFailedTimes($user_id, $time_limit = 300) {
		$sql = st ( 'LogLogin' )->select ( array (
				"count(id)" 
		) )->where ( "user_id = ?", $user_id )
//			->where("ip = ?", ip2long(Fw_Util::clientip()))
			->where( "log_time > ?", date ( 'Y-m-d H:i:s', time () - $time_limit ) )->where ( "is_failed = 'YES'" );
		
		$res = $sql->scalar ();
		
		return $res ? intval ( $res ) : 0;
	}
	
	/**
	 * Get login session id
	 * For limiting user's login must be only in one place
	 * 	 
	 * @param int $user_id        	
	 * @return
	 *
	 */
	public function getLastLogin($user_id) {
		//$mck=__CLASS__.__FUNCTION__.$user_id;
		//$ret=mc_get($mck);
		//if(!$ret){
			$sql = st ( 'LogLogin' )->select ( array (	"*"	) )->where ( "user_id = ?", $user_id )
			->where ( "is_failed = 'NO'" )->order ( "log_time desc" )->limit ( 1 );
			
			$ret= $sql->scalar ();
			//mc_set($mck, $ret, 30);
		//}
		return $ret;
		
		
	}
	public function create($data) {
		return st ( 'LogLogin' )->insert ( $data );
	}
	public function delete($val, $key) {
		return st ( 'LogLogin' )->delete ( "where $key= '$val' " );
	}
}