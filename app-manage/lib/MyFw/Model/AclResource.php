<?php

/**
 * @package MyFw_Dao_Admin
 */
class MyFw_Model_AclResource {
	/**
	 *
	 * @static
	 *
	 */
	const TABLE_NAME = 'acl_resource';
	
	/**
	 * Initialize
	 */
	public function __construct() {
		$this->t1 = self::TABLE_NAME;
		$this->t2 = 'acl_role'; // MyFw_AclRole::TABLE_NAME;
		$this->rsh = 'acl_resource_role'; // MyFw_AclResourceRole::TABLE_NAME;
	}
	public function read($id) {
		return st ( 'AclResource' )->getRow ( $id );
	}
	public function update($data, $where) {
		return st ( 'AclResource' )->update ( $data, $where );
	}
	public function delete($id) {
		return st ( 'AclResource' )->delete ( "where id= $id " );
	}
	public function create($data) {
		return st ( 'AclResource' )->insert ( $data );
	}
	/**
	 * Get all resource from track_acl_resource
	 * 
	 * @see MyFw_Acl
	 * @return array
	 */
	public function getAllResources() {
		return st ( 'AclResource' )->select ( array (
				"*" 
		) )->fetchAll ();
	}
	
	/**
	 * Get acl data from relative tables
	 * For app's acl controls
	 * 
	 * @see MyFw_Acl_Backend
	 * @return array
	 */
	public function getAclResources() {
		$sql = st ( 'AclResource' )->select ( array (
				"{$this->t1}.name as resource",
				"{$this->t2}.id as role" 
		) )->joinInner ( $this->rsh, "{$this->t1}.id = {$this->rsh}.resource_id", null )->joinInner ( $this->t2, "{$this->t2}.id = {$this->rsh}.role_id", null );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Get all resource data from track_acl_resource
	 * Only for backend acl tools
	 * 
	 * @return array
	 */
	public function getResourceList() {
		$sql = st ( 'AclResource' )->select ( array (
				"{$this->t1}.*",
				"group_concat({$this->t2}.name) as role" 
		) )->joinInner ( $this->rsh, "{$this->t1}.id = {$this->rsh}.resource_id", null )->joinInner ( $this->t2, "{$this->t2}.id = {$this->rsh}.role_id", null )->group ( "{$this->t1}.id" );
		
		return $sql->fetchAll ();
	}
	
	/**
	 * Update all resource role from track_acl_resource_role
	 * 
	 * @param int $id
	 *        	Resource ID
	 * @param array $roles
	 *        	Role ID's array
	 * @return bool
	 */
	public function updateRoles($id, $roles = array()) {
		if ($id) {
			st ( 'AclResourceRole' )->delete ( "where resource_id = $id " );
		} else {
			return false;
		}
		
		if ($roles) {
			$cols = array (
					'resource_id',
					'role_id' 
			);
			$vals = array ();
			foreach ( ( array ) $roles as $role ) {
				$vals [] = array (
						$id,
						$role 
				);
			}
			if ($cols && $vals) {
				st ( 'AclResourceRole' )->insertMultiRow ( $cols, $vals );
				return true;
			}
		} else {
			return true;
		}
		
		return false;
	}
}