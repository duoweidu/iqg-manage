<?php

/**
 * @package MyFw_Dao_Mtim
 */
class MyFw_Model_Department {
	/**
	 *
	 * @static table name
	 */
	const TABLE_NAME = 'department';
	
	/**
	 * @Initialize
	 */
	public function __construct() {
		$this->t1 = self::TABLE_NAME;
	}
	public function read($id) {
		return st ( 'Department' )->getRow ( $id );
	}
	public function update($data, $where) {
		return st ( 'Department' )->update ( $data, $where );
	}
	public function delete($id) {
		return st ( 'Department' )->delete ( "where id= $id " );
	}
	
	/**
	 * add new department
	 * 
	 * @param
	 *        	$dp_name
	 * @param
	 *        	$dp_desc
	 * @param $aid the        	
	 * @return $id
	 *
	 */
	public function addNewDP($dp_name, $dp_desc, $user) {
		$row = array (
				'department_name' => $dp_name,
				'department_desc' => $dp_desc,
				'department_createtime' => date ( "Y-m-d H:i:s", time () ),
				'department_updatetime' => date ( "Y-m-d H:i:s", time () ),
				'department_create_user' => $user,
				'department_update_user' => $user 
		);
		$last_insert_id = st ( 'Department' )->insert ( $row );
		return $last_insert_id;
	}
	
	/**
	 * get the department information
	 * 
	 * @param
	 *        	$dp_id
	 * @return array
	 */
	public function getDPById($dp_id) {
		$sql = st ( 'Department' )->select ( array (
				'*' 
		) )->where ( 'department_id= ? ', $dp_id );
		$result = $sql->scalar ();
		return $result;
	}
	
	/**
	 * get all the department list
	 * 
	 * @return array
	 */
	public function getAllDP() {
		$sql = st ( 'Department' )->select ( array (
				'*' 
		) )->where ( 'department_condition= 0' );
		
		return $sql->fetchAll ();
	}
	
	 
	/**
	 * get  department id
	 * @param unknown $dp_name
	 * @param unknown $user
	 * @return Ambigous <$id, multitype:>
	 */
	public function checkDepartment($dp_name, $user) {
	    $ret= self::departmentExist ( $dp_name );
		if (!$ret) {			
			$ret=self::addNewDP ( $dp_name, "", $user );			
		}
		return $ret;
	}
	/**
	 * refresh the department information
	 * 
	 * @param
	 *        	$dp_id
	 * @param
	 *        	$dp_name
	 * @param
	 *        	$dp_desc
	 * @param $aid operator        	
	 */
	public function updateDPById($dp_id, $dp_name, $dp_desc, $aid) {
		$set = array (
				'department_name' => $dp_name,
				'department_desc' => $dp_desc,
				'department_update_user' => $aid 
		);
		
		$rows_affected = st ( 'Department' )->update ( $set, "where department_id = $dp_id" );
	}
	
	/**
	 * delete the department information
	 * 
	 * @param
	 *        	$dp_id
	 */
	public function delDPById($dp_id, $aid) {
		$set = array (
				'department_condition' => '1',
				'department_update_user' => $aid 
		);
		
		$rows_affected = st ( 'Department' )->update ( $set, "where department_id = $dp_id" );
		// $rows_affected = $this->db->delete($this->t1, $where);
	}
	
	/**
	 * check department name unique
	 * 
	 * @param
	 *        	$dp_name
	 * @return array or false
	 */
	public function departmentExist($dp_name) {
		$sql = st ( 'Department' )->select ('department_id')->where ( 'department_name=?', $dp_name )->where ( 'department_condition= 0' );
		
		return $sql->scalar();
	}
	
	/**
	 * check department name unique
	 * 
	 * @param
	 *        	$dp_name
	 * @param
	 *        	$dp_id
	 * @return array or false
	 */
	public function departmentExistEdit($dp_name, $dp_id) {
		$sql = st ( 'Department' )->select ( array (
				'*' 
		) )->where ( 'department_name=?', $dp_name )->where ( 'department_condition= 0' )->where ( 'department_id !=?', $dp_id );
		return $sql->scalar ();
	}
}
