<?php
/**
 * TC Acl
 *
 * @category   TC
 * @package    MyFw_Acl
 * @author     
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'MyFw/Acl.php';
require_once 'MyFw/Dao.php';

/**
 *
 * @package MyFw_Acl
 */
class MyFw_Acl_Backend extends MyFw_Acl {
	/**
	 *
	 * @var MyFw_Acl_Backend
	 */
	public static $_acl = null;
	
	/**
	 * Get static or cached acl object
	 *
	 * @return MyFw_Acl_Backend
	 */
	public static function getInstance() {
		if (! self::$_acl) {
			$class_name = __CLASS__;
			require_once 'MyFw/Cache.php';
			// we store cache in 60 seconds
			$cache = MyFw_Cache::factory ( 'File', 60 );
			if (! (self::$_acl = $cache->load ( $class_name ))) {
				self::$_acl = new $class_name (); // init the acl object
				$cache->save ( self::$_acl, $class_name );
			}
		}
		return self::$_acl;
	}
	
	/**
	 * Construct
	 * Init the acl object
	 * 
	 * @return unknown
	 */
	public function __construct() {
		// add roles for acl
		$roleDao = new MyFw_Model_AclRole (); // MyFw_Dao::load('MyFw_AclRole');
		$roles = $roleDao->getAllRoles ();
		foreach ( ( array ) $roles as $role ) {
			if (! isset ( $role ['id'] ))
				continue;
			$this->addRole ( new Zend_Acl_Role ( $role ['id'] ) );
		}
		
		// add app for acl
		$appDao = new MyFw_Model_App (); // MyFw_Dao::load('MyFw_App');
		$apps = $appDao->getAllApps ();
		foreach ( ( array ) $apps as $app ) {
			if (! strlen ( $app ['path'] ))
				continue;
			
			$aclRes = new Zend_Acl_Resource ( $app ['path'] );
			if (! $this->has ( $aclRes )) {
				$this->add ( $aclRes );
			}
		}
		
		// add app and role's relation for acl
		$aclApps = $appDao->getAclApps ();
		foreach ( ( array ) $aclApps as $acl ) {
			if (! strlen ( $acl ['role'] ))
				continue;
			if (! strlen ( $acl ['path'] ))
				continue;
			$this->allow ( $acl ['role'], $acl ['path'] );
		}
		
		// add resource for acl
		$resourceDao = new MyFw_Model_AclResource (); // MyFw_Dao::load('MyFw_AclResource');
		$resources = $resourceDao->getAllResources ();
		foreach ( ( array ) $resources as $resource ) {
			if (! strlen ( $resource ['name'] ))
				continue;
			$this->add ( new Zend_Acl_Resource ( $resource ['name'] ) );
		}
		
		// add resource and role's relation for acl
		$aclResources = $resourceDao->getAclResources ();
		foreach ( ( array ) $aclResources as $acl ) {
			if (! strlen ( $acl ['role'] ))
				continue;
			if (! strlen ( $acl ['resource'] ))
				continue;
			$this->allow ( $acl ['role'], $acl ['resource'] );
		}
	}
}