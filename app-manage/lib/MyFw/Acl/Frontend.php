<?php
/**
 * TC Acl
 *
 * @category   TC
 * @package    MyFw_Acl
 * @author     
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'MyFw/Acl.php';
require_once 'MyFw/Dao.php';

/**
 *
 * @package MyFw_Acl
 */
class MyFw_Acl_Frontend extends MyFw_Acl {
	/**
	 *
	 * @var MyFw_Acl_Frontend
	 */
	public static $_acl = null;
	
	/**
	 * Get static or cached acl object
	 *
	 * @return MyFw_Acl_Frontend
	 */
	public static function getInstance() {
		if (! self::$_acl) {
			$class_name = __CLASS__;
			require_once 'MyFw/Cache.php';
			// we store cache in 60 seconds
			$cache = MyFw_Cache::factory ( 'File', 60 );
			if (! (self::$_acl = $cache->load ( $class_name ))) {
				self::$_acl = new $class_name (); // init the acl object
				$cache->save ( self::$_acl, $class_name );
			}
		}
		return self::$_acl;
	}
	
	/**
	 * Construct
	 * Init the acl object
	 * 
	 * @return unknown
	 */
	public function __construct() {
		// TODO : initialize the frontend acl object
	}
}