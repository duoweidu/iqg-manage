<?php
/**
 * Created on 2010-4-26
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class MyFw_Util_Excel {
	public $header = "<?xml version=\"1.0\"?> <?mso-application progid=\"Excel.Sheet\"?>
	<Workbook xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
	  xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"
	  xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"><Worksheet ss:Name=\"Sheet1\">
	  <ss:Table>";
	public $footer = "</ss:Table>\n</Worksheet>\n</Workbook>";
	public function addCell($param, $type) {
		$cell = "";
		switch ($type) {
			case '0' :
				$cell .= "<ss:Cell><Data ss:Type=\"String\">" . $param . "</Data></ss:Cell>\n";
				break;
			case '1' :
				$cell .= "<ss:Cell><Data ss:Type=\"Number\">" . $param . "</Data></ss:Cell>\n";
				break;
		}
		return $cell;
	}
	public function addRow($strCell) {
		$row = "<ss:Row>\n" . $strCell . "</ss:Row>\n";
		return $row;
	}
}
