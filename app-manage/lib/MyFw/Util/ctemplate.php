<?
require_once 'MyFw/App/Backend/Page.php';
function getTemplate($tplType, $oid) {
	$pthis = new MyFw_App_Backend_Page ();
	$filePath = CDN_FILE . 'emailtpl/';
	$gtab = new Goform ();
	$query = $gtab->execute ( "select goform_uid,goform_id,gt_name,gt_count,gt_mobile,goform_time,gt_pid,gt_start,gt_end,gt_productname from goform g left join goform_trv gt on g.goform_id = gt.gt_oid where g.goform_id =" . $oid );
	$orderInfo = $gtab->fetch ( $query );
	$sexInfo = $pthis->getRowRecord ( 'Fuser', 'fuser_sex', 'where fuser_id = ' . ( int ) $orderInfo ['goform_uid'] );
	switch ($sexInfo) {
		case 0 :
			$sexInfo = '保密';
			break;
		case 1 :
			$sexInfo = '女';
			break;
		case 2 :
			$sexInfo = '男';
			break;
	}
	if ($tplType == 'customer') {
		$headAdd = "<thead>
                    	<tr>
                            <td style=\"padding:5px 0 10px;font:14px/25px Simsun;color:#cccccc\">亲爱的 <span>" . $orderInfo ['gt_name'] . "</span><br />恭喜您订单购买已成功，感谢您在<span style=\"padding:0 2px;font-family:Arial;\">本站</span>购物！</td>
                    	</tr>
                    </thead>";
		$footAdd = "<tfoot>
    	<tr>
        	<td style=\"width:680px;padding:10px 10px 15px\">
            	<div style=\"padding:5px;background:#e3e3e2;\">
<ul style=\"margin:0;list-style:none;padding:10px 0 20px 15px;border-top:1px solid #fff;\">
<li style=\"font-family:Arial;\">E-mail: <a href=\"mailto:customer@tc.com\" style=\"color:#333;text-decoration:none\">customer@tc.com</a></li>
                        <li>客服电话：<em style=\"font-family:Arial;font-style:normal\">400-666-1212</em> 转 <em style=\"font-family:Arial;font-style:normal\">6</em></li>
                        <li style=\"font-family:Arial;\"><a href=\"http://www.tc.com/\" title=\"俏物悄语\" style=\"color:#ed3f92;text-decoration:none\">www.tc.com</a></li>
                        <li style=\"color:#ed3f92\">俏物悄语网</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tfoot>";
	} else {
		$headAdd = '';
		$footAdd = "    <tfoot>
    	<tr>
        	<td style=\"width:680px;padding:10px 10px 15px\">
            	<div style=\"height:70px;padding:100px 25px 0;background:#e3e3e2;\">
                	<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"text-align:right\">
                    	<tr>
                        	<th style=\"width:105px;font-size:12px;font-weight:300;\">供应商确认处：<br />（签字盖章）&nbsp;</th>
                            <td style=\"width:345px;text-align:left\">
                            	<div style=\"width:142px;border-bottom:1px solid #333\"></div>
                            </td>
                            <td style=\"width:198px;font-size:12px;\">回执传真号：<span style=\"font-family:Arial;\">020 - 123456789</span></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </tfoot>";
	}
	$prArr = $pthis->getRowRecord ( 'Product', 'product_psp_id,product_dprice', 'where product_id = ' . ( int ) $orderInfo ['gt_pid'] );
	$pt_unit = $pthis->getRowRecord ( 'Producttrv', 'pt_unit', 'where pt_pid = ' . ( int ) $orderInfo ['gt_pid'] );
	$product_dprice = $prArr ['product_dprice'];
	$pspecial_name = $pthis->getRowRecord ( 'Pspecial', 'pspecial_name', 'where pspecial_id = ' . ( int ) $prArr ['product_psp_id'] );
	$ccontent = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:700px;margin:0 auto;padding:0;font:12px/1.6em Simsun;font-size:12px;border-collapse:collapse;border-spacing:0;background:#333;color:#333\">
	<thead style=\"background:#fff\">
    	<tr>
        	<td style=\"width:700px;height:34px;line-height:34px;color:#999;text-align:right\">
            	<a href=\"http://www.tc.com/\" title=\"俏物悄语\" style=\"color:#ed3f92;padding-right:15px;\">立即访问</a>
                如果您看不到图片，请点击：<a target='_blank' href=\"http://www.tc.com/travel_order_content.php?id=" . $oid . "\" title=\"俏物悄语\" style=\"color:#ed3f92\">这里</a>
            </td>
        </tr>
        <tr>
        	<td style=\"width:700px;padding-bottom:10px;\">
                <a href=\"#\" title=\"俏物悄语\" style=\"padding-right:168px\"><img src=\"" . $filePath . "/logo.png\" alt=\"俏物悄语\" width=\"203\" height=\"54\" border=\"0\" style=\"vertical-align:bottom;\" /></a>
                <img src=\"" . $filePath . "header.png\" alt=\"俏物悄语\" width=\"322\" height=\"29\" border=\"0\" style=\"vertical-align:bottom;margin-top:26px\" />
            </td>
        </tr>
    </thead>
    " . $footAdd . "
    <tbody>
        <tr>
            <td style=\"width:680px;padding:15px 10px 0\">
            	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
                " . $headAdd . "
                  	<tbody style=\"background:#e3e3e2\">
                      	<tr>
                        	<td style=\"padding:0 0 35px\">
                                <h3 style=\"height:43px;margin:0;padding-left:20px;font-size:14px;line-height:40px;background:#d7d7d7;border-bottom:1px solid #ccc\">订单详情</h3>
                                <div style=\"padding:10px 5px;border-top:1px solid #fff\">
                                	<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"text-align:left\">
                                    	<tr>
                                        	<td valign=\"top\">
                                            	<ul style=\"margin:0;padding:0 0 0 25px;list-style:none;font-size:12px;font-weight:700;line-height:20px\">
                                                	<li style=\"padding-bottom:10px\">订单编号：<span style=\"padding-left:7px;font-family:Arial;font-weight:300;\">" . $oid . "</span>
                                                    <li style=\"padding-bottom:10px\">产品名称：<a href=\"http://www.tc.com/travel_detail.php?pspid=" . $prArr ['product_psp_id'] . "\" title=\"" . $pspecial_name . "\" style=\"padding-left:7px;color:#333;font-weight:300;text-decoration:underline\">" . $pspecial_name . "</a><span style=\"padding-left:10px;font-family:Arial;font-weight:300;color:#795f06\">￥{$product_dprice}/" . $pt_unit . "</span></li>
                                                    <li style=\"padding-bottom:10px\">购买数量：<span style=\"padding-left:7px;font-family:Arial;font-weight:300;color:#795f06\">" . $orderInfo ['gt_count'] . "</span></li>
                                                    <li style=\"padding-bottom:10px\">出行日期：<span style=\"padding-left:7px;font-family:Arial;font-weight:300;color:#795f06\">" . date ( 'Y/m/d', $orderInfo ['gt_start'] ) . ' - ' . date ( 'Y/m/d', $orderInfo ['gt_end'] ) . "</span></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style=\"padding:0 5px;font-size:12px\">
                                	<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"color:#666;text-align:center;background:#fff;border:1px solid #dadada;border-collapse:collapse;border-spacing:0;\">
                                    	<thead style=\"color:#333\">
                                        	<tr>
                                           	  <th style=\"width:130px;height:38px;font-size:12px;line-height:14px;border:1px solid #dadada;background:#e4e4e4 url(" . $filePath . "th.png) 0 0 repeat-x\">旅客姓名</th>
                                                <th style=\"width:76px;font-size:12px;border:1px solid #dadada;background:#e4e4e4 url(" . $filePath . "th.png) 0 0 repeat-x\">性别</th>
                                                <th style=\"width:123px;font-size:12px;border:1px solid #dadada;background:#e4e4e4 url(" . $filePath . "th.png) 0 0 repeat-x\">证件号</th>
                                              <th style=\"width:117px;font-size:12px;border:1px solid #dadada;background:#e4e4e4 url(" . $filePath . "th.png) 0 0 repeat-x\">联系电话</th>
                                              <th style=\"width:106px;font-size:12px;border:1px solid #dadada;background:#e4e4e4 url(" . $filePath . "th.png) 0 0 repeat-x\">套餐详情</th>
                                                <th style=\"border:1px solid #dadada;font-size:12px;background:#e4e4e4 url(" . $filePath . "th.png) 0 0 repeat-x\">确定时间</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<tr>
                                           	  <td style=\"padding:5px 0;font-size:12px;border:1px solid #dadada\">" . $orderInfo ['gt_name'] . "</td>
                                                <td style=\"border:1px solid #dadada;font-size:12px;\">" . $sexInfo . "</td>
                                                <td style=\"border:1px solid #dadada;font-size:12px;\">&nbsp;</td>
                                              <td style=\"border:1px solid #dadada;font-size:12px;\" >" . $orderInfo ['gt_mobile'] . "</td>
                                              <td style=\"border:1px solid #dadada;font-size:12px;\" >" . $orderInfo ['gt_productname'] . "</td>
                                              <td style=\"border:1px solid #dadada;font-size:12px;\" >" . date ( 'Y/m/d', $orderInfo ['goform_time'] ) . "</td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                      </div>
                                <div style=\"margin:10px 5px 25px;padding-bottom:15px;background:#fff;border:1px solid #ccc\">
                                	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
                                        <tr>
                                        	<td valign=\"top\" style=\"width:85px;padding:15px 15px 15px 0;font-size:14px;font-weight:700;text-align:right;border-bottom:1px solid #dadada\">产品包含:</td>
                                            <td style=\"padding:15px 15px 15px 0;font-family:Arial;border-bottom:1px solid #dadada\">
                                            	<ol style=\"margin:0;padding:0;text-align:left;list-style:none;color:#666;line-height:20px\">
                                                	<li>&nbsp;</li>
                                                </ol>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td valign=\"top\" style=\"width:85px;padding:15px 15px 15px 0;font-size:14px;font-weight:700;text-align:right;border-bottom:1px solid #dadada\">产品详情:</td>
                                            <td style=\"padding:15px 15px 15px 0;font-family:Arial;border-bottom:1px solid #dadada\">
                                            	<p style=\"padding:0;margin:0;color:#666;\">&nbsp;</p>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<th valign=\"top\" style=\"width:85px;padding:15px 15px 0 0;font-size:14px;font-weight:700;text-align:right\">备注说明:</th>
                                            <td style=\"padding:15px 40px 0 0;color:#795f06\">&nbsp;</td>
                                        </tr>
                                    </table>
                                </div></td>
                      	</tr>
                  	</tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>";
	unset ( $pthis );
	unset ( $gtab );
	return $ccontent;
}

?>