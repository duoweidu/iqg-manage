<?php
require_once 'MyFw/textedit.php';
/**
 *
 *
 * created on 2011-11-3
 * 
 * @author 
 *        
 */
class MyFw_Util_Fckeditor {
	public $height = 570;
	public $width = '';
	public $toolBars = 'Default';
	protected $_basePath, $_content;
	protected $_config;
	public function __construct() {
		$this->_basePath = $_root . "/js/textedit/";
	}
	
	/**
	 *
	 * @return object
	 */
	public function instanceEditor() {
		return new CKEditor ();
	}
	
	/**
	 * ckeditor 编辑器
	 * 
	 * @param string $content        	
	 * @param string $name        	
	 */
	public function CreateEditor($content = "", $name, $isUnstpack = true) {
		$CKEditor = new CKEditor ();
		$CKEditor->basePath = $this->_basePath;
		$CKEditor->returnOutput = true;
		
		$config ['height'] = $this->height;
		$config ['width'] = $this->width;
		$config ['enterMode'] = "CKEDITOR.ENTER_BR";
		
		if (is_array ( $this->_config )) {
			foreach ( $this->_config as $k => $v ) {
				$config [$k] = $v;
			}
		}
		
		$events ['instanceReady'] = 'function (ev) {	     
		}';
		
		if ($isUnstpack) {
			$content = $this->_unstpackhtml ( $content );
		}
		
		$this->_content = $content;
		
		$FCKEditor = $CKEditor->editor ( $name, $content, $config, $events );
		return $FCKEditor;
	}
	
	/**
	 * 设置Fckeditor 配置参数
	 * 
	 * @param array $config
	 *        	键值数组
	 * @return void
	 */
	public function setConfig($config) {
		if (is_array ( $config )) {
			$this->_config = $config;
		} else {
			throw new Exception ( '请传Array类型参数!', E_ERROR );
		}
	}
	
	/**
	 *
	 * @return void
	 */
	public function printParams() {
		echo '<pre>';
		print_r ( $this->paramsValue () );
		echo '</pre>';
	}
	
	/**
	 *
	 * @return void
	 */
	public function setBasePath($basePath = null) {
		if (! is_null ( $basePath )) {
			$this->_basePath = $basePath;
		}
	}
	
	/**
	 *
	 * @return string
	 */
	public function getBasePath() {
		return $this->_basePath;
	}
	
	/**
	 *
	 * @return array
	 */
	protected function paramsValue() {
		$arr [0] = 'width: ' . $this->width;
		$arr [1] = 'height: ' . $this->height;
		$arr [2] = 'basePath: ' . $this->_basePath;
		$arr [3] = 'toolBas: ' . $this->toolBars;
		return $arr;
	}
	
	/**
	 *
	 * @param string $str        	
	 * @return string
	 */
	protected function _unstpackhtml($str) {
		$str = ereg_replace ( "<br />", "", $str );
		$str = htmlspecialchars_decode ( $str, ENT_QUOTES );
		return $str;
	}
}