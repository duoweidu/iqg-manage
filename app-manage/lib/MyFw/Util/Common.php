<?php

/**
 * Created on 2010-8-3
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class MyFw_Util_Common {
	public function getStrOption($arr_value, $arg_id) {
		if (! is_array ( $arr_value )) {
			return false;
		}
		$strOption = "";
		for($i = 0; $i < count ( $arr_value ); $i ++) {
			$strOption .= "<option value='" . $arr_value [$i] ['department_id'] . "'";
			if (is_array ( $arg_id )) {
				foreach ( $arg_id as $val ) {
					if ($arr_value [$i] ['department_id'] == $val ['td_did']) {
						$strOption .= " selected ";
					}
				}
			} else {
				if ($arr_value [$i] ['department_id'] == $arg_id) {
					$strOption .= " selected ";
				}
			}
			$strOption .= ">" . $arr_value [$i] ['department_name'] . "</option>";
		}
		return $strOption;
	}
	public function getTopicOption($arr_value) {
		$strOption = '<option value="0">--select--</option>';
		for($i = 0; $i < count ( $arr_value ); $i ++) {
			$strOption .= "<option value='" . $arr_value [$i] ['topic_id'] . "'";
			$strOption .= ">" . $arr_value [$i] ['topic_name'] . "</option>";
		}
		return $strOption;
	}
	public function makeTextCSV($arr_value, $file_name) {
		header ( 'Content-Disposition: attachment; filename="' . $file_name . '.csv"' );
		echo iconv ( "UTF-8", "GB2312", '答案' );
		echo "\r\n";
		foreach ( $arr_value as $value ) {
			echo iconv ( "UTF-8", "GB2312", $value ['writing_answer'] ) . "\r\n";
		}
	}
	public function getProvince() {
		$arrValue [] = "北京市";
		$arrValue [] = "天津市";
		$arrValue [] = "河北省";
		$arrValue [] = "山西省";
		$arrValue [] = "内蒙古区";
		$arrValue [] = "辽宁省";
		$arrValue [] = "吉林省";
		$arrValue [] = "黑龙江省";
		$arrValue [] = "上海市";
		$arrValue [] = "江苏省";
		$arrValue [] = "浙江省";
		$arrValue [] = "安徽省";
		$arrValue [] = "福建省";
		$arrValue [] = "江西省";
		$arrValue [] = "山东省";
		$arrValue [] = "河南省";
		$arrValue [] = "湖北省";
		$arrValue [] = "湖南省";
		$arrValue [] = "广东省";
		$arrValue [] = "广西区";
		$arrValue [] = "海南省";
		$arrValue [] = "重庆市";
		$arrValue [] = "四川省";
		$arrValue [] = "贵州省";
		$arrValue [] = "云南省";
		$arrValue [] = "西藏区";
		$arrValue [] = "陕西省";
		$arrValue [] = "甘肃省";
		$arrValue [] = "青海省";
		$arrValue [] = "宁夏区";
		$arrValue [] = "新疆区";
		// $arrValue[] = "台湾省";
		// $arrValue[] = "香港特区";
		// $arrValue[] = "澳门特区";
		/*
		 * $option = ""; while (list($key,$var) = each($arrValue)) { $option .= "<option value='$var'"; if($value == $var) { $option .= " selected "; } $option .= " >$var</option>"; }
		 */
		return $arrValue;
	}
	public function alert($type, $str = '', $rurl = '', $charset = 'gb2312') {
		$alertstr = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="Content-Type" content="text/html; charset=' . $charset . '" /><title></title><script language="javascript">';
		switch ($type) {
			case 'back' :
				$alertstr .= 'alert("' . $str . '");history.back();';
				break;
			case 'redirect' :
				$alertstr .= 'alert("' . $str . '");document.location.href="' . $rurl . '";';
				break;
			case 'noback' :
				$alertstr .= 'alert("' . $str . '");';
				break;
			case 'noalert' :
				$alertstr .= 'document.location.href="' . $rurl . '";';
				break;
		}
		$alertstr .= '</script></head><body></body></html>';
		echo $alertstr;
	}
	function unstpack($str) {
		$str = ereg_replace ( "<br />", "", $str );
		if (strlen ( $str ) <= 10) {
			if (is_numeric ( $str ))
				$str = round ( $str, 2 );
		}
		return $str;
	}
	
	// 得到上传地址
	function getDirPath($intID) {
		$strTypeID = substr ( "00000000" . $intID, - 7, 2 ) . "/" . substr ( "00000000" . $intID, - 5, 2 ) . "/" . substr ( "00000000" . $intID, - 3, 2 ) . "/" . substr ( "00000000" . $intID, - 2 ) . "/";
		
		return trim ( $strTypeID );
	}
	
	// GET File Name
	function getFileName($version, $id, $type) {
		$filename = $version . '_ad_' . $id . '_' . $type;
		return $filename;
	}
	function createDir($path) {
		if (! file_exists ( $path )) {
			$this->createDir ( dirname ( $path ) );
			mkdir ( $path, 0777 );
		}
	}
	function uploadImage($strName, $strNewFileName, $strFilePath, $pageImgW, $pageImgH) {
		$strUploadFilePath = $strFilePath;
		if (! is_dir ( $strUploadFilePath )) {
			@$this->mkdirs ( $strUploadFilePath );
		}
		$strUploadFile = $_FILES [$strName] ['tmp_name'];
		$strFileType = $_FILES [$strName] ['type'];
		$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
		if ($strFileType == "image/pjpeg" || $strFileType == "image/jpeg" || $strFileType == "image/gif" || $strFileType == "image/png" || $strFileType == "image/x-png") {
			if ($strUploadFile != "") {
				if ($strNewFileName == "") {
					$strUploadFileName = $_FILES [$strName] ['name'];
				} else {
					$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
				}
				$strFilePathName = $strUploadFilePath . $strUploadFileName;
				
				if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
					echo ("上传文件失败");
					exit ();
				}
				
				$arrImginfo = getimagesize ( $strFilePathName );
				if (is_array ( $pageImgW )) {
					while ( list ( $key, $var ) = each ( $pageImgW ) ) {
						$strFileName = substr ( $strFilePathName, 0, strrpos ( $strFilePathName, "." ) ) . "_$key" . substr ( $strFilePathName, strrpos ( $strFilePathName, "." ) );
						$this->imgMark ( $strFilePathName, $strFileName, $var, $pageImgH [$key], true, true );
					}
				}
				@unlink ( $strUploadFile );
				return $strUploadFileName;
			}
		}
	}
	function uploadImage2($strName, $strNewFileName = "", $version = "") {
		global $strFilePath, $pageImgW, $pageImgH;
		$strUploadFilePath = $strFilePath . "images/";
		if (! is_dir ( $strUploadFilePath )) {
			@$this->mkdirs ( $strUploadFilePath );
		}
		$strUploadFile = $_FILES [$strName] ['tmp_name'];
		$strFileType = $_FILES [$strName] ['type'];
		$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
		if ($strFileType == "image/pjpeg" || $strFileType == "image/jpeg" || $strFileType == "image/gif" || $strFileType == "image/png" || $strFileType == "image/x-png") {
			if ($strUploadFile != "") {
				if ($strNewFileName == "") {
					$strUploadFileName = $_FILES [$strName] ['name'];
				} else {
					$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
				}
				
				if ($version > 0) {
					$strFilePathName = $strUploadFilePath . $version . "_" . $strUploadFileName;
				} else {
					$strFilePathName = $strUploadFilePath . $strUploadFileName;
				}
				// $strFilePathName = $strUploadFilePath.$strUploadFileName;
				
				if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
					echo ("上传文件失败");
					exit ();
				}
				
				$arrImginfo = getimagesize ( $strFilePathName );
				if (is_array ( $pageImgW )) {
					while ( list ( $key, $var ) = each ( $pageImgW ) ) {
						$strFileName = substr ( $strFilePathName, 0, strrpos ( $strFilePathName, "." ) ) . "_$key" . substr ( $strFilePathName, strrpos ( $strFilePathName, "." ) );
						$this->imgMark ( $strFilePathName, $strFileName, $var, $pageImgH [$key], true, true );
					}
				}
				@unlink ( $strUploadFile );
				return $strUploadFileName;
			}
		}
	}
	
	// 删除图片
	function delpicAction() {
		$intID = $_GET ["sspecial_id"];
		$objImg = $intID . "." . $_GET ["ext"];
		$strNewFile = $this->getDirPath ( $intID );
		if ($_GET ["ver"] > 0) {
			$fPath = USER_SSPECIAL_SABSOLUTEPATH . $strNewFile . "images/" . $_GET ["ver"] . "_" . $intID . "." . $_GET ["ext"];
		} else {
			$fPath = USER_SSPECIAL_SABSOLUTEPATH . $strNewFile . "images/" . $intID . "." . $_GET ["ext"];
		}
		if (file_exists ( $fPath )) {
			@unlink ( $fPath );
		}
		if ($objImg != "") {
			echo "<meta http-equiv=Content-Type content=text/html; charset=UTF-8>
				<script language=\"JavaScript\">
					alert('删除图片成功');
					window.location.href='edit?sspecial_id={$intID}';
				</script>";
			exit ();
		}
	}
	function stpack($str) {
		$str = nl2br ( trim ( $str ) );
		return $str;
	}
	function mkdirs($dir, $mode = 0777) {
		if (is_dir ( $dir ) || @mkdir ( $dir, $mode ))
			return TRUE;
		if (! $this->mkdirs ( dirname ( $dir ), $mode ))
			return FALSE;
		return @mkdir ( $dir, $mode );
	}
	function imgMark($strFilePathName, $strFileName, $intWidth, $intHeight, $isWater = true, $isThumb = false) {
		global $gd_image;
		$arrImginfo = getimagesize ( $strFilePathName );
		$gd_image->watertype = 1;
		$gd_image->strWaterPath = "water.jpg";
		$gd_image->waterstring = "";
		$gd_image->SetVar ( $strFilePathName, "file" );
		if ($isWater) {
			$gd_image->waterMark ( $strFileName );
			$gd_image->SetVar ( $strFileName, "file" );
		}
		if ($isThumb) {
			$gd_image->Prorate ( $strFileName, $intWidth, $intHeight );
		}
	}
	public static function curlSms($url, $field) {
		$ch = curl_init (); // 初始化CURL句柄
		curl_setopt ( $ch, CURLOPT_URL, $url ); // 设置请求的URL
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 ); // 设为TRUE把curl_exec()结果转化为字串，而不是直接输出
		curl_setopt ( $ch, CURLOPT_POST, 1 ); // 启用POST提交
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $field ); // 设置POST提交的字符串
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 25 ); // 超时时间
		curl_setopt ( $ch, CURLOPT_USERAGENT, $user_agent ); // HTTP请求User-Agent:头
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
				'Accept-Language: zh-cn',
				'Connection: Keep-Alive',
				'Cache-Control: no-cache' 
		) ); // 设置HTTP头信息
		$document = curl_exec ( $ch ); // 执行预定义的CURL
		$info = curl_getinfo ( $ch ); // 得到返回信息的特性
		return $document;
	}
	
	/**
	 * 使用linux convert命令压缩图片
	 * 
	 * @param string $inputImage        	
	 * @param string $outputImage        	
	 * @param string $size
	 *        	例:20x20
	 * @param int $quality
	 *        	压缩效率比例
	 * @param int $quality        	
	 */
	public function convertProductImg($inputImage, $outputImage, $size, $quality = 100) {
		$command = 'convert ' . $inputImage . ' -quality ' . $quality . ' -resize ' . $size . ' ' . $outputImage;
		exec ( $command );
	}
	
	/**
	 *
	 *
	 * 通过手机信息获取图片
	 * 
	 * @param string $image        	
	 * @param string $mobileType        	
	 * @return string
	 */
	public function getMobileImagePath($image, $mobileType) {
		$arrImage = explode ( '.', $image );
		return $arrImage [0] . '_' . $mobileType . '.' . $arrImage [1];
	}
}
