<?php
function getDirPath($intID) {
	$strTypeID = substr ( "00000000" . $intID, - 7, 2 ) . "/" . substr ( "00000000" . $intID, - 5, 2 ) . "/" . substr ( "00000000" . $intID, - 3, 2 ) . "/" . substr ( "00000000" . $intID, - 2 ) . "/";
	
	return trim ( $strTypeID );
}
function createDir($strPath) {
	$strPath = trim ( $strPath, "/" );
	$arrDir = explode ( "/", $strPath );
	$strDirPath = "";
	while ( list ( $key, $var ) = each ( $arrDir ) ) {
		$strDirPath .= "/" . $var;
		$strDirPath = trim ( $strDirPath, "/" );
		if (! is_dir ( $strDirPath )) {
			mkdirs ( $strDirPath, 0777 );
		}
	}
}
function uploadImage($strName, $strNewFileName = "", $strFilePath) {
	$strUploadFilePath = $strFilePath . "images/";
	if (! is_dir ( $strUploadFilePath )) {
		mkdirs ( $strUploadFilePath );
	}
	$strUploadFile = $_FILES [$strName] ['tmp_name'];
	$strFileType = $_FILES [$strName] ['type'];
	$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
	if ($strFileType == "image/pjpeg" || $strFileType == "image/gif" || $strFileType == "image/png") {
		if ($strUploadFile != "") {
			if ($strNewFileName == "") {
				$strUploadFileName = $_FILES [$strName] ['name'];
			} else {
				$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
			}
			$strFilePathName = $strUploadFilePath . $strUploadFileName;
			
			if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
				echo ("上传文件失败");
				exit ();
			}
			@unlink ( $strUploadFile );
			return $strUploadFileName;
		}
	}
}
function uploadImageIntro($strName, $strNewFileName = "", $version = "") {
	global $strFilePath, $pageImgW, $pageImgH;
	$strUploadFilePath = $strFilePath . "images/";
	if (! is_dir ( $strUploadFilePath )) {
		mkdirs ( $strUploadFilePath );
	}
	$strUploadFile = $_FILES [$strName] ['tmp_name'];
	$strFileType = $_FILES [$strName] ['type'];
	$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
	if ($strFileType == "image/pjpeg" || $strFileType == "image/gif" || $strFileType == "image/png") {
		if ($strUploadFile != "") {
			if ($strNewFileName == "") {
				$strUploadFileName = $_FILES [$strName] ['name'];
			} else {
				$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
			}
			
			if ($version > 0) {
				$strFilePathName = $strUploadFilePath . $version . "_" . $strUploadFileName;
			} else {
				$strFilePathName = $strUploadFilePath . $strUploadFileName;
			}
			
			// $strFilePathName = $strUploadFilePath.$strUploadFileName;
			
			if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
				echo ("上传文件失败");
				exit ();
			}
			
			@unlink ( $strUploadFile );
			return $strUploadFileName;
		}
	}
}
function uploadImage2($strName, $strNewFileName = "", $version = "", $strFilePath) {
	$strUploadFilePath = $strFilePath . "images/";
	if (! is_dir ( $strUploadFilePath )) {
		mkdirs ( $strUploadFilePath );
	}
	$strUploadFile = $_FILES [$strName] ['tmp_name'];
	$strFileType = $_FILES [$strName] ['type'];
	$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
	if ($strFileType == "image/pjpeg" || $strFileType == "image/gif" || $strFileType == "image/png") {
		if ($strUploadFile != "") {
			if ($strNewFileName == "") {
				$strUploadFileName = $_FILES [$strName] ['name'];
			} else {
				$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
			}
			
			if ($version > 0) {
				$strFilePathName = $strUploadFilePath . $version . "_" . $strUploadFileName;
			} else {
				$strFilePathName = $strUploadFilePath . $strUploadFileName;
			}
			
			// $strFilePathName = $strUploadFilePath.$strUploadFileName;
			
			if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
				echo ("上传文件失败");
				exit ();
			}
			@unlink ( $strUploadFile );
			return $strUploadFileName;
		}
	}
}
function imgMark($strFilePathName, $strFileName, $intWidth, $intHeight, $isWater = true, $isThumb = false) {
	global $gd_image;
	$arrImginfo = getimagesize ( $strFilePathName );
	$gd_image->watertype = 1;
	$gd_image->strWaterPath = "water.jpg";
	$gd_image->waterstring = "173mt.com";
	$gd_image->SetVar ( $strFilePathName, "file" );
	if ($isWater) {
		$gd_image->waterMark ( $strFileName );
		$gd_image->SetVar ( $strFileName, "file" );
	}
	if ($isThumb) {
		$gd_image->Prorate ( $strFileName, $intWidth, $intHeight );
	}
}
function delfile($intID, $strPicName) {
	global $pageImgW, $apspecial;
	
	$strNewFile = getDirPath ( $intID );
	$fPath = $apspecial . $strNewFile . "images/" . $intID . ".jpg";
	if (file_exists ( $filePath )) {
		if (is_array ( $pageImgW )) {
			while ( list ( $key, $var ) = each ( $pageImgW ) ) {
				$strFileName = $strPath . substr ( $strPicName, 0, strrpos ( $strPicName, "." ) ) . "_$key" . substr ( $strPicName, strrpos ( $strPicName, "." ) );
				@unlink ( $strFileName );
			}
		}
	}
	@unlink ( $filePath );
	return true;
}
function mkdirs($dir, $mode = 0777) {
	if (is_dir ( $dir ) || @mkdir ( $dir, $mode ))
		return TRUE;
	if (! mkdirs ( dirname ( $dir ), $mode ))
		return FALSE;
	return @mkdir ( $dir, $mode );
}
function getDir($dirName, $dir, $level = 1) {
	/* 设置临时数组 */
	$tmp = array ();
	/* 打开路径 */
	if ($handle = opendir ( "$dirName" )) {
		/* 打开传入路径的句柄 */
		while ( false !== ($item = readdir ( $handle )) ) {
			/* ..是上一级目录 .是当前 都排除掉 */
			if ($item != "." && $item != "..") {
				/* 检查当前搜索是目录还是文件 */
				if (is_dir ( "$dirName/$item" )) {
					$level_ = $level + 1;
					if ($dirName == './') {
						/* 如果当前目录深度和指定一样 */
						if ($level == $dir) {
							$tmp [] = "$item";
						} else {
							$tmp = array_merge ( $tmp, getDir ( $item, $dir, $level_ ) );
						}
					} else {
						if ($level == $dir) {
							$tmp [] = "$dirName/$item";
						} else {
							$tmp = array_merge ( $tmp, getDir ( "$dirName/$item", $dir, $level_ ) );
						}
					}
				}
			}
		}
		closedir ( $handle );
	}
	return $tmp;
}
?>