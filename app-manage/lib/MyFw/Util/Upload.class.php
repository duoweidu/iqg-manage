<?php
require_once 'IFw/Util/GDImage.class.php';

/**
 * 改写upload.php文件为class形式
 * 
 * @author 
 *        
 */
class TCUploadImg {
	
	/**
	 * 图片水印字符
	 * 
	 * @var string
	 */
	public $waterStr = 'tc.com';
	protected $strFilePath, $pageImgW, $pageImgH;
	
	/**
	 *
	 * @param string $strFilePath        	
	 * @param int $pageImgW        	
	 * @param int $pageImgH        	
	 * @param int $apspecial        	
	 *
	 */
	public function __construct($pageImgW, $pageImgH) {
		$this->pageImgW = $pageImgW;
		$this->pageImgH = $pageImgH;
	}
	
	/**
	 * 上传文件路径设置
	 * 
	 * @param int $intID        	
	 */
	public function getDirPath($intID) {
		$strTypeID = substr ( "00000000" . $intID, - 7, 2 ) . "/" . substr ( "00000000" . $intID, - 5, 2 ) . "/" . substr ( "00000000" . $intID, - 3, 2 ) . "/" . substr ( "00000000" . $intID, - 2 ) . "/";
		
		return trim ( $strTypeID );
	}
	
	/**
	 *
	 * @param string $strFilePath        	
	 * @return void
	 */
	public function setFilePath($strFilePath) {
		$this->strFilePath = $strFilePath;
	}
	
	/**
	 *
	 *
	 * 递归创建上传目录
	 * 
	 * @param string $strPath        	
	 * @return void
	 */
	public function createDir($path) {
		if (! file_exists ( $path )) {
			$this->createDir ( dirname ( $path ) );
			mkdir ( $path, 0777 );
		}
	}
	
	/**
	 *
	 * @param string $strName        	
	 * @param string $strNewFileName        	
	 * @param string $strFilePath        	
	 * @param int $version        	
	 * @return string
	 */
	public function uploadImage($strName, $strNewFileName = "", $version = "", $isMark = FALSE) {
		if (! $this->strFilePath) {
			throw new Exception ( 'strFilePath value is empty, please use setFilePath() method', E_ERROR );
		}
		$strUploadFilePath = $this->strFilePath . "images/";
		if (! is_dir ( $strUploadFilePath )) {
			$this->createDir ( $strUploadFilePath );
		}
		$strUploadFile = $_FILES [$strName] ['tmp_name'];
		$strFileType = $_FILES [$strName] ['type'];
		$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
		if ($strFileType == "image/pjpeg" || $strFileType == "image/jpeg" || $strFileType == "image/gif" || $strFileType == "image/png") {
			if ($strUploadFile != "") {
				if ($strNewFileName == "") {
					$strUploadFileName = $_FILES [$strName] ['name'];
				} else {
					$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
				}
				if ($version > 0) {
					$strFilePathName = $strUploadFilePath . $version . "_" . $strUploadFileName;
				} else {
					$strFilePathName = $strUploadFilePath . $strUploadFileName;
				}
				
				if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
					echo ("上传文件失败");
					exit ();
				}
				
				if ($isMark === true) {
					$arrStrFile = explode ( '/', $strFilePathName );
					$strFileName = $arrStrFile [count ( $arrStrFile ) - 1];
					$this->imgMark ( $strFilePathName, $strFileName, $this->pageImgW, $this->pageImgH, true, true );
				}
				
				@unlink ( $strUploadFile );
				return $strFilePathName;
			}
		}
	}
	
	/**
	 *
	 * @param string $strName        	
	 * @param string $strNewFileName        	
	 * @param int $version        	
	 * @return string
	 */
	public function uploadImageIntro($strName, $strNewFileName = "", $version = "") {
		if (! $this->strFilePath) {
			throw new Exception ( 'strFilePath value is empty, please use setFilePath() method', E_ERROR );
		}
		$strUploadFilePath = $this->strFilePath . "images/";
		if (! is_dir ( $strUploadFilePath )) {
			$this->createDir ( $strUploadFilePath );
		}
		$strUploadFile = $_FILES [$strName] ['tmp_name'];
		$strFileType = $_FILES [$strName] ['type'];
		$strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
		if ($strFileType == "image/pjpeg" || $strFileType == "image/gif" || $strFileType == "image/png") {
			if ($strUploadFile != "") {
				if ($strNewFileName == "") {
					$strUploadFileName = $_FILES [$strName] ['name'];
				} else {
					$strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
				}
				
				if ($version > 0) {
					$strFilePathName = $strUploadFilePath . $version . "_" . $strUploadFileName;
				} else {
					$strFilePathName = $strUploadFilePath . $strUploadFileName;
				}
				
				if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
					echo ("上传文件失败");
					exit ();
				}
				
				@unlink ( $strUploadFile );
				return $strUploadFileName;
			}
		}
	}
	
	/**
	 *
	 *
	 * 添加图片水印
	 * 
	 * @param string $strFilePathName        	
	 * @param string $strFileName        	
	 * @param int $intWidth        	
	 * @param int $intHeight        	
	 * @param boolean $isWater        	
	 * @param boolean $isThumb        	
	 * @return void
	 */
	public function imgMark($strFilePathName, $strFileName, $intWidth, $intHeight, $isWater = true, $isThumb = false) {
		$objGdImage = new CreatMiniature ();
		$arrImginfo = getimagesize ( $strFilePathName );
		$objGdImage->watertype = 1;
		$objGdImage->strWaterPath = "water.jpg";
		$objGdImage->waterstring = $this->waterStr;
		$objGdImage->SetVar ( $strFilePathName, "file" );
		if ($isWater) {
			$objGdImage->waterMark ( $strFileName );
			$objGdImage->SetVar ( $strFileName, "file" );
		}
		if ($isThumb) {
			$objGdImage->Prorate ( $strFileName, $intWidth, $intHeight );
		}
	}
	
	/**
	 * 程序段有小问题不建议使用故先注意*
	 */
	// public function delfile($intID, $strPicName) {
	// $strNewFile = getDirPath ( $intID );
	// $fPath = $this->apspecial . $strNewFile . "images/" . $intID . ".jpg";
	// if (file_exists ( $filePath )) {
	// if (is_array ( $this->pageImgW )) {
	// while ( list ( $key, $var ) = each ( $this->pageImgW ) ) {
	// $strFileName = $strPath . substr ( $strPicName, 0, strrpos ( $strPicName, "." ) ) . "_$key" . substr ( $strPicName, strrpos ( $strPicName, "." ) );
	// @unlink ( $strFileName );
	// }
	// }
	// }
	// @unlink ( $filePath );
	// return true;
	// }
	
	/**
	 *
	 *
	 * Enter description here ...
	 * 
	 * @param string $dirName        	
	 * @param string $dir        	
	 * @param int $level        	
	 */
	public function getDir($dirName, $dir, $level = 1) {
		/* 设置临时数组 */
		$tmp = array ();
		/* 打开路径 */
		if ($handle = opendir ( $dirName )) {
			/* 打开传入路径的句柄 */
			while ( false !== ($item = readdir ( $handle )) ) {
				/* ..是上一级目录 .是当前 都排除掉 */
				if ($item != "." && $item != "..") {
					/* 检查当前搜索是目录还是文件 */
					if (is_dir ( $dirName / $item )) {
						$level_ = $level + 1;
						if ($dirName == './') {
							/* 如果当前目录深度和指定一样 */
							if ($level == $dir) {
								$tmp [] = $item;
							} else {
								$tmp = array_merge ( $tmp, getDir ( $item, $dir, $level_ ) );
							}
						} else {
							if ($level == $dir) {
								$tmp [] = $dirName / $item;
							} else {
								$tmp = array_merge ( $tmp, getDir ( $dirName / $item, $dir, $level_ ) );
							}
						}
					}
				}
			}
			closedir ( $handle );
		}
		return $tmp;
	}
	
	// 以下代码暂时废除 合并至uploadImage()
	// public function uploadImage2($strName, $strNewFileName = "", $version = "", $strFilePath) {
	// $strUploadFilePath = $strFilePath . "images/";
	// if (! is_dir ( $strUploadFilePath )) {
	// mkdirs ( $strUploadFilePath );
	// }
	// $strUploadFile = $_FILES [$strName] ['tmp_name'];
	// $strFileType = $_FILES [$strName] ['type'];
	// $strstr = substr ( $_FILES [$strName] ['name'], strrpos ( $_FILES [$strName] ['name'], "." ) );
	// if ($strFileType == "image/pjpeg" || $strFileType == "image/gif" || $strFileType == "image/png") {
	// if ($strUploadFile != "") {
	// if ($strNewFileName == "") {
	// $strUploadFileName = $_FILES [$strName] ['name'];
	// } else {
	// $strUploadFileName = $strNewFileName . substr ( $_FILES [$strName] ["name"], strrpos ( $_FILES [$strName] ["name"], "." ) );
	// }
	//
	// if ($version > 0) {
	// $strFilePathName = $strUploadFilePath . $version . "_" . $strUploadFileName;
	// } else {
	// $strFilePathName = $strUploadFilePath . $strUploadFileName;
	// }
	//
	// //$strFilePathName = $strUploadFilePath.$strUploadFileName;
	//
	//
	// if (! move_uploaded_file ( $strUploadFile, $strFilePathName )) {
	// echo ("上传文件失败");
	// exit ();
	// }
	// @unlink ( $strUploadFile );
	// return $strUploadFileName;
	// }
	// }
	// }
	//
}
?>