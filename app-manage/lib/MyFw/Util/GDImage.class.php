<?php

/***************************************
*作者：落梦天蝎（beluckly）
*完成时间：2006-12-18
*类名：CreatMiniature
*功能：生成多种类型的缩略图
*基本参数：$srcFile,$echoType
*方法用到的参数：
$toFile,生成的文件
$toW,生成的宽
$toH,生成的高
$bk1,背景颜色参数 以255为最高
$bk2,背景颜色参数
$bk3,背景颜色参数

*例子：

include("thumb.php");
$cm=new CreatMiniature();
$cm->SetVar("1.jpg","file");
$cm->Distortion("dis_bei.jpg",150,200);
$cm->Prorate("pro_bei.jpg",150,200);
$cm->Cut("cut_bei.jpg",150,200);
$cm->BackFill("fill_bei.jpg",150,200);

**************************************
$cm=new CreatMiniature();
$cm->SetVar("zt3.jpg","file");
//$cm->Distortion("dis_bei.jpg",150,200);
//$cm->Prorate("pro_bei.jpg",150,200);
//$cm->Cut("cut_bei.jpg",150,200);
//$cm->BackFill("fill_bei.jpg",150,200);
$cm->waterMark("water_img.jpg");*/
class CreatMiniature {
	// 公共变量
	var $srcFile = ""; // 原图
	var $echoType; // 输出图片类型，link--不保存为文件；file--保存为文件
	var $im = ""; // 临时变量
	var $srcW = ""; // 原图宽
	var $srcH = ""; // 原图高
	var $strWaterPath = "water.jpg";
	var $watertype = 1; // 水印类型1=文字;2=图片
	var $waterstring = "hush-shop.com";
	// 0 = middle
	// 1 = top left
	// 2 = top right
	// 3 = bottom right
	// 4 = bottom left
	// 5 = top middle
	// 6 = middle right
	// 7 = bottom middle
	// 8 = middle left
	var $wm_text_pos = 3; // 水印文字放置位置
	var $wm_text_size = 3; // 水印文字大小
	                       
	// 设置变量及初始化
	function SetVar($srcFile, $echoType) {
		$this->srcFile = $srcFile;
		$this->echoType = $echoType;
		
		$info = "";
		$data = GetImageSize ( $this->srcFile, $info );
		switch ($data [2]) {
			case 1 :
				if (! function_exists ( "imagecreatefromgif" )) {
					echo "你的GD库不能使用GIF格式的图片，请使用Jpeg或PNG格式！<a href='java script:go(-1);'>返回</a>";
					exit ();
				}
				$this->im = ImageCreateFromGIF ( $this->srcFile );
				break;
			case 2 :
				if (! function_exists ( "imagecreatefromjpeg" )) {
					echo "你的GD库不能使用jpeg格式的图片，请使用其它格式的图片！<a href='java script:go(-1);'>返回</a>";
					exit ();
				}
				$this->im = ImageCreateFromJpeg ( $this->srcFile );
				break;
			case 3 :
				$this->im = ImageCreateFromPNG ( $this->srcFile );
				break;
		}
		
		$this->srcW = ImageSX ( $this->im );
		$this->srcH = ImageSY ( $this->im );
	}
	
	// 生成扭曲型缩图
	function Distortion($toFile, $toW, $toH) {
		$cImg = $this->CreatImage ( $this->im, $toW, $toH, 0, 0, 0, 0, $this->srcW, $this->srcH );
		return $this->EchoImage ( $cImg, $toFile );
		ImageDestroy ( $cImg );
	}
	
	// 生成按比例缩放的缩图
	function Prorate($toFile, $toW, $toH) {
		$toWH = $toW / $toH;
		$srcWH = $this->srcW / $this->srcH;
		if ($toWH <= $srcWH) {
			$ftoW = $toW;
			$ftoH = $ftoW * ($this->srcH / $this->srcW);
		} else {
			$ftoH = $toH;
			$ftoW = $ftoH * ($this->srcW / $this->srcH);
		}
		if ($this->srcW > $toW || $this->srcH > $toH) {
			$cImg = $this->CreatImage ( $this->im, $ftoW, $ftoH, 0, 0, 0, 0, $this->srcW, $this->srcH );
			// $backcolor = imagecolorallocate($cImg, 255, 255, 255); //填充的背景颜色
			// ImageFilledRectangle($cImg,0,0,$ftoW,$ftoH,$backcolor);
			return $this->EchoImage ( $cImg, $toFile );
			ImageDestroy ( $cImg );
		} else {
			$cImg = $this->CreatImage ( $this->im, $ftoW, $ftoH, 0, 0, 0, 0, $this->srcW, $this->srcH );
			// $cImg=$this->CreatImage($this->im,$this->srcW,$this->srcH,0,0,0,0,$this->srcW,$this->srcH);
			return $this->EchoImage ( $cImg, $toFile );
			ImageDestroy ( $cImg );
		}
	}
	
	// 生成最小裁剪后的缩图
	function Cut($toFile, $toW, $toH) {
		$toWH = $toW / $toH;
		$srcWH = $this->srcW / $this->srcH;
		if ($toWH <= $srcWH) {
			$ctoH = $toH;
			$ctoW = $ctoH * ($this->srcW / $this->srcH);
		} else {
			$ctoW = $toW;
			$ctoH = $ctoW * ($this->srcH / $this->srcW);
		}
		$allImg = $this->CreatImage ( $this->im, $ctoW, $ctoH, 0, 0, 0, 0, $this->srcW, $this->srcH );
		$cImg = $this->CreatImage ( $allImg, $toW, $toH, 0, 0, ($ctoW - $toW) / 2, ($ctoH - $toH) / 2, $toW, $toH );
		return $this->EchoImage ( $cImg, $toFile );
		ImageDestroy ( $cImg );
		ImageDestroy ( $allImg );
	}
	
	// 生成背景填充的缩图
	function BackFill($toFile, $toW, $toH, $bk1 = 255, $bk2 = 255, $bk3 = 255) {
		$toWH = $toW / $toH;
		$srcWH = $this->srcW / $this->srcH;
		if ($toWH <= $srcWH) {
			$ftoW = $toW;
			$ftoH = $ftoW * ($this->srcH / $this->srcW);
		} else {
			$ftoH = $toH;
			$ftoW = $ftoH * ($this->srcW / $this->srcH);
		}
		if (function_exists ( "imagecreatetruecolor" )) {
			@$cImg = ImageCreateTrueColor ( $toW, $toH );
			if (! $cImg) {
				$cImg = ImageCreate ( $toW, $toH );
			}
		} else {
			$cImg = ImageCreate ( $toW, $toH );
		}
		$backcolor = imagecolorallocate ( $cImg, $bk1, $bk2, $bk3 ); // 填充的背景颜色
		ImageFilledRectangle ( $cImg, 0, 0, $toW, $toH, $backcolor );
		if ($this->srcW > $toW || $this->srcH > $toH) {
			$proImg = $this->CreatImage ( $this->im, $ftoW, $ftoH, 0, 0, 0, 0, $this->srcW, $this->srcH );
			/*
			 * if($ftoW<$toW) { ImageCopyMerge($cImg,$proImg,($toW-$ftoW)/2,0,0,0,$ftoW,$ftoH,100); } else if($ftoH<$toH) { ImageCopyMerge($cImg,$proImg,0,($toH-$ftoH)/2,0,0,$ftoW,$ftoH,100); }
			 */
			if ($ftoW < $toW) {
				ImageCopy ( $cImg, $proImg, ($toW - $ftoW) / 2, 0, 0, 0, $ftoW, $ftoH );
			} else if ($ftoH < $toH) {
				ImageCopy ( $cImg, $proImg, 0, ($toH - $ftoH) / 2, 0, 0, $ftoW, $ftoH );
			} else {
				ImageCopy ( $cImg, $proImg, 0, 0, 0, 0, $ftoW, $ftoH );
			}
		} else {
			ImageCopyMerge ( $cImg, $this->im, ($toW - $ftoW) / 2, ($toH - $ftoH) / 2, 0, 0, $ftoW, $ftoH, 100 );
		}
		return $this->EchoImage ( $cImg, $toFile );
		ImageDestroy ( $cImg );
	}
	function CreatImage($img, $creatW, $creatH, $dstX, $dstY, $srcX, $srcY, $srcImgW, $srcImgH) {
		if (function_exists ( "imagecreatetruecolor" )) {
			@$creatImg = ImageCreateTrueColor ( $creatW, $creatH );
			$backcolor = imagecolorallocate ( $creatImg, 255, 255, 255 ); // 填充的背景颜色
			ImageFilledRectangle ( $creatImg, 0, 0, $creatW, $creatH, $backcolor );
			if ($creatImg)
				ImageCopyResampled ( $creatImg, $img, $dstX, $dstY, $srcX, $srcY, $creatW, $creatH, $srcImgW, $srcImgH );
			else {
				$creatImg = ImageCreate ( $creatW, $creatH );
				ImageCopyResized ( $creatImg, $img, $dstX, $dstY, $srcX, $srcY, $creatW, $creatH, $srcImgW, $srcImgH );
			}
		} else {
			$creatImg = ImageCreate ( $creatW, $creatH );
			ImageCopyResized ( $creatImg, $img, $dstX, $dstY, $srcX, $srcY, $creatW, $creatH, $srcImgW, $srcImgH );
		}
		return $creatImg;
	}
	
	// 输出图片，link---只输出，不保存文件。file--保存为文件
	function EchoImage($img, $to_File) {
		switch ($this->echoType) {
			case "link" :
				if (function_exists ( 'imagejpeg' ))
					return ImageJpeg ( $img );
				else
					return ImagePNG ( $img );
				break;
			case "file" :
				if (function_exists ( 'imagejpeg' ))
					return ImageJpeg ( $img, $to_File, 100 );
				else
					return ImagePNG ( $img, $to_File );
				break;
		}
	}
	
	// 生成水印图片
	function waterMark($toFile) {
		$watertype = $this->watertype;
		// $pco = imagecolorallocate ($this->im, 255, 255, 255);
		
		switch ($watertype) {
			case 1 : // 加水印字符串
			        // 计算水印坐标
				$arrPos = $this->getPos ( $this->srcW, $this->srcH, $this->wm_text_pos );
				// 水印字体颜色
				$pco = ImageColorAllocate ( $this->im, 0, 0, 0 );
				imagestring ( $this->im, $this->wm_text_size, $arrPos ["dest_x"], $arrPos ["dest_y"], $this->waterstring, $pco );
				break;
			case 2 : // 加水印图片
				$simage1 = ImageCreateFromGif ( $this->strWaterPath );
				// print$this -> strWaterPath;
				// 计算水印坐标
				$arrPos = $this->getPos ( $this->srcW, $this->srcH, $this->wm_text_pos, $simage1 );
				$insertfile_width = ImageSx ( $simage1 );
				$insertfile_height = ImageSy ( $simage1 );
				imagecopy ( $this->im, $simage1, $arrPos ["dest_x"], $arrPos ["dest_y"], 0, 0, $insertfile_width, $insertfile_height );
				imagedestroy ( $simage1 );
				break;
		}
		$cImg = $this->CreatImage ( $this->im, $this->srcW, $this->srcH, 0, 0, 0, 0, $this->srcW, $this->srcH );
		return $this->EchoImage ( $cImg, $toFile );
		ImageDestroy ( $cImg );
	}
	
	/**
	 * 根据源图像的长、宽，位置代码，水印图片id来生成把水印放置到源图像中的位置
	 *
	 * @access pravate
	 * @param int $sourcefile_width
	 *        	源图像的宽
	 * @param int $sourcefile_height
	 *        	原图像的高
	 * @param int $pos
	 *        	位置代码
	 *        	0 = middle
	 *        	1 = top left
	 *        	2 = top right
	 *        	3 = bottom right
	 *        	4 = bottom left
	 *        	5 = top middle
	 *        	6 = middle right
	 *        	7 = bottom middle
	 *        	8 = middle left
	 * @param int $wm_image
	 *        	水印图片ID
	 * @return array 返回位置参数数组
	 */
	function getPos($sourcefile_width, $sourcefile_height, $pos, $wm_image = "") {
		if ($wm_image) {
			$insertfile_width = ImageSx ( $wm_image );
			$insertfile_height = ImageSy ( $wm_image );
		} else {
			$lineCount = explode ( "\r\n", $this->waterstring );
			$insertfile_width = ImageFontWidth ( $this->wm_text_size ) * strlen ( $this->waterstring );
			$insertfile_height = imagefontheight ( $this->wm_text_size ) * count ( $lineCount );
		}
		
		switch ($pos) {
			case 0 :
				$dest_x = ($sourcefile_width / 2) - ($insertfile_width / 2);
				$dest_y = ($sourcefile_height / 2) - ($insertfile_height / 2);
				break;
			
			case 1 :
				$dest_x = 0;
				if ($this->waterstring) {
					$dest_y = $insertfile_height;
				} else {
					$dest_y = 0;
				}
				break;
			
			case 2 :
				$dest_x = $sourcefile_width - $insertfile_width;
				if ($this->waterstring) {
					$dest_y = $insertfile_height;
				} else {
					$dest_y = 0;
				}
				break;
			
			case 3 :
				$dest_x = $sourcefile_width - $insertfile_width;
				$dest_y = $sourcefile_height - $insertfile_height;
				break;
			
			case 4 :
				$dest_x = 0;
				$dest_y = $sourcefile_height - $insertfile_height;
				break;
			
			case 5 :
				$dest_x = (($sourcefile_width - $insertfile_width) / 2);
				if ($this->waterstring) {
					$dest_y = $insertfile_height;
				} else {
					$dest_y = 0;
				}
				break;
			
			case 6 :
				$dest_x = $sourcefile_width - $insertfile_width;
				$dest_y = ($sourcefile_height / 2) - ($insertfile_height / 2);
				break;
			
			case 7 :
				$dest_x = (($sourcefile_width - $insertfile_width) / 2);
				$dest_y = $sourcefile_height - $insertfile_height;
				break;
			
			case 8 :
				$dest_x = 0;
				$dest_y = ($sourcefile_height / 2) - ($insertfile_height / 2);
				break;
			
			default :
				$dest_x = $sourcefile_width - $insertfile_width;
				$dest_y = $sourcefile_height - $insertfile_height;
				break;
		}
		return array (
				"dest_x" => $dest_x - 10,
				"dest_y" => $dest_y - 10 
		);
	}
	
	// 得到图片信息
	function getInfo($file) {
		$data = getimagesize ( $file );
		$imageInfo ["width"] = $data [0];
		$imageInfo ["heigh"] = $data [1];
		$imageInfo ["mime"] = $data ["mime"];
		$imageInfo ["name"] = basename ( $file );
		return $imageInfo;
	}
}
?>