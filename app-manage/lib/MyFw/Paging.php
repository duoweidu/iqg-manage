<?php
/**
 * TC Paging
 *
 * @category   TC
 * @package    MyFw_Dao
 * @copyright  Copyright (c) 
 * @version    $Id$
 */
require_once 'Fw/Paging.php';

/**
 *
 * @package MyFw_Paging
 */
class MyFw_Paging extends Fw_Paging {
	/**
	 * 直接生成分页的html代码和供分页查询的limit参数
	 * 
	 * @return array
	 */
	public function process() {
		$result = array (
				'pageHtml' => '', // 显示的分页的html代码
				'start' => 0, // limit的第一个参数
				'offset' => $this->each, // limit的第二个参数
				'currentPage' => $this->page  // 当前页
				); // 返回的结果集合
		$pagingResult = $this->paging ();
		$result ['currentPage'] = $this->page; // 因为有可能用户进入的页码超过了最大页码,paeing后的参数是正规参数
		$link = $pagingResult ['pageLink'];
		$pageSize = $this->getDiyPageSize ();
		$pageSize = $pageSize == Null ? $this->each : $pageSize;
		if (strpos ( $pagingResult ['pageLink'], "?" )) {
			$link = $pagingResult ['pageLink'] . "&";
		} else {
			$link = $pagingResult ['pageLink'] . "?";
		}
		$result ['pageHtml'] = '<script type="text/javascript" language="javascript">
            function changePageSize(obj){
                var regx= /\D+/;
                if(!regx.test(obj.value) && obj.value!=0){
                    window.location.href="' . $link . 'diypagesize="+obj.value;
                }
            }</script>';
		$result ['pageHtml'] .= '<div style="float:right" class="pagelist">';
		$result ['pageHtml'] .= '每页显示<input type="text" onblur="changePageSize(this)" name="pagesize" size="4" value="' . $pageSize . '">条记录&nbsp;';
		$result ['pageHtml'] .= '<span>共 ' . $pagingResult ['totalPage'] . ' 页 / ' . $pagingResult ['totalNum'] . ' 条记录 </span>';
		$result ['pageHtml'] .= '<span class="indexPage">' . $pagingResult ['prevStr'] . '</span>' . $pagingResult ['pageStr'] . '<span class="nextPage">' . $pagingResult ['nextStr'] . '</span></div>';
		$result ['start'] = $pagingResult ['frNum'];
		return $result;
	}
}