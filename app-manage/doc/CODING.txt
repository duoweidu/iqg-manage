﻿编码规范

* 基于 Zend Framework 编码规范

1、缩进为一个 Tab 或者 4 个空格

2、require 以及 include 不要当作函数调用，比如：
require_once 'Fw/Page.php';
require_once 'Fw/Db.php';

3、一般类库的名称必须以 ZF 方式的命名，比如：Path_To_Class
除非 Page 类文件以及 Dao 类文件，他们的命名规则分别如下：
Dao 类：Database_Tablename {} 类文件在 Dao 目录下
Page 类：PageName {} 类文件在页面类库目录下

4、一般变量以及函数的名称以骆驼命名法，比如：
$variableName = 1;
public function methodName()
而私有变量或函数以 _ 开头，比如：private function _methodName()

5、类声名和函数声名后的第一个 { 符号换行，比如：
Class_Name
{
	public function methodName ()
	{
		// TODO : method process
	}
}

6、每个文件顶部必须有该类的说明注释，比如：
<?php
/**
 * Fw Dao
 *
 * @category   Fw
 * @package    Fw_Dao
 * @author     **
 * @copyright  Copyright (c) ** Technologies Inc.  
 * @version    $Id$
 */

7、每个类文件前必须有类定义说明，比如：
...
/**
 * @abstract
 * @package Fw_Dao
 */
class Fw_Dao
{
...

8、被调用类库文件函数名前必须有说明注释（页面类可省略，因为基本不会被调用），比如：
...
	/**
	 * Load data by primary key id
	 * 
	 * @param mixed $id Primary key value
	 * @param string $pk Primary key name
	 * @return array
	 */
	public function methodName ()
	{
		// TODO : method process
	}
...

9、被调用类库文件后面的 php 结束符 ?> 可不用写。要写也可以，但是结束符后不能加任何字符。

10、实例参考：lib/Fw/Dao.php 和 lib/Fw/App/Page.php