/*
Navicat MySQL Data Transfer

Source Server         : 10.0.0.8
Source Server Version : 50161
Source Host           : 10.0.0.8:3306
Source Database       : iqg_manage

Target Server Type    : MYSQL
Target Server Version : 50161
File Encoding         : 65001

Date: 2014-06-09 13:30:11
*/

SET FOREIGN_KEY_CHECKS=0;
 

-- ----------------------------
-- Table structure for kpi_baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `kpi_baseinfo`;
CREATE TABLE `kpi_baseinfo` (
  `kb_shopid` int(10) unsigned NOT NULL,
  `kb_signer_sellerid` int(10) unsigned DEFAULT NULL COMMENT '开发签约uid',
  `kb_holder_sellerid` int(10) unsigned DEFAULT NULL COMMENT '跟进销售id',
  `kb_issuetime` int(10) unsigned DEFAULT NULL,
  `kb_offtime` int(10) unsigned DEFAULT '0',
  `kb_catid` int(10) unsigned NOT NULL,
  `kb_cityid` int(10) unsigned NOT NULL,
  `kb_shop_pid` int(10) unsigned NOT NULL,
  `kb_seller_pid` int(10) unsigned NOT NULL,
  `kb_seller_level` int(10) unsigned NOT NULL DEFAULT '1',
  `kb_utime` int(11) DEFAULT '0' COMMENT '最后更新时间',
  `kb_uuid` int(11) DEFAULT '0' COMMENT '最后修改人',
  PRIMARY KEY (`kb_shopid`),
  KEY `city` (`kb_cityid`),
  KEY `catid` (`kb_catid`),
  KEY `shoppid` (`kb_shop_pid`),
  KEY `sellerid` (`kb_holder_sellerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_rule
-- ----------------------------
DROP TABLE IF EXISTS `kpi_rule`;
CREATE TABLE `kpi_rule` (
  `kr_key` varchar(50) NOT NULL COMMENT '字符串唯一标识: level1 , level2',
  `kr_value` text COMMENT 'json 值',
  `kr_utime` int(11) DEFAULT '0' COMMENT '更新时间',
  `kr_uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`kr_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_rule_log
-- ----------------------------
DROP TABLE IF EXISTS `kpi_rule_log`;
CREATE TABLE `kpi_rule_log` (
  `krl_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `krl_key` varchar(50) NOT NULL COMMENT '字符串唯一标识: level1 , level2',
  `krl_value` text COMMENT 'json 值',
  `krl_utime` int(11) DEFAULT '0' COMMENT '更新时间',
  `krl_uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`krl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_stats
-- ----------------------------
DROP TABLE IF EXISTS `kpi_stats`;
CREATE TABLE `kpi_stats` (
  `ks_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '按门店月汇总',
  `ks_shopid` int(11) DEFAULT '0' COMMENT '门店id',
  `ks_year` smallint(6) DEFAULT '2014' COMMENT '年份',
  `ks_month` smallint(6) DEFAULT '1' COMMENT '月份',
  `ks_onsell` tinyint(4) DEFAULT '0' COMMENT '当前状态： 0 已下线  1 在线',
  `ks_onsell_days` int(11) DEFAULT '0' COMMENT '在售天数',
  `ks_goods_total` int(11) DEFAULT '0' COMMENT '销售商品数',
  `ks_money_total` int(11) DEFAULT '0' COMMENT '月销售总额',
  `ks_money_alipay` int(11) DEFAULT '0' COMMENT '支付宝-月销售总额',
  `ks_money_balance` int(11) DEFAULT '0' COMMENT '余额-月销售总额',
  `ks_utime` int(11) DEFAULT '0',
  PRIMARY KEY (`ks_id`),
  UNIQUE KEY `uniq` (`ks_shopid`,`ks_year`,`ks_month`)
) ENGINE=InnoDB AUTO_INCREMENT=483 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_summary
-- ----------------------------
DROP TABLE IF EXISTS `kpi_summary`;
CREATE TABLE `kpi_summary` (
  `ksm_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '按人月汇总',
  `ksm_uid` int(11) DEFAULT '0' COMMENT '被汇总人ID',
  `ksm_year` smallint(6) DEFAULT NULL,
  `ksm_month` smallint(6) DEFAULT NULL,
  `ksm_level` tinyint(4) DEFAULT '0' COMMENT '级别',
  `ksm_rulever` int(11) DEFAULT '0' COMMENT '规则版本号= kpi_rule.utime',
  `ksm_achieve` tinyint(4) DEFAULT '0' COMMENT '0 未达成指标 1 完成指标',
  `ksm_shop_new` int(11) DEFAULT '0' COMMENT '新签门店数',
  `ksm_shop_lt3m` int(11) DEFAULT '0' COMMENT '未满3月下线门店数',
  `ksm_shop_dyxx` int(11) DEFAULT '0' COMMENT '当月下线门店数',
  `ksm_shop_gtxm` int(11) DEFAULT '0' COMMENT '上线超过x月门店数',
  `ksm_shop_onsell` int(11) DEFAULT '0' COMMENT '在售门店总数',
  `ksm_shop_total` int(11) DEFAULT '0' COMMENT '下属门店总数',
  `ksm_profit_shop` int(11) DEFAULT '0' COMMENT '门店提成',
  `ksm_total_money` int(11) DEFAULT '0' COMMENT '个人月销售总额',
  `ksm_total_money_alipay` int(11) DEFAULT '0' COMMENT '支付宝支付总额',
  `ksm_total_money_balance` int(11) DEFAULT '0' COMMENT '余额支付总额',
  `ksm_profit_money` int(11) DEFAULT '0' COMMENT '个人销售总额提成',
  `ksm_profit_money_alipay` int(11) DEFAULT '0' COMMENT '个人支付宝销售总额提成',
  `ksm_profit_money_balance` int(11) DEFAULT '0' COMMENT '个人余额销售总额提成',
  `ksm_total_subordinate` int(11) DEFAULT '0' COMMENT '[经理级别]下属人数',
  `ksm_achieve_sub` tinyint(4) DEFAULT '0' COMMENT '[经理级别]下属指标是否完成',
  `ksm_total_subshop` int(11) DEFAULT '0' COMMENT '[经理级别]下属新签门店总数',
  `ksm_profit_subshop` int(11) DEFAULT '0' COMMENT '[经理级别]下属门店提成',
  `ksm_total_submoney` int(11) DEFAULT '0' COMMENT '[经理级别]下属销售总额',
  `ksm_total_submoney_alipay` int(11) DEFAULT '0' COMMENT '[经理级别]下属支付宝销售总额',
  `ksm_total_submoney_balance` int(11) DEFAULT '0' COMMENT '[经理级别]下属余额销售总额',
  `ksm_profit_submoney` int(11) DEFAULT '0' COMMENT '[经理级别]下属销售额提成',
  `ksm_profit_submoney_alipay` int(11) DEFAULT '0' COMMENT '[经理级别]下属支付宝销售额提成',
  `ksm_profit_submoney_balance` int(11) DEFAULT '0' COMMENT '[经理级别]下属余额销售额提成',
  `ksm_profit_total` int(11) DEFAULT '0' COMMENT '总提成',
  `ksm_status` tinyint(4) DEFAULT '0' COMMENT '0 待确认 1 已确认',
  `ksm_utime` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`ksm_id`),
  KEY `ym` (`ksm_year`,`ksm_month`),
  KEY `ksm_uid` (`ksm_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

 