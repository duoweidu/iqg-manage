/*
Navicat MySQL Data Transfer

Source Server         : 10.0.0.8
Source Server Version : 50161
Source Host           : 10.0.0.8:3306
Source Database       : iqg_manage_sys

Target Server Type    : MYSQL
Target Server Version : 50161
File Encoding         : 65001

Date: 2014-06-09 13:19:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acl_resource
-- ----------------------------
DROP TABLE IF EXISTS `acl_resource`;
CREATE TABLE `acl_resource` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_resource
-- ----------------------------
INSERT INTO `acl_resource` VALUES ('1', '0', 'acl_user_add', '添加新后台用户');
INSERT INTO `acl_resource` VALUES ('2', '0', 'acl_user_passwd', '权限管理中密码修改');

-- ----------------------------
-- Table structure for acl_resource_role
-- ----------------------------
DROP TABLE IF EXISTS `acl_resource_role`;
CREATE TABLE `acl_resource_role` (
  `resource_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`resource_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_resource_role
-- ----------------------------
INSERT INTO `acl_resource_role` VALUES ('1', '1');
INSERT INTO `acl_resource_role` VALUES ('1', '2');
INSERT INTO `acl_resource_role` VALUES ('1', '7');
INSERT INTO `acl_resource_role` VALUES ('2', '1');
INSERT INTO `acl_resource_role` VALUES ('5', '1');
INSERT INTO `acl_resource_role` VALUES ('5', '2');

-- ----------------------------
-- Table structure for acl_role
-- ----------------------------
DROP TABLE IF EXISTS `acl_role`;
CREATE TABLE `acl_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `alias` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_role
-- ----------------------------
INSERT INTO `acl_role` VALUES ('1', 'SA', '超级管理员');
INSERT INTO `acl_role` VALUES ('2', 'AM', '普通管理员');
INSERT INTO `acl_role` VALUES ('3', 'CS', '客服人员');
INSERT INTO `acl_role` VALUES ('4', 'PM', '产品管理');
INSERT INTO `acl_role` VALUES ('5', 'FS', '财务人员');
INSERT INTO `acl_role` VALUES ('6', 'XSRY', '销售人员');
INSERT INTO `acl_role` VALUES ('7', 'XSZG', '销售主管');

-- ----------------------------
-- Table structure for acl_role_priv
-- ----------------------------
DROP TABLE IF EXISTS `acl_role_priv`;
CREATE TABLE `acl_role_priv` (
  `role_id` int(10) NOT NULL,
  `priv_id` int(10) NOT NULL,
  PRIMARY KEY (`role_id`,`priv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of acl_role_priv
-- ----------------------------
INSERT INTO `acl_role_priv` VALUES ('1', '1');
INSERT INTO `acl_role_priv` VALUES ('1', '2');
INSERT INTO `acl_role_priv` VALUES ('1', '3');
INSERT INTO `acl_role_priv` VALUES ('1', '4');
INSERT INTO `acl_role_priv` VALUES ('1', '5');
INSERT INTO `acl_role_priv` VALUES ('1', '6');
INSERT INTO `acl_role_priv` VALUES ('1', '7');
INSERT INTO `acl_role_priv` VALUES ('2', '3');
INSERT INTO `acl_role_priv` VALUES ('2', '4');
INSERT INTO `acl_role_priv` VALUES ('2', '5');
INSERT INTO `acl_role_priv` VALUES ('2', '6');
INSERT INTO `acl_role_priv` VALUES ('2', '7');
INSERT INTO `acl_role_priv` VALUES ('7', '6');

-- ----------------------------
-- Table structure for acl_user
-- ----------------------------
DROP TABLE IF EXISTS `acl_user`;
CREATE TABLE `acl_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `pass` varchar(32) NOT NULL DEFAULT '',
  `isdel` int(2) NOT NULL DEFAULT '0',
  `passedit` smallint(1) NOT NULL DEFAULT '1',
  `realname` varchar(20) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '电子邮件',
  `department` varchar(30) NOT NULL DEFAULT '' COMMENT '所属部门',
  `lookup_information_nums` int(8) NOT NULL DEFAULT '0' COMMENT '可查看的用户信息数',
  `last_login` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `islock` tinyint(4) DEFAULT '0' COMMENT '0 可用 1 锁定禁用',
  PRIMARY KEY (`id`),
  KEY `name_pass` (`name`,`pass`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_user
-- ----------------------------
INSERT INTO `acl_user` VALUES ('1', 'sa', '56792d299e78503f9422d80903085a81', '0', '0', '管理员2', '88888888', 'xiaotibo@doweidu.com', '', '10', '1402287212', '0');



-- ----------------------------
-- Table structure for acl_user_ext
-- ----------------------------
DROP TABLE IF EXISTS `acl_user_ext`;
CREATE TABLE `acl_user_ext` (
  `ue_id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `realname` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `level` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户级别；1 职员 2 经理 3 总监',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '上级ID',
  `depid` int(11) DEFAULT '0' COMMENT '部门id',
  `cityid` int(11) DEFAULT '0' COMMENT '城市id',
  `isdel` tinyint(4) DEFAULT '0' COMMENT '0  1 已删除',
  PRIMARY KEY (`ue_id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_user_ext
-- ----------------------------
INSERT INTO `acl_user_ext` VALUES ('1', '1', '管理员2', '0', '0', '0', '0', '0');


-- ----------------------------
-- Table structure for acl_user_role
-- ----------------------------
DROP TABLE IF EXISTS `acl_user_role`;
CREATE TABLE `acl_user_role` (
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_user_role
-- ----------------------------
INSERT INTO `acl_user_role` VALUES ('1', '1');
INSERT INTO `acl_user_role` VALUES ('1', '2');
INSERT INTO `acl_user_role` VALUES ('1', '3');
INSERT INTO `acl_user_role` VALUES ('1', '4');
INSERT INTO `acl_user_role` VALUES ('1', '5');
INSERT INTO `acl_user_role` VALUES ('1', '6');


-- ----------------------------
-- Table structure for app
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `path` varchar(50) NOT NULL DEFAULT '',
  `pid` int(10) NOT NULL DEFAULT '0',
  `order` int(10) NOT NULL DEFAULT '0',
  `is_app` enum('YES','NO') NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app
-- ----------------------------
INSERT INTO `app` VALUES ('1', '系统管理', '', '0', '0', 'NO');
INSERT INTO `app` VALUES ('2', '权限管理', '', '1', '0', 'NO');
INSERT INTO `app` VALUES ('3', '角色管理', '/acl/rolelist', '2', '0', 'YES');
INSERT INTO `app` VALUES ('4', '用户管理', '/acl/userlist', '2', '0', 'YES');
INSERT INTO `app` VALUES ('5', '资源管理', '/acl/resourcelist', '2', '0', 'YES');
INSERT INTO `app` VALUES ('6', '菜单管理', '/acl/applist', '2', '0', 'YES');
INSERT INTO `app` VALUES ('7', '销售管理', '', '0', '1', 'NO');
INSERT INTO `app` VALUES ('8', '销售统计管理', '', '7', '2', 'NO');
INSERT INTO `app` VALUES ('13', '统计管理', '/SaleReport/index', '8', '20', 'YES');
INSERT INTO `app` VALUES ('19', '提成管理', '/SaleRule/index', '8', '40', 'YES');
INSERT INTO `app` VALUES ('22', '销售管理', '', '8', '30', 'YES');
INSERT INTO `app` VALUES ('34', '人员管理', '/SaleManage/user', '22', '0', '');
INSERT INTO `app` VALUES ('35', '门店管理', '/SaleManage/shop', '22', '0', '');
INSERT INTO `app` VALUES ('36', '我的报表', '/SaleReport/my', '8', '10', 'YES');
INSERT INTO `app` VALUES ('37', '常用功能', '', '1', '50', '');
INSERT INTO `app` VALUES ('38', '修改密码', '/common/personal', '37', '50', '');
INSERT INTO `app` VALUES ('39', '欢迎页面', '/common/index', '37', '10', '');

-- ----------------------------
-- Table structure for app_role
-- ----------------------------
DROP TABLE IF EXISTS `app_role`;
CREATE TABLE `app_role` (
  `app_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`app_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_role
-- ----------------------------
INSERT INTO `app_role` VALUES ('1', '1');
INSERT INTO `app_role` VALUES ('1', '2');
INSERT INTO `app_role` VALUES ('1', '3');
INSERT INTO `app_role` VALUES ('1', '4');
INSERT INTO `app_role` VALUES ('1', '5');
INSERT INTO `app_role` VALUES ('1', '6');
INSERT INTO `app_role` VALUES ('1', '7');
INSERT INTO `app_role` VALUES ('2', '1');
INSERT INTO `app_role` VALUES ('3', '1');
INSERT INTO `app_role` VALUES ('4', '1');
INSERT INTO `app_role` VALUES ('5', '1');
INSERT INTO `app_role` VALUES ('6', '1');
INSERT INTO `app_role` VALUES ('7', '1');
INSERT INTO `app_role` VALUES ('7', '2');
INSERT INTO `app_role` VALUES ('7', '4');
INSERT INTO `app_role` VALUES ('7', '6');
INSERT INTO `app_role` VALUES ('7', '7');
INSERT INTO `app_role` VALUES ('8', '1');
INSERT INTO `app_role` VALUES ('8', '2');
INSERT INTO `app_role` VALUES ('8', '4');
INSERT INTO `app_role` VALUES ('8', '6');
INSERT INTO `app_role` VALUES ('13', '1');
INSERT INTO `app_role` VALUES ('13', '4');
INSERT INTO `app_role` VALUES ('13', '7');
INSERT INTO `app_role` VALUES ('14', '1');
INSERT INTO `app_role` VALUES ('14', '2');
INSERT INTO `app_role` VALUES ('15', '1');
INSERT INTO `app_role` VALUES ('15', '2');
INSERT INTO `app_role` VALUES ('16', '1');
INSERT INTO `app_role` VALUES ('16', '2');
INSERT INTO `app_role` VALUES ('18', '1');
INSERT INTO `app_role` VALUES ('18', '2');
INSERT INTO `app_role` VALUES ('19', '1');
INSERT INTO `app_role` VALUES ('19', '4');
INSERT INTO `app_role` VALUES ('19', '7');
INSERT INTO `app_role` VALUES ('21', '1');
INSERT INTO `app_role` VALUES ('21', '2');
INSERT INTO `app_role` VALUES ('22', '1');
INSERT INTO `app_role` VALUES ('22', '4');
INSERT INTO `app_role` VALUES ('22', '7');
INSERT INTO `app_role` VALUES ('23', '2');
INSERT INTO `app_role` VALUES ('23', '4');
INSERT INTO `app_role` VALUES ('24', '2');
INSERT INTO `app_role` VALUES ('24', '4');
INSERT INTO `app_role` VALUES ('25', '2');
INSERT INTO `app_role` VALUES ('25', '4');
INSERT INTO `app_role` VALUES ('26', '2');
INSERT INTO `app_role` VALUES ('26', '4');
INSERT INTO `app_role` VALUES ('27', '2');
INSERT INTO `app_role` VALUES ('27', '4');
INSERT INTO `app_role` VALUES ('28', '2');
INSERT INTO `app_role` VALUES ('28', '4');
INSERT INTO `app_role` VALUES ('30', '2');
INSERT INTO `app_role` VALUES ('31', '1');
INSERT INTO `app_role` VALUES ('32', '1');
INSERT INTO `app_role` VALUES ('32', '2');
INSERT INTO `app_role` VALUES ('33', '1');
INSERT INTO `app_role` VALUES ('33', '2');
INSERT INTO `app_role` VALUES ('34', '1');
INSERT INTO `app_role` VALUES ('34', '4');
INSERT INTO `app_role` VALUES ('34', '7');
INSERT INTO `app_role` VALUES ('35', '1');
INSERT INTO `app_role` VALUES ('35', '4');
INSERT INTO `app_role` VALUES ('35', '7');
INSERT INTO `app_role` VALUES ('36', '1');
INSERT INTO `app_role` VALUES ('36', '2');
INSERT INTO `app_role` VALUES ('36', '6');
INSERT INTO `app_role` VALUES ('37', '1');
INSERT INTO `app_role` VALUES ('37', '2');
INSERT INTO `app_role` VALUES ('37', '3');
INSERT INTO `app_role` VALUES ('37', '4');
INSERT INTO `app_role` VALUES ('37', '5');
INSERT INTO `app_role` VALUES ('37', '6');
INSERT INTO `app_role` VALUES ('37', '7');
INSERT INTO `app_role` VALUES ('38', '1');
INSERT INTO `app_role` VALUES ('38', '2');
INSERT INTO `app_role` VALUES ('38', '3');
INSERT INTO `app_role` VALUES ('38', '4');
INSERT INTO `app_role` VALUES ('38', '5');
INSERT INTO `app_role` VALUES ('38', '6');
INSERT INTO `app_role` VALUES ('38', '7');
INSERT INTO `app_role` VALUES ('39', '1');
INSERT INTO `app_role` VALUES ('39', '2');
INSERT INTO `app_role` VALUES ('39', '3');
INSERT INTO `app_role` VALUES ('39', '4');
INSERT INTO `app_role` VALUES ('39', '5');
INSERT INTO `app_role` VALUES ('39', '6');
INSERT INTO `app_role` VALUES ('39', '7');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(80) NOT NULL DEFAULT '' COMMENT '部门名',
  `department_desc` varchar(500) NOT NULL DEFAULT '' COMMENT '部门描述',
  `department_createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `department_create_user` varchar(20) NOT NULL DEFAULT '0' COMMENT '创建人',
  `department_update_user` varchar(20) NOT NULL DEFAULT '0' COMMENT '更新人',
  `department_condition` tinyint(3) NOT NULL DEFAULT '0' COMMENT '有效状态 0 有效， 1 无效',
  `department_updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`department_id`),
  KEY `department_condition` (`department_condition`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('7', '销售部', '', '2012-04-18 17:49:21', 'sa', 'sa', '0', '2012-04-18 17:49:21');
INSERT INTO `department` VALUES ('8', '客服部', '', '2012-05-15 15:03:01', 'ken', 'ken', '0', '2012-05-15 15:03:01');
INSERT INTO `department` VALUES ('9', '财务部', '', '2012-05-15 15:03:01', 'ken', 'ken', '0', '2012-05-15 15:03:01');
INSERT INTO `department` VALUES ('10', '其他', '', '2012-05-15 15:03:01', 'ken', 'ken', '0', '2012-05-15 15:03:01');

-- ----------------------------
-- Table structure for log_login
-- ----------------------------
DROP TABLE IF EXISTS `log_login`;
CREATE TABLE `log_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_id` varchar(50) NOT NULL DEFAULT '',
  `is_failed` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log_login
-- ----------------------------
INSERT INTO `log_login` VALUES ('254', '2130706433', '1', 'le778fmpb2404tvgn1fq4vogi5', 'NO', '2014-06-09 12:18:10');
