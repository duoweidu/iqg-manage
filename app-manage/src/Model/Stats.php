<?php

/**
 * @package Model
 */
class Model_Stats
{

    /**
     * $_SERVER['argv'][1] = "2014-05-28"
     *
     * @return number
     */
    public static function getCurrTime()
    {
        $ret = time();
        $date = $_SERVER['argv'][1];
        if ($date === get_date($date, 'Y-m-d')) {
            $ret = strtotime($date);
        }
        return $ret;
    }

    public static function saveSummary($data)
    {
        $summ = st('KpiSummary');
        // $summ->debug();
        $data['ksm_utime'] = time();
        $where = "where ksm_uid=$data[ksm_uid] and ksm_year=$data[ksm_year] and ksm_month=$data[ksm_month]";
        
        $cnt = $summ->getCount($where);
        if ($cnt == 1) {
            $summ->update($data, $where);
        } else {
            $summ->insert($data);
        }
    }
    
    // 个人总销售额 mix $uid
    public static function getTotalMoney($uid, $year, $month, $sumby)
    {
        if (! $uid)
            return 0;
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("sum($sumby)")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month");
        
        if (is_array($uid)) {
            $sql->where("kb_holder_sellerid in ( " . implode(',', $uid) . ") ");
        } else {
            $sql->where("kb_holder_sellerid=$uid ");
        }
        
        return $sql->scalar();
    }
    
    // 个人 当月 总部销售总销售额汇总 mix $uid
    public static function getDetailDYXS($uid, $year, $month, $paywayfld)
    {
        if (! $uid)
            return 0;
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("kb_shop_pid, GROUP_CONCAT(kb_catid) as catids, sum(ks_goods_total) as gtotal,sum($paywayfld) as mtotal")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month");
        
        if (is_array($uid)) {
            $sql->where("kb_holder_sellerid in ( " . implode(',', $uid) . ") ");
        } else {
            $sql->where("kb_holder_sellerid=$uid ");
        }
        
        $sql->group("kb_shop_pid");
        $sql->order("mtotal desc");
        
        return $sql->fetchAll();
    }
    
    // 下属新店
    public static function getDetailXSXDTC($uid, $monthBegin, $monthEnd, $year, $month)
    {
        if (! $uid)
            return 0;
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("kb_holder_sellerid, kb_shop_pid,count(*) as shopnum,GROUP_CONCAT(kb_catid) as catids, min(kb_issuetime) as issuetime")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month");
        
        $sql->where("ks_onsell=1 and kb_issuetime>=$monthBegin and kb_issuetime<$monthEnd");
        
        $sql->where("kb_seller_pid=$uid ");
        
        $sql->group("kb_holder_sellerid,kb_shop_pid");
        
        return $sql->fetchAll();
    }
    
    // 下属销售提成
    public static function getDetailXSXSTC($uid, $monthBegin, $monthEnd, $year, $month, $paywayfld)
    {
        if (! $uid)
            return 0;
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("kb_holder_sellerid, kb_shop_pid, GROUP_CONCAT(kb_catid) as catids, sum(ks_goods_total) as gtotal,sum($paywayfld) as mtotal")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month");
        
        $sql->where("kb_seller_pid=$uid ");
        
        $sql->group("kb_holder_sellerid,kb_shop_pid");
        
        return $sql->fetchAll();
    }

    public static function getProfitMoney($uid, $year, $month, $rule)
    {
        $GRTC = get_arr_value($rule, 'JL.GRTC') / 100;
        
        $ks_money_total = self::getTotalMoney($uid, $year, $month);
        
        return floor($ks_money_total * $GRTC);
    }
    
    // 更加 shopid ，通过分类和规则，累加门店提成
    public static function getProfitShop($shopIdArr, $rule)
    {
        if (count($shopIdArr) == 0) {
            return 0;
        }
        
        $JL_MDFL = get_arr_value($rule, 'JL.MDFL');
        
        foreach ($shopIdArr as $row) {
            $tmp[] = $row['ks_shopid'];
        }
        $shopids = implode(',', $tmp);
        $shopArr = Model_KShop::getBaseByIDS($shopids);
        
        // 累加合计
        $money = array();
        foreach ($shopArr as $shop) {
            $money[] = $JL_MDFL[$shop['kb_catid']];
        }
        return array_sum($money);
    }
    
    // 新签门店 mix $uid
    public static function getShopNew($uid, $monthBegin, $monthEnd, $year, $month, $field = "ks_shopid")
    {
        if (! $uid)
            return array();
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select($field)
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("ks_onsell=1 and kb_issuetime>=$monthBegin and kb_issuetime<$monthEnd");
        // ->where("ks_onsell=1 and kb_issuetime<$monthEnd");
        if (is_array($uid)) {
            $sql->where("kb_holder_sellerid in ( " . implode(',', $uid) . ") ");
        } else {
            $sql->where("kb_holder_sellerid=$uid ");
        }
        $sql->order("kb_issuetime desc");
        return $sql->fetchAll();
    }
    
    // 新签门店 mix $uid
    public static function getCountShopNew($uid, $monthBegin, $monthEnd, $year, $month)
    {
        if (! $uid)
            return 0;
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("count(*)")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("ks_onsell=1 and kb_issuetime>=$monthBegin and kb_issuetime<$monthEnd");
        if (is_array($uid)) {
            $sql->where("kb_holder_sellerid in ( " . implode(',', $uid) . ") ");
        } else {
            $sql->where("kb_holder_sellerid=$uid ");
        }
        return $sql->scalar();
    }
    
    // 未满三个月下线 mix $uid
    public static function getShopLT3M($uid, $monthBegin, $monthEnd, $year, $month, $field = "ks_shopid")
    {
        if (! $uid)
            return array();
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select($field)
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("ks_onsell=0 and kb_offtime>=$monthBegin and kb_offtime<$monthEnd")
            ->where("ks_onsell_days<90");
        if (is_array($uid)) {
            $sql->where("kb_holder_sellerid in ( " . implode(',', $uid) . ") ");
        } else {
            $sql->where("kb_holder_sellerid=$uid ");
        }
        $sql->order("kb_offtime desc");
        return $sql->fetchAll();
    }
    
    // 当月下线
    public static function getShopDYXX($uid, $monthBegin, $monthEnd, $year, $month, $field = "ks_shopid")
    {
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select($field)
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("kb_holder_sellerid=$uid  and ks_onsell=0 ")
            ->where("kb_issuetime>=$monthBegin and kb_issuetime<$monthEnd")
            ->where("kb_offtime>=$monthBegin and kb_offtime<$monthEnd");
        return $sql->fetchAll();
    }
    
    // 当月下线 mix $uid
    public static function getCountShopDYXX($uid, $monthBegin, $monthEnd, $year, $month)
    {
        if (! $uid)
            return 0;
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("count(*)")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where(" ks_onsell=0 ")
            ->where("kb_issuetime>=$monthBegin and kb_issuetime<$monthEnd")
            ->where("kb_offtime>=$monthBegin and kb_offtime<$monthEnd");
        if (is_array($uid)) {
            $sql->where("kb_holder_sellerid in ( " . implode(',', $uid) . ") ");
        } else {
            $sql->where("kb_holder_sellerid=$uid ");
        }
        return $sql->scalar();
    }
    
    // 上线大于6个月
    public static function getShopGTXM($uid, $monthBegin, $monthEnd, $year, $month, $rule)
    {
        $xm = get_arr_value($rule, 'JL.CQHZ.YS');
        
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("ks_shopid")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("kb_holder_sellerid=$uid  and ks_onsell=1 ")
            ->where("ks_onsell_days>=" . ($xm * 30) . " and ks_onsell_days<" . (($xm + 1) * 30));
        // return $sql->scalar();
        return $sql->fetchAll();
    }
    
    // 在售门店总数
    public static function getShopONSELL($uid, $monthBegin, $monthEnd, $year, $month)
    {
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("count(*)")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("kb_holder_sellerid=$uid  and ks_onsell=1 ");
        return $sql->scalar();
    }
    
    // 在售门店总数
    public static function getShopONSELL2($uid, $monthBegin, $monthEnd, $year, $month)
    {
        $base = new KpiBaseinfo();
        $stat = new KpiStats();
        // $base->debug();
        $sql = $base->select("ks_shopid")
            ->joinLeft($stat->_name, "kb_shopid=ks_shopid ", null)
            ->where("ks_year=$year and ks_month=$month")
            ->where("kb_holder_sellerid=$uid  and ks_onsell=1 ");
        return $sql->fetchAll();
    }
    
    // 所属名下门店总数
    public static function getShopTotal($uid, $monthBegin, $monthEnd, $year, $month)
    {
        $base = new KpiBaseinfo();
        // $stat= new KpiStats();
        // $base->debug();
        $sql = $base->select("count(*)")->where("kb_holder_sellerid=$uid and kb_offtime=0 "); // kb_offtime=0 上线在售中
        return $sql->scalar();
    }
}