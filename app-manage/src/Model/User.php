<?php

/**
 * @package Model
 */
class Model_User {
	 
	public  static function getUserLevel(){
		$userLevel = array(
				'1'=>'业务员',
				'2'=>'经理',
		);
		return $userLevel;
	}
	
	public  static function getPayWay(){
		$arr = array(
				'1'=>'支付宝',
				'2'=>'余额',
		);
		return $arr;
	}
	
	public  static function getPayWay2(){
		$arr[] = array('id'=>1,'name'=>'支付宝');
		$arr[] = array('id'=>2,'name'=>'余额');
		return $arr;
	}
	
	
	public static function getUser($uid) {
		$mck=__CLASS__.__FUNCTION__.$uid.'1';
		$ret=mc_get($mck);
		if(!$ret){
			$ret = st ( 'AclUser' )->scalar('*' , "where id=$uid ");	
			$ret['last_login2'] = $ret['last_login'] ? get_date($ret['last_login']):'-';
			mc_set($mck, $ret, 600);
		}
		return $ret;
	}
	public static function getExt($uid) {
		$mck=__CLASS__.__FUNCTION__.$uid;
		$ret=mc_get($mck);
		if(!$ret){
			$ret = st ( 'AclUserExt' )->scalar('*' , "where uid=$uid ");
			$ret['cityid2']=$GLOBALS['CITY'][$ret['cityid']];
			$ret['level2']=$GLOBALS['USER_LEVEL'][$ret['level']];
			mc_set($mck, $ret, 600);
		}
		return $ret;		
	}
	
	/*
	public function update($data, $where) {
		return st ( 'App' )->update ( $data, $where );
	}
	public function delete($id) {
		return st ( 'App' )->delete ( "where id= $id " );
	}
	
	public function create($data) {
		return st ( 'App' )->insert ( $data );
	}*/
	
	public  static function formatUserList($userList){
		$userList = is_array($userList) ? $userList : array();
		foreach ($userList as &$user){
			$user['depid_name']=   self::getDepartment($user['depid'], "department_name");
			$user['cityid_name']= $GLOBALS['CITY'][ $user['cityid']];
			$user['level_name']= $GLOBALS['USER_LEVEL'][ $user['level']];
			$user['shop_num']=  self::getShopNum($user['id']);
			$user['pid_name']= self::getRealname($user[pid]);// st ( 'AclUser' )->scalar('realname',"where id= $user[pid]");
		}
		
		//var_dump($userList);
		return $userList;
	}
	
	
	public static function getRealname($uid){
		$mck=__CLASS__.__FUNCTION__.$uid;
		$ret=mc_get($mck);
		if(!$ret){
			$ret = st ( 'AclUser' )->scalar('realname',"where id= $uid ");
			mc_set($mck, $ret, 600);
		}
		return $ret;
		
	}
	
	
	public static function getParentID($uid){
		$mck=__CLASS__.__FUNCTION__.$uid;
		$ret=mc_get($mck);
		if(!$ret){
			$ret = st ( 'AclUserExt' )->scalar('pid',"where uid= $uid and isdel=0");
			mc_set($mck, $ret, 600);
		}
		return $ret;
	
	}
	
	public static function getLeaderList($depid=null , $exclude_uid="0"){
		$mck=__CLASS__.__FUNCTION__.$depid;
		$ret=mc_get($mck);
		if(!$ret){
			$tab = new AclUserExt (); 
			//$tab->debug();
			$where_dep = $depid ? " depid='$depid' and " : "";
			$ret = $tab->getAll ( 'uid,realname', "where {$where_dep} level in (2,3,4) and uid not in (1,{$exclude_uid}) and isdel=0" );
			unset ( $tab );
			mc_set($mck, $ret, 300);
		}
		return $ret;
		
	}
	
	
	public static function getSellers($cityid="",$level=""){
		$depid = self::getDepID('销售');
		$mck=__CLASS__.__FUNCTION__.$cityid.$level;
		$ret=mc_get($mck);
		if(!$ret){
			
			$tab = new AclUserExt ();
			//$tab->debug();
			$where_dep="";
			$where_dep .= $level ? " and level in ($level)  " : "";
			$where_dep .= $cityid ? " and cityid='$cityid' " : "";
			$ret = $tab->getAll ( '*', "where depid='$depid' and isdel=0  {$where_dep} order by  realname " );
			unset ( $tab );
			mc_set($mck, $ret, 300);
		}
		return $ret;
	
	}
	
	
	
	public static function checkUserExists($uid, $uname) {
		if (is_numeric ( $uid ) || strlen ( $uname )) {
			if (! $userIdHash || ! $userNameHash) {
				$userIdHash = array ();
				$userNameHash = array ();
				$aclUserDao = new MyFw_Model_AclUser (); // $this->dao->load('MyFw_AclUser');
				$userRes = $aclUserDao->getAllUsers ();
				foreach ( $userRes as $user ) {
					$userIdHash [$user ['id']] = 1;
					$userNameHash [$user ['name']] = 1;
				}
			}
			$idExists = array_key_exists ( $uid, $userIdHash );
			$nameExists = array_key_exists ( $uname, $userNameHash );
			return $idExists || $nameExists;
		}
		return false;
	}
	
	
	public static function getRoleID($name){
		$mck=__CLASS__.__FUNCTION__ .$name;
		$ret= mc_get($mck);
		if(!$ret){
			$ret= st('AclRole')->scalar("id","where name ='$name' ");
			mc_set($mck, $ret);
		}
		return $ret;
	}
	
	public static function getDepID($name){
		$mck=__CLASS__.__FUNCTION__ .$name; 
		$depid= mc_get($mck);
		if(!$depid){
			$depid= st('Department')->scalar("department_id","where department_name like '%$name%' and department_condition=0");
			mc_set($mck, $depid);
		}
		return $depid;
	}
	
	public static function getDepartment($id,$fld){
		$mck=__CLASS__.__FUNCTION__ ."$id,$fld";
		$ret= mc_get($mck);
		if(!$ret){
			$ret= st('Department')->scalar($fld,"where department_id=$id and department_condition=0");
			mc_set($mck, $ret);
		}
		return $ret;
	}
	
	
	public  static function getShopNum($uid){		
		$mck=__CLASS__.__FUNCTION__ .$uid.'2' ;
		$ret= mc_get($mck);
		if(!isset($ret)){		
			$ret= st('KpiSummary')->scalar("ksm_shop_total","where ksm_uid=$uid order by ksm_year desc,ksm_month desc");
			$ret = isset($ret) ? $ret : 0;
			mc_set($mck, $ret);
		}
		return $ret;
		
	}
	
	
	public static  function getRoleIds($roles = array()) {
		$role_ids = array ();
		foreach ( ( array ) $roles as $role ) {
			if ($role ['id'])
				$role_ids [] = $role ['id'];
		}
		return $role_ids;
	}
	
	public static function filterOldRoles($roles = array()) {
		$role_ids = array ();
		foreach ( ( array ) $roles as $role ) {
			if ($role ['id'] && $role ['readonly'])
				$role_ids [] = $role ['id'];
		}
		return $role_ids;
	}
	public static function buildRoles($roles = array()) {
		return is_array ( $roles ) ? implode ( ',', $roles ) : $roles;
	}
	public static function mergeRoles($old_roles, $new_roles) {
		if (! is_array ( $old_roles ))
			$old_roles = explode ( ',', $old_roles );
		if (! is_array ( $new_roles ))
			$new_roles = explode ( ',', $new_roles );
		$merged = array_merge ( $old_roles, $new_roles );
	
		return array_filter ( $merged ); // skip empty item
	}
	
	 
}