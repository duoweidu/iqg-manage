<?php

/**
 * @package Model
 */
class Model_Rule {
	
	
	public static function getLevelKey($level){		
		return 'user_level_'.$level;
	}
	
	
	public static function setValue($key,$value){
		$data['kr_key']= $key;
		$data['kr_value']= $value;
		$data['kr_utime']= time();
		$data['kr_uuid']= get_uid();
		
		$tab= st('KpiRule');
		$cnt=$tab->getCount("where kr_key='$key'");
		if($cnt==1){			
			$ret = $tab->update($data,"where kr_key='$key'");
		}else{
			$ret = $tab->insert($data);
		}
		
		//变动记录，保存版本
		$data2=array();
		foreach ($data as $fld=>$val){
			$fld= str_replace('kr_', 'krl_', $fld);
			$data2[ $fld] = $val;
		}
		st('KpiRuleLog')->insert($data2);
		return $ret;	
		
	}
	 
	 
	public static function getValue($key) {
		$kr_value= st ( 'KpiRule' )->scalar('kr_value',"where kr_key='$key'");
		return json_decode($kr_value,TRUE);
	}
	
	
	public static function getLevelValue($level) {
		$key=self::getLevelKey($level);
		$kr_value= st ( 'KpiRule' )->scalar('kr_value',"where kr_key='$key'");
		return json_decode($kr_value,TRUE);
	}
	
	public static function getLevelVersion($level) {
		$key=self::getLevelKey($level);
		return st ( 'KpiRule' )->scalar('kr_utime',"where kr_key='$key'"); 
	}
	
	 
	public function get($key) {
		return st ( 'KpiRule' )->getRow ( $key );
	}
	
	 
}