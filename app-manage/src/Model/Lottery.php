<?php

/**
 * @package Model
 */
class Model_Lottery
{

    static $TTL = 600;
    
    
    public static function change2Finish($pinid){
        $row = array();
        $row['lc_status'] = 3;
        $row['lc_utime'] = time();
        st('LtChecking')->update($row, "where lc_lpid='$pinid' and lc_status=2");
        
    }
    
    public static function change2Failure($pinid){
    	$row = array();
    	$row['lc_status'] = 4;
    	$row['lc_utime'] = time();
    	st('LtChecking')->update($row, "where lc_lpid='$pinid' and lc_status=2");
    
    }
    
    
    public static function addChecking($pinid,$pincode,$openid){
        
        $check = new LtChecking();
        
        //$pin= self::getPinInfo($pincode);       
        
        $data['lc_lpid'] =  $pinid;
        $data['lc_prize'] =  self::getPin($pinid,'lp_prize');        
        $data['lc_openid'] =  $openid;
        $data['lc_regist'] =  0;
        $data['lc_ctime'] =  time();
        $data['lc_utime'] =  time();
        $data['lc_status'] = 1;
        
        $check->insert($data);   
        
        $data=array();       
        $data['lp_check'] =  1;        
        st('LtPincode')->update($data,"where lp_id =".$pinid );
        
        self::clearPinInfo($pincode);
        
    }
    
    public static function checkRechargeLimit($mobile,$num){
        $ret = st('LtChecking')->getCount("where lc_mobile='$mobile' and lc_status=3 and lc_prize in (4,2)");
       
        return ($ret < $num);
    }
    
    
    public static function clearPinInfo($pincode)
    {
    	$mck = __CLASS__ . 'getPinInfo' . $pincode;
    	return  mc_unset($mck);
    }
    
    
    

    public static function getPinInfo($pincode)
    {
    	$mck = __CLASS__ . __FUNCTION__ . $pincode;
    	
    	//$ret = mc_get($mck);
    	//if (! $ret) {
    		//and lp_check=0
    		$ret = st('LtPincode')->scalar('*', "where lp_pin='$pincode' and lp_status=1 and lp_start<= CURRENT_DATE()  and lp_end>= CURRENT_DATE()");
    		//mc_set($mck, $ret, self::$TTL);
    	//}
    	return $ret;
    }
    
    
    public static function getPin($pinid,$field='*')
    {
    	return st('LtPincode')->scalar($field, "where lp_id='$pinid'  ");
    
    }
    
    public static function getChecking($pinid)
    {
    	 
    	return st('LtChecking')->scalar('*', "where lc_lpid='$pinid'  ");
    	
    }
    
    public static function getMobilePwd($pinid,$openid)
    {    
    	return st('LtChecking')->scalar('lc_mobile,lc_tmp', "where lc_lpid='$pinid' and lc_openid='$openid' ");
    	
    }
    
    public static function isUsed($pinid){
        //1 验证通过， 2 手机已登记，3 兑换领取
        return st('LtChecking')->getCount("where lc_lpid='$pinid' and lc_status>1");
        
    }
    
    public static function updateChecking($pinid,$data)
    {
    	return st('LtChecking')->update($data, "where lc_lpid='$pinid' ");
    }
    
    
    
    

    public static function importExcel($fileName, $force)
    {
        
       
        if (! is_file($fileName)) {
            return false;
        }
        
        $arrItems = Comm_Excel::readRows($fileName);
        
       
        // 去除头部并获取，po单名称
        $title_arr = array_shift($arrItems);
        
        if ($title_arr == "") {
            $this->alert('表头名称 为空!');
            exit();
        }
        
        // s($title_arr,$arrItems,1);
        
        $tab = new LtPincode();
        $tab->dbBeginTransaction();
        //$tab->debug();
        try {
            
            $data = array();
            // 上传时间
            $data['lp_ctime'] = time();
            $data['lp_utime'] = time();            
            $data['lp_uuid'] = $_SESSION['admin']['id'];
            
            for ($i = 0; $i < count($arrItems); $i ++) {
                $count = count($arrItems[$i]);
                for ($j = 0; $j < $count; $j ++) {
                    $arrItems[$i][$j] = trim($arrItems[$i][$j]);
                }
                
                $uniqcode = strtoupper($arrItems[$i][0]); // 抽奖码
                                                             
                // check品牌
                if ($arrItems[$i][1] == '') {
                    unlink($fileName);
                    $tab->dbRollBack();
                    Comm_Excel::alert("奖励等级 不能为空！ 编号：" . $uniqcode);
                    exit();
                }
                
                // 
                if ($arrItems[$i][2] == '') {
                    unlink( $fileName);
                    $tab->dbRollBack();
                    Comm_Excel::alert('奖品不能为空！编号:' . $uniqcode);
                    exit();
                }
                
                //
                if ($arrItems[$i][3] == '') {
                	unlink( $fileName);
                	$tab->dbRollBack();
                	Comm_Excel::alert('开始日期不能为空！编号:' . $uniqcode);
                	exit();
                }
                
                //  
                if ($arrItems[$i][4] == '') {
                	unlink( $fileName);
                	$tab->dbRollBack();
                	Comm_Excel::alert('结束日期不能为空！编号:' . $uniqcode);
                	exit();
                }
                
                // check厂商价格
                $count= st('LtPincode')->getCount("where lp_pin='$uniqcode' ");
                if (!$force && $count) {
                	unlink( $fileName);
                	$tab->dbRollBack();
                	Comm_Excel::alert('抽奖码不能为重复！编号:' . $uniqcode);
                	exit();
                }
                 
                 
                $data['lp_pin'] = $uniqcode;
                $data['lp_batch'] = substr( $uniqcode , 0 ,2);
                $data['lp_prize'] = intval( $arrItems[$i][1] );
                $data['lp_value'] =  $arrItems[$i][2] ;
                $data['lp_start'] =  $arrItems[$i][3] ;  
                $data['lp_end'] =  $arrItems[$i][4] ;
                $data['lp_memo'] =  $arrItems[$i][5] ;
                $data['lp_check'] =  0 ; // 0 未使用，1 已使用
                $data['lp_status'] =  1 ; // -1删除，1 有效
                
                if($count==0){                                        
                    $tab->insert($data);
                }else{
                   if($force){
                       $tab->update($data,"where lp_pin='$uniqcode'");
                   }
                }
            }
             
            $tab->dbCommit();
        } catch (Exception $e) {
            $tab->dbRollBack();
        }
    }


    public static function checkLimit($openid, $num)
    {
        // $mck = __CLASS__ . __FUNCTION__ . $openid;
        // $ret = mc_get($mck);
        // if (! $ret) {
        $ret = st('LtChecking')->getCount("where lc_openid='$openid' and lc_status>0");
        // mc_set($mck, $ret, self::$TTL);
        // }
        return ($ret < $num);
    }

    public static function genCode($num, $charNum)
    {
        $numCount = 6;
        
        $str = str_pad($num, $numCount, "0", STR_PAD_LEFT);
        
        for ($i = 1; $i <= $charNum; $i ++) {
            
            $char = self::randStr();
            $pos = rand(0, $numCount + 1);
            
            $str1 = substr($str, 0, $pos);
            $str2 = substr($str, $pos);
            
            $str = $str1 . $char . $str2;
        }
        
        return $str;
    }

    public static function randStr($i = 1)
    {
        // abcdefghijklmnopqrstuvwxyz0123456789
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $strlen = strlen($str) - 1;
        $finalStr = "";
        for ($j = 0; $j < $i; $j ++) {
            $finalStr .= substr($str, rand(0, $strlen), 1);
        }
        return $finalStr;
    }
}