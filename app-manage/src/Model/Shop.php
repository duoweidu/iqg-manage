<?php

/**
 * @package Model
 */
class Model_Shop {
	 
	public static function getCategories($cityid=null) {
		$mck=__CLASS__.__FUNCTION__.$cityid;
		$ret=mc_get($mck);
		if(!$ret){
			$ret= st ( 'MurcielagoCategory' )->getAll ( "*",  "where isHidden=0 ". ($cityid ? " and cityId='$cityid' ":"") ." order by sortOrder asc" );
			mc_set($mck, $ret, 600);
		}
		return $ret;
	}
	
	
	public static function getShop($shopid,$field="*") {
		$mck=__CLASS__.__FUNCTION__.$shopid.$field;
		$ret=mc_get($mck);
		if(!$ret){
			$ret= st ( 'MurcielagoShop' )->scalar ( $field,  "where isDeleted=0 and shopid = '$shopid' " );
			mc_set($mck, $ret, 600);
		}
		return $ret;
	}
	
	public static function getHQ($headquarterId,$field="*") {
		$mck=__CLASS__.__FUNCTION__.$headquarterId.$field;
		$ret=mc_get($mck);
		if(!$ret){
			$ret= st ( 'MurcielagoHeadquarter' )->scalar ( $field,  "where isDeleted=0 and headquarterId = '$headquarterId' " );
			mc_set($mck, $ret, 600);
		}
		return $ret;
	}
	
	
	public static function getHeadquarters($cityid=null) {
		//st ( 'MurcielagoHeadquarter' )->debug();
		$mck=__CLASS__.__FUNCTION__.$cityid;
		$ret=mc_get($mck);
		if(!$ret){
			$ret= st ( 'MurcielagoHeadquarter' )->getAll ( "*",  "where  isdeleted=0  order by headquarterName asc" );
			mc_set($mck, $ret, 600);
		}
		return $ret;
	}
	
	
 
	
	
	public static function setSeller($shopid,$sellerid){
		$data['kb_signer_sellerid']= $sellerid;
		$data['kb_holder_sellerid']= $sellerid;
		
		$data['kb_seller_pid']= Model_User::getParentID($sellerid);
		
		$data['kb_utime'] = time();
		$data['kb_uuid'] = get_uid();
		//st( 'KpiBaseinfo' )->debug();
		return st ( 'KpiBaseinfo' )->update($data ,"where kb_shopid= '$shopid' ");
	
	}
	
	
	 
	/**
	 * 门店商品种数(含即将上线的)
	 * @param unknown $shopid
	 * @return int
	 */
	public  static function getOnSaleGoodsNum($shopid){
		$mck=__CLASS__.__FUNCTION__ .$shopid ;
		$ret= mc_get($mck);
		if(!isset($ret)){
			$ret= st('KpiBaseinfo')->scalar("kb_goods_num","where kb_shopid=$shopid");
			$ret = isset($ret) ? $ret : 0;
			mc_set($mck, $ret);
		}
		return $ret;
	
	}
	  
	 
}