<?php

/**
 * @package Model
 */
class Model_MUser
{
    private static $CountryCode='+86';
    
    public static  function exists($mobile){
        $tabUser=new MurcielagoUser();
        return $tabUser->getCount( "where mobile='$mobile' and countryCallingCode='".self::$CountryCode."' ");
        
    }
    
    public static  function getUser($mobile){
    	$tabUser=new MurcielagoUser();    	 
    	return $tabUser->scalar('*', "where mobile='$mobile' and countryCallingCode='".self::$CountryCode."' ");
    }
    
    
    public static function add($mobile,$password)
    {
        
    	$tmp = array(
    			'countryCallingCode' => self::$CountryCode,
    	        'username' => $mobile,
    			'mobile' => $mobile,
    			'password' => \PEAR2\Crypt::hash($password),
    			'addIp' => Comm_Util::getClientIP(),
    			'addTime' => time(),
    	);
    	$r = st('MurcielagoUser')->insert($tmp);
    	
    	//$this->sendVcode($data, 'register');
    	 
    	return $r;
    }
    
    
    public static function isMobile($mobile)
    {
    	if(preg_match('/^\d+$/', $mobile) == 1) {
    		return true;
    	}
    	return false;
    } 
    
    
    
     
    
    /**
     * 发送验证码（注册、绑定手机、修改手机、找回密码）
     */
    public static function sendVcode($mobile, $vcode)
    {
    	$cnt = self::checkVcodeSent(self::$CountryCode, $mobile);
    	//每次发送不同的，每个都有效。
    	$logSmsDb = new MurcielagoLogSms();
    	 
    	$content ='注册初始密码：{vcode}。如非本人操作，请忽略本短信。';    	     
    	$content = str_replace('{vcode}', $vcode, $content);
    	 
    
    	$gateways = \PEAR2\Conf::get('sms', 'gateways',__ROOT_ETC__.'/');
    	$gateway = isset($gateways[self::$CountryCode]) ? $gateways[self::$CountryCode] : $gateways['default'];
    	$conf = \PEAR2\Conf::get('sms', $gateway,__ROOT_ETC__.'/');
    
    	$log = array(
    			'smsType' => 0, //self::$smsTypes[$type],
    			'countryCallingCode'  => self::$CountryCode,
    			'mobile'  => $mobile,
    			'ucode'   => $vcode,
    			'content' => $content,
    			'enable'  => 1,
    			'addIp'   => Comm_Util::getClientIP(),
    			'addTime' => time(),
    	);
    
    	$c = new \PEAR2\Services\Sms($gateway, $conf);
    	try {
    		$r = $c->send(self::$CountryCode . $mobile, $content);
    	} catch (\Exception $e) {
    		//$log['enable'] = 0; //如果发送失败，可以看后台来注册
    		//s($e);
    		$logSmsDb->insert($log);
    		throw new Exception('短信发送失败',-1);
    		//throw new \exception\Model(5000, );
    	}
    
    	$logSmsDb->insert($log);
    	return true;
    }
    
    
    /**
     * 检查验证码 是否已发过
     */
    private static function checkVcodeSent($CountryCode, $mobile)
    {
    	$ttl = \PEAR2\Conf::get('sms', 'ttl',__ROOT_ETC__.'/');
    	$where = array(
    			'e' => array(
    					'smsType' => 0,//self::$smsTypes[$type],
    					'countryCallingCode' => $CountryCode,
    					'mobile'  => $mobile,
    					'enable'  => 1,
    			),
    			'gt' => array(
    					'addTime' => time() - $ttl,
    			),
    	);
    	$logSmsDb = new  MurcielagoLogSms();
    	
    	$cnt = $logSmsDb->whereClause($where)->count();  
    	$oneUserMaxLimitInTtl = \PEAR2\Conf::get('sms', 'oneUserMaxLimitInTtl',__ROOT_ETC__.'/'); //每人每天最多发几条
    	if($cnt>=$oneUserMaxLimitInTtl) {
    		throw new Exception("抱歉，已超出每日发送限制",-1);
    		//throw new \exception\Model(4037);
    	}
    
    	$repeatInterval = \PEAR2\Conf::get('sms', 'repeatInterval',__ROOT_ETC__.'/'); //重发间隔，多少秒一次
    	$where['gt']['addTime'] = time() - $repeatInterval;
    	$r = $logSmsDb->whereClause($where)->count(); 
    	if($r>=1) {
    	    throw new Exception("抱歉，发送频率过快，请稍后再试",-1);
    		//throw new \exception\Model(4038, '', array('repeatInterval' => $repeatInterval));
    	}
    	return $cnt;
    }
    
     
}