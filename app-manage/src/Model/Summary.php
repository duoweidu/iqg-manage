<?php

/**
 * @package Model
 */
class Model_Summary {
	
	
	public static function getSummary($uid,$year,$month) {
		
		$where= "where ksm_uid=$uid and ksm_year=$year and ksm_month=$month";	
		return st('KpiSummary')->scalar("*",$where);
	}
	
	
	public static function getYearMonth($startYear="",$startMonth=""){	
		$startYear = $startYear ? $startMonth : 2014;
		$startMonth = $startMonth ? $startMonth : 1;
		$currYear= get_date(time(),'Y');
		$currMonth= get_date(time(),'m');
		
		$ym=array();
		for($y=$startYear; $y<=$currYear; $y++){
			
			$tmpStartMonth = ($y==$startYear ? $startMonth : 1);			
			$tmpEndMonth = ($y==$currYear ? $currMonth : 12);
			
			for($m=$tmpStartMonth; $m<=$tmpEndMonth; $m++){				
				$ym["$y-$m"]= "{$y}年{$m}月";
			}			
			
		}
		//s($ym,1);
		return array_reverse( $ym);
	}
	
	public static function getYearMonth2($startYear="",$startMonth=""){
		$startYear = $startYear ? $startMonth : 2014;
		$startMonth = $startMonth ? $startMonth : 1;
		$currYear= get_date(time(),'Y');
		$currMonth= get_date(time(),'m');
	
		$ym=array();
		for($y=$startYear; $y<=$currYear; $y++){
				
			$tmpStartMonth = ($y==$startYear ? $startMonth : 1);
			$tmpEndMonth = ($y==$currYear ? $currMonth : 12);
				
			for($m=$tmpStartMonth; $m<=$tmpEndMonth; $m++){
				$ym[]=array(
					'id' => "$y-$m",
					'name' =>  "{$y}年{$m}月"
				);
			}
				
		}
		//s($ym,1);
		return array_reverse( $ym);
	}
	
	 
	 
}