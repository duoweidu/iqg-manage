<?php

/**
 * @package Model
 */
class Model_Recharge
{
    public static  function recharge($userId,$cardNumber){
        $tabUser=new MurcielagoUser();
        $tabCard=new MurcielagoCards();
        $tabMoney = new MurcielagoMoneychange();
        
        
        
        $column = array(
        		'money',
        		'status',
        		'categoryId',
        );
        $sql = $tabCard->select($column);
        $sql->where("cardNumber=?",$cardNumber);
        $sql->where("isEnabled=1");
        $card = $sql->scalar();
        
        if(empty($card)) {
        	return false;
        }
        if($card['status'] == 2) {
        	return false;
        }
        
        $ret= self::checkCardCategory($userId, $card['categoryId']);
        if(!$ret){
            return false;
        }
        // 充值
        $sql = 'UPDATE `murcielago_user` SET money=money+' . $card['money'] . ' WHERE uid=' . $userId . ' LIMIT 1';
        $tabUser->execute($sql);
        
        // 作废卡
        $data = array( 'status' => 2,  );
        $tabCard->update($data, "where cardNumber='$cardNumber' ");
        
        //记账单   
        $user = $tabUser->scalar('*', "where uid='$userId'"); 
        $data = array(
        		'uid' => $userId,
        		'money' => $card['money'],
        		'remark' => '充值',
        		'addTime' => time(),
        		'balance' => $user['money'],
        		'cardNumber' => $cardNumber,
        );        
        $tabMoney->insert($data);
        
        return  $card['money'];
        
    }
    
    public static function checkCardCategory($userId, $categoryId)
    {
        $tabCC  = new MurcielagoCardsCategory();
        $tabMoney = new MurcielagoMoneychange();
        $tabCard=new MurcielagoCards();
        
        $tmp=$tabCC->getRow($categoryId);
        // 判断是否过期
        $expiredTime = $tmp['expiredTime'];
        if ( $expiredTime != 0 && $expiredTime <= time() ) {
        	return false;
        }
        
        //这批卡一个人能冲几张
        $paymentNumbers = $tmp['paymentNumbers'];
        if($paymentNumbers > 0) {
        	
        	$rs = $tabMoney->query("cardNumber", "where uid=$userId and cardNumber is not null");
        	$usedCards = $tabMoney->fetchAll($rs);
        	//如果以前冲过卡，则检测是不是同一批
        	if(!empty($usedCards)) {
        		foreach($usedCards as $one) {
        			$cardsNumbers[] = $one['cardNumber'];
        		}
        		
        		$sql=$tabCard->select('');
        		$sql->where("cardNumber in (?)",$cardsNumbers);
        		$sql->where("categoryId=?",$categoryId);        		 
        		$cnt = $sql->count();
        		if($cnt >= $paymentNumbers) {
        			return  false;
        		}
        	}
        }
        
        return true;
        
    }
    
     
    
    
     
}