<?php

 

/**
 *
 * @package
 *
 *
 */
class SaleReportPage extends MyFw_App_Backend_Page {
	//protected $check_role = 'XSZG';  //默认访问角色
	
	//protected $check_res='';
	
	public function _init() {
		$action=$this->getActionName();		
		switch ($action){
			case 'my':
			case 'detailTable':
			case 'detailProfit':				
				$this->check_res='__DEFAULT_ALLOW__'; //该资源部存在，允许
				break;
		}		
	}
	 
	public function indexAction() {
		extract($_GET);
		$payway = flt_int($payway);		
		$paywayArr=array(1=>'_alipay' , 2=>'_balance');		
		$paywayfld= $paywayArr[$payway];
		
		$paywayfld = $paywayfld ? $paywayfld : ''; 
		
		
		// ======TPL 模板 数据==================================
		$hash ["yearMonthOpts"] = Model_Summary::getYearMonth();
		$hash ["userLevelOpts"] = Model_User::getUserLevel();
		$hash ["paywayOpts"] = Model_User::getPayWay();
		$hash ["cityidOpts"] = $GLOBALS['CITY'];
		
		// ======显示 字段==================================
		$fields ['ksm_uid'] = array (
				"title" => "编号" 
		);
		$fields ['username'] = array (
				"title" => "姓名" ,'nosort'=>1 ,'fake'=>1
		);
		$fields ['cityid'] = array (
				"title" => "地区"  ,'fake'=>1
		);
		$fields ['level'] = array (
				"title" => "职位"  ,'fake'=>1
		); 
		$fields ['ksm_shop_new'] = array (
				"title" => "新签门店数" 
		);		
		$fields ['ksm_shop_lt3m'] = array (
				"title" => "未满3月下线" 
		);
		$fields ['ksm_shop_dyxx'] = array (
				"title" => "当月下线"
		);		
		$fields ['ksm_shop_gtxm'] = array (
				"title" => "上线大于n月"
		);
		$fields ['ksm_profit_shop'] = array (
				"title" => "门店提成"
		);
		$fields ['ksm_total_money'.$paywayfld] = array (
				"title" => "月销售额"
		);
		$fields ['ksm_profit_money'.$paywayfld] = array (
				"title" => "销售提成"
		);
		$fields ['ksm_profit_subshop'] = array (
				"title" => "下属新店提成"
		);
		$fields ['ksm_profit_submoney'.$paywayfld] = array (
				"title" => "下属销售提成"
		);
		$fields ['ksm_profit_total'] = array (
				"title" => "合计" 
		); 
		// ======过滤 字段==================================
			
		$yearmonth = $yearmonth ? $yearmonth : get_date(time(),'Y-m');
		$ym= explode('-', $yearmonth) ;
		$year = $ym[0];
		$month = $ym[1];
		
		$cityid= flt_int($cityid);
		$userlevel= flt_int($userlevel);
		 
		
		// =======初始化 条件============
		$table= new KpiSummary();		
		$base= new KpiBaseinfo();
		//$table->debug();
		
		//查询字段
		$columns = prepare_select_fields ( $fields );				
		$sql= $table->select($columns);	
		//关联表
		//$sql->joinLeft($base->_name, "ksm_uid=kb_holder_sellerid");			
		//排序
		$orderby = array_key_exists ( $orderby, $fields ) ? $orderby : "ksm_profit_total";
		$direction = $direction == 'asc' ? 'asc' : 'desc';		
		$sql->order("$orderby $direction");		
		
		$_REQUEST["orderby"] = $orderby;
		$_REQUEST["direction"] = $direction;
		//查询条件		
		$sql->where("ksm_year=$year and ksm_month=$month");
		$sql->where("ksm_islock=0");  //未离职的
		//if($payway){ $sql->where(" kb_cityid='$cityid' ");	}
		
		if($cityid || $userlevel){
			$ext= new AclUserExt();
			$sql2= $ext->select(array('uid'));
			$sql2->where("isdel=0");			
			if($cityid){ $sql2->where(" cityid='$cityid' ");	}
			if($userlevel){ $sql2->where(" level='$userlevel' ");	}
			$uids = $sql2->fetchAll();
			$uidArr = Comm_Util::getColFromArr($uids, 'uid');	
			$uidStr = implode(',', $uidArr)	;	
			
		}
		
		if($uidStr) { $sql->where("ksm_uid in ( $uidStr )"); }
		
		
		
		// =====获取 总数=============================
		$total_rows = $sql->count();	//总数
		$page_size = 9999;
		$total_page = ceil ( $total_rows / $page_size );
		$currpage = is_numeric ( $currpage ) && $currpage > 1 ? $currpage : 1;
		$currpage = ($currpage > $total_page && $total_page > 0) ? $total_page : $currpage;
		
		
		// ======获取 数据==================================	
		//分页
		$sql->page($currpage,$page_size);		
		$rs = $sql->fetchAll();
		
		foreach ($rs as $row){
		
			$ext= Model_User::getExt($row['ksm_uid']);
			
			$row ['username'] = html_link($ext['realname'], 'detail?uid='.$row['ksm_uid']);// "<span id='seller_{$row[kb_shopid]}'>".$ext['realname'] ."</span>";
			$row ['cityid'] = $GLOBALS['CITY'][$ext['cityid']];			
			$row ['level'] = $hash ["userLevelOpts"][$ext['level']];		 
			
			$data [] = $row;
		}
		 
		
		// ======模板 显示=========== =======================
		
		$hash ["pk_field"] = $table->_primarykey;		
		$hash ["total_rows"] = $total_rows;
		$hash ["total_page"] = $total_page;
		$hash ["fields"] = $fields;
		$hash ["rows"] = $data;
		
		
		$this->display ( $hash, 'SaleReport/list.tpl' );
	} 
	
	 
	public function myAction() {
		$this->showDetail(get_uid());
	}
	
	public function detailAction() {
		
		$this->showDetail(req_int('uid'));
	}
	
	private function showDetail($uid="") {	
		
		$uid = $uid ? $uid : get_uid();
		
		$yearmonth = req_str('yearmonth');
		$payway = req_str('payway');
		
		$yearmonth = $yearmonth ? $yearmonth : get_date(time(),'Y-m');
		$ym= explode('-', $yearmonth) ;
		$year = $ym[0];
		$month = $ym[1];
	
		$paywayArr=array(1=>'_alipay' , 2=>'_balance');
		$paywayfld= $paywayArr[$payway];
		$paywayfld = $paywayfld ? $paywayfld : '';
		$paywayfld = 'ksm_profit_money'.$paywayfld;
	
		
	
		$hash['ext']=Model_User::getExt($uid);
		$hash['user']=Model_User::getUser($uid);
		
		$hash['version']=Model_Rule::getLevelVersion($hash['ext']['level']);
		
		
		$summ=Model_Summary::getSummary($uid,$year,$month);	
		//$summ['my_money'] = $summ['ksm_profit_money'.$paywayfld] + $summ['ksm_profit_submoney_'.$paywayfld];
		//$summ['my_shop'] = $summ['ksm_profit_shop'] + $summ['ksm_profit_subshop'];
			
		//个人**销售提成 + 个人门店提成   + 团队**销售提成 + 团队门店提成
		//$summ['my_total'] =$summ['my_money'] + $summ['my_shop'];	 
		
		$hash['summ'] =$summ;
	
		$this->display( $hash, 'SaleReport/detail.tpl'  );
	
	}
	
	
	public function detailProfitAction() {
			
	
		$uid= req_int('uid');
		$yearmonth = req_str('yearmonth');
		$yearmonth = $yearmonth ? $yearmonth : get_date(time(),'Y-m');
		$ym= explode('-', $yearmonth) ;
		$year = $ym[0];
		$month = $ym[1];
		
		
		$payway= req_int('payway');
		$paywayArr=array(1=>'_alipay' , 2=>'_balance');		
		$paywayfld = isset($paywayArr[$payway]) ? $paywayArr[$payway] : '';
	
		$summ=Model_Summary::getSummary($uid,$year,$month);
		$summ['my_money'] = $summ['ksm_profit_money'.$paywayfld] + $summ['ksm_profit_submoney'.$paywayfld];
		$summ['my_shop'] = $summ['ksm_profit_shop'] + $summ['ksm_profit_subshop'];
	
		//个人**销售提成 + 个人门店提成   + 团队**销售提成 + 团队门店提成
		$summ['my_total'] =$summ['my_money'] + $summ['my_shop'];
		
		$summ['ksm_utime'] =  $summ['ksm_utime'] ? get_date( $summ['ksm_utime'] ) : '-';
		
		
		$this->json($summ);
	
	}
	
	
	public function detailTableAction() {
		
		$uid= req_int('uid');
		$yearmonth = req_str('yearmonth');
		$yearmonth = $yearmonth ? $yearmonth : get_date(time(),'Y-m');
		$ym= explode('-', $yearmonth) ;
		$year = $ym[0];
		$month = $ym[1];
		
		
		$monthBegin = strtotime("$year-$month-01 00:00:00");
		$monthEnd = strtotime('+ 1 month',$monthBegin);  // 自然月		
		
		
		$payway= req_int('payway');
		$paywayArr=array(1=>'alipay' , 2=>'balance');
		$paywayfld= $paywayArr[$payway];
		$paywayfld = $paywayfld ? 'ks_money_'.$paywayfld : 'ks_money_total';
		 
		
		$data=$_GET;
		
		$ext =  Model_User::getExt($uid);		
		$catArr= Model_Shop::getCategories();
		$catArr = Comm_Util::getOptFromArr($catArr,'catId','catName');
		//规则
		$rule= Model_Rule::getLevelValue($ext['level']);	
		$GRTC= get_arr_value($rule,'JL.GRTC')/100;
		$TDTC= get_arr_value($rule,'JL.TDTC')/100;
		
		$JL_MDFL= get_arr_value($rule,'JL.MDFL');
		
		
		$userlevel =  $ext['level'];
		$summ= Model_Summary::getSummary($uid, $year, $month);
		
		$achieve  =  $userlevel==1 ? $summ['ksm_achieve'] : $summ['ksm_achieve_sub'];
		
		
		$title =array() ;
		
		$type= req_str("type");
		switch ($type){			
			case "dymd":	//当月门店
				$data=Model_Stats::getShopNew($uid, $monthBegin, $monthEnd, $year, $month,"*");				
				$data2=Model_Stats::getShopDYXX($uid, $monthBegin, $monthEnd, $year, $month,"*");
				$data=@array_merge($data,$data2);
				foreach ($data as &$row){
					$kb_offtime = $row['kb_offtime'];
					$row['kb_issuetime']= get_date($row['kb_issuetime'],"Y.m.d");
					$row['kb_offtime']= $kb_offtime ? get_date($kb_offtime,"Y.m.d") : "-";
					$row['shopname']= Model_Shop::getShop($row['kb_shopid'],'shopName');
					$row['catname']= $catArr[$row['kb_catid']];
					if($achieve){
						$row['profit']= $kb_offtime ? 0 : $JL_MDFL[$row['kb_catid']] ;//floor( $row[$paywayfld] * $GRTC );
					}else{
						$row['profit']= 0;
					}
				}
				
				$title['kb_shop_pid']= '编号';
				$title['shopname']= '分店名称';
				$title['catname']= '商户类型';
				$title['kb_issuetime']= '上线时间';
				$title['kb_offtime']= '下线时间';
				$title['profit']=  '奖金';
				break;
			case "dyxs":	//当月销售 总部汇总
				$data=Model_Stats::getDetailDYXS($uid, $year, $month, $paywayfld);	
				
				foreach ($data as &$row){
					//s($row);					
					$row['hqname']= Model_Shop::getHQ($row['kb_shop_pid'],'headquarterName');
					//分类处理
					$catName=array();
					$catids = explode(',', $row['catids']);
					foreach ($catids as $catid){
						$catName[$catid]=$catArr[$catid];
					}
					$row['catname']= implode(',', $catName);
					if($achieve){
						$row['profit']= round( $row['mtotal'] * $GRTC ,2 );
					}else{
						$row['profit']=0;
					}
				}
				
				
				$title['kb_shop_pid']= '编号';
				$title['hqname']= '总部名称';
				$title['catname']= '商户类型';
				$title['gtotal']= '销量';
				$title['mtotal']= '销售额';
				$title['profit']=  '奖金';
				break;
			case "xsxdtc":	//当月下属新店提成
				$data=Model_Stats::getDetailXSXDTC($uid, $monthBegin, $monthEnd, $year, $month);
			
				foreach ($data as &$row){
					//s($row);					
					$row['username']= Model_User::getRealname($row['kb_holder_sellerid']);
					$row['hqname']= Model_Shop::getHQ($row['kb_shop_pid'],'headquarterName');
					
					//分类处理
					$catName=array();
					$catids = explode(',', $row['catids']);
					foreach ($catids as $catid){
						$catName[$catid]=$catArr[$catid];
					}
					$row['catname']= implode(',', $catName);
					$row['issuetime']= get_date($row['issuetime'],"Y.m.d");
				}
				
				$title['kb_shop_pid']= '编号';
				$title['username']= '下属';
				$title['hqname']= '总部名称';
				$title['shopname']= '分店名称';
				$title['catname']= '商户类型';				
				$title['issuetime']= '上线时间';
				
				break;
			case "xsxstc":	//当月下属销售提成
				$data=Model_Stats::getDetailXSXSTC($uid, $monthBegin, $monthEnd, $year, $month, $paywayfld);
					
				foreach ($data as &$row){
					//s($row);
					$row['username']= Model_User::getRealname($row['kb_holder_sellerid']);
					$row['hqname']= Model_Shop::getHQ($row['kb_shop_pid'],'headquarterName');
						
					//分类处理
					$catName=array();
					$catids = explode(',', $row['catids']);
					foreach ($catids as $catid){
						$catName[$catid]=$catArr[$catid];
					}
					$row['catname']= implode(',', $catName);
					$row['profit']= floor( $row['mtotal'] * $TDTC );
				}
				
				$title['kb_shop_pid']= '编号';
				$title['username']= '下属';
				$title['hqname']= '总部名称';				
				$title['catname']= '商户类型';
				$title['gtotal']= '销量';
				$title['mtotal']= '销售额';
				$title['profit']=  '奖金';
				break;
			case "lt3m":	//未满3个月
				$data=Model_Stats::getShopLT3M($uid, $monthBegin, $monthEnd, $year, $month,"*");				
				foreach ($data as &$row){
					$row['kb_issuetime']= get_date($row['kb_issuetime'],"Y.m.d");
					$row['kb_offtime']= $row['kb_offtime'] ? get_date($row['kb_offtime'],"Y.m.d") : "-";//get_date($row['kb_offtime'],"Y.m.d");
					$row['shopname']= Model_Shop::getShop($row['kb_shopid'],'shopName');
					$row['catname']= $catArr[$row['kb_catid']];
					//$row['profit']= floor( $row[$paywayfld] * $GRTC );
				}
				$title['kb_shop_pid']= '编号';
				$title['shopname']= '分店名称';
				$title['catname']= '商户类型';
				$title['kb_issuetime']= '上线时间';
				$title['kb_offtime']= '下线时间';
				//$title['profit']=  '奖金';
				
				break;
			
		}
		
		//$data = 
	
		$act = req_str('act');
		
		switch ($act){
			case "export":
				
				$filename= "Detail-$uid-$type-R".rand(100, 999) ;				
				$content_list =$data;				
				 
				Comm_Excel::downloadFile($filename,$title,$content_list);
				break;
			default:
				$this->json($data,600);
				break;
		}
		
		
	
	}
	
	  
	
}

?>
