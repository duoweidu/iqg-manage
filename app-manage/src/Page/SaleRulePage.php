<?php

 

/**
 *
 * @package
 *
 *
 */
class SaleRulePage extends MyFw_App_Backend_Page {
    
	//protected $check_role = 'XSZG,YYRY';
	
	//protected $check_res=__CLASS__;
	/*
	public function _init(){		
		$this->checkResourse("xs_manage");
	}*/
	
	public function indexAction() { 
		
		$cats= Model_Shop::getCategories();	//$cityid
		$hash['catidOpts']= get_opts($cats, 'catId', 'catName') ;
		
		$hash['L1']=Model_Rule::getValue (Model_Rule::getLevelKey(1));
		$hash['L2']=Model_Rule::getValue (Model_Rule::getLevelKey(2));
		
		
		$hash['V1']=Model_Rule::getLevelVersion(1);
		$hash['V2']=Model_Rule::getLevelVersion(2);
		 
		
		//s($hash['L1']);
		 
		
		$this->display ( $hash, 'SaleRule/list.tpl' );
	} 
	
	
	public function editAction() {
		 
		 
		$this->view->userLevel = Model_User::getUserLevel();
		
		
		$cats= Model_Shop::getCategories();	//$cityid
		$hash['catidOpts']= get_opts($cats, 'catId', 'catName') ;
		
		$level = flt_int($_GET['level']);
		$hash['level']= $level;
		
		
		// do post
		if ($_POST) {
			 
			//$level = $_POST['level'];
			$rule = $_POST['rule'];		
			//print_r($rule);
			
			if(! $this->checkNumeric($rule)){
				$this->addError('common.formaterror', '');
			}
			
			//print_r($rule);
				
			
			if ($this->noError ()) {
				
				Model_Rule::setValue(Model_Rule::getLevelKey($level), json_encode($rule));
				//$this->alert('修改成功', 'index' );
				//return;
				$this->forward ( 'index' );
			}
		}
		
		
		$hash['rule']=Model_Rule::getValue (Model_Rule::getLevelKey($level));
		
		$hash['version']=Model_Rule::getLevelVersion($level);
		$hash['verdate']=get_date($hash['version']);
		//s($hash['rule']);
		 
		$this->display ( $hash, 'SaleRule/edit.tpl'  );
		
	}
	
	
	public  function  changeSellerAction(){
		
		$shopid= flt_int($_POST['shopid']);
		$sellerid= flt_int($_POST['sellerid']);
		
		$ret = Model_Shop::setSeller($shopid, $sellerid);
		
		$ret =  $ret ? 'succ' :'fail';
		exit($ret);
	}
	
	
	private function checkNumeric(&$arr){
		
		$ret =true;
		foreach ($arr as  &$val){		
			//var_dump($val)	;
			if(is_array($val)){				
				$ret = $this->checkNumeric($val);
			}elseif (!is_numeric(trim($val))){
				$ret = false; 
			}else{
				$val=trim($val);
			}
			
			if(!$ret)  break;
		}
		
		return $ret;
		
	}
	
	
}

?>
