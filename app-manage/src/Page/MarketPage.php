<?php

/**
  
 * Created on 
 *  
 * @package     
 * @author      
 * @copyright  
 * @version    $Id$
 */
class MarketPage extends MyFw_App_Backend_Page
{

    public function _init()
    {
        ini_set("max_execution_time","0");
        ini_set("max_input_time","0");
        set_time_limit(0);
        
        
        $this->depid = Model_User::getDepID('销售');
    }
    
    
    public function deleteAction()
    {
        $id=req_int('id',0);
        $table = new LtPincode();
        $table->delete("where lp_id='$id'");
        //$this->forward('list');
        $this->alert('删除成功','list');
    }
    
    
    public function listAction()
    {
        $hash = array();
        extract($_REQUEST);
        
        $lc_status = array(1 =>'已验证通过', 2=>'已登记手机' , 3=>'已兑换领取' );
        
        // ======显示 字段==================================
        $fields['lp_id'] = array(
        		"title" => "编号"
        );
        $fields['lp_batch'] = array(
        		"title" => "批次"
        );
        $fields['lp_pin'] = array(
        		"title" => "抽奖码"
        );
        $fields['lp_prize'] = array(
        		"title" => "兑奖等级"
        );
        $fields['lp_start'] = array(
        		"title" => "开始日期"
        );
        $fields['lp_end'] = array(
        		"title" => "结束日期"
        );
        /*
        $fields['lp_memo'] = array(
        		"title" => "备注"
        );*/
        $fields['lp_check'] = array(
        		"title" => "已使用"
        );
        $fields['lp_check'] = array(
        		"title" => "验证"
        );
        $fields['lc_mobile'] = array(
        		"title" => "领取人"
        );
        $fields['lc_status'] = array(
        		"title" => "状态"
        );        
        $fields['lc_utime'] = array(
        		"title" => "时间"
        ); 
        // ======过滤 字段==================================
        $pincode = req_str('pincode');
        $mobile = req_int('mobile','');
        
        $hash["pincode"] = $pincode;
        $hash["mobile"] = $mobile;
        
        // =======初始化 条件============
        
        $table = new LtPincode();
        $check = new LtChecking();
        //$table->debug();
        
     //查询字段
		$columns = prepare_select_fields ( $fields );				
		$sql= $table->select($columns);	
		//关联表
		$sql->joinLeft($check->_name, "lp_id=lc_lpid");			
		//排序
		$orderby = array_key_exists ( $orderby, $fields ) ? $orderby : "lp_id";
		$direction = $direction == 'asc' ? 'asc' : 'desc';		
		$sql->order("$orderby $direction");		
		
		$_REQUEST["orderby"] = $orderby;
		$_REQUEST["direction"] = $direction;
		//查询条件	
		$sql->where("lp_status=1");  //未离职的
		if($mobile){ $sql->where(" lc_mobile='$mobile' ");	}
		if($pincode){ $sql->where(" lp_pin='$pincode' ");	}
		
    // =====获取 总数=============================
		$total_rows = $sql->count();	//总数
		$page_size = 20;
		$total_page = ceil ( $total_rows / $page_size );
		$currpage = is_numeric ( $currpage ) && $currpage > 1 ? $currpage : 1;
		$currpage = ($currpage > $total_page && $total_page > 0) ? $total_page : $currpage;
		
		
		// ======获取 数据==================================	
		//分页
		$sql->page($currpage,$page_size);		
		$rs = $sql->fetchAll();
		
		foreach ($rs as $row){
		    $row ['lp_prize'] =  $row ['lp_prize'] .'等奖';
			$row ['lp_check'] =  $row ['lp_check']  ? '已验证':'未验证';	 
			
			$row ['lc_utime'] =  $row ['lc_utime'] ? get_date($row ['lc_utime'] ) :"-";
			$row ['lc_status'] = $lc_status[ $row ['lc_status'] ];
			$data [] = $row;
		}
		 
        // ======模板 显示=========== =======================
        
        $hash["pk_field"] = $table->_primarykey;
        
        $hash["total_rows"] = $total_rows;
        $hash["total_page"] = $total_page;
        $hash["fields"] = $fields;
        $hash["rows"] = $data;
         
        
        $this->display($hash, 'Market/list.tpl');
        
    }
    

    public function gencodeAction()
    {        
        //s(\PEAR2\Crypt::hash('asdqwe'),1);
        $hash = array();
        
        $start = req_str('start',time());
        $end = req_str('end',time()+86400*30);
        $start = get_date($start,'Y-m-d');
        $end = get_date($end,'Y-m-d');
        
        $hash['start'] = $start;
        $hash['end'] = $end;
        
        
        if ($_POST) {
            
            $prefix = strtoupper(req_str('prefix'));
            $number = req_int('number');
            
            $startNum = req_int('startNum', 1);
            $endNum = $startNum + $number;
            
            $hash['prefix'] = $prefix;
            $hash['number'] = $number;
            $hash['startNum'] = $startNum;
            
            if (! Zend_Validate::is($this->param('prefix'), 'NotEmpty')) {
                $this->addError('common.notempty', '请输入抽奖码前缀');
            }
            
            if (! strlen($prefix) == 2) {
                $this->addError('请输入两个字母');
            }
            
            $data = array();
            for ($i = $startNum; $i < $endNum; $i ++) {
                $row = array();
                $row['lp_pin'] = $prefix . Model_Lottery::genCode($i, 2);
                $row['lp_start'] = $start;
                $row['lp_end'] = $end;
                $data[] = $row;
            }
            
            $filename = "CODE-$prefix-" . get_date(time(), 'Ymd');
            $title = array();
            $title['lp_pin'] = '抽奖码';
            $title['lp_prize'] = '获奖等级';
            $title['lp_value'] = '兑换奖品';
            $title['lp_start'] = '开始日期';
            $title['lp_end'] = '结束日期';
            $title['lp_memo'] = '备注';
            $content_list = $data;
            Comm_Excel::downloadFile($filename, $title, $content_list);
        }
        
        $this->display($hash, 'Market/gencode.tpl');
    }
    
    
    public function uploadcodeAction(){
        $hash=array();
        if($_POST){
        	 
        	$path=__UPLOAD_DIR.'/code/';
        	Comm_Excel::createDir($path);
        	$name = "code_". date('YmdHis-') . rand(100,999);
        	 
        	//是否强制上传
        	$forceUpload = req_int('forceUpload');
        	
        
        	$fileName = Comm_Excel::uploadExcel("excelfile",$path,$name);
        
        	 
        	Model_Lottery::importExcel($path . $fileName,$forceUpload);
        	 
        
        	$this->forward('list');
        }
    	$this->display($hash, 'Market/uploadcode.tpl');
    	 
    }

   

    public function indexAction()
    {}

    public function shopAction()
    {
        extract($_REQUEST);
        
        // ======显示 字段==================================
        $fields['kb_shopid'] = array(
            "title" => "编号"
        );
        $fields['kb_hqname'] = array(
            "title" => "总部名称"
        );
        $fields['kb_shopname'] = array(
            "title" => "门店名称"
        );
        $fields['kb_goods_num'] = array(
            "title" => "商品数"
        );
        $fields['kb_cityid'] = array(
            "title" => "地区"
        );
        $fields['kb_catid'] = array(
            "title" => "分类"
        );
        $fields['kb_issuetime'] = array(
            "title" => "上线时间"
        );
        $fields['kb_holder_sellerid'] = array(
            "title" => "跟进销售"
        );
        // ======过滤 字段==================================
        $cityid = flt_int($cityid);
        $shoppid = flt_int($shoppid);
        
        // $table = new KpiBaseinfo ();
        // $table->debug();
        
        $cats = Model_Shop::getCategories(""); // $cityid
        $hash['catidOpts'] = get_opts($cats, 'catId', 'catName');
        
        $shoppids = Model_Shop::getHeadquarters(""); // $cityid
        $hash['shoppidOpts'] = get_opts($shoppids, 'headquarterId', 'headquarterName');
        
        $sellerids = Model_User::getSellers($cityid); // $cityid
        $hash['selleridOpts'] = get_opts($sellerids, 'uid', 'realname');
        
        // =======初始化 条件============
        $currpage = is_numeric($currpage) && $currpage > 1 ? $currpage : 1;
        $orderby = array_key_exists($orderby, $fields) ? $orderby : "kb_shopid";
        $direction = $direction == 'asc' ? 'asc' : 'desc';
        $orderby_clause = "order by $orderby $direction";
        
        $where_clause = "where 1  ";
        $where_clause .= $cityid ? " and kb_cityid='$cityid' " : "";
        $where_clause .= $catid ? " and kb_catid='$catid'  " : "";
        $where_clause .= $shoppid ? " and kb_shop_pid='$shoppid'  " : "";
        $where_clause .= $sellerid ? " and kb_holder_sellerid='$sellerid'  " : "";
        
        $table = new KpiBaseinfo();
        // $table->debug();
        
        // =====获取 总数=============================
        $total_rows = $table->getCount($where_clause);
        $page_size = 20;
        $total_page = ceil($total_rows / $page_size);
        $currpage = ($currpage > $total_page && $total_page > 0) ? $total_page : $currpage;
        
        // ======模板 数据==================================
        // $selectfields = $fields;
        // unset($selectfields['kb_shopid2']);
        
        // ======获取 数据==================================
        $columns = prepare_select_fields($fields);
        // s($columns,$fields,1);
        
        // $select_fields = implode ( ',', array_keys ( $selectfields ) );
        $rs = $table->query($columns . ' ', $where_clause . ' ' . $orderby_clause, $currpage, $page_size);
        
        while ($row = $table->fetch($rs)) {
            
            // $row['mng'].=' | '. html_link('删除',"javascript:do_delete(\"{$row[dc_id]}\",\"{$row[username]}\");");
            $row['mng'] = html_btn('更改销售', "editSeller(this)", array(
                'data-shopid' => $row['kb_shopid'],
                'data-sellerid' => $row['kb_holder_sellerid']
            ));
            
            $row['kb_catid'] = $hash['catidOpts'][$row['kb_catid']];
            $row['kb_cityid'] = $GLOBALS['CITY'][$row['kb_cityid']];
            $row['kb_issuetime'] = get_date($row['kb_issuetime'], 'Y-m-d');
            
            $row['kb_holder_sellerid'] = "<span id='seller_{$row[kb_shopid]}'>" . Model_User::getRealname($row['kb_holder_sellerid']) . "</span>";
            
            // $row ['kb_shop_pid'] = $hash['shoppidOpts'][$row['kb_shop_pid']] ;
            
            // $row ['kb_shopid2'] = Model_Shop::getShop($row['kb_shopid'],'shopName');
            // $row ['kb_goods_num'] = Model_Shop::getOnSaleGoodsNum($row['kb_shopid']);
            
            $data[] = $row;
        }
        // show(user_access('sysuser','access_edit'));
        $fields['mng'] = array(
            "title" => "操作",
            'nosort' => 1
        );
        
        // ======模板 显示=========== =======================
        
        $hash["pk_field"] = $table->_primarykey;
        
        $hash["total_rows"] = $total_rows;
        $hash["total_page"] = $total_page;
        $hash["fields"] = $fields;
        $hash["rows"] = $data;
        
        $hash["cityidOpts"] = $GLOBALS['CITY'];
        
        $this->display($hash, 'SaleManage/shoplist.tpl');
    }

     
     
     
}

?>
