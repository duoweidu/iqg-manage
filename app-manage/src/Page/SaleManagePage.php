<?php

/**
  
 * Created on 
 *  
 * @package     
 * @author      
 * @copyright  
 * @version    $Id$
 */
class SaleManagePage extends MyFw_App_Backend_Page
{

    //protected $check_role = 'XSZG,YYRY';

    public function _init()
    {
        $this->depid = Model_User::getDepID('销售');
    }

    public function userAction()
    {
        // $admin=get_session("admin");
        // $depid = isset($admin['depid']) ? $admin['depid'] : null;
        $depid = $this->depid;
        
        $cityid = $_GET['cityid'];
        $pid = $_GET['pid'];
        $keyword = trim($_GET['keyword']);
        
        $this->view->cityid = $cityid;
        $this->view->pid = $pid;
        $this->view->keyword = $keyword;
        
        $where = array();
        if ($cityid) {
            $where[] = " cityid='$cityid' ";
        }
        if ($pid) {
            $where[] = " pid='$pid' ";
        }
        if ($keyword) {
            $where[] = " acl_user.realname like '%$keyword%' ";
        }
        
        $aclUserDao = new MyFw_Model_AclUser();
        $userList = $aclUserDao->getUserList($depid, $where);
        $userList = is_array($userList) ? $userList : array();
        
        $userList = Model_User::formatUserList($userList);
        
        $this->view->userList = $userList;
        
        // s($_SESSION);
        
        $this->view->userCity = $GLOBALS['CITY'];
        $this->view->userLevel = $GLOBALS['USER_LEVEL'];
        $this->view->leaderList = Model_User::getLeaderList($this->depid); // $this->getTableList ( 'AclUserExt', 'uid,realname', 'where level in (2,3,4) and uid != 1' );
        
        $this->render('SaleManage/userlist.tpl');
    }

    public function indexAction()
    {}

    public function shopAction()
    {
        extract($_REQUEST);
        
        // ======显示 字段==================================
        $fields['kb_shopid'] = array(
            "title" => "编号"
        );
        $fields['kb_hqname'] = array(
            "title" => "总部名称"
        );
        $fields['kb_shopname'] = array(
            "title" => "门店名称"
        );
        $fields['kb_goods_num'] = array(
            "title" => "商品数"
        );
        $fields['kb_cityid'] = array(
            "title" => "地区"
        );
        $fields['kb_catid'] = array(
            "title" => "分类"
        );
        $fields['kb_issuetime'] = array(
            "title" => "上线时间"
        );
        $fields['kb_holder_sellerid'] = array(
            "title" => "跟进销售"
        );
        // ======过滤 字段==================================
        $cityid = flt_int($cityid);
        $shoppid = flt_int($shoppid);
        
        // $table = new KpiBaseinfo ();
        // $table->debug();
        
        $cats = Model_Shop::getCategories(""); // $cityid
        $hash['catidOpts'] = get_opts($cats, 'catId', 'catName');
        
        $shoppids = Model_Shop::getHeadquarters(""); // $cityid
        $hash['shoppidOpts'] = get_opts($shoppids, 'headquarterId', 'headquarterName');
        
        $sellerids = Model_User::getSellers($cityid); // $cityid
        $hash['selleridOpts'] = get_opts($sellerids, 'uid', 'realname');
        
        // =======初始化 条件============
        $currpage = is_numeric($currpage) && $currpage > 1 ? $currpage : 1;
        $orderby = array_key_exists($orderby, $fields) ? $orderby : "kb_shopid";
        $direction = $direction == 'asc' ? 'asc' : 'desc';
        $orderby_clause = "order by $orderby $direction";
        
        $where_clause = "where 1  ";
        $where_clause .= $cityid ? " and kb_cityid='$cityid' " : "";
        $where_clause .= $catid ? " and kb_catid='$catid'  " : "";
        $where_clause .= $shoppid ? " and kb_shop_pid='$shoppid'  " : "";
        $where_clause .= $sellerid ? " and kb_holder_sellerid='$sellerid'  " : "";
        
        $table = new KpiBaseinfo();
        // $table->debug();
        
        // =====获取 总数=============================
        $total_rows = $table->getCount($where_clause);
        $page_size = 20;
        $total_page = ceil($total_rows / $page_size);
        $currpage = ($currpage > $total_page && $total_page > 0) ? $total_page : $currpage;
        
        // ======模板 数据==================================
        // $selectfields = $fields;
        // unset($selectfields['kb_shopid2']);
        
        // ======获取 数据==================================
        $columns = prepare_select_fields($fields);
        // s($columns,$fields,1);
        
        // $select_fields = implode ( ',', array_keys ( $selectfields ) );
        $rs = $table->query($columns . ' ', $where_clause . ' ' . $orderby_clause, $currpage, $page_size);
        
        while ($row = $table->fetch($rs)) {
            
            // $row['mng'].=' | '. html_link('删除',"javascript:do_delete(\"{$row[dc_id]}\",\"{$row[username]}\");");
            $row['mng'] = html_btn('更改销售', "editSeller(this)", array(
                'data-shopid' => $row['kb_shopid'],
                'data-sellerid' => $row['kb_holder_sellerid']
            ));
            
            $row['kb_catid'] = $hash['catidOpts'][$row['kb_catid']];
            $row['kb_cityid'] = $GLOBALS['CITY'][$row['kb_cityid']];
            $row['kb_issuetime'] = get_date($row['kb_issuetime'], 'Y-m-d');
            
            $row['kb_holder_sellerid'] = "<span id='seller_{$row[kb_shopid]}'>" . Model_User::getRealname($row['kb_holder_sellerid']) . "</span>";
            
            // $row ['kb_shop_pid'] = $hash['shoppidOpts'][$row['kb_shop_pid']] ;
            
            // $row ['kb_shopid2'] = Model_Shop::getShop($row['kb_shopid'],'shopName');
            // $row ['kb_goods_num'] = Model_Shop::getOnSaleGoodsNum($row['kb_shopid']);
            
            $data[] = $row;
        }
        // show(user_access('sysuser','access_edit'));
        $fields['mng'] = array(
            "title" => "操作",
            'nosort' => 1
        );
        
        // ======模板 显示=========== =======================
        
        $hash["pk_field"] = $table->_primarykey;
        
        $hash["total_rows"] = $total_rows;
        $hash["total_page"] = $total_page;
        $hash["fields"] = $fields;
        $hash["rows"] = $data;
        
        $hash["cityidOpts"] = $GLOBALS['CITY'];
        
        $this->display($hash, 'SaleManage/shoplist.tpl');
    }

    public function userAddAction()
    {
        $aclUserDao = new MyFw_Model_AclUser(); // $this->dao->load('MyFw_AclUser');
        
        $this->view->userCity = $GLOBALS['CITY'];
        $this->view->userLevel = $GLOBALS['USER_LEVEL'];
        $this->view->leaderList = Model_User::getLeaderList($this->depid); // $this->getTableList ( 'AclUserExt', 'uid,realname', 'where level in(2,3,4) and depid=' .$this->depid );
        $tab = new AclUserExt();
        // $tab->debug();
        // do post
        if ($_POST) {
            $depid = Model_User::getDepID('销售');
            $roles = array(
                Model_User::getRoleID('SL')
            );
            
            // validation
            
            if (! Zend_Validate::is($this->param('pass'), 'NotEmpty')) {
                $this->addError('common.notempty', '用户密码');
            }
            if (! Zend_Validate::is($this->param('realname'), 'NotEmpty')) {
                $this->addError('common.notempty', '真实姓名');
            }
            // if (!Zend_Validate::is($this->param('phone'), 'NotEmpty')) {
            // $this->addError('common.notempty', '联系电话');
            // }
            // else if (!preg_match("/((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/", $this->param('phone'))) {
            // $this->addError('common.formaterror', '联系电话');
            // }
            if (! Zend_Validate::is($this->param('name'), 'NotEmpty')) {
                $this->addError('common.notempty', '电子邮件');
            } else 
                if (! preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $this->param('name'))) {
                    $this->addError('common.formaterror', '电子邮件');
                }
            
            if (Model_User::checkUserExists(null, $this->param('name'))) {
                $this->addError('common.exists', '用户名');
            }
            
            if ($this->noError()) {
                
                $department_name = st('Department')->scalar("department_name", "where department_id=" . intval($this->param('department')));
                // prepare data
                $data['name'] = $this->param('name');
                $data['realname'] = $this->param('realname');
                $data['phone'] = $this->param('phone');
                $data['email'] = $this->param('name');
                $data['department'] = '销售部'; // $this->param('department');
                $data['passedit'] = $this->param('passedit');
                $data['lookup_information_nums'] = 0;
                $data['pass'] = Fw_Util::md5($this->param('pass'));
                // do create
                $userId = $aclUserDao->create($data);
                if ($userId) {
                    $aclUserDao->updateRoles($userId, $roles);
                }
                // Check Department Exists
                $MyFw_Department = new MyFw_Model_Department();
                // $MyFw_Department->CheckDepartmentExists(trim($_POST['department']),$this->admin['name']);
                // update user extend info
                $level = $this->param('level');
                $userExtData['pid'] = (in_array($level, array(
                    1,
                    2
                ))) ? $this->param('pid') : 0;
                $userExtData['realname'] = $this->param('realname');
                $userExtData['level'] = $level;
                $userExtData['cityid'] = $this->param('cityid');
                $userExtData['uid'] = $userId;
                $userExtData['depid'] = $depid;
                
                $this->insertIntoTable('AclUserExt', $userExtData);
                
                $this->forward('user');
            }
        }
        
        // default data
        $this->view->user = $_POST;
        
        $this->render('SaleManage/useradd.tpl');
    }

    public function userdelAction()
    {
        $uid = $this->param('id');
        if (Model_User::getShopNum($uid) > 0) {
            $this->alert("该用户门店数大于0，不能删除", "./user");
            return;
        }
        
        if ($this->param('id')) {
            $aclUserDao = new MyFw_Model_AclUser();
            $aclUserDao->deleteUser($uid); // logically delete user
            $aclUserDao->updateRoles($uid);
        }
        
        $this->forward('user');
    }

    public function usereditAction()
    {
        $aclUserDao = new MyFw_Model_AclUser();
        
        $this->view->yesno = $GLOBALS['YES_NO'];
        $this->view->userCity = $GLOBALS['CITY'];
        $this->view->userLevel = $GLOBALS['USER_LEVEL'];
        $this->view->leaderList = Model_User::getLeaderList($this->depid, $this->param('id')); // $this->getTableList ( 'AclUserExt', 'uid,realname', 'where level in (2,3,4) and uid != ' . $this->param ( 'id' ) .' and depid='.$this->depid );
        $this->view->curLevel = $this->getRowRecord('AclUserExt', '*', 'where uid = ' . $this->param('id'));
        
        $user = $aclUserDao->read($this->param('id'));
        
        // select department
        $aclDepartmentDao = new MyFw_Model_Department();
        $this->view->alldepartment = $aclDepartmentDao->getAllDP();
        // do post
        if ($_POST) {
            $depid = Model_User::getDepID('销售');
            $roles = array(
                Model_User::getRoleID('SL')
            );
            
            // validation
            if (! Zend_Validate::is($this->param('realname'), 'NotEmpty')) {
                $this->addError('common.notempty', '真实姓名');
            }
            // if (!Zend_Validate::is($this->param('phone'), 'NotEmpty')) {
            // $this->addError('common.notempty', '联系电话');
            // }
            // else if (!preg_match("/((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/", $this->param('phone'))) {
            // $this->addError('common.formaterror', '联系电话');
            // }
            if (! Zend_Validate::is($this->param('name'), 'NotEmpty')) {
                $this->addError('common.notempty', '电子邮件');
            } else 
                if (! preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $this->param('name'))) {
                    $this->addError('common.formaterror', '电子邮件');
                }
            
            if ($this->noError()) {
                $department_name = st('Department')->scalar("department_name", "where department_id=" . intval($this->param('department')));
                // prepare data
                $data['name'] = $this->param('name');
                $data['realname'] = $this->param('realname');
                $data['phone'] = $this->param('phone');
                // $data ['email'] = $this->param ( 'name' );
                $data['department'] = '销售部'; // $this->param('department');
                $data['lookup_information_nums'] = 0;
                $data['islock'] = $this->param('islock');
                
                if ($this->param('pass')) {
                    $data['pass'] = Fw_Util::md5($this->param('pass'));
                }
                // st('AclUserExt')->debug();
                // do update
                if ($this->param('id')) {
                    $aclUserDao->update($data, 'where id=' . $this->param('id'));
                    
                    // 增加的时候设定好
                    // $aclUserDao->updateRoles ( $this->param ( 'id' ), $roles );
                    
                    // update user extend info
                    $level = $this->param('level');
                    $userExtData['pid'] = (in_array($level, array(
                        1,
                        2
                    ))) ? $this->param('pid') : 0;
                    $userExtData['realname'] = $this->param('realname');
                    $userExtData['level'] = $level;
                    $userExtData['cityid'] = $this->param('cityid');
                    $userExtData['depid'] = $depid;
                    
                    $count = $this->getTableCount('AclUserExt', 'where uid = ' . $this->param('id'));
                    if ($count) {
                        $this->updateTable('AclUserExt', $userExtData, 'where uid = ' . $this->param('id'));
                    } else {
                        $userExtData['uid'] = $this->param('id');
                        $this->insertIntoTable('AclUserExt', $userExtData);
                    }
                    $this->forward('user');
                }
            }
        }
        
        // default data
        $this->view->user = $user;
        
        $this->render('SaleManage/useredit.tpl');
    }

    public function changeSellerAction()
    {
        $shopid = flt_int($_POST['shopid']);
        $sellerid = flt_int($_POST['sellerid']);
        
        $ret = Model_Shop::setSeller($shopid, $sellerid);
        
        $ret = $ret ? 'succ' : 'fail';
        exit($ret);
    }
}

?>
