<?php

/**
 *
 * @package
 *
 */
class ApiPage extends MyFw_App_Backend_Page
{

    protected $check_acl = false;

    function _init()
    {
        // for html5 app
        $this->setCORSHeader();
    }

    function checkMobileAction()
    {
        $errno = 0;
        $msg = '';
        $data = '';
        
        $mobile = req_str('mobile');
        $openid = req_str('openid');
        $pinid = req_int('pinid');
        
        if (empty($mobile) || ! Model_MUser::isMobile($mobile)) {
            $errno = - 1;
            $msg = 'invalid-mobile';
            // $data = '手机号格式无效';
            $this->jsonResulte($errno, $msg, $data);
        }
        
        if (Model_Lottery::isUsed($pinid)) {
            $errno = - 2;
            $msg = 'invalid-pincode';
            // $data = '本次奖品已被领取';
            $this->jsonResulte($errno, $msg, $data);
        }
        
        $pindata = Model_Lottery::getPin($pinid);
        
        // 一个手机号只能冲3次
        if (($pindata['lp_prize'] == '2' || $pindata['lp_prize'] == '4') && ! Model_Lottery::checkRechargeLimit($mobile, 3)) {
            $errno = - 3;
            $msg = 'recharge-limit';
            $this->jsonResulte($errno, $msg, $data);
        }
        
        // st('MurcielagoUser')->debug();
        // 检查注册用户
        if (Model_MUser::exists($mobile)) {
            // 存在
            $password = '';
            $user = Model_MUser::getUser($mobile);
            $uid = $user['uid'];
            $errno = 0;
            $msg = 'user-exist';
        } else {
            // 注册用户
            $password = rand(100000, 999999);
            $uid = Model_MUser::add($mobile, $password);
            $errno = 1;
            $msg = 'user-reg';
        }
        
        // 更新抽奖验证信息
        $row = array();
        $row['lc_tmp'] = $password;
        $row['lc_mobile'] = $mobile;
        $row['lc_regist'] = 1;
        $row['lc_utime'] = time();
        $row['lc_status'] = 2;
        st('LtChecking')->update($row, "where lc_lpid='$pinid' and lc_status=1");
        // Model_Lottery::updateChecking($pinid, $row);
        
        switch ($pindata['lp_prize']) {
            case '2': // 充值卡 20 元
            case '4': // 充值卡 5 元
                $cardNumber = $pindata['lp_value'];
                $ret = Model_Recharge::recharge($uid, $cardNumber);
                $data = $ret;
                if ($ret === false) {
                    $errno = - 4;
                    $msg = 'recharge-fail'; 
                    $this->jsonResulte($errno, $msg, $data);
                    // Model_Lottery::change2Failure($pinid);
                } else {
                    $msg = 'recharge-succ';
                    Model_Lottery::change2Finish($pinid);
                }
                break;
            case '1':
                // ipad mini
                $data = $pindata['lp_value'];
                Model_Lottery::change2Finish($pinid);
                break;
            case '3':
                // 消费验证吗
                $data = $pindata['lp_value'];
                Model_Lottery::change2Finish($pinid);
                break;
        }
        
        $this->jsonResulte($errno, $msg, $data);
    }

    function searchViewAction()
    {
        $errno = 0;
        $msg = '';
        $data = '';
        
        $mobile = req_str('mobile');
        $openid = req_str('openid');
        
        if (empty($mobile) || ! Model_MUser::isMobile($mobile)) {
            $errno = - 1;
            $msg = '手机号格式无效';
            $data = '';
            $this->jsonResulte($errno, $msg, $data);
        }
        
        // st('MurcielagoUser')->debug();
        // 检查注册用户
        if (! Model_MUser::exists($mobile)) {
            $errno = - 2;
            $msg = '手机号未注册';
            $data = '';
            $this->jsonResulte($errno, $msg, $data);
        }
        
        $tab = new LtChecking();
        $sql = $tab->select("lc_prize");
        $sql->where("lc_mobile=?", $mobile);
        $sql->where("lc_openid=?", $openid);
        $sql->where("lc_regist=1");
        $sql->where("lc_status=3");
        $sql->order("lc_prize asc");
        $data = $sql->fetchAll();
        
        $this->jsonResulte($errno, $msg, $data);
    }

    /**
     * 检查活动验证码
     */
    function checkPinAction()
    {
        $errno = 0;
        $msg = '';
        $data = array();
        
        $pincode = req_str('pincode');
        $openid = req_str('openid');
        
        $pin = Model_Lottery::getPinInfo($pincode);
        
        if ($pin) {
            // 去除，避免提前暴露
            unset($pin['lp_value']);
            
            $pinid = $pin['lp_id'];
            
            // 检查是否已被验证过
            $checkInfo = Model_Lottery::getChecking($pinid);
            if ($checkInfo) {
                if ($checkInfo['lc_openid'] != $openid) {
                    $errno = 3; // 有效，可用
                    $msg = 'used';
                    $data = array();
                } else {
                    $errno = 0; // 有效，可用
                    $msg = 'skip'; // 跳过转盘
                    $data = $pin;
                }
            } else { // 首次验证
                $pass = Model_Lottery::checkLimit($openid, 3);
                if ($pass) {
                    $errno = 0; // 有效，可用
                    $msg = 'pass';
                    $data = $pin;
                    // 没被验证过
                    if (! $pin['lp_check']) {
                        Model_Lottery::addChecking($pinid, $pincode, $openid);
                    }
                } else {
                    $errno = 2; // 超限制
                    $msg = 'exceed n-times';
                }
            }
        } else {
            $errno = 1; // 不存在
            $msg = 'invalid pincode';
        }
        
        $this->jsonResulte($errno, $msg, $data);
    }

    function sendPwdAction()
    {
        $openid = req_str('openid');
        $pinid = req_int('pinid', 0);
        
        $data = Model_Lottery::getMobilePwd($pinid, $openid);
        
        try {
            Model_MUser::sendVcode($data['lc_mobile'], $data['lc_tmp']);
        } catch (Exception $e) {
            $this->jsonResulte($e->getCode(), $e->getMessage());
        }
        
        $this->jsonResulte(0, 'succ');
    }
}

?>
