<?php 

date_default_timezone_set('Etc/GMT-8'); 
$_SERVER['START_TIME']=microtime(true);
define('__ROOT_DIR__',dirname(dirname(__FILE__)));
define('__ROOT_LOG__', realpath(__ROOT_DIR__ . '/../app-env/log'));


//初始准备
include __ROOT_DIR__.'/lib/core/init.php';
include __ROOT_DIR__.'/lib/core/comm.php';
include __ROOT_LIB_DIR.'/Core/inc/memcached.php';



require 'Fw/App.php';

$app = new Fw_App();



$app->setTplDir(__TPL_SMARTY_PATH)
	->setCacheDir(__TPL_CACHE_PATH)
	//->setErrorPage('html/404.htm')
	->addMapFile(__MAP_INI_FILE)
	->addAppDir(__ROOT_DIR__.'/src/Page')
	->addAppDir(__ROOT_DIR__.'/lib/Page');
	//->addAppDir(__ROOT_DIR__.'/lib/Page/ERP')
	//->addAppDir(__ROOT_DIR__.'/lib/Page/CRM');
//	->addAppDir(__LIB_PATH_REMOTE);

/**
 * skip 404 page and trace exception
 * TODO : should be commented in www environment
 */
if (!defined('__ENV__') || __ENV__ != 'prod') {
	$app->setDebug(true);
}

/**
 * session.save_handler = memcache
 * session.save_path = tcp://ip:1121
 */
if(ini_get('session.save_handler')=='files'){
	include __ROOT_LIB_DIR.'/Core/inc/mc_session.php';	
}



/**
 * start session for $_SESSION variable
 * TODO : u'd better have it off and use Hush_Util::session to replace $_SESSION
 */
$app->startSession();

/**
 * if magic quotes option is opened 
 * TODO : we do stripMagicQuotes manually
 */
$app->closeMagicQuotes();

/**
 * 
 */
$app->run();
