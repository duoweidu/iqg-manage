String.prototype.replaceAll  = function(s1,s2)
{
	return this.replace(new RegExp(s1,"gm"),s2);
}
function searchPost(strFormID)
{
	var intType = document.getElementById("searchType").value;
	var frmForm = document.getElementById(strFormID);
	var strString = window.location.href;
	strString = strString.substr(0,strString.lastIndexOf("/"));
	strString = strString.substr(strString.lastIndexOf("/")+1);
	var strAction;
	if(intType == 0)
	{
		strAction = "../ccartyped/list.php";
		if(strString != "ccartyped")
		{
			strAction += "?action=doSearch";
		}
	}
	else if(intType == 1)
	{
		strAction = "../ccartype/list.php";
		if(strString != "ccartype")
		{
			strAction += "?action=doSearch";
		}
	}
	else
	{
		strAction = "../ccard/list.php";
	}
	frmForm.action = strAction;
}
function clearSearch(strFormID)
{
	var objForm = document.getElementById(strFormID);
	var arrObjText = document.getElementsByTagName("input");
	for(intI = 0 ; intI < arrObjText.length ; intI ++)
	{
		if(arrObjText[intI].type == "text")
		{
			arrObjText[intI].value = "";
		}
	}
	var arrObjSelect = document.getElementsByTagName("select");
	for(intI = 0 ; intI < arrObjSelect.length ; intI ++)
	{
		arrObjSelect[intI].value = "";
	}
}
function checkAll()
{
	var objCheckAll = document.getElementById("chkAll");
	var arrObjInput = document.getElementsByTagName("input");
	for(intI = 0 ; intI < arrObjInput.length ; intI ++)
	{
		if(arrObjInput[intI].type == "checkbox" && arrObjInput[intI].id != "chkAll")
		{
			if(objCheckAll.checked == true )
			{
				arrObjInput[intI].checked = true;
			}
			else
			{
				arrObjInput[intI].checked = false;
			}
		}
	}
}

function selectAll(strString)
{
	var arrObjInput = document.getElementsByTagName("input");
	for(intI = 0 ; intI < arrObjInput.length ; intI ++)
	{
		if(arrObjInput[intI].type == "checkbox")
		{
			if(arrObjInput[intI].checked == false )
			{
				arrObjInput[intI].checked = true;
			}
			else if(strString == "F")
			{
				arrObjInput[intI].checked = false;
			}
		}
	}
}

function checkChecked()
{
	var objCheckAll = document.getElementById("chkAll");
	var arrObjInput = document.getElementsByTagName("input");
	var blnChecked = true;
	for(intI = 0 ; intI < arrObjInput.length ; intI ++)
	{
		if(arrObjInput[intI].type == "checkbox" && arrObjInput[intI].id != "chkAll")
		{
			if(arrObjInput[intI].checked == false)
			{
				blnChecked = false;
			}
		}
	}
	objCheckAll.checked = blnChecked;
}
//删除汽车品牌、款式、型号图片
function deletePic(strFormID,intID)
{
	if (confirm("是否删除图片?"))
	{
		var objForm = document.getElementById(strFormID);
		objForm.action = "edit?action=dodelfile&id="+intID;
		objForm.submit();
	}
}
//删除文章图片
function deleteArtPic(strFormID,intID,strFileName)
{
	if (confirm("是否删除图片?"))
	{
		var objForm = document.getElementById(strFormID);
		objForm.action = "edit?action=dodelfile&id="+intID+"&fname="+strFileName;
		objForm.submit();
	}
}
function checkRegMore(strFormID,type)
{
	if(type == "check")
	{
		if (confirm("是否审核选中项?"))
		{
			var strGet = "";
			if(document.getElementById("searchCid"))
			{
				var intCid = document.getElementById("searchCid").value;
				strGet = "&cid="+intCid;
			}
			var objForm = document.getElementById(strFormID);
			objForm.action = "check.php?act=check&action=checkMore"+strGet;
			objForm.submit();
		}
	}
	else
	{
		if (confirm("是否改变选中项为未审核?"))
		{
			var strGet = "";
			if(document.getElementById("searchCid"))
			{
				var intCid = document.getElementById("searchCid").value;
				strGet = "&cid="+intCid;
			}
			var objForm = document.getElementById(strFormID);
			objForm.action = "check.php?act=nocheck&action=checkMore"+strGet;
			objForm.submit();
		}
	}

}
function deleteMore(strFormID)
{
	if (confirm("是否删除选中项?"))
	{
		var strGet = "";
		if(document.getElementById("searchCid"))
		{
			var intCid = document.getElementById("searchCid").value;
			strGet = "&cid="+intCid;
		}
		var objForm = document.getElementById(strFormID);
		objForm.action = "delete?action=deleteMore"+strGet;
		objForm.submit();
	}
}

function reliveMore(strFormID)
{
	if (confirm("是否恢复选中项?"))
	{
		var strGet = "";
		if(document.getElementById("searchCid"))
		{
			var intCid = document.getElementById("searchCid").value;
			strGet = "&cid="+intCid;
		}
		var objForm = document.getElementById(strFormID);
		objForm.action = "relive?action=reliveMore"+strGet;
		objForm.submit();
	}
}
function getEditorTextContents(strTextareaName)
{
	var objectEditor = FCKeditorAPI.GetInstance(strTextareaName);
	return(objectEditor.EditorDocument.body.innerText);
}
function getEditorHTMLContents(strTextareaName)
{
	var objectEditor = FCKeditorAPI.GetInstance(strTextareaName);
	return(objectEditor.GetXHTML(true));
}
function setEditorContents(strTextareaName,strContent)
{
	var objectEditor = FCKeditorAPI.GetInstance(strTextareaName);
	objectEditor.SetHTML(strContent) ;
}
function resetTextarea(strTextareaName,strHidName)
{
	setEditorContents(strTextareaName,document.getElementById(strHidName).value);
}
function resetTextarea_All()
{
	var strFrameID;
	var strDivID;
	var arrFrame = document.getElementsByTagName("iframe");
	for (intI = 0 ; intI < arrFrame.length ; intI ++)
	{
		if(arrFrame[intI].name != "postFrame")
		{
			strFrameID = arrFrame[intI].id.substring(0,arrFrame[intI].id.length-8);
			strHidID = strFrameID;
			resetTextarea(strFrameID,strHidID);
		}
	}
}
function openWindow(strUrl, strName, intHeight, intWidth)
{
	window.open(strUrl,strName, 'height='+intHeight + ",width=" + intWidth + ', top=0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');
}

function checkUser(frm)
{
	if (!frm.uname.value)
	{
		alert("请输入用户名");
		frm.uname.focus();
		return false;
	}
	if(frm.pass)
	{
		if (!frm.pass.value)
		{
			alert("请输入密码");
			frm.pass.focus();
			return false;
		}
		if (frm.pass.value != frm.confirm_pass.value)
		{
			alert("两次输入的密码不一致");
			frm.confirm_pass.focus();
			return false;
		}
	}
	return true;
}

function checkFuser(frm)
{
	if (!frm.uname.value)
	{
		alert("请输入用户名");
		frm.uname.focus();
		return false;
	}
	if(frm.pass)
	{
		if (!frm.pass.value)
		{
			alert("请输入密码");
			frm.pass.focus();
			return false;
		}
		if (frm.pass.value != frm.confirm_pass.value)
		{
			alert("两次输入的密码不一致");
			frm.confirm_pass.focus();
			return false;
		}
	}
	return true;
}

function checkGarticle(frm)
{
	if(frm.title.value == "")
	{
		alert("请输入标题!");
		frm.title.focus();
		return false;
	}
	if(frm.type.value == "0" || frm.type.value == "")
	{
		alert("请选择类别!");
		frm.type.focus();
		return false;
	}
	return true;
}

function checkArticle(frm)
{
	if(frm.title.value == "")
	{
		alert("请输入标题!");
		frm.title.focus();
		return false;
	}
	if(frm.type.value == "0" || frm.type.value == "")
	{
		alert("请选择类别!");
		frm.type.focus();
		return false;
	}
	/*
	if(frm.start.value == "0" || frm.start.value == "")
	{
	alert("请选择起点!");
	frm.type.focus();
	return false;
	}
	if(frm.end.value == "0" || frm.end.value == "")
	{
	alert("请选择终点!");
	frm.type.focus();
	return false;
	}*/
	var newPar=/^\d+$/ ;
	if(newPar.test(frm.days.value))
	{
		if(frm.days.value > 30 || frm.days.value < 0)
		{
			alert("请输入0-30的整数");
			frm.days.focus();
			return false;
		}
	}
	else
	{
		alert("请输入0-30的整数");
		frm.days.focus();
		return false;
	}
	if(!newPar.test(frm.cheat1.value))
	{
		alert("请输入大于0的正整数");
		frm.cheat1.focus();
		return false;
	}
	if(!newPar.test(frm.cheat2.value))
	{
		alert("请输入大于0的正整数");
		frm.cheat2.focus();
		return false;
	}
	if(!newPar.test(frm.cheat3.value))
	{
		alert("请输入大于0的正整数");
		frm.cheat3.focus();
		return false;
	}
	return true;
}

copyToClipboard = function(txt)
{
	if(window.clipboardData)
	{
		window.clipboardData.clearData();
		window.clipboardData.setData('Text', txt);
	}
	else if(navigator.userAgent.indexOf('Opera') != -1)
	{
		window.location = txt;
	}
	else if (window.netscape)
	{
		try {
			netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

		} catch (e) {
			alert("您的firefox安全限制限制您进行剪贴板操作，请打开'about:config'将signed.applets.codebase_principal_support'设置为true'之后重试");
			return false;
		}
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
		if (!clip)
		return;
		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
		if (!trans)
		return;
		trans.addDataFlavor('text/unicode');
		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
		var copytext = txt;
		str.data = copytext;
		trans.setTransferData("text/unicode",str,copytext.length*2);
		var clipid = Components.interfaces.nsIClipboard;
		if (!clip)
		return false;
		clip.setData(trans,null,clipid.kGlobalClipboard);
	}
	alert("拷贝成功!");
}

function ltrim(s){
	return s.replace( /^\s*/,"");
}
//去右空格;
function rtrim(s){
	return s.replace( /\s*$/,"");
}
//去左右空格;
function trim(s){
	return ltrim(rtrim(s));
}
/***************************************************************************/
function checkMore(strFormID)
{
	if (confirm("选中项已对帐?"))
	{
		var strGet = "";
		if(document.getElementById("searchCid"))
		{
			var intCid = document.getElementById("searchCid").value;
			strGet = "&cid="+intCid;
		}
		var objForm = document.getElementById(strFormID);
		objForm.action = "check.php?action=checkMore"+strGet;
		objForm.submit();
	}
}

String.prototype.trim = function()
{
	return this.replace(/(^\s*)|(\s*$)/g, "");
}

function shipMore(strFormID)
{
	var arrObjInput = document.getElementsByTagName("input");
	var blnChecked = false;
	for(intI = 0 ; intI < arrObjInput.length ; intI ++)
	{
		if(arrObjInput[intI].type == "checkbox" )
		{
			if(arrObjInput[intI].checked == true)
			{
				blnChecked = true;
			}
		}
	}
	if(blnChecked ==false)
	{
		alert('请选择您要发货的订单！');
		return false;
	}
	var scode=document.getElementById("scode").value;
	scode=scode.trim();
/*
	if(scode=="")
	{
		alert('请输入快递单号！');
		return false;
	}
	return confirm("选中项合并发货?");*/
/*
	if (confirm("选中项合并发货?"))
	{
		var strGet = "";
		if(document.getElementById("hidOrderID"))
		{
			var orderid = document.getElementById("hidOrderID").value;
			strGet = "&orderid="+orderid;
		}
		if(document.getElementById("scode"))
		{
			var scode = document.getElementById("scode").value;
			strGet =strGet+"&scode="+scode;
		}
		var objForm = document.getElementById(strFormID);
		objForm.action = "doedit.php?action=shipMore"+strGet;
		objForm.submit();
	}*/
}