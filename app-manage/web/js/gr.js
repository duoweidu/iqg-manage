var smit_arrReferConfig=new Array();

var smit_intReturnKeyCode=13;
var smit_intSpaceKeyCode=32;

var smit_intHomeKeyCode=36;
var smit_intEndKeyCode=35;

var smit_intPageUpKeyCode=33;
var smit_intPageDownKeyCode=34;

var smit_intArrowLeftKeyCode=37;
var smit_intArrowUpKeyCode=38;
var smit_intArrowRightKeyCode=39;
var smit_intArrowDownKeyCode=40;

var smit_intDefaultDivHeight=200;

var smit_intDefaultPageUpStep=-5;
var smit_intDefaultPageDownStep=5;

function smit_DefineArrays(strID){
    smit_PutDebugInfo(strID,"I am smit_DefineArrays start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    
    if(!smit_arrReferConfig[intNO][7]) smit_arrReferConfig[intNO][7]=new Array();
    if(!smit_arrReferConfig[intNO][7][2]) smit_arrReferConfig[intNO][7][2]=new Array();
    if(!smit_arrReferConfig[intNO][7][4]) smit_arrReferConfig[intNO][7][4]=new Array();
    if(!smit_arrReferConfig[intNO][9]) smit_arrReferConfig[intNO][9]=new Array();
    if(!smit_arrReferConfig[intNO][9][1]) smit_arrReferConfig[intNO][9][1]=new Array();
    if(!smit_arrReferConfig[intNO][9][2]) smit_arrReferConfig[intNO][9][2]=new Array();
    if(!smit_arrReferConfig[intNO][9][3]) smit_arrReferConfig[intNO][9][3]=new Array();
    if(!smit_arrReferConfig[intNO][9][6]) smit_arrReferConfig[intNO][9][6]=new Array();
        
    if(!smit_arrReferConfig[intNO][7][5]) smit_arrReferConfig[intNO][7][5]=new Array();
    smit_arrReferConfig[intNO][7][5][0]=new Image(10,10);
    smit_arrReferConfig[intNO][7][5][1]=new Image(10,10);


    smit_PutDebugInfo(strID,"I am smit_DefineArrays ended");
}




function smit_GetNOFromID(strID){
    var intNO=-1;
    var i;
    for(i=0;i<smit_arrReferConfig.length;i++)
    	if(smit_arrReferConfig[i][2]==strID) intNO=i;
    return(intNO)
}

function smit_PutDebugInfo(strID,strMsg){
	return;
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    var intCurrTime=smit_GetCurrentTime();
    if(!smit_arrReferConfig[intNO][7][6])
        smit_arrReferConfig[intNO][7][6]=0;
    var intTimePassed=intCurrTime-smit_arrReferConfig[intNO][7][6];
    strMsg=(intTimePassed+":"+strMsg);
    if(document.all("smitdebug"+strID)){
    	document.all("smitdebug"+strID).innerHTML+="<pre>"+strMsg+"\n</pre>";
    }
    smit_arrReferConfig[intNO][7][6]=intCurrTime;
}

function smit_GetCurrentTime(){
    var objDate=new Date();
    return objDate.getTime();
}


function smit_ToggleReferDiv(strID){
    smit_PutDebugInfo(strID,"I am smit_ToggleReferDiv start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    if(document.all("smitdiv"+strID).style.display=="none"){
        smit_ShowReferDiv(strID,true);
        document.all("smitinput"+strID).focus();
        smit_ExecUserDefinedEventHandler(strID,"ontoggleopen");
    }
    else{
        smit_ShowReferDiv(strID,false);
        smit_ExecUserDefinedEventHandler(strID,"ontoggleclose");
        smit_ExecUserDefinedEventHandler(strID,"ondecisionmade");
    }
    smit_PutDebugInfo(strID,"I am smit_ToggleReferDiv ended");
}

function smit_ProcessMouseOverImg(strID){
    var intNO=smit_GetNOFromID(strID);	
    if(intNO==-1) return;
    
    smit_arrReferConfig[intNO][7][4][0]=1;
}

function smit_ProcessMouseOutImg(strID){
    var intNO=smit_GetNOFromID(strID);	
    if(intNO==-1) return;
    
    smit_arrReferConfig[intNO][7][4][0]=0;
}
function smit_ProcessMouseOverDiv(strID){
    var intNO=smit_GetNOFromID(strID);	
    if(intNO==-1) return;
    
    smit_arrReferConfig[intNO][7][4][1]=1;
}
function smit_ProcessMouseOutDiv(strID){
    var intNO=smit_GetNOFromID(strID);	
    if(intNO==-1) return;
    smit_arrReferConfig[intNO][7][4][1]=0;
}

function smit_ShowReferDiv(strID,blnShowIt){
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;

    var i;
    if(blnShowIt){
        for(i=0;i<smit_arrReferConfig.length;i++){
    	    if((document.all("smitdiv"+smit_arrReferConfig[i][2]).style.display=="block")&&(i!=intNO)){
    	        smit_ShowReferDiv(smit_arrReferConfig[i][2],false);
    	    }
        }
    }

    if(blnShowIt)
        smit_PutDebugInfo(strID,"I am smit_ShowReferDiv start:show");
    else
        smit_PutDebugInfo(strID,"I am smit_ShowReferDiv start:hide");
    var strShowIt=(blnShowIt)?"block":"none";
    var strImgSrc=(blnShowIt)?smit_arrReferConfig[intNO][7][5][1].src:smit_arrReferConfig[intNO][7][5][0].src;
    if(!document.all("smitinput"+strID)) return;
    if((document.all("smitdiv"+strID))&&(document.all("smitdiv"+strID).style.display!=strShowIt)){
        document.all("smitdiv"+strID).style.display=strShowIt;
    }
    if(blnShowIt){
        smit_HideElement(strID,"IMG");
        smit_HideElement(strID,"SELECT");
        smit_HideElement(strID,"OBJECT");
        smit_HideElement(strID,"IFRAME");
    }
    else{
        smit_ShowElement(strID,"IMG");
        smit_ShowElement(strID,"SELECT");
        smit_ShowElement(strID,"OBJECT");
        smit_ShowElement(strID,"IFRAME");
    }
    if((blnShowIt)&&(document.all("smitdiv"+strID).style.display=="none"))
        document.all("smitinput"+strID).focus();
    if((!blnShowIt)&&(document.all("smitdiv"+strID).style.display=="block"))
        document.all("smitinput"+strID).blur();

    smit_PutDebugInfo(strID,"I am smit_ShowReferDiv ended");
}

function smit_GetOffsetLeft(strID,src){
    var set=0;
    if(src && src.name!="divMain"){
        if (src.offsetParent){
      	    set+=src.offsetLeft+smit_GetOffsetLeft(strID,src.offsetParent);
	}
	if(src.tagName.toUpperCase()!="BODY"){
	    var x=parseInt(src.scrollLeft,10);
	    if(!isNaN(x))
   	        set-=x;
	}
    }
    return set;
}

function smit_GetOffsetTop(strID,src){
    var set=0;
    if(src && src.name!="divMain"){
        if (src.offsetParent){
            set+=src.offsetTop+smit_GetOffsetTop(strID,src.offsetParent);
  	}
	if(src.tagName.toUpperCase()!="BODY"){
	    var y=parseInt(src.scrollTop,10);
	    if(!isNaN(y))
		set-=y;
	}
    }
    return set;
}

function smit_GetOffsetLeftInDiv(strID,src){
    var set=0;
    if(src && src.name!="divMain"){
        if (src.offsetParent){
            if( (src.offsetParent.style.position.toUpperCase()=="RELATIVE")||
                (src.offsetParent.style.position.toUpperCase()=="ABSOLUTE")
              ){
              	set+=src.offsetLeft;
            }
            else
      	        set+=src.offsetLeft+smit_GetOffsetLeftInDiv(strID,src.offsetParent);
	}
	if(src.tagName.toUpperCase()!="BODY"){
	    var x=parseInt(src.scrollLeft,10);
	    if(!isNaN(x))
   	        set-=x;
	}
    }
    return set;
}

function smit_GetOffsetTopInDiv(strID,src){
    var set=0;
    if(src && src.name!="divMain"){
        if (src.offsetParent){
            if( (src.offsetParent.style.position.toUpperCase()=="RELATIVE")||
                (src.offsetParent.style.position.toUpperCase()=="ABSOLUTE")
              ){
              	set+=src.offsetTop;
            }
            else
     	        set+=src.offsetTop+smit_GetOffsetTopInDiv(strID,src.offsetParent);
  	}
	if(src.tagName.toUpperCase()!="BODY"){
	    var y=parseInt(src.scrollTop,10);
	    if(!isNaN(y))
		set-=y;
	}
    }
    return set;
}




function smit_AssignResult(strID,strReason){
    smit_PutDebugInfo(strID,"I am smit_AssignResult start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    if(document.all("smitinput"+strID).value == ""){
        smit_arrReferConfig[intNO][6]=0;
    }

    if(smit_arrReferConfig[intNO][6]>0){
        document.all("smitinput"+strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][2]];
        document.all(strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][0]];
        eval(strID+"=smit_arrReferConfig["+intNO+"]["+(smit_arrReferConfig[intNO].length-1)+"]["+smit_arrReferConfig[intNO][6]+"]");
    }
    else{
        if(smit_arrReferConfig[intNO][9][0]==0){
    	    document.all(strID).value="";
    	    document.all("smitinput"+strID).value="";
    	}
    	else{
    	    document.all(strID).value=document.all("smitinput"+strID).value;
    	}
    	eval(strID+"=new Array();");
    }
    smit_PutDebugInfo(strID,"I am smit_AssignResult ended");
}


function st_PMS(intIndex,strID){
    smit_PutDebugInfo(strID,"I am ProcessMouesSelect start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    var objTheInput=document.all("smitinput"+strID);
    smit_arrReferConfig[intNO][6]=intIndex;
    smit_arrReferConfig[intNO][6]=intIndex;
    objTheInput.value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][intIndex][smit_arrReferConfig[intNO][1][2]];
    smit_AssignResult(strID,"mouseselect");
    smit_ShowReferDiv(strID,false);
    smit_ExecUserDefinedEventHandler(strID,"onmouseselect");
    smit_ExecUserDefinedEventHandler(strID,"ondecisionmade");
    smit_PutDebugInfo(strID,"I am ProcessMouesSelect ended");
}

function smit_GetNewReferTable(strID){
    smit_PutDebugInfo(strID,"I am smit_GetNewReferTable start");
    var intRowsFound=0;
    var intTheFirstRowFound=-1;
    var intTheLastRowFound=-1;
    var blnHighlightItemFound=false;
    var i,j;
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    if(document.all("smitinput"+strID)&&(document.all("smitinput"+strID).value == ""))
        smit_arrReferConfig[intNO][6]=0;


    var strDictTableStyle=(smit_arrReferConfig[intNO][9][2]["titletable"])?smit_arrReferConfig[intNO][9][2]["titletable"]:"BACKGROUND-color:#ffffff;BORDER-TOP: LightGrey solid 1px;BORDER-LEFT: LightGrey solid 1px;BORDER-BOTTOM: black solid 1px;BORDER-RIGHT: black solid 1px;";
    
    var strDictRowStyle=(smit_arrReferConfig[intNO][9][2]["dictrow"])?smit_arrReferConfig[intNO][9][2]["dictrow"]:"background-color:#ffffff;color:#fffacd;font:9pt;text-align:left;";
    var strDictItemStyle=(smit_arrReferConfig[intNO][9][2]["dictitem"])?smit_arrReferConfig[intNO][9][2]["dictitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM: #dddddd solid 1px;BORDER-RIGHT:#dddddd solid 1px;";
    var strDictWantItemStyle=(smit_arrReferConfig[intNO][9][2]["dictwantitem"])?smit_arrReferConfig[intNO][9][2]["dictwantitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM:#dddddd solid 1px;BORDER-RIGHT:#dddddd solid 1px;";
    var strDictKeyItemStyle=(smit_arrReferConfig[intNO][9][2]["dictkeyitem"])?smit_arrReferConfig[intNO][9][2]["dictkeyitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM:#dddddd solid 1px;BORDER-RIGHT:#dddddd solid 1px;";
    var strDictValueItemStyle=(smit_arrReferConfig[intNO][9][2]["dictvalueitem"])?smit_arrReferConfig[intNO][9][2]["dictvalueitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM:#dddddd solid 1px;BORDER-RIGHT:#dddddd solid 1px;";
    var strDictItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictitemtext"])?smit_arrReferConfig[intNO][9][2]["dictitemtext"]:"";
    var strDictWantItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictwantitemtext"])?smit_arrReferConfig[intNO][9][2]["dictwantitemtext"]:"";
    var strDictKeyItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictkeyitemtext"])?smit_arrReferConfig[intNO][9][2]["dictkeyitemtext"]:"";
    var strDictValueItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictvalueitemtext"])?smit_arrReferConfig[intNO][9][2]["dictvalueitemtext"]:"";

    var strDictSelectedRowStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedrow"])?smit_arrReferConfig[intNO][9][2]["dictselectedrow"]:"background-color:yellow;color:#fffacd;font:9pt;text-align:left;";
    var strDictSelectedItemStyle=(smit_arrReferConfig[intNO][9][2]["dictselecteditem"])?smit_arrReferConfig[intNO][9][2]["dictselecteditem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM: red solid 1px;BORDER-RIGHT:#eeeeee solid 1px;";
    var strDictSelectedWantItemStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedwantitem"])?smit_arrReferConfig[intNO][9][2]["dictselectedwantitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM: red solid 1px;BORDER-RIGHT:#eeeeee solid 1px;";
    var strDictSelectedKeyItemStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedkeyitem"])?smit_arrReferConfig[intNO][9][2]["dictselectedkeyitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM: red solid 1px;BORDER-RIGHT:#eeeeee solid 1px;";
    var strDictSelectedValueItemStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedvalueitem"])?smit_arrReferConfig[intNO][9][2]["dictselectedvalueitem"]:"color:#000000;font:9pt;text-align:left;BORDER-BOTTOM: red solid 1px;BORDER-RIGHT:#eeeeee solid 1px;";
    var strDictSelectedItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictselecteditemtext"])?smit_arrReferConfig[intNO][9][2]["dictselecteditemtext"]:"";
    var strDictSelectedWantItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedwantitemtext"])?smit_arrReferConfig[intNO][9][2]["dictselectedwantitemtext"]:"";
    var strDictSelectedKeyItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedkeyitemtext"])?smit_arrReferConfig[intNO][9][2]["dictselectedkeyitemtext"]:"";
    var strDictSelectedValueItemTextStyle=(smit_arrReferConfig[intNO][9][2]["dictselectedvalueitemtext"])?smit_arrReferConfig[intNO][9][2]["dictselectedvalueitemtext"]:"";
        
    var strFilterValue;
    if(smit_arrReferConfig[intNO][9][7]=="yes"){
        strFilterValue=smit_arrReferConfig[intNO][7][3];
    }
    else{
        strFilterValue=smit_arrReferConfig[intNO][7][3].toLowerCase();
    }
     
    var strTable="";
	strTable+="<table border=0 cellspacing=0 cellpadding=0 style=\"position:relative;left:0;right:0;"+strDictTableStyle+"\" id=\"smittable"+strID+"\" cellspacing=0 width=300px bgcolor=\""+smit_arrReferConfig[intNO][5][1]+"\">\n";
    for(i=1;i<smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1].length;i++){
        var blnIsSelected=false;
        var strTempKey;
        
        if(smit_arrReferConfig[intNO][9][7]!="yes"){
            strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]].toLowerCase();
        }
        else{
            strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]];
        }
        
        if(strTempKey.indexOf(strFilterValue)==0){
            if(i==(smit_arrReferConfig[intNO][6])){
            	blnIsSelected=true;
                strTable+=("<tr class=clsTR onclick=\"st_PMS(\'"+smit_arrReferConfig[intNO][6]+"\',\'"+strID+"\')\">\n");
                blnHighlightItemFound=true;
            }
            else
                strTable+=("<tr class=clsTR onclick=\"st_PMS("+i+",\'"+strID+"\')\">\n");
            for(j=0;j<smit_arrReferConfig[intNO][0];j++){
      	        var strTdStyle="";
    	        var strTdTextStyle="";
                strTable+=("<td  class=clsTD>"+smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][3][j]]+"</td>\n");
            }
            strTable+="</tr>\n";
            intRowsFound+=1;
            if(intTheFirstRowFound == -1) intTheFirstRowFound=i;
            intTheLastRowFound=i;
        }
    }
    strTable+="</table>\n"; 
    if(intRowsFound==0){
    	strTable="";
    	smit_arrReferConfig[intNO][6]=0;
    }
    else
    {
        if( (!blnHighlightItemFound)
            &&(document.all("smitinput"+strID)
            &&(document.all("smitinput"+strID).value != ""))
          ){
    	    if( (intRowsFound==1)
    	        ||(smit_arrReferConfig[intNO][7][0]==smit_intArrowDownKeyCode)
    	        ||(smit_arrReferConfig[intNO][9][5]=="yes")
    	      ){
    	        smit_arrReferConfig[intNO][6]=intTheFirstRowFound;
    	    }
    	    else{
    	        if(smit_arrReferConfig[intNO][7][0]==smit_intArrowUpKeyCode){
       	            smit_arrReferConfig[intNO][6]=intTheLastRowFound;
    	        }
     	        else{
    	            smit_arrReferConfig[intNO][6]=0;
    	        }
    	    }
        }
    }
    
    smit_PutDebugInfo(strID,"I am smit_GetNewReferTable ended");
    return(strTable);
}

function smit_GetNewHighlightItem(strID,intStep){
    smit_PutDebugInfo(strID,"I am smit_GetNewHighlightItem start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    
    var intPageUpStep=smit_arrReferConfig[intNO][9][6][0]?new Number(smit_arrReferConfig[intNO][9][6][0]):smit_intDefaultPageUpStep;
    var intPageDownStep=smit_arrReferConfig[intNO][9][6][1]?new Number(smit_arrReferConfig[intNO][9][6][1]):smit_intDefaultPageDownStep;
    
    var intTotalItems=0;
    var arrCurrentItems=new Array();
    var strFilterValue;
    if(smit_arrReferConfig[intNO][9][7]=="yes"){
        strFilterValue=smit_arrReferConfig[intNO][7][3];
    }
    else{
        strFilterValue=smit_arrReferConfig[intNO][7][3].toLowerCase();
    }
    
    var i,j;
    for(i=1;i<smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1].length;i++){
        var strTempKey;
        if(!smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]])
            continue;
        if(smit_arrReferConfig[intNO][9][7]=="yes"){
            strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]];
        }
        else{
            strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]].toLowerCase();
        }
        if(strTempKey.indexOf(strFilterValue)==0){
            arrCurrentItems[intTotalItems]=i;
            intTotalItems++;
        }
    }
    intTotalItems--;
    if(intTotalItems >0){
        if(smit_arrReferConfig[intNO][7][0] == smit_intHomeKeyCode){
            smit_arrReferConfig[intNO][6]=arrCurrentItems[0];
            return;
        }
        if(smit_arrReferConfig[intNO][7][0] == smit_intEndKeyCode){
            smit_arrReferConfig[intNO][6]=arrCurrentItems[arrCurrentItems.length-1];
            return;
        }
    }        
    	
    var strFilterValue;
    var strTempKey;
    if(smit_arrReferConfig[intNO][9][7]=="yes"){
        strFilterValue=smit_arrReferConfig[intNO][7][3];
        strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
    }
    else{
        strFilterValue=smit_arrReferConfig[intNO][7][3].toLowerCase();
        strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]].toLowerCase();
    }
    
    if( (document.all("smitinput"+strID).value != "")&&
        (smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]])&&
        (strTempKey.indexOf(strFilterValue)==0)
      ){
        for(j=0;j<arrCurrentItems.length;j++){
            if(arrCurrentItems[j]==smit_arrReferConfig[intNO][6]){
                if(intStep==1){
                    if(j<(arrCurrentItems.length-1)){
                        smit_arrReferConfig[intNO][6]=arrCurrentItems[j+1];
                        break;
                    }   
                }
                if(intStep==-1){
                    if(j>0){
                        smit_arrReferConfig[intNO][6]=arrCurrentItems[j-1];
                        break;
                    }
                }
                if(intStep==intPageDownStep){                
                    if(j<(arrCurrentItems.length-intPageDownStep)){
                        smit_arrReferConfig[intNO][6]=arrCurrentItems[j+intPageDownStep];
                        break;
                    }   
                    else{
                        smit_arrReferConfig[intNO][6]=arrCurrentItems[arrCurrentItems.length-1];
                        break;
                    }
                }
                if(intStep==intPageUpStep){
                    if(j>-(intPageUpStep+1)){
                        smit_arrReferConfig[intNO][6]=arrCurrentItems[j+intPageUpStep];
                        break;
                    }   
                    else{
                        smit_arrReferConfig[intNO][6]=arrCurrentItems[0];
                        break;
                    }
                }
            }
        }          
    }
    else{
        if((intStep==1)||(intStep==intPageDownStep)){
            smit_arrReferConfig[intNO][6]=arrCurrentItems[0];
        }
        if((intStep==-1)||(intStep==intPageUpStep)){
            smit_arrReferConfig[intNO][6]=arrCurrentItems[arrCurrentItems.length-1];
        }
    }
    smit_PutDebugInfo(strID,"I am smit_GetNewHighlightItem ended");
}

function smit_RefreshReferTable(strID,strReason,blnTmp){
    smit_PutDebugInfo(strID,"I am smit_RefreshReferTable start");
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    if(!document.all("smitinput"+strID))
    	return;

    switch(event.keyCode){
        case 0:  
    	    break;
        case smit_intReturnKeyCode:  
            return;
    	    break;
    	case smit_intArrowLeftKeyCode:
    	    break;
    	case smit_intArrowUpKeyCode:
            smit_arrReferConfig[intNO][7][0]=smit_intArrowUpKeyCode;
            smit_GetNewHighlightItem(strID,-1);
            if(smit_arrReferConfig[intNO][6]>0)
                document.all("smitinput"+strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
            break;
        case smit_intArrowRightKeyCode:
            break;
        case smit_intArrowDownKeyCode:
            smit_arrReferConfig[intNO][7][0]=smit_intArrowDownKeyCode;
            smit_GetNewHighlightItem(strID,1);
            if(smit_arrReferConfig[intNO][6]>0)
                document.all("smitinput"+strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
            break;
        case smit_intPageUpKeyCode:
            var intPageUpStep=smit_arrReferConfig[intNO][9][6][0]?(smit_arrReferConfig[intNO][9][6][0]):smit_intDefaultPageUpStep;
            smit_arrReferConfig[intNO][7][0]=smit_intPageUpKeyCode;
            smit_GetNewHighlightItem(strID,intPageUpStep);    
            if(smit_arrReferConfig[intNO][6]>0)
                document.all("smitinput"+strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
            break;
        case smit_intPageDownKeyCode:
            var intPageDownStep=smit_arrReferConfig[intNO][9][6][1]?(smit_arrReferConfig[intNO][9][6][1]):smit_intDefaultPageDownStep;
            smit_arrReferConfig[intNO][7][0]=smit_intPageDownKeyCode;
            smit_GetNewHighlightItem(strID,intPageDownStep);
            if(smit_arrReferConfig[intNO][6]>0)
                document.all("smitinput"+strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
            break;
        case smit_intHomeKeyCode:
            smit_arrReferConfig[intNO][7][0]=smit_intHomeKeyCode;
            smit_GetNewHighlightItem(strID,0);
            break;
        case smit_intEndKeyCode:
            smit_arrReferConfig[intNO][7][0]=smit_intEndKeyCode;
            smit_GetNewHighlightItem(strID,0);
            break;
        default:
            smit_arrReferConfig[intNO][7][3]=document.all("smitinput"+strID).value;
            break;
    }

    
    var objTheDiv=document.all("smitdiv2"+strID);
    if(blnTmp){
    var strTable=smit_GetNewReferTable(strID);
    objTheDiv.innerHTML=strTable;
    }

    smit_SetDiv(strID,strReason);
	
    window.event.cancelBubble=true;
    var varReturn=smit_ExecUserDefinedEventHandler(strID,"onkeyup");
    smit_PutDebugInfo(strID,"I am smit_RefreshReferTable ended");
    return varReturn;
}

function new_setDiv(strID)
{
	document.all("smitdiv1"+strID).style.posHeight = 22;
	document.all("smitdiv2"+strID).style.posHeight = 178;
	document.all("smitdiv"+strID).style.posHeight = 200;
}


function smit_ProcessKeyPress(strID){
    smit_PutDebugInfo(strID,"I am smit_ProcessKeyPress start");
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    

    var varReturn=smit_ExecUserDefinedEventHandler(strID,"onkeypress");
    smit_PutDebugInfo(strID,"I am smit_ProcessKeyPress ended");
    return varReturn;
}

function smit_ProcessKeyDown(strID){
    smit_PutDebugInfo(strID,"I am smit_ProcessKeyDown start");
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    switch(event.keyCode){
    	case smit_intSpaceKeyCode:
    	    if(smit_arrReferConfig[intNO][9][5] != "yes")
    	        break;
        case smit_intReturnKeyCode:
            smit_AssignResult(strID,"returnpressed");
            var intTempMouseStatus=smit_arrReferConfig[intNO][7][4][1];
            smit_arrReferConfig[intNO][7][4][1]=1;
            smit_ShowReferDiv(strID,false);
            smit_ExecUserDefinedEventHandler(strID,"onreturn");
            smit_ExecUserDefinedEventHandler(strID,"ondecisionmade");
            
            smit_arrReferConfig[intNO][7][4][1]=intTempMouseStatus;
            return;
            break;
        default:break;
    }
    
    var varReturn=smit_ExecUserDefinedEventHandler(strID,"onkeydown");
    smit_PutDebugInfo(strID,"I am smit_ProcessKeyDown ended");
    return varReturn;
}

function smit_HideElement(strID,strElementTagName){
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    var i;
    if(!smit_arrReferConfig[intNO][7][2][strElementTagName]){
        smit_arrReferConfig[intNO][7][2][strElementTagName]=new Array();
    }
    if(smit_arrReferConfig[intNO][7][2][strElementTagName].length>0){
        return;
    }

    var intDivLeft=smit_GetOffsetLeft(strID,document.all("smitinput"+strID));
    var intDivTop=smit_GetOffsetTop(strID,document.all("smitinput"+strID))+document.all("smitinput"+strID).offsetHeight;
    for(i=0;i<document.all.tags(strElementTagName).length; i++){
	var objTemp = document.all.tags(strElementTagName)[i];
	if(!objTemp||!objTemp.offsetParent)
	    continue;
	var intObjLeft=smit_GetOffsetLeft(strID,objTemp);
	var intObjTop=smit_GetOffsetTop(strID,objTemp);

	if(((intObjLeft+objTemp.clientWidth)>intDivLeft)&&
	   (intObjLeft<intDivLeft+document.all("smitdiv"+strID).style.posWidth)&&
	   (intObjTop+objTemp.clientHeight>intDivTop)&&
	   (intObjTop<intDivTop+document.all("smitdiv"+strID).style.posHeight)){
	    var intTempIndex=smit_arrReferConfig[intNO][7][2][strElementTagName].length;
	    smit_arrReferConfig[intNO][7][2][strElementTagName][intTempIndex]=new Array(objTemp,objTemp.style.visibility);
	    objTemp.style.visibility="hidden";
	    smit_PutDebugInfo(strID,strElementTagName+':'+smit_arrReferConfig[intNO][7][2][strElementTagName][intTempIndex][1]);
        }
    }
}
    
function smit_ShowElement(strID,strElementTagName){
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    if(!smit_arrReferConfig[intNO][7][2][strElementTagName]) return;
    var i;
    for(i=0;i<smit_arrReferConfig[intNO][7][2][strElementTagName].length; i++){
	var objTemp = smit_arrReferConfig[intNO][7][2][strElementTagName][i][0];
	if(!objTemp||!objTemp.offsetParent)
	    continue;
	objTemp.style.visibility=smit_arrReferConfig[intNO][7][2][strElementTagName][i][1];
    }
    smit_arrReferConfig[intNO][7][2][strElementTagName]=new Array();
}

function smit_ProcessFocus(strID){
    smit_PutDebugInfo(strID,"I am smit_ProcessFocus start");
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    if(document.all("smitinput"+strID).disabled) return false;
    
    smit_arrReferConfig[intNO][7][0]=0;
    
    var strInputValue=(smit_arrReferConfig[intNO][6]>0)?smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]]:document.all("smitinput"+strID).value;
    if( (smit_arrReferConfig[intNO][6]>0)
        &&(smit_arrReferConfig[intNO][9][4]=="yes")
      )
        smit_arrReferConfig[intNO][7][3]=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
    else
        smit_arrReferConfig[intNO][7][3]="";
    document.all('smitinput'+strID).value=strInputValue;
    document.all('smitinput'+strID).select();
    smit_RefreshReferTable(strID,"focus",false);

    var varReturn=smit_ExecUserDefinedEventHandler(strID,"onfocus");
    smit_PutDebugInfo(strID,"I am smit_ProcessFocus ended");
    return varReturn;
}

function smit_ExecUserDefinedEventHandler(strID,strEventName){
    smit_PutDebugInfo(strID,"I am smit_ExecUserDefinedEventHandler "+strEventName+" start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;

    var varReturn;
    if(smit_arrReferConfig[intNO][8][strEventName]){
    	if(smit_arrReferConfig[intNO][8][strEventName].toLowerCase().indexOf("return")==0){
    	    smit_arrReferConfig[intNO][8][strEventName]=smit_arrReferConfig[intNO][8][strEventName].replace(/^return/,"");
    	    varReturn=eval(smit_arrReferConfig[intNO][8][strEventName]);
    	}
    	else
    	    eval(smit_arrReferConfig[intNO][8][strEventName]);
    }
    smit_PutDebugInfo(strID,"I am smit_ExecUserDefinedEventHandler ended");
    return varReturn;
}




function smit_ProcessBlur(strID){
    smit_PutDebugInfo(strID,"I am smit_ProcessBlur start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    var blnDisableRefer=true;
    if( (smit_arrReferConfig[intNO][7][4][0]==1)
        ||(smit_arrReferConfig[intNO][7][4][1]==1)
      ){
       blnDisableRefer=false;
    }
    var strToDo="smit_ShowResultTimeout(\""+strID+"\","+blnDisableRefer+")";
    var intProcessBlurTimeOut=(smit_arrReferConfig[intNO][9][1]&&smit_arrReferConfig[intNO][9][1][0])?smit_arrReferConfig[intNO][9][1][0]:0;
    setTimeout(strToDo,intProcessBlurTimeOut);

    smit_PutDebugInfo(strID,"I am smit_ProcessBlur ended");
}

function smit_ShowResultTimeout(strID,blnDisableRefer){
    smit_PutDebugInfo(strID,"I am smit_ShowResultTimeout start");
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;

    var varReturn;
    if(blnDisableRefer){
        smit_AssignResult(strID,"blur");
        smit_ShowReferDiv(strID,false);
        varReturn=smit_ExecUserDefinedEventHandler(strID,"onblur");
    }

    smit_PutDebugInfo(strID,"I am smit_ShowResultTimeout ended");
    return varReturn;
}

function smit_GetCurrRowNo(strID){
    smit_PutDebugInfo(strID,"I am smit_GetCurrRowNo start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    
    var intCurrRowNo=-1;
    var blnRowFound=false;

    var intTotalItems=0;
    var arrCurrentItems=new Array();
    var strFilterValue;
    if(smit_arrReferConfig[intNO][9][7]=="yes"){
        strFilterValue=smit_arrReferConfig[intNO][7][3];
    }
    else{
        strFilterValue=smit_arrReferConfig[intNO][7][3].toLowerCase();
    }
    
    var i,j;
    for(i=1;i<smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1].length;i++){
        var strTempKey;
        if(!smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]])
            continue;
        if(smit_arrReferConfig[intNO][9][7]=="yes"){
            strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]];
        }
        else{
            strTempKey=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][smit_arrReferConfig[intNO][1][1]].toLowerCase();
        }

        if(strTempKey.indexOf(strFilterValue)==0){
            arrCurrentItems[intTotalItems]=i;
            if(i == smit_arrReferConfig[intNO][6]){
            	intCurrRowNo=intTotalItems;
            }
            intTotalItems++;
        }
    }
    intTotalItems--;
    
    smit_PutDebugInfo(strID,"I am smit_GetCurrRowNo ended");
    return(intCurrRowNo);
}
    
function getOffsetLeft(src){
	var set=0;
	if(src)
	{
		if (src.offsetParent)
			set+=src.offsetLeft+getOffsetLeft(src.offsetParent);
		
		if(src.tagName!="BODY")
		{
			var x=parseInt(src.scrollLeft,10);
			if(!isNaN(x))
				set-=x;
		}
	}
	return set;
}
function getOffsetTop(src){
	var set=0;
	if(src)
	{
		if (src.offsetParent)
			set+=src.offsetTop+getOffsetTop(src.offsetParent);
		
		if(src.tagName!="BODY")
		{
			var y=parseInt(src.scrollTop,10);
			if(!isNaN(y))
				set-=y;
		}
	}
	return set;
}

    
function smit_SetDiv(strID,strReason){
	if(strReason=="")return;

    smit_PutDebugInfo(strID,"I am smit_SetDiv start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    if(!document.all("smitinput"+strID)) return;
    var intInputOffsetLeft;
    var intInputOffsetTop;
    var strOriDisplay=document.all("smitdiv"+strID).style.display;

//    if((document.all("smitinput"+strID).offsetParent.style.position.toUpperCase()=="RELATIVE")||
  //     (document.all("smitinput"+strID).offsetParent.style.position.toUpperCase()=="ABSOLUTE")
    //  ){
        intInputOffsetLeft=document.all("smitinput"+strID).offsetLeft;
        intInputOffsetTop=document.all("smitinput"+strID).offsetTop;
        
//    }
  //  else{
    //    intInputOffsetLeft=smit_GetOffsetLeftInDiv(strID,document.all("smitinput"+strID));
      //  intInputOffsetTop=smit_GetOffsetTopInDiv(strID,document.all("smitinput"+strID));
//    }
    smit_PutDebugInfo(strID,"HeheheLeft:"+intInputOffsetLeft+";Top:"+intInputOffsetTop);
//	intInputOffsetLeft = getOffsetLeft(document.all("smitinput"+strID));
//	intInputOffsetTop = getOffsetTop(document.all("smitinput"+strID));

    if(document.all("smitimg"+strID)){
        var strBorderRightWidth=document.all("smitinput"+strID).style.borderRightWidth;
        var strBorderBottomWidth=document.all("smitinput"+strID).style.borderBottomWidth;
        if(strBorderRightWidth=="") strBorderRightWidth="2";
        if(strBorderBottomWidth=="") strBorderBottomWidth="2";
        var intBorderRightWidth=strBorderRightWidth.replace(/\D+/g,"");
        var intBorderBottomWidth=strBorderBottomWidth.replace(/\D+/g,"");
		
        document.all("smitimg"+strID).style.width=(smit_arrReferConfig[intNO][4][2]&&smit_arrReferConfig[intNO][4][2][0])?smit_arrReferConfig[intNO][4][2][0]:"12";
        document.all("smitimg"+strID).style.height=(smit_arrReferConfig[intNO][4][2]&&smit_arrReferConfig[intNO][4][2][1])?smit_arrReferConfig[intNO][4][2][1]:"12";
        document.all("smitimg"+strID).style.left =intInputOffsetLeft+document.all("smitinput"+strID).offsetWidth
                                                 -document.all("smitimg"+strID).clientWidth-intBorderRightWidth;
        document.all("smitimg"+strID).style.top =intInputOffsetTop+document.all("smitinput"+strID).offsetHeight
                                                 -document.all("smitimg"+strID).clientHeight-intBorderBottomWidth;        
    }

    var intLength=smit_arrReferConfig[intNO][0];
    var i;
    
    if((document.all("smittable"+strID))&&(document.all("smittable"+strID).rows[0])){
    	if(document.all("smitdiv1"+strID).style.display!="none"){
    	    intCellSpacing=1;
    	    document.all("smittitletable"+strID).cellSpacing=intCellSpacing;
    	    document.all("smittable"+strID).cellSpacing=intCellSpacing;
    	    var strOriDisplay=document.all("smitdiv"+strID).style.display;
    	    document.all("smitdiv"+strID).style.display="block";
            for(i=0;i<intLength;i++){
            	var intNewCellWidth=0;
                if(document.all("smittable"+strID).rows[0].cells[i].clientWidth>document.all("smittitletable"+strID).rows[0].cells[i].clientWidth){
                    intNewCellWidth=document.all("smittable"+strID).rows[0].cells[i].clientWidth;
                }
                else{
                    intNewCellWidth=document.all("smittitletable"+strID).rows[0].cells[i].clientWidth;
                }
                if(intNewCellWidth<0) continue;
                document.all("smittable"+strID).rows[0].cells[i].style.width=intNewCellWidth;
                document.all("smittitletable"+strID).rows[0].cells[i].style.width=intNewCellWidth;
            }
            document.all("smitdiv"+strID).style.display=strOriDisplay;
        }
    }
    
    if(document.all("smittable"+strID)){
        for(i=0;i<intLength;i++){
            if(document.all("smittable"+strID).rows[0].cells[i].clientWidth <(document.all("smitinput"+strID).clientWidth/intLength)){
                document.all("smittable"+strID).rows[0].cells[i].style.width=document.all("smitinput"+strID).clientWidth/intLength;
                document.all("smittitletable"+strID).rows[0].cells[i].style.width=document.all("smitinput"+strID).clientWidth/intLength;
            }
            smit_PutDebugInfo(strID,document.all("smittable"+strID).rows[0].cells[i].clientWidth+";;;;;"+document.all("smitinput"+strID).clientWidth/intLength);
        }
    }
    
    document.all("smitdiv"+strID).style.display="block";
    if(document.all("smitdiv2"+strID).scrollHeight>document.all("smitdiv2"+strID).clientHeight){
    	if(document.all("smittable"+strID))
            document.all("smitdiv"+strID).style.width=document.all("smittable"+strID).clientWidth+16;
        else
            document.all("smitdiv"+strID).style.width=document.all("smittitletable"+strID).clientWidth+16;
    }
    else{
    	if(document.all("smittable"+strID))
            document.all("smitdiv"+strID).style.width=document.all("smittable"+strID).clientWidth;
        else
            document.all("smitdiv"+strID).style.width=document.all("smittitletable"+strID).clientWidth;
    }
    	
    document.all("smitdiv"+strID).style.left=intInputOffsetLeft;
    document.all("smitdiv"+strID).style.top=intInputOffsetTop+document.all("smitinput"+strID).offsetHeight;
    if(document.all("smittitletable"+strID).style.display=="none"){
        document.all("smitdiv1"+strID).style.posHeight=0;
    }
    else{
        document.all("smitdiv1"+strID).style.posHeight=document.all("smittitletable"+strID).clientHeight;
    }
    
    if(document.all("smittable"+strID)){
        document.all("smitdiv2"+strID).style.posHeight=document.all("smittable"+strID).clientHeight+2;
    }
    else{
        document.all("smitdiv2"+strID).style.posHeight=0;
    }
    
    var intDivHeight=(smit_arrReferConfig[intNO][9][3][0])?smit_arrReferConfig[intNO][9][3][0]:smit_intDefaultDivHeight;
    if(document.all('smitdiv1'+strID).style.posHeight+document.all('smitdiv2'+strID).style.posHeight>intDivHeight){
        document.all("smitdiv2"+strID).style.posHeight=intDivHeight-document.all("smitdiv1"+strID).style.posHeight;
    }
    document.all('smitdiv'+strID).style.posHeight=document.all('smitdiv1'+strID).style.posHeight+document.all('smitdiv2'+strID).style.posHeight;
                                             
    if(document.all("smitdiv2"+strID).scrollHeight>document.all("smitdiv2"+strID).clientHeight){
    	document.all("smitdiv"+strID).style.borderBottomWidth=1;
    }
    else{
    	document.all("smitdiv"+strID).style.borderBottomWidth=0;
    }
        
    var intCurrRowNo=smit_GetCurrRowNo(strID);
    if( (intCurrRowNo>=0)&&
        ((smit_arrReferConfig[intNO][9][3][0]!="yes")||(strReason != "focus"))&&
        (document.all("smittable"+strID))&&
        (document.all("smittable"+strID).rows[intCurrRowNo])&&
        (document.all("smitdiv2"+strID).scrollHeight>document.all("smitdiv2"+strID).clientHeight)
      ){
        if( (smit_arrReferConfig[intNO][7][0]==smit_intArrowDownKeyCode)
            ||(smit_arrReferConfig[intNO][7][0]==smit_intPageDownKeyCode)
          ){
            document.all("smittable"+strID).rows[intCurrRowNo].scrollIntoView(false);
        }
        else{
            document.all("smittable"+strID).rows[intCurrRowNo].scrollIntoView();
        }
    }
    document.all("smitdiv"+strID).style.display=strOriDisplay;
    
	document.all("smitdiv1"+strID).style.posHeight = 22;
	document.all("smitdiv2"+strID).style.posHeight = 178;
	document.all("smitdiv"+strID).style.posHeight = 200;
    
    smit_PutDebugInfo(strID,"I am smit_SetDiv ended");
}


function smit_GetTitleTable(strID){
    smit_PutDebugInfo(strID,"I am smit_GetTitleTable start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    
    var strTitleTableStyle=(smit_arrReferConfig[intNO][9][2]["titletable"])?smit_arrReferConfig[intNO][9][2]["titletable"]:"BACKGROUND-color:#ffffff;BORDER-TOP: LightGrey solid 1px;BORDER-LEFT: LightGrey solid 1px;BORDER-BOTTOM: black solid 1px;BORDER-RIGHT: black solid 1px;cursor:default;";
    var strTitleRowStyle=(smit_arrReferConfig[intNO][9][2]["titlerow"])?smit_arrReferConfig[intNO][9][2]["titlerow"]:"background-color:#00008b;color:#fffacd;font:10pt;text-align:center;";
    var strTitleItemStyle=(smit_arrReferConfig[intNO][9][2]["titleitem"])?smit_arrReferConfig[intNO][9][2]["titleitem"]:"color:#ffffff;font:9pt;text-align:center;BORDER-BOTTOM: red solid 0px;";
    var strTitleWantItemStyle=(smit_arrReferConfig[intNO][9][2]["titlewantitem"])?smit_arrReferConfig[intNO][9][2]["titlewantitem"]:"color:##ffffff;font:9pt;text-align:center;BORDER-BOTTOM: red solid 0px;";
    var strTitleKeyItemStyle=(smit_arrReferConfig[intNO][9][2]["titlekeyitem"])?smit_arrReferConfig[intNO][9][2]["titlekeyitem"]:"color:##ffffff;font:9pt;text-align:center;BORDER-BOTTOM: red solid 0px;";
    var strTitleValueItemStyle=(smit_arrReferConfig[intNO][9][2]["titlevalueitem"])?smit_arrReferConfig[intNO][9][2]["titlevalueitem"]:"color:##ffffff;font:9pt;text-align:center;BORDER-BOTTOM: red solid 0px;";
    var strTitleItemTextStyle=(smit_arrReferConfig[intNO][9][2]["titleitemtext"])?smit_arrReferConfig[intNO][9][2]["titleitemtext"]:"";
    var strTitleWantItemTextStyle=(smit_arrReferConfig[intNO][9][2]["titlewantitemtext"])?smit_arrReferConfig[intNO][9][2]["titlewantitemtext"]:"";
    var strTitleKeyItemTextStyle=(smit_arrReferConfig[intNO][9][2]["titlekeyitemtext"])?smit_arrReferConfig[intNO][9][2]["titlekeyitemtext"]:"";
    var strTitleValueItemTextStyle=(smit_arrReferConfig[intNO][9][2]["titlevalueitemtext"])?smit_arrReferConfig[intNO][9][2]["titlevalueitemtext"]:"";
    
    var strTable="";
    strTable+="<table border=0 cellspacing=0 cellpadding=0 nowrap style=\"position:absolute;left:0;right:0;BORDER-TOP:lightgray solid 1px;BORDER-TOP:black solid 1px;"+strTitleTableStyle+"\" id=\"smittitletable"+strID+"\" border=1 cellspacing=0 bgcolor=\""+smit_arrReferConfig[intNO][5][0]+"\">\n";
    strTable+="<tr height=20 nowrap valign=center style=\""+strTitleRowStyle+"\">\n";
    for(j=0;j<smit_arrReferConfig[intNO][0];j++){
    	var strTdStyle,strTdTextStyle;
    	switch(j){
    	    case smit_arrReferConfig[intNO][1][0]:
    	        strTdStyle=strTitleWantItemStyle;
    	        strTdTextStyle=strTitleWantItemTextStyle;
    	        break;
    	    case smit_arrReferConfig[intNO][1][1]:
    	        strTdStyle=strTitleKeyItemStyle;
    	        strTdTextStyle=strTitleKeyItemTextStyle;
    	        break;
    	    case smit_arrReferConfig[intNO][1][2]:
    	        strTdStyle=strTitleValueItemStyle;
    	        strTdTextStyle=strTitleValueItemTextStyle;
    	        break;
    	    default:
    	        strTdStyle=strTitleItemStyle;
    	        strTdTextStyle=strTitleItemTextStyle;
    	        break;
    	}
        strTable+=("<td nowrap style=\""+strTdStyle+"\">"+
                   "<span style=\""+strTdTextStyle+"\">"+smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][0][smit_arrReferConfig[intNO][3][j]]+"</span></td>\n");
    }
    strTable+="</tr>\n</table>\n";
    smit_PutDebugInfo(strID,"I am smit_GetTitleTable ended");
    return(strTable);
}


function GenerateReference(blnNeedHeader,arrOneReferConfig,arrInputConfig){
    var strTheInput,strReferGate,strDivContent="";
    var intNO=smit_arrReferConfig.length;
    smit_arrReferConfig[intNO]=arrOneReferConfig;
    var strID=smit_arrReferConfig[intNO][2];

    eval(strID+"=new Array()");

    smit_DefineArrays(strID);

    var strTheRealInput="<input type=hidden id=\""+strID+"\" name=\""+strID+"\" value=\"\">\n";
    var strTheInput=("<input type=text id=\"smitinput"+strID+"\" name=\"smitinput"+strID+"\" ");
    var arrInputProperties=new Array("accesskey","align","alt","class","lang","maxlength","size","tabindex","title");
    var i;
    for(i=0;i<arrInputProperties.length;i++)
        if(arrInputConfig[arrInputProperties[i]]){strTheInput+=arrInputProperties[i]+"=\""+arrInputConfig[arrInputProperties[i]]+"\" ";}
    
    var strInputStyle=(smit_arrReferConfig[intNO][9][2]["input"])?smit_arrReferConfig[intNO][9][2]["input"]:"";
    if(arrInputConfig["style"])
        strTheInput+="style=\"position:relative;"+arrInputConfig["style"];
    else
        strTheInput+="style=\"position:relative;";
    strTheInput+=strInputStyle+"\"";
        
    var arrInputEvents=new Array("onafterupdate","onbeforeupdate","onclick","ondblclick","onerrorupdate","onfilterchange","onhelp","onmousedown","onmousemove","onmouseout","onmouseover","onmouseup","onresize","onselect");
    for(i=0;i<arrInputEvents.length;i++)
        if(smit_arrReferConfig[intNO][8][arrInputEvents[i]]){strTheInput+=arrInputEvents[i]+"=\""+smit_arrReferConfig[intNO][8][arrInputEvents[i]]+"\" ";}
    strTheInput+=("onfocus=\"smit_ProcessFocus(\'"+strID+"\')\" ");
    strTheInput+="onkeypress=\"smit_ProcessKeyPress(\'"+strID+"\')\" ";
    strTheInput+="onkeydown=\"smit_ProcessKeyDown(\'"+strID+"\')\" ";
    strTheInput+="onkeyup=\"smit_RefreshReferTable(\'"+strID+"\',\'\',true)\" ";
    strTheInput+="onblur=\"smit_ProcessBlur(\'"+strID+"\')\" ";
    strTheInput+=">\n";

    var strImgStyle=(smit_arrReferConfig[intNO][9][2]["img"])?smit_arrReferConfig[intNO][9][2]["img"]:"";
    var strReferGate=("<img name=\"smitimg"+strID+"\" alt=\"�����ʾ����\" src=\""+smit_arrReferConfig[intNO][4][0]+"\" "+
                  "onclick=\"smit_ToggleReferDiv(\'"+strID+"\');return(false)\" "+
                  "onmouseover=\"smit_ProcessMouseOverImg(\'"+strID+"\');return(false)\" "+
                  "onmouseout=\"smit_ProcessMouseOutImg(\'"+strID+"\');return(false)\" "+
                  "style=\"position:absolute;"+strImgStyle+"\">\n");
    strDivContent+=strTheRealInput;
    strDivContent+=strTheInput; 
    strDivContent+=strReferGate;
    document.write(strDivContent);
    
    var strDivStyle=(smit_arrReferConfig[intNO][9][2]["div"])?smit_arrReferConfig[intNO][9][2]["div"]:"";
    var strDiv1Style=(smit_arrReferConfig[intNO][9][2]["div1"])?smit_arrReferConfig[intNO][9][2]["div1"]:"";
    var strDiv2Style=(smit_arrReferConfig[intNO][9][2]["div2"])?smit_arrReferConfig[intNO][9][2]["div2"]:"";

    var intDivHeight=(smit_arrReferConfig[intNO][9][3][0])?smit_arrReferConfig[intNO][9][3][0]:smit_intDefaultDivHeight;
    
    strDivContent="<div id=\"smitdiv"+strID+"\" style=\"Z-INDEX: 100;position:absolute;display:none;height:"+intDivHeight+";BORDER-LEFT:lightgray solid 1px;BORDER-BOTTOM:black solid 0px;"+strDivStyle+"\" "+
                   "onmouseover=\"smit_ProcessMouseOverDiv(\'"+strID+"\');return(false)\" "+
                   "onmouseout=\"smit_ProcessMouseOutDiv(\'"+strID+"\');return(false)\" "+
                   ">\n";
    strDivContent+="<div id=\"smitdiv1"+strID+"\" style=\"position:static;left=0;top=0;"+strDiv1Style+"\">\n";
    strDivContent+=smit_GetTitleTable(strID);
    strDivContent+="</div>\n";
    strDivContent+="<div id=\"smitdiv2"+strID+"\" style=\"overflow:auto;position:static;height:250px;"+strDiv2Style+"\">\n";
    smit_arrReferConfig[intNO][7][3]="";
    strDivContent+=smit_GetNewReferTable(strID);
    strDivContent+="</div>\n";
    strDivContent+="</div>\n";

    document.write(strDivContent);

    if(intNO==-1)
        smit_arrReferConfig[intNO][7][1]=1;
    if(smit_arrReferConfig[intNO][7][1]==1){
    	var strDebugDiv="<div id=\"smitdebug"+strID+"\">Debug Information:<br></div>";
    	document.write(strDebugDiv);
    }
    

//    smit_SetDiv(strID);

    document.all("smitdiv"+strID).style.display="none";

    if(blnNeedHeader == 0)
        document.all("smitdiv1"+strID).style.display="none";
    
}



function ArrangeRefers(){
    var i;
    for(i=0;i<smit_arrReferConfig.length;i++){
    	if(document.all("smitinput"+smit_arrReferConfig[i][2])){
    	    document.all("smitdiv"+smit_arrReferConfig[i][2]).style.display="none";
    	    smit_SetDiv(smit_arrReferConfig[i][2],"Redraw");
    	}
    }
}



function SetReferStyle(strID,strStyleName,strStyleValue){
    strStyleName=strStyleName.toLowerCase();
    if(!document.all('smitinput'+strID)) return;
    var arrAllowedStyles=new Array("display","visibility","width","height");
    var blnStyleNameFound=false;
    var i;
    for(i=0;i<arrAllowedStyles.length;i++){
    	if(arrAllowedStyles[i]==strStyleName)
    	    blnStyleNameFound=true;
    }
    if(!blnStyleNameFound) return;
    switch(strStyleName){
    	case "display":
    	    document.all("smitinput"+strID).style.display=strStyleValue;
    	    document.all("smitimg"+strID).style.display=strStyleValue;
    	    break;
    	case "visibility":
    	    document.all("smitinput"+strID).style.visibility=strStyleValue;
    	    document.all("smitimg"+strID).style.visibility=strStyleValue;
    	    break;
    	case "width":
    	
    	    document.all("smitinput"+strID).style.width=strStyleValue;
    	    smit_SetDiv(strID,"setwidth");
      	    break;
    	case "height":
    	    document.all("smitinput"+strID).style.height=strStyleValue;

    		new_setDiv(strID);

    	    break;
    	default:
            eval("document.all(\"smitinput"+strID+"\").style."+strStyleName+"=\""+strStyleValue+"\";");
            break;
    }
}

function SetReferFocus(strID){
    document.all("smitinput"+strID).focus();
    document.all("smitinput"+strID).select();
}


    

function ClearRefer(strID){
    smit_PutDebugInfo(strID,"I am ClearRefer start");
    var intNO;
    intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    document.all("smitinput"+strID).value="";
    smit_AssignResult(strID,"refercleared");
    smit_PutDebugInfo(strID,"I am ClearRefer ended");
}
    

function SetReferValue(strID,intFieldSN,strKeyID){
    smit_PutDebugInfo(strID,"I am SetReferValue start");
    var intNO;
    intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    if(intFieldSN<0){
    	if(intFieldSN==-1){
    	    var intRowNO=strKeyID.valueOf();
    	    intRowNO++;
    	    if(smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][intRowNO]){
    	        smit_arrReferConfig[intNO][6]=intRowNO;
    	        smit_arrReferConfig[intNO][7][3]=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
    	        document.all("smitinput"+strID).value=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
    	    }
    	    else
    	        return;
    	}
    	if(intFieldSN=-2){
        }
    }
    else{
        if(!smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][0][intFieldSN]) return;
        var intIndexOfThisKey=0;
        var i;
        for(i=1;i<smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1].length;i++)
            if(smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][intFieldSN] == strKeyID){
                intIndexOfThisKey=i;
                break;
            }
        if(intIndexOfThisKey==0){
            smit_arrReferConfig[intNO][6]=0;
            smit_arrReferConfig[intNO][7][3]="";
            
            document.all("smitinput"+strID).value=strKeyID;
        }
        else{
            smit_arrReferConfig[intNO][6]=intIndexOfThisKey;
            smit_arrReferConfig[intNO][7][3]=smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][smit_arrReferConfig[intNO][6]][smit_arrReferConfig[intNO][1][1]];
            document.all("smitinput"+strID).value=strKeyID;
        }
    }

    smit_AssignResult(strID,"");

    var objTheDiv=document.all("smitdiv2"+strID);
    var strTable=smit_GetNewReferTable(strID);


    objTheDiv.innerHTML=strTable;


    smit_PutDebugInfo(strID,"I am SetReferValue ended");
}

        
function EnableRefer(strID,blnEnable){
    smit_PutDebugInfo(strID,"I am EnableRefer start");
    var intNO=smit_GetNOFromID(strID);	    
    if(intNO==-1) return;
    if(!document.all("smitinput"+strID)) return;
    document.all("smitinput"+strID).disabled=!blnEnable;
    if(blnEnable)
        document.all("smitimg"+strID).style.display="block";
    else
        document.all("smitimg"+strID).style.display="none";
    smit_PutDebugInfo(strID,"I am EnableRefer ended");
}


function GenerateClientReference(strInputName,arrReferData,blnNeedHeader,arrWhatWeWant,arrWhoIsWho,arrImgConfig,arrColorConfig,arrInputConfig,arrActionConfig){
    var arrReferConfigTest=new Array();
    arrReferConfigTest[0]=arrWhoIsWho.length;
    arrReferConfigTest[1]=arrWhatWeWant;
    arrReferConfigTest[2]=strInputName;
    arrReferConfigTest[3]=arrWhoIsWho;
    arrReferConfigTest[4]=arrImgConfig;
    arrReferConfigTest[5]=arrColorConfig;
    arrReferConfigTest[6]=0;
    arrReferConfigTest[7]=new Array();
    arrReferConfigTest[8]=arrActionConfig;
    arrReferConfigTest[9]=new Array();
    arrReferConfigTest[10]=arrReferData;
    GenerateReference(blnNeedHeader,arrReferConfigTest,arrInputConfig);
}


function SetReferProperty(strID,strPropertyName,strPropertyValue){
    smit_PutDebugInfo(strID,"I am SetReferProperty start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    strPropertyName=strPropertyName.toLowerCase();
    strPropertyValue=strPropertyValue.toLowerCase();
    switch(strPropertyName){
    	case "allowstrangeitem":
    	    if(strPropertyValue=="forbidden"){
    	    	smit_arrReferConfig[intNO][9][0]="0";
    	    }
    	    else{
    	    	smit_arrReferConfig[intNO][9][0]="1";
    	    }
    	    break;
    	case "autoselect":
            smit_arrReferConfig[intNO][9][5]=strPropertyValue;
            break;
        case "casesensitive":
            smit_arrReferConfig[intNO][9][7]=strPropertyValue;
            break;
    	case "filteronfocus":
            smit_arrReferConfig[intNO][9][4]=strPropertyValue;
            break;
    	case "height":
    	    var intHeight=strPropertyValue.replace(/\D+/g,"");
            smit_arrReferConfig[intNO][9][3][0]=intHeight;
    	    break;
        case "pagesize":
    	    var intPageSize=strPropertyValue.replace(/\D+/g,"");
    	    if(intPageSize<2) break;
    	    smit_arrReferConfig[intNO][9][6][0]=-intPageSize;
    	    smit_arrReferConfig[intNO][9][6][1]=intPageSize;
            break;
        case "pageupstep":
    	    var intPageUpStep=strPropertyValue.replace(/\D+/g,"");
    	    if(intPageUpStep<2) break;
    	    smit_arrReferConfig[intNO][9][6][0]=-intPageUpStep;
    	    break;
        case "pagedownstep":
    	    var intPageDownStep=strPropertyValue.replace(/\D+/g,"");
    	    if(intPageDownStep<2) break;
    	    smit_arrReferConfig[intNO][9][6][1]=intPageDownStep;
    	    break;        
    	default:break;
    }
    smit_PutDebugInfo(strID,"I am SetReferProperty ended");
}

function SetReferWhoIsWho(strID,arrWhoIsWho){
    smit_PutDebugInfo(strID,"I am SetReferWhoIsWho start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return;
    
    smit_arrReferConfig[intNO][0]=arrWhoIsWho.length;
    smit_arrReferConfig[intNO][3]=arrWhoIsWho;
    document.all("smitdiv1"+strID).innerHTML=smit_GetTitleTable(strID);
    document.all("smitdiv2"+strID).innerHTML=smit_GetNewReferTable(strID);
    smit_SetDiv(strID,"");
    smit_PutDebugInfo(strID,"I am SetReferWhoIsWho ended");
}
    


function GetReferItem(strID,intKeyFieldSN,strKeyValue,intColNO){
    smit_PutDebugInfo(strID,"I am GetItemFromKey start");
    var intNO=smit_GetNOFromID(strID);
    if(intNO==-1) return(null);
    
    var intFieldSN=intKeyFieldSN;
    if(!smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][0][intFieldSN]) return(null);
    var intIndexOfThisKey=0;
    var i;
    for(i=1;i<smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1].length;i++){
        if(smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][i][intFieldSN] == strKeyValue){
            intIndexOfThisKey=i;
            break;
        }
    }
    if(intIndexOfThisKey==0){
    	return(null);
    }
    smit_PutDebugInfo(strID,"I am GetItemFromKey ended");
    return(smit_arrReferConfig[intNO][smit_arrReferConfig[intNO].length-1][intIndexOfThisKey][intColNO]);
}


function getDebugTime()
{
	var tm = new Date();
	return tm.getTime();
}