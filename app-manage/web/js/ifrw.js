(function($){
        
	// string
	$.string = {
		striptags : function (s) {
			return s.replace(/<[^>].*?>/g,"");
		}
	}

	// form funcs
	$.form = {
		confirm: function (url, msg) {
			if (confirm(msg)) {
				location.href = encodeURI(url);
			}
		},
		filter: function (options) {
			var filter_input = options.filter_input;
			var filter_rows = options.filter_rows;
            var filter_brand = options.filter_brand;
			var filter_val = options.filter_val;
			$(filter_input).unbind("keyup");
			$(filter_input).keyup(function(){ 
				input = $(this).val();                                
				if (input == '') {
                                        $(filter_rows).empty();
                                        $(filter_rows).append(filter_brand.html());
					$(filter_rows).show();
				} else {     
					$(filter_rows).empty();
					filter_val.each(function(){                                                
						var text = $.string.striptags($(this).html());
						var regexp = new RegExp("^" + input);
						if (regexp.test(text)) {
                                                    $(filter_rows).append("<option value='"+$(this).val()+"'>"+text+"</option>");
						}
					});

				}
			});
		}
	}

})(jQuery);

$(document).ready(function(){	
	// tlist table styles
	$('.tlist').find('tbody tr').mouseover(function(){
		$(this).css({'background':'#F8F8F8'});
	}).mouseout(function(){
		$(this).css({'background':'#FFFFFF'});
	});
	
});