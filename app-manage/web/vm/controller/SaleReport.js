//页面初始化
angular.element(document).ready(function(){   
	// $('#btn_search').click();
});

//控制器
var myApp = angular.module('myApp', []);

myApp.controller('SaleReport', ['$scope', '$http', function ($scope, $http) {
	$scope.query={};
	$scope.query.uid= $scope.userid;
	
	$scope.ready=0;
	
    //初始化
	$http.get('/ng/yearmonth').success(function(data) {
      $scope.yearmonthModel = data;
	  $scope.selectYearmonth= data[0];
	  $scope.query.yearmonth= $scope.selectYearmonth.id;	  
	  $scope.ready++;
    });
	
	$http.get('/ng/payway').success(function(data) {
      $scope.paywayModel = data;
	  $scope.selectPayway= null;
	  $scope.ready++;
    });
	
	$scope.refresh = function($event) {
		try{
		 $scope.query.uid= $scope.userid;
		 $scope.query.yearmonth= $scope.selectYearmonth.id;
		 $scope.query.payway=  $scope.selectPayway ? $scope.selectPayway.id : "";   
		}catch(e ){}
		$http.get('./detailProfit', {params: $scope.query} )
		.success(function(data) {
			$scope.summ = data;
		   // console.log(data);
		});
             
    };
	
	//监控初始化完成
	$scope.$watch("ready", function(){
		if($scope.ready==2){
			$scope.refresh(null);
		}
	});	
	//监控查询条件
	$scope.$watch("query", function(){
		try{
		  if(!$scope.query.uid){	  $scope.query.uid = $scope.userid;	  }
		  if(!$scope.query.payway){	  $scope.query.payway = $scope.selectPayway.id;	  }
		  if(!$scope.query.yearmonth){	  $scope.query.yearmonth = $scope.selectYearmonth.id;	  }
		}catch(e ){	}
		  if(Object.keys($scope.query).length<3) return;		  
		  
		  $http.get('./detailTable', {params:$scope.query }).success(function(data) {
			  $scope['tab_'+$scope.query.type] = data;	
			  //console.log(data);
			});
			
    },true);
	
	$scope.dowload= function (){
		var query=$scope.query;
		//query.act='export';
		var url='./detailTable?act=export&';
		var flds=Object.keys(query);
		//console.log(flds);
		for( var index in flds){
			url += flds[index]+'='+query[flds[index]]+'&';
			//console.log(flds[index]);
		}
		window.open( url );
		
		 
	};
	
	
	$scope.getTotal = function ( tab, fld ) {		
		  var arr=$scope['tab_'+ tab ];
		  var sum = 0;
		  angular.forEach(arr, function(itm) {
            sum += parseFloat(itm[fld]);
          });		 
		  return Math.floor(sum);
	};
	
	 
     
}]);


myApp.directive('myTabs', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: false, //{ currpane:'=' },
      controller: function($scope,$http) {
        var panes = $scope.panes = [];

        $scope.select = function(pane) {
          angular.forEach(panes, function(pane) {
            pane.selected = false;
          });
          pane.selected = true;
		  
		  $scope.currpane=pane.type;		  
		  $scope.query.type= pane.type;		  
		  //console.log($scope.query);
        };

        this.addPane = function(pane) {
          if (panes.length == 0) {
            $scope.select(pane);
          }
          panes.push(pane);
        };
      },
      templateUrl: '/vm/view/my-tabs.html'
    };
  });
  
myApp.directive('myPane', function() {
    return {
      require: '^myTabs',
      restrict: 'E',
      transclude: true,
      scope: {
        title: '@',
		type: '@',
		visible:'@'
      },
      link: function(scope, element, attrs, tabsCtrl) {
        if(attrs.visible!='false'){
			tabsCtrl.addPane(scope);
		}
		//console.log(element.is(':visible'));
      },
      templateUrl: '/vm/view/my-pane.html'
    };
  });
  
  
/*

//属性值类型的自定义
myApp.directive('xcolor', function(){
     var link = function($scope, $element, $attrs){
		 //console.log($attrs);
       $scope.$watch($attrs.xcolor, function(new_v){
        $element.css('color', $attrs.cc);
		// console.log($attrs.cc);
       });
     }
     return link;
  });

*/

